package com.o2.plotos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.authentication.signin.SignInActivityNew;
import com.o2.plotos.authentication.signup.CreateAnAccountActivity;
import com.o2.plotos.authentication.signup.SignUpActivity;
import com.o2.plotos.home.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 3/21/2018.
 */

public class StartActivity extends AppCompatActivity {

    @BindView(R.id.activity_sign_in_btn_sign_in)
    Button activitySignInBtnSignIn;
    @BindView(R.id.txt_register)
    TextView txtRegister;

    @BindView(R.id.txt_cont_guest)
    TextView txt_cont_guest;
    @BindView(R.id.activity_sign_in)
    LinearLayout activitySignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.activity_sign_in_btn_sign_in, R.id.txt_register, R.id.txt_cont_guest})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.activity_sign_in_btn_sign_in:

                Intent i = new Intent(this, SignInActivityNew.class);
                startActivity(i);
                break;
            case R.id.txt_register:
                Intent i1 = new Intent(this, CreateAnAccountActivity.class);
                startActivity(i1);
                break;
            case R.id.txt_cont_guest:
                Intent i2 = new Intent(this, HomeActivity.class);
                startActivity(i2);
                break;
        }
    }
}
