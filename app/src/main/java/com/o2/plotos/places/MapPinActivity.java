package com.o2.plotos.places;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.o2.plotos.R;
import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.ChangePhoneNumberInterface;
import com.o2.plotos.restapi.responses.ChnageMobileResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;
import com.o2.plotos.utils.UserPreferenceUtil;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapPinActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.delivery_location_autocomplteTextView)
    EditText deliveryLocationAutocomplteTextView;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.apartment)
    EditText apartment;
    @BindView(R.id.apartmentInputLayout)
    TextInputLayout apartmentInputLayout;
    @BindView(R.id.mobile)
    EditText mobile;
    @BindView(R.id.mobileInputLayout)
    TextInputLayout mobileInputLayout;


    @BindView(R.id.linlay_card)
    LinearLayout linlayCard;
    @BindView(R.id.linlay_category)
    LinearLayout linlayCategory;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.btnSelect)
    Button btnSelect;
    @BindView(R.id.activity_map_pin)
    LinearLayout activityMapPin;
    @BindView(R.id.edit_customename)
    EditText editCustomename;
    @BindView(R.id.btn_add_delivery_location)
    Button btnAddDeliveryLocation;
    @BindView(R.id.img_check_home)
    ImageView imgCheckHome;
    @BindView(R.id.img_check_work)
    ImageView imgCheckWork;
    @BindView(R.id.linlay_done)
    LinearLayout linlayDone;
    @BindView(R.id.linlay_done2)
    LinearLayout linlayDone2;
    @BindView(R.id.linlay_done3)
    LinearLayout linlay_done3;

    @BindView(R.id.txt_choosecategory)
    TextView txtChoosecategory;
    @BindView(R.id.item_search_title)
    TextView itemSearchTitle;
    @BindView(R.id.lin_check_home)
    LinearLayout linCheckHome;
    @BindView(R.id.lin_check_work)
    LinearLayout linCheckWork;
    private Button mBtnSelect;
    private String mAddress;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mGoogleMap;
    private Location mLastLocation;
    private ProgressDialog mProgressDialog;
    private String mLatitude;
    private String mLongitude;
    private String mLocation;
    private String mName;
    private String mMobile;

    Double latitude, longitude;


    private SharedPreferences pref;

    private PlaceAutocompleteFragment autocompleteFragment;

    private final int PICK_LOCATION = 0;

    int test = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_pin);
        ButterKnife.bind(this);

        mBtnSelect = (Button) findViewById(R.id.btnSelect);

      /*  DataBaseHelper dataBaseHelper = new DataBaseHelper(MapPinActivity.this);
        try {
            List<User> users = dataBaseHelper.getUserIntegerDao().queryForAll();
            if (users.size() == 0) {
                UserPreferenceUtil.getInstance(this).setReturnActivity("LOCATION");
                test = 1;

                mobileInputLayout.setVisibility(View.GONE);
                // Intent intent = new Intent(DeliveryLocationActivity.this, SignInActivity.class);
                //startActivity(intent);
                // finish();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }*/


        // mAddress   = getIntent().getStringExtra("address");
        // mLatitude  = getIntent().getStringExtra("latitude");
        // mLongitude = getIntent().getStringExtra("longitude");

        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



       /* autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setHint("Address...");*/

        // setListeners();


        editCustomename.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() == 0) {
                    btnAddDeliveryLocation.setVisibility(View.GONE);
                    imgCheckWork.setImageResource(R.drawable.grey_check);
                    imgCheckHome.setImageResource(R.drawable.big_dilevery_selected);
                } else {
                    imgCheckWork.setImageResource(R.drawable.grey_check);
                    imgCheckHome.setImageResource(R.drawable.grey_check);
                    btnAddDeliveryLocation.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
        } else {
            Toast.makeText(MapPinActivity.this, "Please accept to use all app's services!", Toast.LENGTH_LONG).show();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_LOCATION) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);

                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;

                deliveryLocationAutocomplteTextView.setText(place.getAddress());
                LatLng latLng = place.getLatLng();

                drawMarker(latLng.latitude, latLng.longitude);

                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18f));
                //Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                // Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

   /*     if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_LOCATION:
                    if (data.hasExtra("lat") &&
                            data.hasExtra("lng")) {

                        Intent _data = new Intent();
                        _data.putExtra("location", mLocation);
                        _data.putExtra("name", mName);
                        _data.putExtra("number", mMobile);
                        _data.putExtra("address", mAddress);
                        _data.putExtra("lat", data.getDoubleExtra("lat", 0));
                        _data.putExtra("lng", data.getDoubleExtra("lng", 0));

                        if (getParent() == null) {
                            setResult(Activity.RESULT_OK, _data);
                        } else {
                           // getParent().setResult(Activity.RESULT_OK, _data);

                          //  deliveryLocationAutocomplteTextView.setText();
                        }

                        finish();
                    } else {
                       *//* Snackbar.make(btnNext, "We couldn't confirm your location. Please try again.", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();*//*
                    }
                    break;
            }
        }*/
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;

        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null)

        {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            LatLng latLng = new LatLng(latitude, longitude);

            drawMarker(latitude, longitude);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18f));

        }


        // Toast.makeText(MapPinActivity.this, "Drag the map to adjust your location.", Toast.LENGTH_SHORT).show();

        if (pref.getBoolean("map-drag-acknowledged", false) == false) {

            /*Snackbar.make(mBtnSelect/*findViewById(R.id.activity_map_pin)* /, "Drag the map to adjust your location.", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Dismiss", new AdjustLocationListener()).show();*/
        }

    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng latLng = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            latLng = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return latLng;
    }

    private void setListeners() {

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                mLatitude = String.valueOf(place.getLatLng().latitude);
                mLongitude = String.valueOf(place.getLatLng().longitude);

                String address = place.getAddress().toString().replace(" - United Arab Emirates", "");
                // etAddress.setText(address);
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Place", "An error occurred: " + status);
            }
        });

        mBtnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LatLng latLng = mGoogleMap.getCameraPosition().target;

                Intent data = new Intent();
                data.putExtra("lat", latLng.latitude);
                data.putExtra("lng", latLng.longitude);

                if (getParent() == null) {
                    setResult(Activity.RESULT_OK, data);
                } else {
                    getParent().setResult(Activity.RESULT_OK, data);
                }

                /*Snackbar.make(btnBack, brandId+" "+modelId+" "+name, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/

                finish();
            }
        });

    }

    @OnClick({R.id.delivery_location_autocomplteTextView, R.id.img_back, R.id.linlay_done, R.id.txt_choosecategory, R.id.linlay_category, R.id.btn_add_delivery_location, R.id.linlay_done3})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.delivery_location_autocomplteTextView:

                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .build(this);
                    startActivityForResult(intent, PICK_LOCATION);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
                break;

            case R.id.img_back:

                if (linlayCategory.getVisibility() == View.VISIBLE) {
                    linlayCard.setVisibility(View.VISIBLE);
                    linlayCategory.setVisibility(View.GONE);
                    linlayDone2.setVisibility(View.VISIBLE);
                } else if (linlayCard.getVisibility() == View.VISIBLE)

                {
                    linlayCard.setVisibility(View.GONE);
                    linlayDone.setVisibility(View.VISIBLE);
                    deliveryLocationAutocomplteTextView.setVisibility(View.VISIBLE);
                } else {
                    finish();
                }


                break;

            case R.id.linlay_done:



                if (linlayCard.getVisibility() == View.GONE) {

                    if (linlayCategory.getVisibility() == View.GONE) {

                        if (deliveryLocationAutocomplteTextView.getText().toString().isEmpty()) {
                            Toast.makeText(this, "please pick location", Toast.LENGTH_SHORT).show();
                        } else {
//                            txtTitle.setText("Delivery Address Details");
                            String phone = UserPreferenceUtil.getInstance(this).getNumber();
                            if (phone != null) {
                                mobile.setText(phone);
                            }
                            deliveryLocationAutocomplteTextView.setVisibility(View.GONE);
                            linlayCard.setVisibility(View.VISIBLE);
//                            linlayDone.setVisibility(View.GONE);
//                            linlayDone2.setVisibility(View.VISIBLE);

                            //  Toast.makeText(this, "good job", Toast.LENGTH_SHORT).show();
                        }


                    }


                }


           /*     if(linlayCard.getVisibility()==View.VISIBLE)
                {
                    if (validateForm()) {
                        linlayCard.setVisibility(View.GONE);

                        if(!UserPreferenceUtil.getInstance(MapPinActivity.this).getNumber().equals(mobile.getText().toString()))
                        {
                            mProgressDialog = new ProgressDialog(this);
                            mProgressDialog.setMessage("Please wait...");
                            mProgressDialog.setCancelable(true);

                            change_mobile(UserPreferenceUtil.getInstance(MapPinActivity.this).UserId(),mobile.getText().toString(),mProgressDialog);
                        }


                        try {
                            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        txtTitle.setText("Give a name for your address");
                        linlayCategory.setVisibility(View.VISIBLE);

                    }

                }*/


                if (linlayCategory.getVisibility() == View.VISIBLE) {


                    Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.big_dilevery_selected);

                    Bitmap bitmap = ((BitmapDrawable) imgCheckHome.getDrawable()).getBitmap();
                    if (imagesAreEqual(icon, bitmap)) {
                        editCustomename.setText("Home");
                    } else {
                        if (editCustomename.getText().toString().isEmpty()) {
                            editCustomename.setText("Work");
                        }


                    }


                    Intent _data = new Intent();
                    _data.putExtra("location", editCustomename.getText().toString());
                    _data.putExtra("number", mobile.getText().toString());
                    _data.putExtra("address", apartment.getText().toString() + "\n" + deliveryLocationAutocomplteTextView.getText().toString());
                    _data.putExtra("lat", latitude);
                    _data.putExtra("lng", longitude);

                    if (getParent() == null) {
                        setResult(Activity.RESULT_OK, _data);
                    } else {
                        getParent().setResult(Activity.RESULT_OK, _data);
                    }

                    finish();
                }
                if (txtChoosecategory.getVisibility()==View.VISIBLE){
                    if (validateForm()) {
                        linlayCard.setVisibility(View.GONE);
                        linlayDone.setVisibility(View.VISIBLE);
                        linlayDone2.setVisibility(View.GONE);
                        if (!UserPreferenceUtil.getInstance(MapPinActivity.this).getNumber().equals(mobile.getText().toString())) {
                            mProgressDialog = new ProgressDialog(this);
                            mProgressDialog.setMessage("Please wait...");
                            mProgressDialog.setCancelable(true);

                            changeMobile(UserPreferenceUtil.getInstance(MapPinActivity.this).UserId(), mobile.getText().toString(), mProgressDialog);
                        }
                    /*Intent i = new Intent(this,FlatPoupActivity.class);
                    startActivity(i);*/
//                    txtTitle.setText("Give a name for your address");
                        linlayCategory.setVisibility(View.VISIBLE);

                    }
                }

                break;

            case R.id.txt_choosecategory:


                if (validateForm()) {
                    linlayCard.setVisibility(View.GONE);
                    linlayDone.setVisibility(View.VISIBLE);
                    linlayDone2.setVisibility(View.GONE);
                    if (!UserPreferenceUtil.getInstance(MapPinActivity.this).getNumber().equals(mobile.getText().toString())) {
                        mProgressDialog = new ProgressDialog(this);
                        mProgressDialog.setMessage("Please wait...");
                        mProgressDialog.setCancelable(true);

                        changeMobile(UserPreferenceUtil.getInstance(MapPinActivity.this).UserId(), mobile.getText().toString(), mProgressDialog);
                    }
                    /*Intent i = new Intent(this,FlatPoupActivity.class);
                    startActivity(i);*/
//                    txtTitle.setText("Give a name for your address");
                    linlayCategory.setVisibility(View.VISIBLE);

                }


                break;

            case R.id.btn_add_delivery_location:

                Intent _data = new Intent();
                _data.putExtra("location", editCustomename.getText().toString());
                _data.putExtra("number", mobile.getText().toString());
                _data.putExtra("address", apartment.getText().toString() + "\n" + deliveryLocationAutocomplteTextView.getText().toString());
                _data.putExtra("lat", latitude);
                _data.putExtra("lng", longitude);

                if (getParent() == null) {
                    setResult(Activity.RESULT_OK, _data);
                } else {
                    getParent().setResult(Activity.RESULT_OK, _data);
                }

                finish();
                break;

            case R.id.linlay_done3:
                if (!editCustomename.getText().toString().equalsIgnoreCase("")) {
                    btnAddDeliveryLocation.performLongClick();
                } else {
                    Toast.makeText(this, "please add name", Toast.LENGTH_SHORT).show();
                }
                break;
        }


    }

    boolean imagesAreEqual(Bitmap i1, Bitmap i2) {
        if (i1.getHeight() != i2.getHeight())
            return false;
        if (i1.getWidth() != i2.getWidth()) return false;

        for (int y = 0; y < i1.getHeight(); ++y)
            for (int x = 0; x < i1.getWidth(); ++x)
                if (i1.getPixel(x, y) != i2.getPixel(x, y)) return false;

        return true;
    }

    private boolean validateForm() {

        boolean valid = true;
        String[] fields;

        if (test == 0) {
            fields = new String[]{"mobile", "apartment"};
        } else {
            fields = new String[]{"apartment"};
        }


        for (int i = 0; i < fields.length; i++) {

            int id = getResources().getIdentifier(fields[i], "id", getPackageName());
            EditText field = (EditText) findViewById(id);
//            String fieldtext=field.getText().toString().trim();
            id = getResources().getIdentifier(fields[i] + "InputLayout", "id", getPackageName());
            TextInputLayout input = (TextInputLayout) findViewById(id);

            if (field != null &&
                    input != null) {
                if (field.getText().toString().trim().length() == 0 || field.getText().toString().matches("")) {
                    input.setError(field.getTag().toString());
                    //input.setError("Please specify " + input.getHint());

                    /*field.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            ((EditText) v).setError("");
                        }
                    });*/

                    valid = false;
                }
            }
        }

        return valid;
    }

    @OnClick({R.id.lin_check_home, R.id.lin_check_work})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.lin_check_home:
                imgCheckHome.setImageResource(R.drawable.big_dilevery_selected);
                imgCheckWork.setImageResource(R.drawable.grey_check);
                break;
            case R.id.lin_check_work:

                imgCheckWork.setImageResource(R.drawable.big_dilevery_selected);
                imgCheckHome.setImageResource(R.drawable.grey_check);

                break;
        }
    }

    @OnClick(R.id.linlay_done2)
    public void onViewClicked() {


        if (validateForm()) {
            linlayCard.setVisibility(View.GONE);

            linlayDone.setVisibility(View.VISIBLE);
            linlayDone2.setVisibility(View.GONE);

            if (!UserPreferenceUtil.getInstance(MapPinActivity.this).getNumber().equals(mobile.getText().toString())) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Please wait...");
                mProgressDialog.setCancelable(true);

                changeMobile(UserPreferenceUtil.getInstance(MapPinActivity.this).UserId(), mobile.getText().toString(), mProgressDialog);
            }
                    /*Intent i = new Intent(this,FlatPoupActivity.class);
                    startActivity(i);*/
            txtTitle.setText("Give a name for your address");
            linlayCategory.setVisibility(View.VISIBLE);

        }

    }

    /**
     * Adjust location
     */
    public class AdjustLocationListener implements View.OnClickListener {

        public AdjustLocationListener() {
        }

        @Override
        public void onClick(View v) {
            SharedPreferences.Editor prefsEditor = pref.edit();
            prefsEditor.putBoolean("map-drag-acknowledged", true);
            prefsEditor.commit();
        }
    }

    private void drawMarker(Double latitude, Double longitude) {
        mGoogleMap.clear();
        LatLng currentPosition = new LatLng(latitude, longitude);
        mGoogleMap.addMarker(new MarkerOptions()
                .position(currentPosition)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin)));

    }

    public void changeMobile(String user_id, final String mobile, final ProgressDialog mProgressDialog) {

        mProgressDialog.show();
        ChangePhoneNumberInterface changemobilerequest = ServiceGenrator.createService(ChangePhoneNumberInterface.class);
        Call<ChnageMobileResponse> forgotcall = changemobilerequest.ChangePhoneRequest(user_id, mobile);
        forgotcall.enqueue(new Callback<ChnageMobileResponse>() {
            @Override
            public void onResponse(Call<ChnageMobileResponse> call, Response<ChnageMobileResponse> response) {
                if (response.code() == 200) {
                    mProgressDialog.hide();
                    ChnageMobileResponse apiResponse = response.body();
                    UserPreferenceUtil.getInstance(MapPinActivity.this).setNumber(mobile);
                    Toast.makeText(MapPinActivity.this, apiResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ChnageMobileResponse> call, Throwable t) {
                mProgressDialog.hide();
                LogCat.LogDebug(ConstantUtil.TAG, "Banner ap onFailure " + t.getMessage());
            }
        });
    }

}
