package com.o2.plotos.places;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.places.AutocompletePrediction;
import com.o2.plotos.R;
import com.o2.plotos.cart.CartAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by snveiga on 05/04/17.
 */

public class PlaceAdapter extends BaseAdapter {

    private JSONArray mList;
    private String mAddress;
    private LayoutInflater mLayoutInflater;
    private ViewHolder viewHolder;
    OnActionMenuClicked mOnActionMenuClicked;

    public PlaceAdapter(Context context, JSONArray list, String address, OnActionMenuClicked onActionMenuClicked) {;
        mList  = list;
        mAddress = address;
        mLayoutInflater = LayoutInflater.from(context);
        mOnActionMenuClicked = onActionMenuClicked;
    }


    @Override
    public int getCount() {
        return (mList!=null) ? mList.length() : 0;
    }

    @Override
    public JSONObject getItem(int i) {
        try {
            return mList.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void updateContent(JSONArray _list) {
        mList = _list;
        this.notifyDataSetChanged();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        PlaceAdapter.ViewHolder viewHolder;

        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_place_search, viewGroup,false);
            viewHolder = new PlaceAdapter.ViewHolder();
            viewHolder.txtTitle = (TextView) view.findViewById(R.id.item_search_title);
            viewHolder.txtSubTitle = (TextView) view.findViewById(R.id.item_search_sub);
            viewHolder.icCheck = (ImageView) view.findViewById(R.id.delivery_location_check);

            viewHolder.menuButton = (ImageButton) view.findViewById(R.id.menu_button);
            viewHolder.menuLayout = (LinearLayout) view.findViewById(R.id.menu_layout);
            viewHolder.closeMenu = (ImageButton) view.findViewById(R.id.close_menu);
            viewHolder.editText = (TextView) view.findViewById(R.id.edit_text);
            viewHolder.deleteText = (TextView) view.findViewById(R.id.delete_text);
            view.setTag(viewHolder);
        } else {
            viewHolder = (PlaceAdapter.ViewHolder) view.getTag();
        }

        JSONObject item = getItem(i);

        if( item!=null ) {
            try {
                viewHolder.txtTitle.setText(item.getString("location"));
                viewHolder.txtSubTitle.setText(item.getString("address").replace("\n", ", "));
                if(!(item.getString("address").equals(mAddress)))
                {
                    viewHolder.icCheck.setImageResource(R.drawable.grey_check);
                }
                else
                {
                    viewHolder.icCheck.setImageResource(R.drawable.big_dilevery_selected);
                }

               // viewHolder.icCheck.setVisibility((!item.getString("address").equals(mAddress))?View.INVISIBLE:View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        viewHolder.menuButton.setVisibility((i!=0)?View.VISIBLE:View.GONE);
        viewHolder.menuButton.setFocusable(false);

        viewHolder.menuLayout.setTag(i);
        viewHolder.menuButton.setTag(viewHolder.menuLayout);
        viewHolder.closeMenu.setTag(viewHolder.menuLayout);
        viewHolder.deleteText.setTag(viewHolder.menuLayout);
        viewHolder.editText.setTag(viewHolder.menuLayout);


        // disabling edition temporarily
        viewHolder.editText.setVisibility(View.GONE);


        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((View) v.getTag()).setVisibility(View.VISIBLE);
            }
        });

        viewHolder.closeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((View) v.getTag()).setVisibility(View.GONE);
            }
        });

        viewHolder.editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnActionMenuClicked.onEdit(i);
                ((View) v.getTag()).setVisibility(View.GONE);
            }
        });

        viewHolder.deleteText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnActionMenuClicked.onDelete(i);
                ((View) v.getTag()).setVisibility(View.GONE);
            }
        });

        return view;
    }

    private class ViewHolder {
        public TextView txtTitle;
        public TextView txtSubTitle;
        public ImageView icCheck;

        public ImageButton menuButton;
        public LinearLayout menuLayout;
        public ImageButton closeMenu;
        public TextView editText;
        public TextView deleteText;
    }

    public interface OnActionMenuClicked {
        void onEdit(int position);
        void onDelete(int position);
    }
}