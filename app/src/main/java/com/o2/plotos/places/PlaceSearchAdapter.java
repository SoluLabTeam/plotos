package com.o2.plotos.places;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.android.gms.location.places.AutocompletePrediction;
import com.o2.plotos.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hassan on 2/23/2017.
 */

public class PlaceSearchAdapter extends BaseAdapter {

    private ArrayList<AutocompletePrediction> mList;
    private LayoutInflater mLayoutInflater;

    public PlaceSearchAdapter(Context context,ArrayList<AutocompletePrediction> list) {;
        mList  = list;
        mLayoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_place_search, viewGroup,false);
            viewHolder = new ViewHolder();
            viewHolder.txtTitle = (TextView) view.findViewById(R.id.item_search_title);
            viewHolder.txtSubTitle = (TextView) view.findViewById(R.id.item_search_sub);
            view.setTag(viewHolder);
        } else {
           viewHolder = (ViewHolder) view.getTag();
        }
         viewHolder.txtTitle.setText(mList.get(i).getPrimaryText(null));
        viewHolder.txtSubTitle.setText(mList.get(i).getSecondaryText(null));
        return view;
    }

    private class ViewHolder {
        public TextView txtTitle;
        public TextView txtSubTitle;

    }
}
