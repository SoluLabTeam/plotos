package com.o2.plotos.places;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.authentication.signin.SignInActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.User;
import com.o2.plotos.utils.UserPreferenceUtil;

import java.sql.SQLException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlacesActivity extends AppCompatActivity {
    @BindView(R.id.activity_places_address)
    TextView address;

    private ActionBar mActionbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        ButterKnife.bind(this);


        DataBaseHelper dataBaseHelper = new DataBaseHelper(PlacesActivity.this);
        try {
            List<User> users = dataBaseHelper.getUserIntegerDao().queryForAll();
            if( users.size() == 0 ) {
                UserPreferenceUtil.getInstance(this).setReturnActivity("PLACES");
                Intent intent = new Intent(PlacesActivity.this, SignInActivity.class);
                startActivity(intent);
                finish();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        setAddress();
        mActionbar = getSupportActionBar();
        mActionbar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.activity_places_edit_btn)
    public void onClickEdit(){
        placestoDelivery();
    }
    @OnClick(R.id.activity_places_singleRow)
    public void placestoDelivery(){
        startActivity(new Intent(PlacesActivity.this,DeliveryLocationActivity.class));
    }

    public void setAddress(){
        Double lat ;
        Double lng ;
        if(UserPreferenceUtil.getInstance(this).getCurrentLocationCheck()){
            //TODO: Remove lat lang if not used
            lat = Double.parseDouble(UserPreferenceUtil.getInstance(this).getCurrentLat());
            lng = Double.parseDouble(UserPreferenceUtil.getInstance(this).getCurrentLng());
            address.setText(UserPreferenceUtil.getInstance(this).getCurrentLocation());

        }else{
            lat = Double.parseDouble(UserPreferenceUtil.getInstance(this).getLocationLat());
            lng = Double.parseDouble(UserPreferenceUtil.getInstance(this).getLocationLng());
            address.setText(UserPreferenceUtil.getInstance(this).getLocation());

        }
    }
}
