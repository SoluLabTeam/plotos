package com.o2.plotos.places;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.o2.plotos.R;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.User;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.JSONArrayUtil;
import com.o2.plotos.utils.TimeUtil;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.wx.wheelview.widget.WheelView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeliveryLocationActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, PlaceAdapter.OnActionMenuClicked {

    @BindView(R.id.delivery_location_txt_current_location)
    TextView txtCurrentLocation;

    @BindView(R.id.delivery_location_txt_select_date)
    TextView txtDeliveryDateTime;

    @BindView(R.id.wheelviewDate)
    WheelView wheelViewDate;

    @BindView(R.id.wheelviewTime)
    WheelView wheelViewTime;

    @BindView(R.id.activity_delivery_layout_dailog)
    LinearLayout layoutDialog;

    @BindView(R.id.activity_delivery_btn_set_time)
    Button btnSelectDateTime;

    @BindView(R.id.delivery_location_listView)
    ListView listView;

    @BindView(R.id.delivery_location_is_current_check)
    ImageView imgCheckCurrentLocation;

    @BindView(R.id.delivery_location_as_soon_check)
    ImageView imgCheckAsSoonAs;

    @BindView(R.id.delivery_location_future_date)
    ImageView imgCheckFutureDate;

    @BindView(R.id.delivery_location_layout_as_soon)
    LinearLayout layoutAsSoon;

    @BindView(R.id.delivery_location_layout_current)
    LinearLayout layoutCurrentLocation;
    @BindView(R.id.img_close)
    ImageView imgClose;
    @BindView(R.id.btn_add_delivery_location)
    LinearLayout btnAddDeliveryLocation;
    private SharedPreferences pref;

    private JSONArray mAddresses;
    private PlaceAdapter mAdapter;

    private PendingResult<AutocompletePredictionBuffer> mResult;
    private GoogleApiClient mGoogleApiClient;
    private String tempString;
    private ArrayList<AutocompletePrediction> mSearchList;
    // private ActionBar mActionBar;
    private String selectedDateTime;
    private String dateFormate[] = new String[2];

    TimeUtil timeUtil;

    private String placeId;
    private List<String> tempTimeList;
    private List<String> timeList;

    private boolean firstPick;
    private static int PLACE_PICKER_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_location);
        ButterKnife.bind(this);
     /*   String text1 = "+";
        String text2 = " ADD NEW ADDRESS...";

        String s= "+ ADD NEW ADDRESS...";
        SpannableString ss1=  new SpannableString(s);

        btnAddDeliveryLocation.setText(ss1);*/

     /*   SpannableString span1 = new SpannableString(text1);
        span1.setSpan(new AbsoluteSizeSpan(35), 0, text1.length(), SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString span2 = new SpannableString(text2);
        span2.setSpan(new AbsoluteSizeSpan(18), 0, text2.length(), SPAN_INCLUSIVE_INCLUSIVE);

// let's put both spans together with a separator and all
        CharSequence finalText = TextUtils.concat(span1, " ", span2);

        btnAddDeliveryLocation.setText(finalText);*/

        //    btnAddDeliveryLocation.setText(Html.fromHtml("<html><body><font size=25>+</font> ADD NEW ADDRESS... </body><html>"));


     /*   DataBaseHelper dataBaseHelper = new DataBaseHelper(DeliveryLocationActivity.this);
        try {
            List<User> users = dataBaseHelper.getUserIntegerDao().queryForAll();
            if (users.size() == 0) {
                UserPreferenceUtil.getInstance(this).setReturnActivity("LOCATION");



               // Intent intent = new Intent(DeliveryLocationActivity.this, SignInActivity.class);
                //startActivity(intent);
               // finish();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
*/

        timeUtil = new TimeUtil(this);
        mSearchList = new ArrayList<>();

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        initializeXml();
    }

    private void initializeXml() {

        ButterKnife.bind(this);

        // mActionBar = getSupportActionBar();
        // mActionBar.setDisplayHomeAsUpEnabled(true);
        setUserPreferenceDate();


        String name = "";
        String number = "";
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        User user;
        try {
            user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();
            if (user != null) {
                name = user.getFirstName() + " " + user.getLastName();
                number = user.getNumber();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        mAddresses = new JSONArray();
        JSONObject address = new JSONObject();

        try {
            address.put("location", "Deliver to my current location");
            address.put("name", name);
            address.put("number", number);
            address.put("address", UserPreferenceUtil.getInstance(this).getCurrentLocation());
            address.put("lat", UserPreferenceUtil.getInstance(this).getCurrentLat());
            address.put("lng", UserPreferenceUtil.getInstance(this).getCurrentLng());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String json = pref.getString("addresses", "");

        if (!json.isEmpty()) {
            try {
                mAddresses = new JSONArray(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            mAddresses.put(0, address);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        final UserPreferenceUtil userPref = UserPreferenceUtil.getInstance(DeliveryLocationActivity.this);

        mAdapter = new PlaceAdapter(DeliveryLocationActivity.this, mAddresses, userPref.getLocation(), this);

        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int i, long l) {

                JSONObject address = mAdapter.getItem(i);

                try {
                    userPref.setNumber(address.getString("number"));
                    userPref.setLocation(address.getString("address"));
                    userPref.setLocationLat(String.valueOf(address.getDouble("lat")));
                    userPref.setLocationLng(String.valueOf(address.getDouble("lng")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                finish();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /*@OnClick(R.id.delivery_location_txt_select_date)
    public void selectDateTime() {
        if (layoutDialog.getVisibility() == View.INVISIBLE) {
            layoutDialog.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.activity_delivery_btn_set_time)
    public void setDeliveryDateTime() {
        if (layoutDialog.getVisibility() == View.VISIBLE) {
            layoutDialog.setVisibility(View.INVISIBLE);
        }
        selectedDateTime = dateFormate[0] + " " + dateFormate[1];
        txtDeliveryDateTime.setText(selectedDateTime);

        UserPreferenceUtil.getInstance(this).setDeliveryDate(selectedDateTime);
        UserPreferenceUtil.getInstance(this).setAsSoonCheck(false);
        UserPreferenceUtil.getInstance(this).setFutureCheck(true);

        imgCheckFutureDate.setVisibility(View.VISIBLE);
        imgCheckAsSoonAs.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.delivery_location_layout_as_soon)
    public void selectAsSoon() {
        imgCheckAsSoonAs.setVisibility(View.VISIBLE);
        imgCheckFutureDate.setVisibility(View.INVISIBLE);
        UserPreferenceUtil.getInstance(this).setAsSoonCheck(true);
        UserPreferenceUtil.getInstance(this).setFutureCheck(false);
    }*/

    @OnClick(R.id.delivery_location_layout_current)
    public void selectCurrentLocation() {
        imgCheckCurrentLocation.setVisibility(View.VISIBLE);
        //imgCheckLocation.setVisibility(View.INVISIBLE);
        UserPreferenceUtil.getInstance(this).setCurrentLocationCheck(true);
        UserPreferenceUtil.getInstance(this).setLocationCheck(false);
    }

    /*@OnClick(R.id.delivery_location_layout_location)
    public void selectLocation() {
        imgCheckCurrentLocation.setVisibility(View.INVISIBLE);
        //imgCheckLocation.setVisibility(View.VISIBLE);
        UserPreferenceUtil.getInstance(this).setCurrentLocationCheck(false);
        UserPreferenceUtil.getInstance(this).setLocationCheck(true);
    }*/

    @OnClick(R.id.btn_add_delivery_location)
    public void addLocation() {

        //Intent intent = new Intent(DeliveryLocationActivity.this, CreateAddressActivity.class);

        // startActivityForResult(intent, PLACE_PICKER_REQUEST);

        Intent intent = new Intent(DeliveryLocationActivity.this, MapPinActivity.class);

        startActivityForResult(intent, PLACE_PICKER_REQUEST);

        //startActivityForResult(intent, PLACE_PICKER_REQUEST);
    }

    /**
     * Set the user preference
     */
    private void setUserPreferenceDate() {
        txtCurrentLocation.setText(UserPreferenceUtil.getInstance(this).getCurrentLocation());
        //txtDeliveryDateTime.setText(UserPreferenceUtil.getInstance(this).getDeliveryDate());
        //txtLocation.setText(UserPreferenceUtil.getInstance(DeliveryLocationActivity.this).getLocation());
        /*if (UserPreferenceUtil.getInstance(this).getAsSoonCheck()) {
            imgCheckAsSoonAs.setVisibility(View.VISIBLE);
            imgCheckFutureDate.setVisibility(View.INVISIBLE);
        } else {
            imgCheckAsSoonAs.setVisibility(View.INVISIBLE);
            imgCheckFutureDate.setVisibility(View.VISIBLE);
        }*/

        if (UserPreferenceUtil.getInstance(this).getCurrentLocationCheck()) {
            imgCheckCurrentLocation.setVisibility(View.VISIBLE);
            //imgCheckLocation.setVisibility(View.INVISIBLE);
        } else {
            imgCheckCurrentLocation.setVisibility(View.INVISIBLE);
            //imgCheckLocation.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v(ConstantUtil.TAG, "onConnectedFailed");
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {

                JSONObject address = new JSONObject();

                try {
                    address.put("location", data.getStringExtra("location"));
                    address.put("number", data.getStringExtra("number"));
                    address.put("address", data.getStringExtra("address"));
                    address.put("lat", data.getDoubleExtra("lat", 0));
                    address.put("lng", data.getDoubleExtra("lng", 0));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (mAddresses == null) {
                    mAddresses = new JSONArray();
                }

                mAddresses.put(address);

                mAdapter.updateContent(mAddresses);


                final UserPreferenceUtil userPref = UserPreferenceUtil.getInstance(DeliveryLocationActivity.this);

                try {
                    userPref.setNumber(address.getString("number"));
                    userPref.setLocation(address.getString("address"));
                    userPref.setLocationLat(String.valueOf(address.getDouble("lat")));
                    userPref.setLocationLng(String.valueOf(address.getDouble("lng")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // save to user pref
                SharedPreferences.Editor prefsEditor = pref.edit();
                prefsEditor.putString("addresses", mAddresses.toString());
                prefsEditor.commit();


              /*  Set<String> stationCodes = new HashSet<String>();
                JSONArray tempArray = new JSONArray();
                for (int i = 0; i < mAddresses.length(); i++) {
                    String stationCode = null;
                    try {
                        stationCode = mAddresses.getJSONObject(i).getString("address");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (stationCodes.contains(stationCode)) {
                        continue;
                    } else {
                        stationCodes.add(stationCode);
                        try {
                            tempArray.put(mAddresses.getJSONObject(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }


                mAddresses = tempArray; //assign temp to original*/


                Intent i = new Intent(this, DeliveryLocationActivity.class);
                startActivity(i);
                finish();


                //imgCheckLocation.setVisibility(View.INVISIBLE);

            }
        }
    }


    @Override
    public void onEdit(int position) {
        /*mCartOnEdit = cartList.get(position);
        CartViewModel cartViewModel = new CartViewModel(this);
        cartViewModel.getDishByID(mCartOnEdit.getDishId());*/
        //Log.d("test2", "onEdit: " + mCartOnEdit.toString());
        //finish();
    }

    @Override
    public void onDelete(final int position) {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Are you sure you want to delete this location?");
        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (mAddresses != null) {

                    // check if it's selected address
                    try {
                        if (mAddresses.getJSONObject(position).getString("address").equals(UserPreferenceUtil.getInstance(getApplicationContext()).getLocation())) {
                            listView.getChildAt(0).performClick();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //mAddresses.remove(position);
                    mAddresses = JSONArrayUtil.remove(mAddresses, position);

                    mAdapter.updateContent(mAddresses);

                    // save to user pref
                    SharedPreferences.Editor prefsEditor = pref.edit();
                    prefsEditor.putString("addresses", mAddresses.toString());
                    prefsEditor.commit();

                    final UserPreferenceUtil userPref = UserPreferenceUtil.getInstance(DeliveryLocationActivity.this);

                    userPref.setLocation(UserPreferenceUtil.getInstance(DeliveryLocationActivity.this).getCurrentLocation());


                }

                dialog.dismiss();

            }
        });
        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
    }

    @OnClick(R.id.img_close)
    public void onViewClicked() {

        finish();
    }


    /**
     * Adjust location
     * */
    /*public class AdjustLocationListener implements View.OnClickListener {

        private LatLngBounds latLngBounds;

        public AdjustLocationListener(LatLngBounds latLngBounds) {
            this.latLngBounds = latLngBounds;
        }

        @Override
        public void onClick(View v) {
            firstPick = false;

            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            builder.setLatLngBounds(latLngBounds);

            try {
                startActivityForResult(builder.build(DeliveryLocationActivity.this), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }
    }*/
}
