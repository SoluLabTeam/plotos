package com.o2.plotos.places;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.o2.plotos.R;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.User;

import java.sql.SQLException;

public class CreateAddressActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    private ActionBar mActionBar;
    private Button btnNext;
    private String mLocation;
    private String mName;
    private String mMobile;
    private String mAddress;

    private EditText etLocation;
    private EditText etName;
    private EditText etMobile;
    private EditText etAddress;
    private EditText etApartment;
    private EditText etCity;
    private EditText etPostalCode;

    private String mLatitude;
    private String mLongitude;

    private PlaceAutocompleteFragment autocompleteFragment;

    private final int PICK_LOCATION=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_address);

        /*
        mLocation = getIntent().getStringExtra("location");
        mName = getIntent().getStringExtra("name");
        mMobile = getIntent().getStringExtra("mobile");
        mAddress = getIntent().getStringExtra("address");
        mLatitude = getIntent().getStringExtra("longitude");
        mLongitude = getIntent().getStringExtra("latitude");
        */


        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);

        btnNext = (Button) findViewById(R.id.btnNext);

        etLocation = (EditText) findViewById(R.id.location);
        etName = (EditText) findViewById(R.id.name);
        etMobile = (EditText) findViewById(R.id.mobile);
        etAddress = (EditText) findViewById(R.id.address);
        etApartment = (EditText) findViewById(R.id.apartment);
        etCity = (EditText) findViewById(R.id.city);
        etPostalCode = (EditText) findViewById(R.id.postal_code);

        String name = "";
        String number = "";
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        User user;
        try {
            user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();
            name = user.getFirstName() + " " + user.getLastName();
            number = user.getNumber();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        etName.setText(name);
        etMobile.setText(number);

        autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setHint("Enter the address here");

        setListeners();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_LOCATION:
                    if( data.hasExtra("lat") &&
                            data.hasExtra("lng") ) {

                        Intent _data = new Intent();
                        _data.putExtra("location", mLocation);
                        _data.putExtra("name", mName);
                        _data.putExtra("number", mMobile);
                        _data.putExtra("address", mAddress);
                        _data.putExtra("lat", data.getDoubleExtra("lat", 0));
                        _data.putExtra("lng", data.getDoubleExtra("lng", 0));

                        if (getParent() == null) {
                            setResult(Activity.RESULT_OK, _data);
                        } else {
                            getParent().setResult(Activity.RESULT_OK, _data);
                        }

                        finish();
                    }
                    else {
                        Snackbar.make(btnNext, "We couldn't confirm your location. Please try again.", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                    break;
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private boolean validateForm() {

        boolean valid = true;
        String[] fields = {"location", "name", "mobile", "address", "apartment"};

        for( int i=0; i<fields.length; i++ ) {

            int id = getResources().getIdentifier(fields[i], "id", getPackageName());
            EditText field = (EditText)findViewById(id);

            id = getResources().getIdentifier(fields[i]+"InputLayout", "id", getPackageName());
            TextInputLayout input = (TextInputLayout)findViewById(id);

            if( field != null &&
                    input != null ) {
                if( field.getText().length() == 0 ) {
                    input.setError(field.getTag().toString());
                    //input.setError("Please specify " + input.getHint());

                    /*field.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            ((EditText) v).setError("");
                        }
                    });*/

                    valid = false;
                }
            }
        }

        return valid;
    }

    private void setListeners() {

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                mLatitude  = String.valueOf(place.getLatLng().latitude);
                mLongitude = String.valueOf(place.getLatLng().longitude);

                String address = place.getAddress().toString().replace(" - United Arab Emirates", "");
                etAddress.setText(address);
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Place", "An error occurred: " + status);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( validateForm() ) {
                    mLocation = etLocation.getText().toString();
                    mName     = etName.getText().toString();
                    mMobile   = etMobile.getText().toString();

                    mAddress = etAddress.getText().toString() + "\n" +
                                    etApartment.getText().toString();/* + "\n" +
                                    etCity.getText().toString() + "\n" +
                                    etPostalCode.getText().toString();*/

                    Intent intent = new Intent(CreateAddressActivity.this, MapPinActivity.class);
                    intent.putExtra("latitude", mLatitude);
                    intent.putExtra("longitude", mLongitude);
                    intent.putExtra("address", mAddress);
                    startActivityForResult(intent, PICK_LOCATION);
                }
            }
        });

    }
}
