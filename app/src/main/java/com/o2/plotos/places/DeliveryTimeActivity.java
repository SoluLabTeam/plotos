package com.o2.plotos.places;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.wx.wheelview.widget.WheelView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 1/31/2018.
 */

public class DeliveryTimeActivity extends AppCompatActivity {

    @BindView(R.id.img_close)
    ImageView imgClose;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.delivery_location_as_soon_check)
    ImageView imgCheckAsSoonAs;
    @BindView(R.id.delivery_location_layout_as_soon)
    LinearLayout deliveryLocationLayoutAsSoon;
    @BindView(R.id.delivery_location_txt_select_date)
    TextView deliveryLocationTxtSelectDate;
    @BindView(R.id.delivery_location_future_date)
    ImageView imgCheckFutureDate;
    @BindView(R.id.day_range)
    HorizontalScrollView dayRange;
    @BindView(R.id.time_range)
    HorizontalScrollView timeRange;
    @BindView(R.id.day_time_range)
    LinearLayout dayTimeRange;
    @BindView(R.id.wheelviewDate)
    WheelView wheelviewDate;
    @BindView(R.id.wheelviewTime)
    WheelView wheelviewTime;
    @BindView(R.id.activity_delivery_btn_set_time)
    Button activityDeliveryBtnSetTime;
    @BindView(R.id.activity_delivery_layout_dailog)
    LinearLayout activityDeliveryLayoutDailog;

    private String deliveryDay, deliveryTime,deliveryDay2,deliveryTime2;
    private String selectedDateTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_delivery_time);
        ButterKnife.bind(this);

        setUserPreferenceDate();
        dayRange.setHorizontalFadingEdgeEnabled(true);
        timeRange.setHorizontalFadingEdgeEnabled(true);




       /* Calendar calendar = Calendar.getInstance();

        if(DateUtils.isToday(calendar.getTimeInMillis()))
        {
            initTimeRangeView(timeRange, 8, 23, 30, true);

        }
        else
        {
            SimpleDateFormat mdformat = new SimpleDateFormat("HH");
            String starts = mdformat.format(calendar.getTime());
            int start = Integer.parseInt(starts);

            initTimeRangeView(timeRange, start, 23, 30, true);


        }*/

        initTimeRangeView(timeRange, 8, 23, 30, true);
        initDayRangeView(dayRange);

    }

    /*
       * Select delivery time
       */
    public void selectDeliveryTime(View v) {

        TextView textView = (TextView) v.findViewById(R.id.label);
        Context context = getApplicationContext();
        LinearLayout parent = (LinearLayout) v.getParent();

        for (int index = 0; index < parent.getChildCount(); index++) {
            LinearLayout _view = (LinearLayout) parent.getChildAt(index);
            TextView _textView = (TextView) _view.getChildAt(0);
            _view.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
            _textView.setTextColor(ContextCompat.getColor(context, R.color.textGray));
        }

        v.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
        textView.setTextColor(Color.WHITE);

        if (deliveryDay != null && deliveryTime != null) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = format.parse(deliveryDay);
                SimpleDateFormat sdfDisplay = new SimpleDateFormat("EEE, d MMM", Locale.UK);

                selectedDateTime = sdfDisplay.format(date.getTime()) + " | " + deliveryTime;
                deliveryLocationTxtSelectDate.setText(selectedDateTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            try {

                Date date2 = format2.parse(deliveryDay2);
                SimpleDateFormat sdfDisplay2 = new SimpleDateFormat("EEE, MMM d", Locale.UK);

                String  selectedDateTime = sdfDisplay2.format(date2.getTime()) + " " + deliveryTime2;



            } catch (ParseException e) {
                e.printStackTrace();
            }

            imgCheckAsSoonAs.setVisibility(View.GONE);
            imgCheckFutureDate.setVisibility(View.VISIBLE);
        }
    }


    /*
     * Init time range scroll view
     */
    public void initDayRangeView(HorizontalScrollView container) {

        Context context = getApplicationContext();
        LayoutInflater inflater = getLayoutInflater();
        LinearLayout parent = new LinearLayout(context);

        SimpleDateFormat sdfDisplay = new SimpleDateFormat("EEE, MMM d", Locale.UK);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

        Calendar cal = Calendar.getInstance();

        for (int i = 0; i <= 3; i++) {

            View view = inflater.inflate(R.layout.item_range, null);
            TextView textView = (TextView) view.findViewById(R.id.label);

            textView.setText(sdfDisplay.format(cal.getTime()));

            view.setTag(sdf.format(cal.getTime()));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deliveryDay = (String) v.getTag();
                    deliveryDay2 = (String) v.getTag();
                    selectDeliveryTime(v);
                }
            });

            parent.addView(view);

            cal.add(Calendar.DATE, 1);
        }

        container.addView(parent);

    }


    /*
     * Init time range scroll view
     */
    public void initTimeRangeView(HorizontalScrollView container, int start, int end, int step, boolean format) {

        Context context = getApplicationContext();
        LayoutInflater inflater = getLayoutInflater();
        final LinearLayout parent = new LinearLayout(context);

        String mFormat = "HH:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(mFormat, Locale.UK);

        // add 1 hour to start time
        //start += 1;

        // subtract 1 hour from end time
        //end += 1;

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, start);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        //cal.add(Calendar.MINUTE, -step);

        String label;
        do {
            cal.add(Calendar.MINUTE, step);
            label = sdf.format(cal.getTime());




            if(String.valueOf(label.charAt(0)).equals("0"))
            {
                label= label.substring(1);
            }

            cal.add(Calendar.MINUTE, step);

            String lll = sdf.format(cal.getTime());

            if(String.valueOf(lll.charAt(0)).equals("0"))
            {
                lll=   lll.substring(1);
            }

            label += " - " + lll;

            cal.add(Calendar.MINUTE, -step);

            View view = inflater.inflate(R.layout.item_range, null);
            TextView textView = (TextView) view.findViewById(R.id.label);

            textView.setText(label);

            textView.setTag(textView.getTextSize());

            view.setId(cal.get(Calendar.HOUR_OF_DAY));

            view.setTag(label);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deliveryTime = (String) v.getTag();
                    deliveryTime2 = (String) v.getTag();
                    selectDeliveryTime(v);
                }
            });

            parent.addView(view);

        } while (!(cal.get(Calendar.HOUR_OF_DAY) == end && (int) cal.get(Calendar.MINUTE) == 30));

        container.addView(parent);

    }

    /**
     * Set the user preference
     */
    private void setUserPreferenceDate() {

        if (UserPreferenceUtil.getInstance(this).getAsSoonCheck()) {
            imgCheckAsSoonAs.setVisibility(View.VISIBLE);
            imgCheckFutureDate.setVisibility(View.GONE);
            dayTimeRange.setVisibility(View.GONE);
        } else {
            deliveryLocationTxtSelectDate.setText(UserPreferenceUtil.getInstance(this).getDeliveryDate());
            imgCheckAsSoonAs.setVisibility(View.GONE);
            imgCheckFutureDate.setVisibility(View.VISIBLE);
            dayTimeRange.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.txt_show_time)
    public void selectDateTime() {
        activityDeliveryBtnSetTime.setVisibility(View.VISIBLE);
        imgCheckAsSoonAs.setVisibility(View.GONE);
        imgCheckFutureDate.setVisibility(View.VISIBLE);

        if (dayTimeRange.getVisibility() != View.VISIBLE) {
            dayTimeRange.setVisibility(View.VISIBLE);

            Calendar cal = Calendar.getInstance();
            int h = cal.get(Calendar.HOUR_OF_DAY);

            final View view = timeRange.findViewWithTag(h + ":00 - " + h + ":30");
            if (view != null) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        timeRange.scrollTo((int) view.getX(), 0);
                    }
                }, 100);
            }
        } else {
            // dayTimeRange.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.delivery_location_layout_as_soon)
    public void selectAsSoon() {
        imgCheckAsSoonAs.setVisibility(View.VISIBLE);
        imgCheckFutureDate.setVisibility(View.GONE);
        dayTimeRange.setVisibility(View.GONE);
        activityDeliveryBtnSetTime.setVisibility(View.GONE);
        UserPreferenceUtil.getInstance(this).setAsSoonCheck(true);
        UserPreferenceUtil.getInstance(this).setFutureCheck(false);
    }


    @OnClick({R.id.img_close, R.id.activity_delivery_btn_set_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_close:
                finish();
                break;
            case R.id.activity_delivery_btn_set_time:



                if(imgCheckAsSoonAs.getVisibility()==View.VISIBLE)
                {
                    finish();
                    UserPreferenceUtil.getInstance(this).setAsSoonCheck(true);
                    UserPreferenceUtil.getInstance(this).setFutureCheck(false);
                }else
                {
                    if(selectedDateTime!=null || !deliveryLocationTxtSelectDate.getText().toString().isEmpty()) {
                        finish();

                        String time = deliveryDay2 + " " + deliveryTime2.substring(0, 5);

                        UserPreferenceUtil.getInstance(this).setDeliveryDate2(time);
                        UserPreferenceUtil.getInstance(this).setDeliveryDate(deliveryLocationTxtSelectDate.getText().toString());
                        UserPreferenceUtil.getInstance(this).setAsSoonCheck(false);
                        UserPreferenceUtil.getInstance(this).setFutureCheck(true);
                    }

                    else

                    {


                        Toast.makeText(this, "please select delivery time",Toast.LENGTH_SHORT).show();
                    }

                }








                break;
        }
    }
}
