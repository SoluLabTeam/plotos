package com.o2.plotos.authentication.signup;

import android.content.Context;

/**
 * Created by Hassan on 29/01/2017.
 */

public class SignUpPresenterImpl implements ISignupPresenter, ISignupDataModel.OnSignUpRequestFinishLisener {

    private ISignupView iSignupView;
    private SignUpDataModelImpl mSignUpDataModelImpl;


    public SignUpPresenterImpl(Context context) {
        mSignUpDataModelImpl = new SignUpDataModelImpl(context,this);
    }

    @Override
    public void sendSignupRequest(String firstName, String lastName, String email, String password, String number, String fbId, String imgUrl) {
        if(iSignupView!=null){
            iSignupView.showProgress();
            mSignUpDataModelImpl.sendSignUpRequest(firstName,lastName,email,password,number,fbId,imgUrl);
        }
    }

    @Override
    public void bindView(Object view) {
        iSignupView = (ISignupView) view;

    }

    @Override
    public void unBindView() {
        iSignupView = null;
    }

    @Override
    public void onSignUpSuccess() {
        if(iSignupView!=null){
            iSignupView.hideProgress();
            iSignupView.onSignUpSuccess();
        }
    }

    @Override
    public void onSignUpFailed(String error) {
        if(iSignupView!=null){
            iSignupView.hideProgress();
            iSignupView.onSignUpFailed(error);
        }
    }
}
