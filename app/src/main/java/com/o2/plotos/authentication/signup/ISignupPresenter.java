package com.o2.plotos.authentication.signup;

/**
 * Created by Hassan on 29/01/2017.
 */

public interface ISignupPresenter {
    void sendSignupRequest(String firstName, String lastName, String email ,String password, String number, String fbId, String imgUrl);
    void bindView(Object view);
    void unBindView();
}
