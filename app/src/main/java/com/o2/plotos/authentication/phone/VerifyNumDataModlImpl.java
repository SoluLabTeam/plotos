package com.o2.plotos.authentication.phone;

import android.content.Context;
import android.util.Log;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.VerifiedRequest;
import com.o2.plotos.restapi.endpoints.VerifyNumRequest;
import com.o2.plotos.restapi.responses.VerifiedResponse;
import com.o2.plotos.restapi.responses.VerifyNumResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;
import com.o2.plotos.utils.UserPreferenceUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hassan on 28/02/2017.
 */

public class VerifyNumDataModlImpl implements IVerifyNumDataModl {

    private OnRequestFinishLisener mListener;
    private Context context;

    public VerifyNumDataModlImpl(Context context, OnRequestFinishLisener lisener){
        mListener = lisener;
        this.context  = context;
    }

    @Override
    public void sendRequest(String number) {
        VerifyNumRequest verifyNumRequest = ServiceGenrator.createService(VerifyNumRequest.class);

        Call<VerifyNumResponse> responseCall = verifyNumRequest.numberResquest(number);
        responseCall.enqueue(new Callback<VerifyNumResponse>() {
            @Override
            public void onResponse(Call<VerifyNumResponse> call, Response<VerifyNumResponse> response) {
                LogCat.LogDebug(ConstantUtil.TAG,"verify number response "+response.raw());
                if(response.code()== 200){
                    VerifyNumResponse apiResponse =  response.body();
                    String responseCode = apiResponse.response;

                    if(responseCode.equalsIgnoreCase("0")){
                        mListener.onVerifyNumSuccess(apiResponse.code);
                        LogCat.LogDebug(ConstantUtil.TAG, "Code: "+apiResponse.code);
                    }else if(responseCode.equalsIgnoreCase("2")){
                        mListener.onVerifyNumFailed("Some Error Occured");
                    }
                }
            }

            @Override
            public void onFailure(Call<VerifyNumResponse> call, Throwable t) {
                mListener.onVerifyNumFailed("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"onFailure "+t.getMessage());
            }
        });
    }

    @Override
    public void sendVerifiedRequest(String user_id) {
        VerifiedRequest verifiedRequest = ServiceGenrator.createService(VerifiedRequest.class);

        Log.d("sendVerifiedRequest", user_id);


        Call<VerifiedResponse> responseCall = verifiedRequest.update(user_id);
        responseCall.enqueue(new Callback<VerifiedResponse>() {
            @Override
            public void onResponse(Call<VerifiedResponse> call, Response<VerifiedResponse> response) {
                LogCat.LogDebug(ConstantUtil.TAG,"verify number response "+response.raw());
                if(response.code()== 200){
                    /*VerifiedResponse apiResponse =  response.body();
                    String responseCode = apiResponse.response;

                    if(responseCode.equalsIgnoreCase("0")){
                        mListener.onVerifyNumSuccess(apiResponse.code);
                        LogCat.LogDebug(ConstantUtil.TAG, "Code: "+apiResponse.code);
                    }else if(responseCode.equalsIgnoreCase("2")){
                        mListener.onVerifyNumFailed("Some Error Occured");
                    }*/
                }
            }

            @Override
            public void onFailure(Call<VerifiedResponse> call, Throwable t) {
                //mListener.onVerifyNumFailed("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"onFailure "+t.getMessage());
            }
        });
    }
}
