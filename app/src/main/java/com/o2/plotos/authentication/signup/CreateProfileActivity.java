package com.o2.plotos.authentication.signup;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.home.menu.EditProfileActivity;
import com.o2.plotos.home.menu.SaveCardActivity;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.editProfile.EditProfileModel;
import com.o2.plotos.utils.UserPreferenceUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 3/23/2018.
 */

public class CreateProfileActivity extends BaseActivity {

    public static final int MULTIPLE_PERMISSIONS = 10;

    String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.edit_firstname)
    EditText editFirstname;
    @BindView(R.id.txtinput_firstname)
    TextInputLayout txtinputFirstname;
    @BindView(R.id.edit_lastname)
    EditText editLastname;
    @BindView(R.id.txtinput_lastname)
    TextInputLayout txtinputLastname;
    @BindView(R.id.btn_done)
    Button btnDone;
    @BindView(R.id.txt_policy)
    TextView txt_policy;
    @BindView(R.id.iv_profile)
    ImageView ivProfile;

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;

    boolean isImageUploaded = false;

    File destination;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        ButterKnife.bind(this);
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), descriptionString);
    }


    private void selectImage() {
        final CharSequence[] items;
        items = new CharSequence[]{"Take Photo", "Choose from Library"};
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (checkPermissions())
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (checkPermissions())
                        galleryIntent();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }


    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Uri tempUri = getImageUri(getApplicationContext(), bm);
        destination = new File(getRealPathFromURI(tempUri));
        ivProfile.setImageBitmap(bm);
        isImageUploaded = true;
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        isImageUploaded = true;
        ivProfile.setImageBitmap(thumbnail);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                int permissionCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
                int permissionStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (permissionCamera == PackageManager.PERMISSION_GRANTED && permissionStorage == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask != null && userChoosenTask.equals("Take Photo")) {
                        cameraIntent();
                    } else if (userChoosenTask != null && userChoosenTask.equals("Choose from Library")) {
                        galleryIntent();
                    }
                } else {
                    Toast.makeText(this, "Enable permissions in settings/app to upload photo", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @OnClick({R.id.img_back, R.id.btn_done, R.id.iv_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_done:
                getValues();
                break;
            case R.id.iv_profile:
                selectImage();
                break;
        }
    }

    private void getValues() {
        if (!editFirstname.getText().toString().equals("")
                && !editLastname.getText().toString().equals("")) {
//            Bundle bundle = new Bundle();
//            bundle.putString("fname", editFirstname.getText().toString());
//            bundle.putString("lname", editLastname.getText().toString());
//            if (isImageUploaded) {
//                bundle.putString("image", destination.getAbsolutePath());
//            }
//            Intent i = new Intent(this, SaveCardActivity.class);
//            i.putExtras(bundle);
//            startActivity(i);
            callApi();
        } else {
            Toast.makeText(this, "One of the field is empty or invalid", Toast.LENGTH_SHORT).show();
        }
    }

    private void callApi(){
        showProgress();
        RequestBody ids = createPartFromString(UserPreferenceUtil.getInstance(getApplicationContext()).UserId());
        RequestBody fnames = createPartFromString(editFirstname.getText().toString().trim());
        RequestBody lnames = createPartFromString(editLastname.getText().toString().trim());
        RequestBody email = createPartFromString(UserPreferenceUtil.getInstance(CreateProfileActivity.this).getUserData().getEmail());
        RequestBody mobile = createPartFromString(UserPreferenceUtil.getInstance(CreateProfileActivity.this).getUserData().getNumber());
        RequestBody address;
//        if (selectedLocation.equals("current")){
//            address = createPartFromString(txtCurrentlocation.getText().toString());
//        }else {
//            address = createPartFromString(editCustomLocation.getText().toString());
//        }
        address = createPartFromString("");

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("user_id", ids);
        map.put("first_name", fnames);
        map.put("last_name", lnames);
        map.put("number", mobile);
        map.put("email", email);
        map.put("address", address);
        Call<EditProfileModel> call;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        if (isImageUploaded){
            File image = new File(destination.getAbsolutePath());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
            call = apiInterface.getEditProfileResponse(map,body);
        }else{
            call = apiInterface.getEditProfileResponse(map);
        }
        call.enqueue(new Callback<EditProfileModel>() {
            @Override
            public void onResponse(Call<EditProfileModel> call, Response<EditProfileModel> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    UserPreferenceUtil.getInstance(CreateProfileActivity.this).storeUserData(response.body().getUser_data());
                    Intent i = new Intent(CreateProfileActivity.this, SaveCardActivity.class);
                    i.putExtra("from","registration");
                    startActivity(i);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<EditProfileModel> call, Throwable t) {
                hideProgress();
            }
        });
    }

}
