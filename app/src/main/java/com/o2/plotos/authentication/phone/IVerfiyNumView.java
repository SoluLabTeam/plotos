package com.o2.plotos.authentication.phone;

/**
 * Created by Hassan on 28/02/2017.
 */

public interface IVerfiyNumView {
    void showProgress();
    void hideProgress();
    void onVerifyNumSuccess(String code);
    void onVerifyNumFailed(String error);
}
