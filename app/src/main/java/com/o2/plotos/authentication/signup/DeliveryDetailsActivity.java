package com.o2.plotos.authentication.signup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.LocationBaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.places.DeliveryLocationActivity;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.editProfile.EditProfileModel;
import com.o2.plotos.restapi.newApis.models.editProfile.EditProfileResponse;
import com.o2.plotos.restapi.newApis.models.login.UserData;
import com.o2.plotos.utils.UserPreferenceUtil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 3/23/2018.
 */

public class DeliveryDetailsActivity extends LocationBaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_currentlocation2)
    TextView txtCurrentlocation;
    @BindView(R.id.img_check_current)
    ImageView imgCheckCurrent;
    @BindView(R.id.img_check_custom)
    ImageView img_check_custom;
    @BindView(R.id.edit_firstname)
    EditText editCustomLocation;
    @BindView(R.id.txtinput_newaddress)
    TextInputLayout txtinputNewaddress;
    @BindView(R.id.btn_done)
    Button btnDone;

    Bundle bundle;

    String selectedLocation;

    Geocoder geocoder;
    List<Address> addresses;
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";
    private ProgressDialog dialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_details);
        ButterKnife.bind(this);
        if (getIntent() != null && getIntent().getExtras() != null) {
            bundle = getIntent().getExtras();
        }
        imgCheckCurrent.performClick();
    }

    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        geocoder = new Geocoder(this,Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            String address = addresses.get(0).getAddressLine(0);
            txtCurrentlocation.setText(address);
            UserPreferenceUtil.getInstance(this).setCurrentLocation(address);
            UserPreferenceUtil.getInstance(this).setLocationLat(String.valueOf(location.getLatitude()));
            UserPreferenceUtil.getInstance(this).setLocationLng(String.valueOf(location.getLatitude()));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void showProgress() {
        if (dialog == null) {
            dialog = new ProgressDialog(this);
            dialog.setMessage("Doing something, please wait.");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
        }
        dialog.show();
    }

    public void hideProgress() {
        dialog.dismiss();
    }

    @OnClick({R.id.img_back, R.id.btn_done, R.id.img_check_current, R.id.img_check_custom})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_done:
                if (selectedLocation.equals("custom")) {
                    if (!editCustomLocation.getText().toString().equals("")) {
                        callApi();
                    }else{
                        Toast.makeText(this, "Please a address", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    callApi();
                }
                break;
            case R.id.img_check_current:
                selectedLocation = "current";
                checkAddress(imgCheckCurrent, img_check_custom);
                break;
            case R.id.img_check_custom:
                selectedLocation = "custom";
                checkAddress(img_check_custom, imgCheckCurrent);
                break;
        }
    }

    private void checkAddress(ImageView selected, ImageView unselect) {
        selected.setImageResource(R.drawable.check);
        unselect.setImageResource(R.drawable.uncheck);
    }

    private void callApi(){
        showProgress();
        RequestBody ids = createPartFromString(UserPreferenceUtil.getInstance(getApplicationContext()).UserId());
        RequestBody fnames = createPartFromString(bundle.getString("fname"));
        RequestBody lnames = createPartFromString(bundle.getString("lname"));
        RequestBody email = createPartFromString(UserPreferenceUtil.getInstance(DeliveryDetailsActivity.this).getUserData().getEmail());
        RequestBody mobile = createPartFromString(UserPreferenceUtil.getInstance(DeliveryDetailsActivity.this).getUserData().getNumber());
        RequestBody address;
        if (selectedLocation.equals("current")){
            address = createPartFromString(txtCurrentlocation.getText().toString());
        }else {
            address = createPartFromString(editCustomLocation.getText().toString());
        }
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("user_id", ids);
        map.put("first_name", fnames);
        map.put("last_name", lnames);
        map.put("number", mobile);
        map.put("email", email);
        map.put("address", address);
        Call<EditProfileModel> call;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        if (bundle.containsKey("image")){
            File image = new File(bundle.getString("image"));
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
            call = apiInterface.getEditProfileResponse(map,body);
        }else{
            call = apiInterface.getEditProfileResponse(map);
        }
        call.enqueue(new Callback<EditProfileModel>() {
            @Override
            public void onResponse(Call<EditProfileModel> call, Response<EditProfileModel> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    UserPreferenceUtil.getInstance(DeliveryDetailsActivity.this).storeUserData(response.body().getUser_data());
                    Intent i = new Intent(DeliveryDetailsActivity.this, DeleverdLocationActivity.class);
                    if (selectedLocation.equals("current")){
                        i.putExtra("address",txtCurrentlocation.getText().toString());
                    }else {
                        i.putExtra("address",editCustomLocation.getText().toString());
                    }
                    startActivity(i);
                }
            }

            @Override
            public void onFailure(Call<EditProfileModel> call, Throwable t) {
                hideProgress();
            }
        });
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), descriptionString);
    }
}
