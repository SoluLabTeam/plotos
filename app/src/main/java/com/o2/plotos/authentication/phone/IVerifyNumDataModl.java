package com.o2.plotos.authentication.phone;

/**
 * Created by Hassan on 28/02/2017.
 */

public interface IVerifyNumDataModl {
    void sendRequest(String number);
    void sendVerifiedRequest(String number);
    interface OnRequestFinishLisener{
        void onVerifyNumSuccess(String code);
        void onVerifyNumFailed(String error);
    }
}
