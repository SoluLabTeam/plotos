package com.o2.plotos.authentication.signin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.authentication.signup.CreateAnAccountActivity;
import com.o2.plotos.authentication.signup.DeleverdLocationActivity;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.login.LoginResponce;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 3/21/2018.
 */

public class SignInActivityNew extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.edit_email)
    EditText editEmail;
    @BindView(R.id.txtinput_email)
    TextInputLayout txtinputEmail;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.txtinput_password)
    TextInputLayout txtinputPassword;
    @BindView(R.id.txt_forgotpass)
    TextView txtForgotpass;

    @BindView(R.id.txt_register)
    TextView txt_register;
    @BindView(R.id.btn_signin)
    Button Btn_signin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_signin);
        ButterKnife.bind(this);
    }

    public boolean validateFields() {
        if (!validateEmail() || !validatePassword()) {
            validateEmail();
            validatePassword();
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validateEmail() {
        String eMail = editEmail.getText().toString().trim();

        if (eMail.isEmpty() || !isValidEmail(eMail)) {
            txtinputEmail.setError(getString(R.string.error_empty_email));
            return false;
        } else {
            txtinputEmail.setErrorEnabled(false);
            requestFocus(editPassword);
        }
        return true;
    }

    private boolean validatePassword() {
        String pswrd = editPassword.getText().toString().trim();
        if (pswrd.isEmpty()) {
            txtinputPassword.setError(getString(R.string.error_empty_password_signin));
            return false;
        } else {
            txtinputPassword.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @OnClick({R.id.img_back, R.id.txt_forgotpass, R.id.btn_signin, R.id.txt_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.txt_forgotpass:
                Intent i = new Intent(this, ForgotPasswordActivity.class);
                startActivity(i);
                break;
            case R.id.btn_signin:
                if (validateFields()) {
                    if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
                        callLoginApi();
                    } else {
                        Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.txt_register:
                Intent i1 = new Intent(this, CreateAnAccountActivity.class);
                startActivity(i1);
                break;
        }
    }

    private void callLoginApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponce> call = apiInterface.getLoginResponse(editEmail.getText().toString().trim(), editPassword.getText().toString().trim());
        call.enqueue(new Callback<LoginResponce>() {
            @Override
            public void onResponse(Call<LoginResponce> call, Response<LoginResponce> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    UserPreferenceUtil.getInstance(SignInActivityNew.this).storeUserData(response.body().getUserData());
                    UserPreferenceUtil.getInstance(SignInActivityNew.this).setUserId(response.body().getUserData().getUser_id());
                    Intent i = new Intent(SignInActivityNew.this, HomeActivity.class);
                    startActivity(i);
                    finishAffinity();
                } else {
                    showToast(response.body().getMsg());

                }
            }

            @Override
            public void onFailure(Call<LoginResponce> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }
}
