package com.o2.plotos.authentication.phone;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.models.Country;
import com.o2.plotos.utils.CustomFonts;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hassan on 28/02/2017.
 */

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder> {

    private List<Country> mCountryList;
    private LayoutInflater mInflater;
    private  OnCountryClickListener mListener;
    private CustomFonts customFonts;

    public CountryAdapter(Context context, List<Country> countryList, OnCountryClickListener listener) {
        mInflater = LayoutInflater.from(context);
        mListener = listener;
        mCountryList = countryList;
        customFonts = new CustomFonts(context);
    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_country, parent, false);
        CountryViewHolder dietViewHolder = new CountryViewHolder(v);
        return dietViewHolder;
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, final int position) {
        holder.code.setText(mCountryList.get(position).getDialCode());
        holder.country.setText(mCountryList.get(position).getName());
        customFonts.setOpenSansBold(holder.country);
        final String countryCode = mCountryList.get(position).getDialCode();
        final String countryName = mCountryList.get(position).getName();
        holder.country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onCountryClick(countryName ,countryCode);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCountryList.size();
    }

    public static class CountryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_country_code)
        TextView code;
        @BindView(R.id.item_country_countryName)
        TextView country;

        public CountryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnCountryClickListener {
        void onCountryClick( String name, String code);
    }
}
