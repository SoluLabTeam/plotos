package com.o2.plotos.authentication.phone;

import android.content.Context;

/**
 * Created by Hassan on 28/02/2017.
 */

public class VerifyNumPrsnterImpl implements  IVerifyNumPresnter,IVerifyNumDataModl.OnRequestFinishLisener{
    private IVerfiyNumView iVerfiyNumView;
    private VerifyNumDataModlImpl mVerifyNumDataModl;

    public VerifyNumPrsnterImpl(Context context){
        mVerifyNumDataModl = new VerifyNumDataModlImpl(context ,this);
    }

    @Override
    public void onVerifyNumSuccess(String code) {
        if(iVerfiyNumView!=null){
            iVerfiyNumView.hideProgress();
            iVerfiyNumView.onVerifyNumSuccess(code);
        }
    }

    @Override
    public void onVerifyNumFailed(String error) {
        if(iVerfiyNumView!=null){
            iVerfiyNumView.hideProgress();
            iVerfiyNumView.onVerifyNumFailed(error);
        }
    }

    @Override
    public void sendVerifyNumRequest(String number) {
        if(iVerfiyNumView!=null){
            iVerfiyNumView.showProgress();
            mVerifyNumDataModl.sendRequest(number);
        }

    }

    @Override
    public void sendVerifiedRequest(String user_id) {
        mVerifyNumDataModl.sendVerifiedRequest(user_id);
    }

    @Override
    public void bindView(Object view) {
        iVerfiyNumView = (IVerfiyNumView) view;
    }

    @Override
    public void unBindView() {
        iVerfiyNumView = null;
    }
}
