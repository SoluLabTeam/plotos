package com.o2.plotos.authentication.phone;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.authentication.signup.MobileVerficationActivity;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.OpensanBtn;
import com.o2.plotos.utils.UserPreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChgMobActivity extends BaseActivity {
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.edit_mobile)
    EditText editMobile;
    @BindView(R.id.txtinput_mobile)
    TextInputLayout txtinputMobile;
    @BindView(R.id.btn_save)
    OpensanBtn btnSave;
    String mobile = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_mobile);
        ButterKnife.bind(this);
        mobile = getIntent().getStringExtra(Constant.MOBILE);
        editMobile.setText(mobile);
    }

    @OnClick({R.id.img_back, R.id.btn_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_save:
                if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
                    callChngMobile();
                } else {
                    Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void callChngMobile() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BaseApiModel> call = apiInterface.changeMobile(getDataFromEditText(editMobile), UserPreferenceUtil.getInstance(this).UserId());
        call.enqueue(new Callback<BaseApiModel>() {
            @Override
            public void onResponse(Call<BaseApiModel> call, Response<BaseApiModel> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    showToast(response.body().getMsg());
                    Intent intent = new Intent(ChgMobActivity.this, MobileVerficationActivity.class);
                    intent.putExtra("mobile", getDataFromEditText(editMobile));
                    startActivity(intent);
                    finish();
                } else {
                    showToast(response.body().getMsg());
                }

            }

            @Override
            public void onFailure(Call<BaseApiModel> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();

            }
        });

    }

}
