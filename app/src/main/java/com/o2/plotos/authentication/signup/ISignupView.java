package com.o2.plotos.authentication.signup;

/**
 * Created by Hassan on 29/01/2017.
 */

public interface ISignupView {
    void showProgress();
    void hideProgress();
    void onSignUpSuccess();
    void onSignUpFailed(String error);
}
