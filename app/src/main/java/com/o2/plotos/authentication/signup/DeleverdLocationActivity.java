package com.o2.plotos.authentication.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.home.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 3/23/2018.
 */

public class DeleverdLocationActivity extends AppCompatActivity {

    @BindView(R.id.img_tuch)
    ImageView imgTuch;
    @BindView(R.id.txt_location)
    TextView txtLocation;
    @BindView(R.id.activity_sign_in)
    LinearLayout activitySignIn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delevered_loaction);
        ButterKnife.bind(this);
//        txtLocation.setText(getIntent().getStringExtra("address"));
    }

    @OnClick(R.id.img_tuch)
    public void onViewClicked() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("CurrentTab", 2);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
