package com.o2.plotos.authentication.signin;

import android.content.Context;
import android.util.Log;

import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.User;
import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.SignInRequest;
import com.o2.plotos.restapi.responses.SignInRquestResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;
import com.o2.plotos.utils.UserPreferenceUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hassan on 1/28/2017.
 */

public class SignInDataModelIml implements ISigninDataModel {

    private OnRequestFinishLisener mListener;
    private Context context;

    public SignInDataModelIml(Context context, OnRequestFinishLisener lisener){
        mListener = lisener;
        this.context  = context;
    }

    @Override
    public void sendRequest(String email, String password) {

        SignInRequest signInRequest = ServiceGenrator.createService(SignInRequest.class);

        Call<SignInRquestResponse> responseCall = signInRequest.signInRequest(email,password,"nadsnjk34kjdakjbsdv");
        responseCall.enqueue(new Callback<SignInRquestResponse>() {
            @Override
            public void onResponse(Call<SignInRquestResponse> call, Response<SignInRquestResponse> response) {
                Log.d(ConstantUtil.TAG,"sign in response "+response.raw());
                if(response.code()== 200){
                    SignInRquestResponse apiResponse =  response.body();
                    String responseCode = apiResponse.response;

                    if(responseCode.equalsIgnoreCase("0")){
                        if(apiResponse.user != null) {
                            saveUser(apiResponse.user);
                        }else{
                            mListener.onSignInFailed("Something went wrong, please try again.");
                        }
                    }else if(responseCode.equalsIgnoreCase("2")){
                        mListener.onSignInFailed("Email and password not matched");
                    }
                }
            }

            @Override
            public void onFailure(Call<SignInRquestResponse> call, Throwable t) {
                mListener.onSignInFailed("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"onFailure "+t.getMessage());
            }
        });
    }

    private void saveUser(User user){

        user.setLat("");
        user.setLng("");
        user.setImageUrl("");
        //user.setNumber("");

        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        dataBaseHelper.saveUser(user);

        Log.d("sign in user", user.getVerified()?"Verified":"NOT Verified");

        UserPreferenceUtil.getInstance(context).setUserId(user.getId());

        UserPreferenceUtil.getInstance(context).setNumber(user.getNumber());
        //UserPreferenceUtil.getInstance(context).setVerified(user.getVerified());

        LogCat.LogDebug(ConstantUtil.TAG,"user "+user.toString());

        mListener.onSignInSuccess();
    }
}
