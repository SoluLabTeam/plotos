package com.o2.plotos.authentication.signup;

/**
 * Created by Hassan on 29/01/2017.
 */

public interface ISignupDataModel {
    void sendSignUpRequest(String firstName, String lastName, String email ,String password, String number, String fbId, String imgUrl);
    interface OnSignUpRequestFinishLisener{
        void onSignUpSuccess();
        void onSignUpFailed(String error);
    }
}
