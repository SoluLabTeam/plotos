package com.o2.plotos.authentication.signup;

import android.content.Context;
import android.util.Log;

import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.User;
import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.SignUpRequest;
import com.o2.plotos.restapi.responses.SignUpRequestResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;
import com.o2.plotos.utils.UserPreferenceUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hassan on 29/01/2017.
 */

public class SignUpDataModelImpl implements ISignupDataModel {

    private OnSignUpRequestFinishLisener mlistener;
    private Context context;

    public SignUpDataModelImpl(Context context ,OnSignUpRequestFinishLisener listener){
       mlistener = listener;
        this.context = context;
    }

    @Override
    public void sendSignUpRequest(final String firstName, final String lastName, String email, String password, final String number, String fbId, final String imgUrl) {

        SignUpRequest signUpRequest = ServiceGenrator.createService(SignUpRequest.class);
        Call<SignUpRequestResponse> signUpRequestResponseCall = signUpRequest.signUpRequest
                (firstName,lastName,email,password,number,fbId,"sajkbdhkabscasjkcnjks");

        signUpRequestResponseCall.enqueue(new Callback<SignUpRequestResponse>() {
            @Override
            public void onResponse(Call<SignUpRequestResponse> call, Response<SignUpRequestResponse> response) {
                Log.d(ConstantUtil.TAG,"sign up response "+response.raw());
                if(response.code()== 200){
                    SignUpRequestResponse apiResponse =  response.body();
                    String responseCode = apiResponse.response;


                    if(responseCode.equalsIgnoreCase("0")){
                        if(apiResponse.message.equalsIgnoreCase("User has been added successfully.")) {
                            saveUser(apiResponse.user_id, firstName, lastName, number, imgUrl, apiResponse.verified);
                        } else {
                            saveFbUser(apiResponse.user,number,imgUrl, apiResponse.verified);
                        }
                    }else if(responseCode.equalsIgnoreCase("1")){
                        mlistener.onSignUpFailed("Please fill all required fields.");
                    }else if(responseCode.equalsIgnoreCase("2")){
                        mlistener.onSignUpFailed("This email already exists.");
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpRequestResponse> call, Throwable t) {
                mlistener.onSignUpFailed("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"onFailure "+t.getMessage());
            }
        });

    }

    private void saveUser(String userId,String firstName,String lastName, String number, String imgUrl, String verified){

        User user = new User();
        user.setId(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setLat("");
        user.setLng("");
        user.setImageUrl(imgUrl);
        user.setNumber(number);
        user.setVerified(verified);

        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        dataBaseHelper.saveUser(user);

        Log.d("user", user.getVerified()?"Verified":"NOT Verified");

        UserPreferenceUtil.getInstance(context).setUserId(user.getId());
        UserPreferenceUtil.getInstance(context).setNumber(number);
        //UserPreferenceUtil.getInstance(context).setVerified(user.getVerified());

        mlistener.onSignUpSuccess();

    }

    private void saveFbUser(User user, String number, String imageUrl, String verified){

        user.setLat("");
        user.setLng("");
        user.setImageUrl(imageUrl);
        user.setNumber(number);
        user.setVerified(verified);

        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        dataBaseHelper.saveUser(user);

        Log.d("Fb user", user.getVerified()?"Verified":"NOT Verified");

        UserPreferenceUtil.getInstance(context).setUserId(user.getId());
        UserPreferenceUtil.getInstance(context).setNumber(number);
        //UserPreferenceUtil.getInstance(context).setVerified(user.getVerified());

        mlistener.onSignUpSuccess();

    }
}
