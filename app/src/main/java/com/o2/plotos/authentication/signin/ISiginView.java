package com.o2.plotos.authentication.signin;

/**
 * Created by Hassan on 1/28/2017.
 */

public interface ISiginView {
    void showProgress();
    void hideProgress();
    void onSignInSuccess();
    void onSignInFailed(String error);
    void onSignUpSuccess();
    void onSignUpFailed(String error);
}
