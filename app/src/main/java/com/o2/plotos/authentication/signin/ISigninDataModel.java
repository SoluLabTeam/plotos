package com.o2.plotos.authentication.signin;

/**
 * Created by Hassan on 1/28/2017.
 */

public interface ISigninDataModel {
    void sendRequest(String email ,String password);
    interface OnRequestFinishLisener{
       void onSignInSuccess();
        void onSignInFailed(String error);
    }
}
