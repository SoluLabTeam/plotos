package com.o2.plotos.authentication.phone;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.o2.plotos.R;
import com.o2.plotos.models.Country;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryActivtiy extends AppCompatActivity implements CountryAdapter.OnCountryClickListener {
    @BindView(R.id.activity_country_code_listview)
    RecyclerView mRecyclerView;
    public static final String COUNTRY_DATA ="country";

    private CountryAdapter mAdapter;
    private ArrayList<Country> countries = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_activtiy);
        ButterKnife.bind(this);
        parseJson();
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new CountryAdapter(this, countries, this);
        mRecyclerView.setAdapter(mAdapter);

    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("countries.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void parseJson(){
        String myJsonString = loadJSONFromAsset();
        try {
            JSONArray jsonArray = new JSONArray(myJsonString);

            for (int i = 0; i<jsonArray.length();i++){
                Country country = new Country();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (i<jsonArray.length()){
                    country.setName(jsonObject.getString("name"));
                    country.setCode(jsonObject.getString("code"));
                    country.setDialCode(jsonObject.getString("dial_code"));
                    countries.add(country);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCountryClick(String name, String code) {
        SharedPreferences.Editor editor = getSharedPreferences(COUNTRY_DATA, MODE_PRIVATE).edit();
        editor.putString("countryCode", code);
        editor.putString("countryName", name);
        editor.commit();
        finish();
        //overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
    }
}
