package com.o2.plotos.authentication.phone;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.cart.CartActivity;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.o2.plotos.authentication.phone.CountryActivtiy.COUNTRY_DATA;

public class PhoneVerificationActivity extends AppCompatActivity implements IVerfiyNumView {
    @BindView(R.id.activity_phone_verification_phonenumber_edittext)
    EditText phoneNumber;
    @BindView(R.id.activity_phone_verification_country_name)
    TextView countryName;
    @BindView(R.id.activity_phone_verification_code_textview)
    TextView countryCode;
    //@BindView(R.id.activity_phone_verification_account_text)
    //TextView verifyAccount;
    @BindView(R.id.activity_phone_verification_description)
    TextView verifyDescrition;
    @BindView(R.id.activity_phone_verification_sms_text)
    TextView receiveSmsText;

    CustomFonts customFonts;

    private ActionBar mActionbar;
    private ProgressDialog mProgressDialog;
    private IVerifyNumPresnter mPresneter;

    String name;
    String code;
    String userNmber;

    String firstName;
    String fbId;
    String lastName;
    String eMail;
    String fbImage;
    boolean fbLogin = false;

    private final int VERIFICATION=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);
        initializeView();
        settingCode();
        mPresneter = new VerifyNumPrsnterImpl(this);
        mActionbar = getSupportActionBar();
        mActionbar.setDisplayHomeAsUpEnabled(true);
    }


    @Override
    protected void onResume() {
        mPresneter.bindView(this);
        super.onResume();
        setCountryDetails();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if( phoneNumber!=null ) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(phoneNumber.getWindowToken(), 0);
        }
    }

    @Override
    protected void onDestroy() {
        mPresneter.unBindView();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }

    @Override
    public void onVerifyNumSuccess(String code) {
        mProgressDialog.dismiss();

        Intent codeIntent = new Intent(this, CodeActivity.class);
        codeIntent.putExtra("verifyCode", code);
        codeIntent.putExtra("userNumber", userNmber);

        /*if (fbLogin) {
            codeIntent.putExtra("fbId", fbId);
            codeIntent.putExtra("first_name", firstName);
            codeIntent.putExtra("last_name", lastName);
            codeIntent.putExtra("email", eMail);
            codeIntent.putExtra("fbImage", fbImage);
            codeIntent.putExtra("fbLogin", "true");
        } else{
            codeIntent.putExtra("fbLogin", "false");
        }*/

        startActivityForResult(codeIntent, VERIFICATION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case VERIFICATION:
                    if( data.getBooleanExtra("verified", false) ) {
                        //sendOrderRequest();
                        /*Intent data = new Intent();
                        data.putExtra("id", categoryId);
                        data.putExtra("name", categoryName);*/

                        if (getParent() == null) {
                            setResult(Activity.RESULT_OK, data);
                        } else {
                            getParent().setResult(Activity.RESULT_OK, data);
                        }

                        finish();
                    }
                    else {
                        Toast.makeText(PhoneVerificationActivity.this, "Please verify your account before placing your order.", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    @Override
    public void onVerifyNumFailed(String error) {
        mProgressDialog.dismiss();
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    public boolean validatePhoneNumber() {
        String number = phoneNumber.getText().toString();

        if (number.length() > 8 && number.matches("[0-9]{7,11}")) {
            return true;
        } else {
            Toast.makeText(this, number + " is not a valid number", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void initializeView() {
        ButterKnife.bind(this);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.msg_please_wait));
        mProgressDialog.setCancelable(false);
        customFonts = new CustomFonts(this);
        //customFonts.setOswaldBold(verifyAccount);
        customFonts.setOswldRegulr(countryName);
        customFonts.setOpenSansRegulr(verifyDescrition);
        customFonts.setOpenSansRegulr(receiveSmsText);

        if (getIntent().getExtras() != null) {
            fbLogin = true;
            firstName = getIntent().getStringExtra("first_name");
            fbId = getIntent().getStringExtra("fbId");
            lastName = getIntent().getStringExtra("last_name");
            eMail = getIntent().getStringExtra("email");
            fbImage = getIntent().getStringExtra("fbImage");
        }

    }


    public void setCountryDetails() {
        SharedPreferences prefs = getSharedPreferences(COUNTRY_DATA, MODE_PRIVATE);
        String restoredText = prefs.getString("countryName", null);
        if (restoredText != null) {
            name = prefs.getString("countryName", "Pakistans");
            code = prefs.getString("countryCode", "+92");
            countryCode.setText(code);
            countryName.setText(name);
        } else {
            settingCode();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("countries.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void settingCode() {
        String myJsonString = loadJSONFromAsset();
        try {
            JSONArray jsonArray = new JSONArray(myJsonString);
            String locationCode = UserPreferenceUtil.getInstance(this).getCountryCode();

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (i < jsonArray.length()) {
                    String code = jsonObject.getString("code");
                    if (code.equalsIgnoreCase(locationCode)) {
                        countryName.setText(jsonObject.getString("name"));
                        countryCode.setText(jsonObject.getString("dial_code"));
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String removeZero(String num) {
        for (int i = 0; i < num.length(); i++) {
            char first = num.charAt(0);
            if (first == '0') {
                num = num.substring(1);
            } else {
                i = num.length();
            }
        }
        return num;
    }

    @OnClick(R.id.activity_phone_verification_submit_btn)
    public void onEnterPhone() {

        String cntryCode = (countryCode.getText().toString()).replace(" ", "");
        String phnNum = phoneNumber.getText().toString();
        String numReal = removeZero(phnNum);
        //Toast.makeText(this, ""+numReal, Toast.LENGTH_SHORT).show();
        userNmber = cntryCode + numReal;
        if (validatePhoneNumber()) {
            if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
                mProgressDialog.show();
                mPresneter.sendVerifyNumRequest(userNmber);
            } else {
                Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.activity_phone_verification_country_name)
    public void onClickCountry() {
        startActivity(new Intent(this, CountryActivtiy.class));
    }
}
