package com.o2.plotos.authentication.signin;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.o2.plotos.LocationBaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.authentication.signup.ISignupPresenter;
import com.o2.plotos.authentication.signup.ISignupView;
import com.o2.plotos.authentication.signup.SignUpActivity;
import com.o2.plotos.authentication.signup.SignUpPresenterImpl;
import com.o2.plotos.cart.CartActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.models.User;
import com.o2.plotos.places.DeliveryLocationActivity;
import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.ForgotRequestInterface;
import com.o2.plotos.restapi.responses.ForgotResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.LogCat;
import com.o2.plotos.utils.UserPreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends LocationBaseActivity implements ISiginView, ISignupView {

    @BindView(R.id.activity_sign_in_editText_email)
    EditText editTextEmail;
    @BindView(R.id.activity_sign_in_editText_password)
    EditText editTextPassword;
    @BindView(R.id.activity_sign_in_textInputLayout_email)
    TextInputLayout textInputLayoutEmail;
    @BindView(R.id.activity_sign_in_textInputLayout_password)
    TextInputLayout textInputLayoutPassword;
    @BindView(R.id.activity_sign_in_slogan)
    TextView textViewSlogan;
    //    @BindView(R.id.activity_sign_in_or)
//    TextView textOr;
    @BindView(R.id.activity_sign_in_btn_create_account)
    Button createAccntBtn;
    @BindView(R.id.img_forgot_pass)
    TextView btnForgotPass;
    @BindView(R.id.txt_forgotpass)
    TextView txtForgotpass;
    @BindView(R.id.activity_sign_in_btn_sign_in)
    Button activitySignInBtnSignIn;

    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;

    CustomFonts customFonts;

    private String fbId;
    private String fbFirstName;
    private String fbLastName;
    private String fbEmail;
    private String fbImgUrl;

    private ProgressDialog mProgressDialog;
    private ISiginPresneter mPresneter;
    private ISignupPresenter mSignupPresenter;


    static Context context;


    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_in);

        ButterKnife.bind(this);
        initializeView();
        mPresneter = new SignInPresenterImpl(this);
        // forgotPassword = new F
        mSignupPresenter = new SignUpPresenterImpl(this);

        context = this;

        editTextEmail.setHintTextColor(getResources().getColor(R.color.white));
        editTextPassword.setHintTextColor(getResources().getColor(R.color.white));
        editTextEmail.setTextColor(getResources().getColor(R.color.white));
        editTextPassword.setTextColor(getResources().getColor(R.color.white));


        //LoginManager.getInstance().logOut();
//        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
//        loginButton.setReadPermissions(Arrays.asList("public_profile,email"));
//
//        //Generate facebook hash key/////////////////////////////
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.o2.plotos",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }
//        /////////////////////////////////////////////////
//
//
//        callbackManager = CallbackManager.Factory.create();
//
//        updateWithToken(AccessToken.getCurrentAccessToken());
//
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                mProgressDialog.show();
//                final String token = loginResult.getAccessToken().getToken();
//                GraphRequest request = GraphRequest.newMeRequest(
//                        loginResult.getAccessToken(),
//                        new GraphRequest.GraphJSONObjectCallback() {
//                            @Override
//                            public void onCompleted(
//                                    JSONObject object,
//                                    GraphResponse response) {
//                                try {
//                                    fbId = object.getString("id");
//                                    fbFirstName = object.getString("first_name");
//                                    fbLastName = object.getString("last_name");
//                                    fbEmail = object.getString("email");
//                                    fbImgUrl = "https://graph.facebook.com/" + fbId + "/picture?type=large";
//
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//
//                                /*
//                                 * verification should take place before critical actions, like placing an order
//                                 * /
//                                Intent verifyPhone = new Intent(getApplicationContext(), PhoneVerificationActivity.class);
//                                verifyPhone.putExtra("fbId", fbId);
//                                verifyPhone.putExtra("first_name", fbFirstName);
//                                verifyPhone.putExtra("last_name", fbLastName);
//                                verifyPhone.putExtra("email", fbEmail);
//                                verifyPhone.putExtra("fbImage", fbImgUrl);
//                                startActivity(verifyPhone);
//                                */
//
//                                if (InternetUtil.getInstance(SignInActivity.this).isNetWorkAvailable()) {
//                                    // sign up service should check if user exists, performing sign in if so
//                                    mSignupPresenter.sendSignupRequest(fbFirstName, fbLastName, fbEmail, "0", "0", fbId, fbImgUrl);
//                                } else {
//                                    Toast.makeText(SignInActivity.this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
//                                }
//                            }
//
//                        });
//                Bundle parameters = new Bundle();
//                parameters.putString("fields", "id,name,first_name,last_name,email");
//                request.setParameters(parameters);
//                request.executeAsync();
//
//
//                /**
//                 * AccessTokenTracker to manage logout
//                 */
//                accessTokenTracker = new AccessTokenTracker() {
//                    @Override
//                    protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
//                                                               AccessToken currentAccessToken) {
//                        updateWithToken(currentAccessToken);
//                    }
//                };
//            }
//
//            @Override
//            public void onCancel() {
//                //Toast.makeText(SignInActivity.this, "User has cancelled Facebook Connect!", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Log.v("FaceBook Exception", "" + error);
//
//            }
//        });
    }

    @Override
    protected void onResume() {
//        LoginManager.getInstance().logOut();
        mPresneter.bindView(this);

        mSignupPresenter.bindView(this);
        super.onResume();
        mProgressDialog.hide();
    }

    @Override
    protected void onDestroy() {
        mPresneter.unBindView();
        mSignupPresenter.unBindView();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog.hide();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSignInSuccess() {
        Toast.makeText(this, "Signed in successfully", Toast.LENGTH_SHORT).show();
        mProgressDialog.dismiss();

        String returnActivity = UserPreferenceUtil.getInstance(this).getReturnActivity();

        if (returnActivity.equals("CART")) {
            startActivity(new Intent(SignInActivity.this, CartActivity.class));
        } else if (returnActivity.equals("LOCATION")) {
            startActivity(new Intent(SignInActivity.this, DeliveryLocationActivity.class));
        } else {
            startActivity(new Intent(SignInActivity.this, HomeActivity.class));
        }

        UserPreferenceUtil.getInstance(this).setReturnActivity("");

        finish();
    }

    @Override
    public void onSignInFailed(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    private void initializeView() {
        ButterKnife.bind(this);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.msg_please_wait));
        mProgressDialog.setCancelable(false);
        customFonts = new CustomFonts(this);
        //customFonts.setOswldRegulr(textViewSlogan);
        customFonts.setOpenSansBold(textViewSlogan);
        customFonts.setOpenSansRegulr(createAccntBtn);
        customFonts.setOpenSansBold(txtForgotpass);
    }

    private boolean validateEmail() {
        String eMail = editTextEmail.getText().toString().trim();

        if (eMail.isEmpty() || !isValidEmail(eMail)) {
            textInputLayoutEmail.setError(getString(R.string.error_empty_email));
            return false;
        } else {
            textInputLayoutEmail.setErrorEnabled(false);
            requestFocus(editTextPassword);
        }
        return true;
    }

    private boolean validatePassword() {
        String pswrd = editTextPassword.getText().toString().trim();
        if (pswrd.isEmpty()) {
            textInputLayoutPassword.setError(getString(R.string.error_empty_password_signin));
            return false;
        } else {
            textInputLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public boolean validateFields() {
        if (!validateEmail() || !validatePassword()) {
            validateEmail();
            validatePassword();
            return false;
        }
        return true;
    }

    @OnClick(R.id.activity_sign_in_btn_sign_in)
    public void onClick() {
        if (validateFields()) {
            if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
                mPresneter.sendLoginRequest(editTextEmail.getText().toString(), editTextPassword.getText().toString());
            } else {
                Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.activity_sign_in_btn_create_account)
    public void openCreateAccount() {
        //Intent signUpIntent = new Intent(SignInActivity.this, PhoneVerificationActivity.class);
        Intent signUpIntent = new Intent(SignInActivity.this, SignUpActivity.class);
        startActivity(signUpIntent);
    }

    private void updateWithToken(AccessToken currentAccessToken) {

        if (currentAccessToken != null) {

        } else {

        }
    }

    /*
     * Facebook sign in / sign up request result
     */
    @Override
    public void onSignUpSuccess() {
        //Toast.makeText(this, R.string.msg_sign_up_successfully, Toast.LENGTH_SHORT).show();
        mProgressDialog.dismiss();


        User user = getUserDetail();
        if (user != null) {
            if (user.getEmail() != null && !user.getFirstName().equals("") && !user.getNumber().equals("")
                    && !user.getEmail().equals("") && !user.getNumber().equals("0")) {

                Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
                homeIntent.setFlags(homeIntent.FLAG_ACTIVITY_CLEAR_TOP | homeIntent.FLAG_ACTIVITY_CLEAR_TASK);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(homeIntent);
            } else {
                Intent signUpIntent = new Intent(getApplicationContext(), SignUpActivity.class);
                signUpIntent.setFlags(signUpIntent.FLAG_ACTIVITY_CLEAR_TOP | signUpIntent.FLAG_ACTIVITY_CLEAR_TASK);
                signUpIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                signUpIntent.putExtra("completeData", true);
                startActivity(signUpIntent);
            }
        }
    }

    private User getUserDetail() {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        User user = null;
        try {
            user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public void onSignUpFailed(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }


    public void dialogForgot()

    {
        final Dialog dialog2 = new Dialog(context);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setCancelable(true);
        dialog2.setContentView(R.layout.dialog_forgotpassword);
        dialog2.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_top_bootom;
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView txt_join = (TextView) dialog2.findViewById(R.id.txt_join);
        TextView txt_cancel = (TextView) dialog2.findViewById(R.id.txt_cancel);
        final EditText edit_code = (EditText) dialog2.findViewById(R.id.edit_code);


        // TextView txt_dailog = (TextView) dialog2.findViewById(R.id.txt_dailog);

        final String email_pattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        txt_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!edit_code.getText().toString().matches(email_pattern)) {
                    Toast.makeText(context, "Enter Valid Email", Toast.LENGTH_SHORT).show();
                } else {
                    sendEmail(edit_code.getText().toString());
                }

                dialog2.dismiss();


            }
        });


        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog2.dismiss();
            }
        });

        dialog2.show();

    }

    public static void sendEmail(String email) {
        ForgotRequestInterface forgotRequest = ServiceGenrator.createService(ForgotRequestInterface.class);
        Call<ForgotResponse> forgotcall = forgotRequest.ForgotRequest(email);
        forgotcall.enqueue(new Callback<ForgotResponse>() {
            @Override
            public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {
                if (response.code() == 200) {
                    ForgotResponse apiResponse = response.body();
                    Toast.makeText(context, apiResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ForgotResponse> call, Throwable t) {
                LogCat.LogDebug(ConstantUtil.TAG, "Banner ap onFailure " + t.getMessage());
            }
        });
    }

    @OnClick(R.id.txt_forgotpass)
    public void onViewClicked() {

        dialogForgot();
    }
}
