package com.o2.plotos.authentication.phone;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.authentication.signup.ISignupPresenter;
import com.o2.plotos.authentication.signup.ISignupView;
import com.o2.plotos.authentication.signup.SignUpActivity;
import com.o2.plotos.authentication.signup.SignUpPresenterImpl;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CodeActivity extends AppCompatActivity implements IVerfiyNumView/*, ISignupView*/ {

    @BindView(R.id.activity_code_edittext_code)
    TextView userCode;
    /*@BindView(R.id.activity_code_account_text)
    TextView verifyAccount;*/
    @BindView(R.id.activity_code_enter_code_text)
    TextView enterCodeText;
    @BindView(R.id.activity_code_notreceived_code_text)
    TextView notReceivedCode;
    @BindView(R.id.activity_code_resend_textview)
    TextView reSend;

    CustomFonts customFonts;

    String orignalCode;
    String number;
    private ActionBar mActionbar;

    private IVerifyNumPresnter mPresneter;
    //private ISignupPresenter mSignUpPresenter;
    private ProgressDialog mProgressDialog;

    String firstName;
    String fbId;
    String lastName;
    String eMail;
    String fbImage;
    boolean fbLogin = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code);
        initializeView();
        mPresneter = new VerifyNumPrsnterImpl(this);
        //mSignUpPresenter = new SignUpPresenterImpl(this);
        mActionbar = getSupportActionBar();
        mActionbar.setDisplayHomeAsUpEnabled(true);

        //Toast.makeText(this, "orignalCode activity " + orignalCode, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        mPresneter.bindView(this);
        //mSignUpPresenter.bindView(this);
        super.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if( userCode!=null ) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(userCode.getWindowToken(), 0);
        }
    }

    @Override
    protected void onDestroy() {
        mPresneter.unBindView();
        //mSignUpPresenter.unBindView();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog.hide();
    }

    /*@Override
    public void onSignUpSuccess() {
        mProgressDialog.dismiss();
        Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
        homeIntent.setFlags(homeIntent.FLAG_ACTIVITY_CLEAR_TOP| homeIntent.FLAG_ACTIVITY_CLEAR_TASK);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
        finish();
    }

    @Override
    public void onSignUpFailed(String error) {

    }*/

    @Override
    public void onVerifyNumSuccess(String code) {
        mProgressDialog.dismiss();
        orignalCode = code;
    }

    @Override
    public void onVerifyNumFailed(String error) {
        mProgressDialog.dismiss();
    }

    private void initializeView() {
        ButterKnife.bind(this);
        customFonts = new CustomFonts(this);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.msg_please_wait));
        mProgressDialog.setCancelable(false);

        //customFonts.setOswaldBold(verifyAccount);
        customFonts.setOpenSansSemiBold(reSend);
        customFonts.setOpenSansRegulr(enterCodeText);
        customFonts.setOpenSansRegulr(notReceivedCode);

        if (getIntent().getExtras() != null) {
            orignalCode = getIntent().getStringExtra("verifyCode");
            number = getIntent().getStringExtra("userNumber");
            /*if (getIntent().getStringExtra("fbLogin").equalsIgnoreCase("true")) {
                firstName = getIntent().getStringExtra("first_name");
                fbId = getIntent().getStringExtra("fbId");
                lastName = getIntent().getStringExtra("last_name");
                eMail = getIntent().getStringExtra("email");
                fbImage = getIntent().getStringExtra("fbImage");
                fbLogin = true;
            }*/
        }

    }

    public boolean matchcode(String apiCode) {
        mProgressDialog.show();
        if (userCode.getText().length() > 0) {
            if ((userCode.getText().toString()).equalsIgnoreCase(apiCode)) {
                return true;
            }
            return false;
        }
        return false;
    }

    @OnClick(R.id.activity_code_submit_btn)
    public void onClickSubmit() {
        if (matchcode(orignalCode)) {
            mProgressDialog.dismiss();
            /*if (fbLogin) {
                if (InternetUtil.getInstance(CodeActivity.this).isNetWorkAvailable()) {
                    mSignUpPresenter.sendSignupRequest(firstName, lastName, eMail, "0", number,fbId, fbImage);
                } else {
                    Toast.makeText(CodeActivity.this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                }
            } else {
                Intent signUpIntent = new Intent(getApplicationContext(), SignUpActivity.class);
                signUpIntent.putExtra("userNumber", number);
                startActivity(signUpIntent);
                finish();
            }*/

            // update "verified" on shared preferences
            UserPreferenceUtil.getInstance(getApplicationContext()).setVerified(true);

            // update "verified" on shared preferences
            if (InternetUtil.getInstance(this).isNetWorkAvailable()) {

                Log.d("User ID", UserPreferenceUtil.getInstance(getApplicationContext()).UserId());
                mPresneter.sendVerifiedRequest(UserPreferenceUtil.getInstance(getApplicationContext()).UserId());
            } else {
                Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
            }


            Intent data = new Intent();
            data.putExtra("verified", true);

            if (getParent() == null) {
                setResult(Activity.RESULT_OK, data);
            } else {
                getParent().setResult(Activity.RESULT_OK, data);
            }

            finish();

        } else {
            Toast.makeText(this, "Please enter a valid code!", Toast.LENGTH_SHORT).show();
        }
        mProgressDialog.dismiss();
    }

    @OnClick(R.id.activity_code_resend_textview)
    public void onClickResend() {
        if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
            mPresneter.sendVerifyNumRequest(number);
        } else {
            Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }
}
