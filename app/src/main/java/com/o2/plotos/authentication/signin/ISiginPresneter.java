package com.o2.plotos.authentication.signin;

/**
 * Created by Hassan on 1/28/2017.
 */

public interface ISiginPresneter {
    void sendLoginRequest(String email ,String password);
    void bindView(Object view);
    void unBindView();
}
