package com.o2.plotos.authentication.phone;

/**
 * Created by Hassan on 28/02/2017.
 */

public interface IVerifyNumPresnter {
    void sendVerifyNumRequest(String number);
    void sendVerifiedRequest(String user_id);
    void bindView(Object view);
    void unBindView();
}
