package com.o2.plotos.authentication.signin;

import android.content.Context;

/**
 * Created by Hassan on 1/28/2017.
 */

public class SignInPresenterImpl implements ISiginPresneter,ISigninDataModel.OnRequestFinishLisener {

    private ISiginView iSiginView;
    private SignInDataModelIml mSignInDataModelIml;

    public SignInPresenterImpl(Context context){
        mSignInDataModelIml = new SignInDataModelIml(context ,this);
    }

    @Override
    public void sendLoginRequest(String email, String password) {
        if(iSiginView!=null){
            iSiginView.showProgress();
            mSignInDataModelIml.sendRequest(email,password);
        }
    }

    @Override
    public void bindView(Object view) {
        iSiginView = (ISiginView) view;
    }

    @Override
    public void unBindView() {
        iSiginView = null;
    }

    @Override
    public void onSignInSuccess() {
        if(iSiginView!=null){
            iSiginView.hideProgress();
            iSiginView.onSignInSuccess();
        }
    }

    @Override
    public void onSignInFailed(String error) {
        if(iSiginView!=null){
            iSiginView.hideProgress();
            iSiginView.onSignInFailed(error);
        }
    }
}
