package com.o2.plotos.authentication.signin;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.OpensanBtn;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 3/21/2018.
 */

public class ForgotPasswordActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.edit_email)
    EditText editEmail;
    @BindView(R.id.txtinput_email)
    TextInputLayout txtinputEmail;
    @BindView(R.id.btn_submit)
    OpensanBtn btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.img_back, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_submit:
                if (isValidate()) {
                    if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
                        callApi();
                    } else {
                        Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private void callApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BaseApiModel> call = apiInterface.doForgotPass(editEmail.getText().toString().trim());
        call.enqueue(new Callback<BaseApiModel>() {
            @Override
            public void onResponse(Call<BaseApiModel> call, Response<BaseApiModel> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    showToast(response.body().getMsg());
                    finish();
                } else {
                    showToast(response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseApiModel> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }

    private boolean isValidate() {
        List<EditText> list = new ArrayList<>();
        list.add(editEmail);
        if (isAllFieldFill(list)) {
            if (isValidemail(editEmail.getText().toString().trim())) {
                return true;
            }
        }
        return false;
    }


}
