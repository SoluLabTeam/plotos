package com.o2.plotos.authentication.signup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.o2.plotos.R;
import com.o2.plotos.authentication.signin.SignInActivity;
import com.o2.plotos.cart.CartActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.models.User;
import com.o2.plotos.places.DeliveryLocationActivity;
import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.EdieProfileRequestObj;
import com.o2.plotos.restapi.endpoints.EditProfileRequest;
import com.o2.plotos.restapi.responses.EditProfileResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements ISignupView {

    @BindView(R.id.activity_sign_up_editText_firstname)
    EditText firstName;
    @BindView(R.id.activity_sign_up_editText_lastname)
    EditText lastName;
    @BindView(R.id.activity_sign_up_editText_email)
    EditText email;
    @BindView(R.id.activity_sign_up_editText_password)
    EditText password;
    @BindView(R.id.activity_sign_up_editText_re_enter_password)
    EditText reEnterPassword;
    @BindView(R.id.activity_sign_up_textInputLayout_firstname)
    TextInputLayout textInputLayoutFirstName;
    @BindView(R.id.activity_sign_up_textInputLayout_lastname)
    TextInputLayout textInputLayoutLastName;
    @BindView(R.id.activity_sign_up_textInputLayout_email)
    TextInputLayout textInputLayoutEmail;
    @BindView(R.id.activity_sign_up_textInputLayout_password)
    TextInputLayout textInputLayoutPassword;
    @BindView(R.id.activity_sign_up_textInputLayout_re_enter_password)
    TextInputLayout textInputLayoutRePassword;
    @BindView(R.id.activity_sign_up_editText_phone)
    EditText phoneNumber;
    @BindView(R.id.activity_sign_up_textInputLayout_phone)
    TextInputLayout textInputLayout_phone;
    @BindView(R.id.activity_sign_up_button_create_account)
    Button mCreateButton;

    String fbId = "0";
//    String number ;
    boolean isCompleteData = false;

    private ProgressDialog mProgressDialog;
    private ISignupPresenter mSignupPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initializeView();
        mSignupPresenter = new SignUpPresenterImpl(this);
    }

    @Override
    protected void onResume() {
        mSignupPresenter.bindView(this);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        mSignupPresenter.unBindView();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog.hide();
    }

    @Override
    public void onSignUpSuccess() {
        Toast.makeText(this, R.string.msg_sign_up_successfully, Toast.LENGTH_SHORT).show();
        mProgressDialog.dismiss();

        Intent intent;
        String returnActivity = UserPreferenceUtil.getInstance(this).getReturnActivity();

        if( returnActivity.equals("CART") ) {
            intent = new Intent(SignUpActivity.this, CartActivity.class);
        }
        else if( returnActivity.equals("LOCATION") ) {
            intent =  new Intent(SignUpActivity.this, DeliveryLocationActivity.class);
        }
        else {
            intent = new Intent(SignUpActivity.this, HomeActivity.class);
        }

        UserPreferenceUtil.getInstance(this).setReturnActivity("");

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
    }

    @Override
    public void onSignUpFailed(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    private boolean validateFirstName() {
        if (firstName.getText().toString().trim().isEmpty()) {
            textInputLayoutFirstName.setError(getString(R.string.error_first_name));
            return false;
        } else {
            textInputLayoutFirstName.setErrorEnabled(false);
            requestFocus(lastName);
        }
        return true;
    }

    private boolean validateLastName() {
        if (lastName.getText().toString().trim().isEmpty()) {
            textInputLayoutLastName.setError(getString(R.string.error_last_name));
            return false;
        } else {
            textInputLayoutLastName.setErrorEnabled(false);
            requestFocus(email);
        }
        return true;
    }

    private boolean validateEmail() {
        String eMail = email.getText().toString().trim();

        if (eMail.isEmpty() || !isValidEmail(eMail)) {
            textInputLayoutEmail.setError(getString(R.string.error_empty_email));
            return false;
        } else {
            textInputLayoutEmail.setErrorEnabled(false);
            requestFocus(password);
        }
        return true;
    }

    private boolean validatePassword() {
        String pswrd = password.getText().toString().trim();
        String pswrdSpace = password.getText().toString();
        if (pswrdSpace.contains(" ")) {
            textInputLayoutPassword.setError(getString(R.string.error_space_password));
            return false;
        } else if (pswrd.isEmpty() || pswrd.length() < 6) {
            textInputLayoutPassword.setError(getString(R.string.error_short_password));
            return false;
        } else {
            textInputLayoutPassword.setErrorEnabled(false);
            requestFocus(reEnterPassword);
        }

        return true;
    }

    private boolean validateRePassword() {
        String pswrd = password.getText().toString().trim();
        String rePswrd = reEnterPassword.getText().toString().trim();
        if (rePswrd.isEmpty()) {
            textInputLayoutRePassword.setError(getString(R.string.error_empty_repassword));
            return false;
        } else if (pswrd.equals(rePswrd)) {
            textInputLayoutRePassword.setErrorEnabled(false);
        } else {
            textInputLayoutRePassword.setError(getString(R.string.error_password_match));
            return false;
        }

        return true;
    }

    private boolean validatePhone() {
        if (phoneNumber.getText().toString().trim().isEmpty()) {
            textInputLayout_phone.setError(getString(R.string.error_phone));
            return false;
        } else {
            textInputLayout_phone.setErrorEnabled(false);
            requestFocus(phoneNumber);
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public boolean validateFields() {
        if(isCompleteData){
            if (!validateFirstName() || !validateLastName() || !validateEmail() || !validatePhone()) {
                validateFirstName();
                validateLastName();
                validateEmail();
                validatePhone();
                return false;
            }
        }else{
            if (!validateFirstName() || !validateLastName() || !validateEmail() || !validatePassword() ||
                    !validateRePassword() || !validatePhone()) {
                validateFirstName();
                validateLastName();
                validateEmail();
                validatePassword();
                validateRePassword();
                validatePhone();
                return false;
            }
        }


        return true;
    }

    private void initializeView() {
        ButterKnife.bind(this);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.msg_please_wait));
        mProgressDialog.setCancelable(false);

        if (getIntent().getExtras() != null) {
//            number = getIntent().getStringExtra("userNumber");
            isCompleteData =  getIntent().getBooleanExtra("completeData", false);

            // Complete Data from facebook signup
            if(isCompleteData){
                password.setVisibility(View.GONE);
                reEnterPassword.setVisibility(View.GONE);
                mCreateButton.setText("Save");

                User user = getUserDetail();
                if(user != null){
                    firstName.setText(user.getFirstName());
                    lastName.setText(user.getLastName());
                    email.setText(user.getEmail());
                    phoneNumber.setText(user.getNumber());
                }
            }
        }

    }

    private User getUserDetail() {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        User user = null;
        try {
            user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();

        }catch (Exception e){
            e.printStackTrace();
        }
        return user;
    }


    @OnClick(R.id.activity_sign_up_button_create_account)
    public void performSignUp() {
        if (validateFields()) {
            if (InternetUtil.getInstance(this).isNetWorkAvailable()) {

                if(isCompleteData){
                    editProfile();
                }
                else {
                    mSignupPresenter.sendSignupRequest(firstName.getText().toString(), lastName.getText()
                            .toString(), email.getText().toString(), password.getText().toString(), phoneNumber.getText().toString(), fbId, "");
                }
            } else {
                Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void editProfile() {
        mProgressDialog.show();
        final User user = getUserDetail();
        EdieProfileRequestObj profileRequestObj = new EdieProfileRequestObj();
        profileRequestObj.setUser_id(user.getId());
        profileRequestObj.setFirst_name(firstName.getText().toString().trim());
        profileRequestObj.setLast_name(lastName.getText().toString().trim());
        profileRequestObj.setEmail(email.getText().toString().trim());
        profileRequestObj.setNumber(phoneNumber.getText().toString().trim());
        profileRequestObj.setPlatform("0");

        Gson gson = new Gson();
        String jsonObject = gson.toJson(profileRequestObj);

        EditProfileRequest editProfileRequest = ServiceGenrator.createService(EditProfileRequest.class);
        Call<EditProfileResponse> responseCall = editProfileRequest.editProfile(jsonObject);
        responseCall.enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                mProgressDialog.hide();
                EditProfileResponse editProfileResponse = response.body();
                String code = editProfileResponse.response;

                if (code.equalsIgnoreCase("0")) {
                    Toast.makeText(SignUpActivity.this, "Your Profile has been updated", Toast.LENGTH_SHORT).show();

                    user.setFirstName(firstName.getText().toString().trim());
                    user.setLastName(lastName.getText().toString().trim());
                    user.setEmail(email.getText().toString().trim());
                    user.setNumber(phoneNumber.getText().toString().trim());

                    DataBaseHelper dataBaseHelper = new DataBaseHelper(SignUpActivity.this);
                    dataBaseHelper.editUser(user);

                    UserPreferenceUtil.getInstance(SignUpActivity.this).setNumber(phoneNumber.getText().toString().trim());

                    Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
                    homeIntent.setFlags(homeIntent.FLAG_ACTIVITY_CLEAR_TOP | homeIntent.FLAG_ACTIVITY_CLEAR_TASK);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(homeIntent);

                } else if (code.equalsIgnoreCase("1")) {
                    Toast.makeText(SignUpActivity.this, "Some error while updating profile", Toast.LENGTH_SHORT).show();
                } else if (code.equalsIgnoreCase("2")) {
                    Toast.makeText(SignUpActivity.this, "Some error while updating profile", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                Log.v(ConstantUtil.TAG, "Update Profile onFailer " + t.getMessage());
                mProgressDialog.hide();
            }
        });

    }

    @OnEditorAction(R.id.activity_sign_up_editText_firstname)
    public boolean onEnterFirstName() {
        validateFirstName();
        return true;
    }

    @OnEditorAction(R.id.activity_sign_up_editText_lastname)
    public boolean onEnterLastName() {
        validateLastName();
        return true;
    }

    @OnEditorAction(R.id.activity_sign_up_editText_email)
    public boolean onEnterEmail() {
        validateEmail();
        return true;
    }

    @OnEditorAction(R.id.activity_sign_up_editText_password)
    public boolean onEnterPassword() {
        validatePassword();
        return true;
    }

    @OnEditorAction(R.id.activity_sign_up_editText_re_enter_password)
    public boolean onReEnterPassword() {
        if( validateRePassword() ) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(reEnterPassword.getWindowToken(), 0);
        }
        return true;
    }

    @OnClick(R.id.activity_sign_up_back_arrow)
    public void goToSignIn(){
        startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
        finish();
    }

}
