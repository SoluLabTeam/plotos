package com.o2.plotos.authentication.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.authentication.signin.SignInActivityNew;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;
import com.o2.plotos.restapi.newApis.models.register.RegisterResponse;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 3/21/2018.
 */


public class CreateAnAccountActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.edit_email)
    EditText editEmail;
    @BindView(R.id.txtinput_email)
    TextInputLayout txtinputEmail;
    @BindView(R.id.edit_mobile)
    EditText editMobile;
    @BindView(R.id.txtinput_mobile)
    TextInputLayout txtinputMobile;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.txtinput_password)
    TextInputLayout txtinputPassword;
    @BindView(R.id.btn_signin)
    Button btnSignin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createaccount);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.img_back, R.id.btn_signin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_signin:
                if (isValidate()) {
                    if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
                        callApi();
                    } else {
                        Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                }
                /*Intent i = new Intent(CreateAnAccountActivity.this, MobileVerficationActivity.class);
                startActivity(i);*/
                break;
        }
    }

    private boolean isValidate() {
        List<EditText> list = new ArrayList<>();
        list.add(editEmail);
        list.add(editMobile);
        list.add(editPassword);
        if (isAllFieldFill(list)) {
            if (isValidemail(editEmail.getText().toString().trim())) {
                return true;
            }
        }
        return false;
    }

    private void callApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Map<String, String> map = new HashMap<>();
        map.put("email", getDataFromEditText(editEmail));
        map.put("mobile", getDataFromEditText(editMobile));
        map.put("password", getDataFromEditText(editPassword));

        Call<RegisterResponse> call = apiInterface.doRegister(map);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    UserPreferenceUtil.getInstance(CreateAnAccountActivity.this).setUserId(String.valueOf(response.body().getRegisterData().getUser_id()));
                    Intent intent = new Intent(CreateAnAccountActivity.this, MobileVerficationActivity.class);
                    intent.putExtra(Constant.MOBILE, getDataFromEditText(editMobile));
                    startActivity(intent);

                } else {
                    showToast(response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }


}
