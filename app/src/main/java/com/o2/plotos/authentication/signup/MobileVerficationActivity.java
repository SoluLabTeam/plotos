package com.o2.plotos.authentication.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.authentication.phone.ChgMobActivity;
import com.o2.plotos.authentication.signin.SignInActivityNew;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;
import com.o2.plotos.restapi.newApis.models.login.LoginResponce;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 3/23/2018.
 */

public class MobileVerficationActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_resend)
    TextView txtResend;
    @BindView(R.id.txt_changemobile)
    TextView txtChangemobile;
    @BindView(R.id.btn_done)
    Button btnDone;
    @BindView(R.id.edt_first)
    EditText edtFirst;
    @BindView(R.id.edt_second)
    EditText edtSecond;
    @BindView(R.id.edt_third)
    EditText edtThird;
    @BindView(R.id.edt_fourth)
    EditText edtFourth;
    private String mobile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);
        ButterKnife.bind(this);
        if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
            if (getIntent() != null && getIntent().getExtras() != null) {
                mobile = getIntent().getStringExtra(Constant.MOBILE);
                callGetOtpApi(mobile);
            }

        } else {
            Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
        }

        edtFirst.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() == 1) {
                    edtSecond.requestFocus();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        edtSecond.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    edtThird.requestFocus();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
        });
        edtThird.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    edtFourth.requestFocus();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
        });
    }

    @OnClick({R.id.img_back, R.id.txt_resend, R.id.txt_changemobile, R.id.btn_done})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.txt_resend:
                edtFirst.setText("");
                edtSecond.setText("");
                edtThird.setText("");
                edtFourth.setText("");
                edtFirst.requestFocus();
                callGetOtpApi(mobile);
                break;
            case R.id.txt_changemobile:
                Intent intent = new Intent(this, ChgMobActivity.class);
                intent.putExtra(Constant.MOBILE, mobile);
                startActivity(intent);
                break;
            case R.id.btn_done:
                if (isValidated()) {
                    if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
                        String otp = getDataFromEditText(edtFirst) + getDataFromEditText(edtSecond) + getDataFromEditText(edtThird)
                                + getDataFromEditText(edtFourth);
                        callApi(otp);
                    } else {
                        Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private boolean isValidated() {
        List<EditText> list = new ArrayList<>();
        list.add(edtFirst);
        list.add(edtSecond);
        list.add(edtThird);
        list.add(edtFourth);
        if (isAllFieldFill(list)) {
            return true;
        }
        return false;
    }

    private void callApi(String otp) {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponce> call = apiInterface.verifyOTP(otp, UserPreferenceUtil.getInstance(this).UserId());
        call.enqueue(new Callback<LoginResponce>() {
            @Override
            public void onResponse(Call<LoginResponce> call, Response<LoginResponce> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    UserPreferenceUtil.getInstance(MobileVerficationActivity.this).storeUserData(response.body().getUserData());
                    Intent i = new Intent(MobileVerficationActivity.this, CreateProfileActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    showToast(response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<LoginResponce> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }

    private void callGetOtpApi(String mobile) {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BaseApiModel> call = apiInterface.getOTP(mobile, UserPreferenceUtil.getInstance(this).UserId());
        call.enqueue(new Callback<BaseApiModel>() {
            @Override
            public void onResponse(Call<BaseApiModel> call, Response<BaseApiModel> response) {
                hideProgress();

            }

            @Override
            public void onFailure(Call<BaseApiModel> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }
}
