package com.o2.plotos.authentication.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2.plotos.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 3/23/2018.
 */

public class AddpaymentActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.edit_cardnumber)
    EditText editCardnumber;
    @BindView(R.id.txtinput_cardnumber)
    TextInputLayout txtinputCardnumber;
    @BindView(R.id.edit_month_year)
    EditText editMonthYear;
    @BindView(R.id.cvv)
    EditText cvv;
    @BindView(R.id.text_input_cvv)
    TextInputLayout textInputCvv;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.txt_skip)
    TextView txt_skip;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.img_back, R.id.btn_save,R.id.txt_skip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_save:
                Intent i = new Intent(this, DeliveryDetailsActivity.class);
                startActivity(i);
                break;
            case R.id.txt_skip:
                Intent i1 = new Intent(this, DeliveryDetailsActivity.class);
                startActivity(i1);
                break;
        }
    }
}
