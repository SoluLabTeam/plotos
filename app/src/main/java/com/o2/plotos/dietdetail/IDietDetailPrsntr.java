package com.o2.plotos.dietdetail;

/**
 * Created by Hassan on 9/02/2017.
 */

public interface IDietDetailPrsntr {
    void sendDietDishRqust(String type,String restaurant_id, String category_id, String calorie, String latitude, String longitude);
    void bindView(Object view);
    void unBindView();
}
