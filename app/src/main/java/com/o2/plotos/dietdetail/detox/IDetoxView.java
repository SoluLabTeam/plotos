package com.o2.plotos.dietdetail.detox;

import com.o2.plotos.models.Dish;

import java.util.List;

/**
 * Created by Hassan on 14/02/2017.
 */

public interface IDetoxView {
    void showProgress();
    void hideProgress();
    void onGetDetoxDishSuccess(List<Dish> dishList);
    void onGetDetoxDishFailed(String error);
}
