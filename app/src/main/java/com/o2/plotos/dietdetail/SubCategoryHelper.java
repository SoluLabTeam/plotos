package com.o2.plotos.dietdetail;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.GroceryByCategoryRequest;
import com.o2.plotos.restapi.endpoints.SubCategoriesRequest;
import com.o2.plotos.restapi.responses.GroceryResponse;
import com.o2.plotos.restapi.responses.SubCategoryResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rania on 5/7/2017.
 */

public class SubCategoryHelper {

    SubCategoryListener mListener;
    GroceryListener mGroceryListener;

    public SubCategoryHelper(SubCategoryListener subCategoryListener, GroceryListener groceryListener){
        mListener = subCategoryListener;
        mGroceryListener = groceryListener;
    }

    public void getSubCategory(String CatID, String supplierID){
        SubCategoriesRequest subCategoriesRequest = ServiceGenrator.createService(SubCategoriesRequest.class);
        Call<SubCategoryResponse> subCategoryResponseCall = subCategoriesRequest.getSubCategoriesRequest(CatID, supplierID);
        subCategoryResponseCall.enqueue(new Callback<SubCategoryResponse>() {
            @Override
            public void onResponse(Call<SubCategoryResponse> call, Response<SubCategoryResponse> response) {
                if(response.code()== 200) {
                    SubCategoryResponse apiResponse = response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        mListener.OnResponseSuccess(apiResponse.subCategoryList);
                    }
//                    else if(responseCode.equalsIgnoreCase("0")){
//                        mListener.onResponseFailure("Restaurant is not in your radius");
//                    }
                    else{
                        mListener.onResponseFailure(apiResponse.message);
                    }
                }
            }

            @Override
            public void onFailure(Call<SubCategoryResponse> call, Throwable t) {
                mListener.onResponseFailure("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"sub category ap onFailure "+t.getMessage());
            }
        });
    }

    public void getGroceryByCategory(final String catID, String supplierID, String latitude, String longitude){
        GroceryByCategoryRequest groceryByCategoryRequest = ServiceGenrator.createService(GroceryByCategoryRequest.class);
        Call<GroceryResponse> groceryResponseCall = groceryByCategoryRequest.getGroceryByCategoryRequest(catID, supplierID, latitude, longitude);
        groceryResponseCall.enqueue(new Callback<GroceryResponse>() {
            @Override
            public void onResponse(Call<GroceryResponse> call, Response<GroceryResponse> response) {
                if(response.code()== 200) {
                    GroceryResponse apiResponse = response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){

                        if(mGroceryListener!=null)
                        {
                            mGroceryListener.OnGroceryResponseSuccess(catID, apiResponse.groceryList);
                        }

                    }
//                    else if(responseCode.equalsIgnoreCase("0")){
//                        mListener.onResponseFailure("Restaurant is not in your radius");
//                    }
                    else{
                        mGroceryListener.OnGroceryResponseFailure(catID, apiResponse.message);
                    }
                }
            }

            @Override
            public void onFailure(Call<GroceryResponse> call, Throwable t) {
                mGroceryListener.OnGroceryResponseFailure(catID, "unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"Grocery ap onFailure "+t.getMessage());
            }
        });
    }
}
