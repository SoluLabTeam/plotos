package com.o2.plotos.dietdetail.detox;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.DetoxDishesRequest;
import com.o2.plotos.restapi.responses.DetoxDshesRequestResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hassan on 14/02/2017.
 */

public class DetoxDataModlImpl implements IDetoxDataModel {

    OnGetDetoxDishRqustFinishLisener mLisener;

    public DetoxDataModlImpl(OnGetDetoxDishRqustFinishLisener lisener){
        mLisener = lisener;
    }
    @Override
    public void getDetoxDishRqust(String restaurant_id, String latitude, String longitude) {
        DetoxDishesRequest detoxDishRqust = ServiceGenrator.createService(DetoxDishesRequest.class);
        Call<DetoxDshesRequestResponse> detoxDishRequestCall =
                detoxDishRqust.dishesRequest(restaurant_id, latitude, longitude);
        detoxDishRequestCall.enqueue(new Callback<DetoxDshesRequestResponse>() {
            @Override
            public void onResponse(Call<DetoxDshesRequestResponse> call, Response<DetoxDshesRequestResponse> response) {
                LogCat.LogDebug(ConstantUtil.TAG, " -> Detox dishes api response " + response.raw());
                if (response.code() == 200) {
                    DetoxDshesRequestResponse apiResponse = response.body();
                    String responseCode = apiResponse.response;
                    if (responseCode.equalsIgnoreCase("0")) {
                        mLisener.OnGetDetoxDishSuccess(apiResponse.dish);

                    } else if (responseCode.equalsIgnoreCase("1")) {
                        mLisener.OnGetDetoxDishFailed("Detox dishes request Failed(1)");
                    } else if (responseCode.equalsIgnoreCase("2")) {
                        mLisener.OnGetDetoxDishFailed("Detox dishes request Failed(2)");
                    }
                }
            }

            @Override
            public void onFailure(Call<DetoxDshesRequestResponse> call, Throwable t) {
                mLisener.OnGetDetoxDishFailed("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG, "Detox dishes ap onFailure " + t.getMessage());
            }
        });
    }
}
