package com.o2.plotos.dietdetail;

import com.o2.plotos.models.Dish;

import java.util.List;

/**
 * Created by Hassan on 9/02/2017.
 */

public class DietDetailPrsntrImpl implements IDietDetailPrsntr, IDietDetailDataModl.OnGetDietDishRqustFinishLisener {

    IDietDetailView iDietDetailView;
    IDietDetailDataModl mDietDetalDataModl;

    public DietDetailPrsntrImpl() {
        mDietDetalDataModl = new DietDetailDataModelImpl(this);
    }

    @Override
    public void OnGetDietDishSuccess(List<Dish> dishList) {
        if(iDietDetailView!=null){
            //iDietDetailView.hideProgress();
            iDietDetailView.onGetDietDishSuccess(dishList);
        }
    }

    @Override
    public void OnGetDietDishFailed(String error) {
        if(iDietDetailView!=null){
            //iDietDetailView.hideProgress();
            iDietDetailView.onGetDietDishFailed(error);
        }
    }

    @Override
    public void sendDietDishRqust(String type,String restaurant_id, String category_id, String calorie, String latitude, String longitude) {
        if (iDietDetailView != null) {
            //iDietDetailView.showProgress();
            mDietDetalDataModl.getDietDishRqust(type,restaurant_id, category_id, calorie, latitude, longitude);
        }

    }

    @Override
    public void bindView(Object view) {
        iDietDetailView = (IDietDetailView) view;
    }

    @Override
    public void unBindView() {
        iDietDetailView = null;

    }
}
