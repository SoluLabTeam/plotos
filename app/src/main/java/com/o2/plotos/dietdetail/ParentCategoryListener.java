package com.o2.plotos.dietdetail;

import com.o2.plotos.models.ParentCategory;

import java.util.List;

/**
 * Created by Rania on 5/7/2017.
 */

public interface ParentCategoryListener {

    void OnResponseSuccess(List<ParentCategory> parentCategoriesList);
    void onResponseFailure(String error);
}
