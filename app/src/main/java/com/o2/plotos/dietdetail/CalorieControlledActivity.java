package com.o2.plotos.dietdetail;

import android.support.v7.app.AppCompatActivity;

public class CalorieControlledActivity extends AppCompatActivity
        //implements IDietDetailView,
       // DietDetailAdapter.OnDishListener
        {

//    @BindView(R.id.calorie_controlled_viewpager)
//    ViewPager viewPager;
//    @BindView(R.id.calorie_controlled_tablayout)
//    TabLayout tabLayout;
//    @BindView(R.id.calorie_controlled_img_cross)
//    ImageButton imageCross;
//    @BindView(R.id.calorie_controlled_image_cover)
//    ImageView coverImage;
//    @BindView(R.id.calorie_controlled_txt_title)
//    TextView txtTitle;
//    @BindView(R.id.calorie_controlled_descrptn_text)
//    TextView descrptnTxt;
//    @BindView(R.id.calorie_controlled_category_name)
//    TextView textCategory;
//    @BindView(R.id.calorie_controlled_text)
//    TextView textSelectCategory;
//    @BindView(R.id.calorie_controlled_list_view)
//    ListView listCategory;
//    @BindView(R.id.calorie_controlled_drop_listcard)
//    CardView cardDropDown;
//    String categryName;
//    String category_Id;
//    @BindView(R.id.calorie_controlled_image_drop_down)
//    ImageView imgDropDown;
//    private ProgressDialog mProgressDialog;
//    CustomFonts customFonts;
//    private List<Category> categoryList;
//
//    private String REQUEST_TYPE = "get_dish_by_category";
//
//    public static final String EXTRA_DIET = "Category_Name";
//
//    // private List<ResturandResult> mDishList;
//    private IDietDetailPrsntr mDietDetailPrsntr;
//    private ArrayList<ResturandResult> mDishList = new ArrayList<>();
//
//
//    private ArrayList<ResturandResult> breakFastDishList = new ArrayList<>();
//    private ArrayList<ResturandResult> launcDishList = new ArrayList<>();
//    private ArrayList<ResturandResult> dinnerDishList = new ArrayList<>();
//    private ArrayList<ResturandResult> snackDishList = new ArrayList<>();
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_calorie_controlled);
//        //ButterKnife.bind(this);
//
//        mDietDetailPrsntr = new DietDetailPrsntrImpl();
//        mDietDetailPrsntr.bindView(this);
//        initializeView();
//        mDishList = new ArrayList<>();
//
//
//        categryName = getIntent().getExtras().getString("catName");
//        category_Id = getIntent().getExtras().getString("cat_Id");
//
//        textCategory.setText(categryName);
//
//        if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
//            mDietDetailPrsntr.sendDietDishRqust(REQUEST_TYPE,"0", category_Id, "0");
//        } else {
//            setupViewPager(viewPager);
//            tabLayout.setupWithViewPager(viewPager);
//        }
//        categoryList = new ArrayList<>();
//
//        Category cat1 = new Category();
//        cat1.setTitle("1200");
//        cat1.setCatgryDscrptn(getString(R.string.calorie_controlled_1200_description));
//        categoryList.add(cat1);
//        Category cat2 = new Category();
//        cat2.setTitle("1500");
//        cat2.setCatgryDscrptn(getString(R.string.calorie_controlled_1500_description));
//        categoryList.add(cat2);
//        Category cat3 = new Category();
//        cat3.setTitle("1800");
//        cat3.setCatgryDscrptn(getString(R.string.calorie_controlled_1800_description));
//        categoryList.add(cat3);
//        Category cat4 = new Category();
//        cat4.setTitle("2000");
//        cat4.setCatgryDscrptn(getString(R.string.calorie_controlled_2000_description));
//        categoryList.add(cat4);
//
//        textSelectCategory.setText(categoryList.get(0).getTitle() + " Cal");
//        txtTitle.setText(categoryList.get(0).getTitle() + " Cal diet program");
//        descrptnTxt.setText(categoryList.get(0).getCatgryDscrptn());
//        final SpinnerCustomdapter adapter = new SpinnerCustomdapter();
//        listCategory.setAdapter(adapter);
//        imgDropDown.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (cardDropDown.getVisibility() == View.GONE) {
//                    cardDropDown.setVisibility(View.VISIBLE);
//                    imgDropDown.setImageResource(R.drawable.ic_arrow_up);
//                } else {
//                    cardDropDown.setVisibility(View.GONE);
//                    imgDropDown.setImageResource(R.drawable.ic_arrow_down);
//                }
//
//            }
//        });
//        listCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                textSelectCategory.setText(categoryList.get(i).getTitle() + " Cal");
//                txtTitle.setText(categoryList.get(i).getTitle() + " Cal Diet Program");
//                descrptnTxt.setText(categoryList.get(i).getCatgryDscrptn());
//                imgDropDown.setImageResource(R.drawable.ic_arrow_down);
//                for (int j = 0; j < categoryList.size(); j++) {
//                    categoryList.get(j).setSelected(false);
//                }
//                categoryList.get(i).setSelected(true);
//                adapter.notifyDataSetChanged();
//                cardDropDown.setVisibility(View.GONE);
//                if (InternetUtil.getInstance(CalorieControlledActivity.this).isNetWorkAvailable()) {
//                    breakFastDishList.clear();
//                    dinnerDishList.clear();
//                    launcDishList.clear();
//                    snackDishList.clear();
//                    mDietDetailPrsntr.sendDietDishRqust(REQUEST_TYPE,"0", "0", categoryList.get(i).getTitle());
//                } else {
//                    setupViewPager(viewPager);
//                    tabLayout.setupWithViewPager(viewPager);
//                }
//            }
//        });
//    }
//
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        mDietDetailPrsntr.unBindView();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        mDietDetailPrsntr.bindView(this);
//    }
//
//    private void initializeView() {
//        ButterKnife.bind(this);
//        mProgressDialog = new ProgressDialog(this);
//        mProgressDialog.setMessage("Please wait");
//        coverImage.setImageResource(R.drawable.calorie_controlled);
//        customFonts = new CustomFonts(this);
//        customFonts.setOswaldBold(txtTitle);
//        customFonts.setOswaldBold(textCategory);
//        customFonts.setOpenSansSemiBold(descrptnTxt);
//        textCategory.setText(categryName);
//    }
//
//    private void setupViewPager(ViewPager viewPager) {
//
//        for (int i = 0; i < mDishList.size(); i++) {
//            if (mDishList.get(i).getBreakFast().equalsIgnoreCase("1")) {
//                breakFastDishList.add(mDishList.get(i));
//            }
//            if (mDishList.get(i).getDinner().equalsIgnoreCase("1")) {
//                dinnerDishList.add(mDishList.get(i));
//            }
//            if (mDishList.get(i).getLaunch().equalsIgnoreCase("1")) {
//                launcDishList.add(mDishList.get(i));
//            }
//            if (mDishList.get(i).getSnacks().equalsIgnoreCase("1")) {
//                snackDishList.add(mDishList.get(i));
//            }
//        }
//
//        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        adapter.addFrag(DishFragment.newInstance(breakFastDishList), "BreakFast");
//        adapter.addFrag(DishFragment.newInstance(launcDishList), "Lunch");
//        adapter.addFrag(DishFragment.newInstance(dinnerDishList), "Dinner");
//        adapter.addFrag(DishFragment.newInstance(snackDishList), "Snacks");
//        viewPager.setAdapter(adapter);
//        viewPager.setOffscreenPageLimit(4);
//    }
//
//    @OnClick(R.id.calorie_controlled_img_cross)
//    public void closeScreen() {
//        finish();
//        //overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
//    }
//
//    /*@Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
//    }
//
//    @Override
//    public void showProgress() {
//        mProgressDialog.show();
//    }
//
//    @Override
//    public void hideProgress() {
//        mProgressDialog.hide();
//    }*/
//
//
//    @Override
//    public void onGetDietDishSuccess(List<ResturandResult> dishList) {
//
//        mDishList = (ArrayList<ResturandResult>) dishList;
//        setupViewPager(viewPager);
//        tabLayout.setupWithViewPager(viewPager);
//
//
//    }
//
//    @Override
//    public void onGetDietDishFailed(String error) {
//        mDishList = new ArrayList<>();
//        setupViewPager(viewPager);
//        tabLayout.setupWithViewPager(viewPager);
//        //Toast.makeText(this, "test "+error, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onDishClick(ResturandResult dish) {
//        Intent intent = new Intent(CalorieControlledActivity.this, DishDetailActivity.class);
//        intent.putExtra(DishDetailActivity.EXTRA_DISH, dish);
//        startActivity(intent);
//        //CalorieControlledActivity.this.overridePendingTransition(R.anim.slide_up, android.R.anim.fade_out);
//    }
//
//    class Category {
//        private String title;
//        private boolean isSelected;
//        private String catgryDscrptn;
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public boolean isSelected() {
//            return isSelected;
//        }
//
//        public void setSelected(boolean selected) {
//            isSelected = selected;
//        }
//
//        public String getCatgryDscrptn() {
//            return catgryDscrptn;
//        }
//
//        public void setCatgryDscrptn(String catgryDscrptn) {
//            this.catgryDscrptn = catgryDscrptn;
//        }
//    }
//
//
//    class SpinnerCustomdapter extends BaseAdapter {
//        LayoutInflater layoutInfalter;
//
//        public SpinnerCustomdapter() {
//            layoutInfalter = LayoutInflater.from(CalorieControlledActivity.this);
//        }
//
//        @Override
//        public int getCount() {
//            return categoryList.size();
//        }
//
//        @Override
//        public Object getItem(int i) {
//            return i;
//        }
//
//        @Override
//        public long getItemId(int i) {
//            return i;
//        }
//
//        @Override
//        public View getView(final int i, View view, ViewGroup viewGroup) {
//            ViewHolder v;
//            if (view == null) {
//                view = layoutInfalter.inflate(R.layout.item_spinner_cal, viewGroup, false);
//                v = new ViewHolder();
//                v.title = (TextView) view.findViewById(R.id.item_cal_text_spinner);
//                v.imageCheck = (ImageView) view.findViewById(R.id.item_cal_check_spinner);
//
//                view.setTag(v);
//            } else {
//                v = (ViewHolder) view.getTag();
//            }
//            v.title.setText(categoryList.get(i).getTitle() + " Cal");
//            if (categoryList.get(i).isSelected()) {
//                v.imageCheck.setVisibility(View.VISIBLE);
//            } else {
//                v.imageCheck.setVisibility(View.INVISIBLE);
//            }
//
//            return view;
//        }
//
//        class ViewHolder {
//            public TextView title;
//            public ImageView imageCheck;
//        }
//    }
}