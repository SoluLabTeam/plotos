package com.o2.plotos.dietdetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.dishdetail.DishDetailActivity;
import com.o2.plotos.models.Dish;
import com.o2.plotos.models.Grocery;
import com.o2.plotos.models.PlaceItem;
import com.o2.plotos.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RANIA on 5/07/2017.
 */

public class GroceryFragment extends Fragment implements GroceryAdapter.OnGroceryListener {
    @BindView(R.id.fragment_diet_dish_recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.empty_view)
    LinearLayout emptyView;
    @BindView(R.id.text_no_dish)
    TextView emptyTextView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private ArrayList<PlaceItem> mGroceryList;

    public GroceryFragment() {
        // Required empty public constructor
    }

    public static GroceryFragment newInstance(ArrayList<PlaceItem> groceryList) {
        GroceryFragment groceryFragment = new GroceryFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("groceryList", groceryList);
        groceryFragment.setArguments(bundle);
        return groceryFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGroceryList = getArguments().getParcelableArrayList("groceryList");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_diet_dish, container, false);
        ButterKnife.bind(this, view);
        setDishAdapter();
        return view;
    }

    private void setDishAdapter() {
        progressBar.setVisibility(View.GONE);
        if (mGroceryList != null && mGroceryList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(linearLayoutManager);
            mRecyclerView.setVerticalFadingEdgeEnabled(true);
            GroceryAdapter groceryAdapter = new GroceryAdapter(mGroceryList, getActivity(), this);
            mRecyclerView.setAdapter(groceryAdapter);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGroceryClick(Grocery grocery) {
        Intent intent = new Intent(getActivity(), DishDetailActivity.class);
        intent.putExtra(DishDetailActivity.EXTRA_DISH, grocery);
        startActivity(intent);
    }

    @Override
    public void onDishClick(Dish dish) {
        Intent intent = new Intent(getActivity(), DishDetailActivity.class);
        intent.putExtra(DishDetailActivity.EXTRA_DISH, dish);
        startActivity(intent);
    }

    @Override
    public void onCartChanged() {

        ((GroceryDetailsActivity)getActivity()).setCartFab();
    }

    @Override
    public void onShowToast(int resID) {
        Utils.showCartToast(getActivity(), resID);
    }
}