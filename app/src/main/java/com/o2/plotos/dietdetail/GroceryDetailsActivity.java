package com.o2.plotos.dietdetail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.adapters.ViewPagerAdapter;
import com.o2.plotos.cart.CartActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.home.OfferChildCategoriesListener;
import com.o2.plotos.models.Dish;
import com.o2.plotos.models.Grocery;
import com.o2.plotos.models.PlaceItem;
import com.o2.plotos.models.SubCategory;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Rania on 5/7/2017.
 */

public class GroceryDetailsActivity extends AppCompatActivity implements SubCategoryViewListener, GroceryViewListener
        , OfferChildCategoriesListener {


    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.text_no_dish)
    TextView textNoDish;
    @BindView(R.id.empty_view)
    LinearLayout emptyView;
    @BindView(R.id.viewpager)
    ViewPager mViewPager;
    @BindView(R.id.tablayout)
    TabLayout mTabLayout;
    @BindView(R.id.restuarant_detail_image_cover)
    ImageView mCoverImage;
    @BindView(R.id.no_location_layout)
    RelativeLayout mNoLocationLayout;
    @BindView(R.id.no_data_title)
    TextView mNoDataTile;
    @BindView(R.id.no_data_message)
    TextView mNoDataMessage;
    @BindView(R.id.activity_home_btn_cart)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.activity_home_txt_item)
    TextView txtCartItem;
    @BindView(R.id.diet_detail_sharing)
    ImageButton dietDetailSharing;
    @BindView(R.id.no_data_image)
    ImageView noDataImage;
    @BindView(R.id.cart_layout)
    LinearLayout cartLayout;

    private ProgressDialog mProgressDialog;
    CustomFonts customFonts;
    private Unbinder mUnbinder;
    String mCatID, mCatName, mCatDescriprion;
    GroceryDetailsViewModel mViewModel;
    List<SubCategory> mSubCategoryList;
    int groceryLoadedListsCount = 0;
    int dishLoadedListsCount = 0;
    HashMap<String, List> map = new HashMap<>();
    private String mSupplierID = "0";
    private String mSpecialOfferID = "";
    private String mSpecialOfferImage = "";
    public static String EXTRA_SPECIAL_OFFER = "special_offer_id";
    public static String EXTRA_SPECIAL_OFFER_IMAGE = "special_offer_image";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_details);
        mUnbinder = ButterKnife.bind(this);
        mViewModel = new GroceryDetailsViewModel(this, this, this);

        mCatID = getIntent().getExtras().getString("cat_Id");
        mCatName = getIntent().getExtras().getString("cat_name");
        mCatDescriprion = getIntent().getExtras().getString("cat_desc");

        if (getIntent() != null && getIntent().getExtras() != null) {
            mSupplierID = getIntent().getExtras().getString(GroceryCategoryActivity.EXTRA_SUPPLIER, "0");
            mSpecialOfferID = getIntent().getExtras().getString(EXTRA_SPECIAL_OFFER);
            mSpecialOfferImage = getIntent().getExtras().getString(EXTRA_SPECIAL_OFFER_IMAGE);
            if (mSpecialOfferImage != null && !mSpecialOfferImage.equals("")) {
                Picasso.with(this).load(mSpecialOfferImage)
                        .resize(400, 200)
                        .centerCrop()
                        .error(R.drawable.placeholder)
                        .into(mCoverImage);

//                name.setText("Special Offer");
                name.setText("");
            }
        }
        initializeView();
    }

    private void initializeView() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait");
        if (mSpecialOfferImage != null && !mSpecialOfferImage.equals("")) {
//            name.setText("Special Offer");
            name.setText("");
        } else {
            name.setText(mCatName);
        }

        customFonts = new CustomFonts(this);
        customFonts.setOswaldBold(name);
        customFonts.setOpenSansBold(mNoDataTile);
        customFonts.setOpenSansRegulr(mNoDataMessage);

        if (mSpecialOfferID == null || mSpecialOfferID.equals("")) {
            mViewModel.getSubCategories(mCatID, mSupplierID);
        } else {
            mViewModel.getOfferSubCategories(mSpecialOfferID);
        }
        mProgressDialog.show();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GroceryDetailsActivity.this, CartActivity.class));
            }
        });
        setCartFab();
    }

    /**
     * Show cart item from database
     */
    public void setCartFab() {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        int cartItemSize = dataBaseHelper.getCartItems().size();
        if (cartItemSize > 0) {
            floatingActionButton.show();
            txtCartItem.setVisibility(View.VISIBLE);
            txtCartItem.setText("" + cartItemSize);
        } else {
            txtCartItem.setVisibility(View.GONE);
            floatingActionButton.hide();
        }
    }

    @Override
    public void onGetCategoriesSuccess(List<SubCategory> subCategoryList) {
        mSubCategoryList = subCategoryList;
        String userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
        String userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
        if (!userLat.equals("0.0") && !userLong.equals("0.0")) {
            for (int i = 0; i < mSubCategoryList.size(); i++) {
                if (mSupplierID.equals("")) {
                    mSupplierID = "0";
                }
                mViewModel.getGroceryByCatID(mSubCategoryList.get(i).getId(), mSupplierID, userLat, userLong);
            }
        } else {
            Toast.makeText(this, getString(R.string.error_no_location), Toast.LENGTH_LONG).show();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        int currentItem = 0;
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        int itemsCount = 0;

        Iterator myIterator = map.keySet().iterator();
        while (myIterator.hasNext()) {
            String key = (String) myIterator.next();
            ArrayList<PlaceItem> groceryList = (ArrayList<PlaceItem>) map.get(key);
            if (groceryList != null) {
                itemsCount = itemsCount + groceryList.size();
            }
            SubCategory currentCat = null;
            for (SubCategory category : mSubCategoryList) {
                if (category.getId().equals(key)) {
                    currentCat = category;
                }
            }
            if (currentCat != null) {
                adapter.addFrag(GroceryFragment.newInstance(groceryList), currentCat.getName());
            }
            currentItem++;
        }

        if (itemsCount > 0) {
            emptyView.setVisibility(View.GONE);
            mNoLocationLayout.setVisibility(View.GONE);
        } else {
            mNoLocationLayout.setVisibility(View.VISIBLE);
        }
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(currentItem);
    }

    @Override
    public void onGetCategoriesFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        mProgressDialog.hide();
    }

    @OnClick(R.id.img_cross)
    public void closeScreen() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mViewModel != null) {
            mViewModel.onDestroy();
        }
        mUnbinder.unbind();
    }

    @Override
    public void OnGroceryResponseSuccess(String catID, List<Grocery> groceryList) {
        map.put(catID, groceryList);
        groceryLoadedListsCount = groceryLoadedListsCount + 1;
        if (groceryLoadedListsCount == mSubCategoryList.size()) {
            setupViewPager(mViewPager);
            mTabLayout.setupWithViewPager(mViewPager);
            mProgressDialog.hide();
        }
    }

    @Override
    public void OnGroceryResponseFailure(String catID, String error) {
        map.put(catID, null);
        groceryLoadedListsCount = groceryLoadedListsCount + 1;
        if (groceryLoadedListsCount == mSubCategoryList.size()) {
            setupViewPager(mViewPager);
            mTabLayout.setupWithViewPager(mViewPager);
            mProgressDialog.hide();
        }
    }

    @Override
    public void onGetOfferChildCategoriesSuccessfully(List<SubCategory> offerChildCategories) {
        mSubCategoryList = offerChildCategories;
        String userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
        String userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
        if (!userLat.equals("0.0") && !userLong.equals("0.0")) {
            for (int i = 0; i < mSubCategoryList.size(); i++) {
                mViewModel.getOfferDishByCatID(mSubCategoryList.get(i).getId(), userLat, userLong);
                mViewModel.getOfferGroceryByCatID(mSubCategoryList.get(i).getId(), userLat, userLong);
            }
        } else {
            Toast.makeText(this, getString(R.string.error_no_location), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onGetOfferChildCategoriesFailure(String message) {
        // call same APIs but with special offer id instead of subcatIDs
        //Create fake sub category
        SubCategory category = new SubCategory();
        category.setName("");
        category.setId("0");
        mSubCategoryList.add(category);
        String userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
        String userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
        if (!userLat.equals("0.0") && !userLong.equals("0.0")) {
            mViewModel.getOfferDishByCatID(mSpecialOfferID, userLat, userLong);
            mViewModel.getOfferGroceryByCatID(mSpecialOfferID, userLat, userLong);
        } else {
            Toast.makeText(this, getString(R.string.error_no_location), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onGetOfferDishesSuccess(List<Dish> placeItems, String catID) {
        if (map.get(catID) != null)
            map.get(catID).addAll(placeItems);
        else
            map.put(catID, placeItems);

        dishLoadedListsCount = dishLoadedListsCount + 1;
        if (groceryLoadedListsCount == mSubCategoryList.size() && dishLoadedListsCount == mSubCategoryList.size()) {
            setupViewPager(mViewPager);
            mTabLayout.setupWithViewPager(mViewPager);
            mProgressDialog.hide();
        }
    }

    @Override
    public void onGetOfferDishesFailure(String message, String catID) {
        map.put(catID, null);
        dishLoadedListsCount = dishLoadedListsCount + 1;
        if (mSubCategoryList.size() == 0) {
            mProgressDialog.hide();

        } else if (groceryLoadedListsCount == mSubCategoryList.size() && dishLoadedListsCount == mSubCategoryList.size()) {
            setupViewPager(mViewPager);
            mTabLayout.setupWithViewPager(mViewPager);
            mProgressDialog.hide();
        }
    }

    @Override
    public void onGetOfferGrocerySuccess(List<Grocery> groceries, String catID) {
        if (map.get(catID) != null)
            map.get(catID).addAll(groceries);
        else
            map.put(catID, groceries);

        groceryLoadedListsCount = groceryLoadedListsCount + 1;
        if (groceryLoadedListsCount == mSubCategoryList.size() && dishLoadedListsCount == mSubCategoryList.size()) {
            setupViewPager(mViewPager);
            mTabLayout.setupWithViewPager(mViewPager);
            mProgressDialog.hide();
        }
    }

    @Override
    public void onGetOfferGroceryFailure(String message, String catID) {
        map.put(catID, null);
        groceryLoadedListsCount = groceryLoadedListsCount + 1;
        if (mSubCategoryList.size() == 0) {
            mProgressDialog.hide();

        } else if (groceryLoadedListsCount == mSubCategoryList.size() && dishLoadedListsCount == mSubCategoryList.size()) {
            setupViewPager(mViewPager);
            mTabLayout.setupWithViewPager(mViewPager);
            mProgressDialog.hide();
        }
    }

    @OnClick(R.id.diet_detail_sharing)
    public void onViewClicked() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Check out this app https://play.google.com/store/apps/details?id=com.o2.plotos&hl=en ");
        startActivity(Intent.createChooser(sharingIntent,"Share using"));

    }


}
