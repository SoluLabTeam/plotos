package com.o2.plotos.dietdetail;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.ParentCategoryRequest;
import com.o2.plotos.restapi.responses.ParentCategoryResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rania on 5/7/2017.
 */

public class ParentCategoryHelper {

    ParentCategoryListener mListener;

    public ParentCategoryHelper(ParentCategoryListener parentCategoryListener){
        mListener = parentCategoryListener;
    }

    public void getParentCategory(String supplierID){
        ParentCategoryRequest parentCategoryRequest = ServiceGenrator.createService(ParentCategoryRequest.class);
        Call<ParentCategoryResponse> parentCategoryResponseCall;
        if(supplierID.equals("")){
            parentCategoryResponseCall = parentCategoryRequest.getParentCategoryRequest();
        }else{
            parentCategoryResponseCall = parentCategoryRequest.getParentCategoryBySupplierRequest(supplierID);
        }
        parentCategoryResponseCall.enqueue(new Callback<ParentCategoryResponse>() {
            @Override
            public void onResponse(Call<ParentCategoryResponse> call, Response<ParentCategoryResponse> response) {
                if(response.code()== 200) {
                    ParentCategoryResponse apiResponse = response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        if(mListener!=null)
                        {
                            mListener.OnResponseSuccess(apiResponse.parentCategoryList);
                        }

                    }
                    else{
                        mListener.onResponseFailure(apiResponse.message);
                    }
                }
            }

            @Override
            public void onFailure(Call<ParentCategoryResponse> call, Throwable t) {
                mListener.onResponseFailure("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"parent category api onFailure "+t.getMessage());
            }
        });
    }
}
