package com.o2.plotos.dietdetail;

import com.o2.plotos.models.Dish;

import java.util.List;

/**
 * Created by Hassan on 9/02/2017.
 */

public interface IDietDetailDataModl {
    void getDietDishRqust(String type,String restaurant_id, String category_id, String calorie, String latitude, String longitude);
    interface OnGetDietDishRqustFinishLisener{
        void OnGetDietDishSuccess(List<Dish> dishList);
        void OnGetDietDishFailed(String error);
    }
}
