package com.o2.plotos.dietdetail;

import com.o2.plotos.models.Dish;

import java.util.List;

/**
 * Created by Hassan on 9/02/2017.
 */

public interface IDietDetailView {
    //void showProgress();
    //void hideProgress();
    void onGetDietDishSuccess(List<Dish> dishList);
    void onGetDietDishFailed(String error);
}
