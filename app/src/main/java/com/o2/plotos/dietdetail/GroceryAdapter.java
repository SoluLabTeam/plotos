package com.o2.plotos.dietdetail;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.Cart;
import com.o2.plotos.models.Dish;
import com.o2.plotos.models.Grocery;
import com.o2.plotos.models.PlaceItem;
import com.o2.plotos.models.User;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.TimeUtil;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;

import me.grantland.widget.AutofitTextView;

/**
 * Created by Hassan on 9/02/2017.
 */

public class GroceryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private OnGroceryListener mGroceryListener;
    private List<PlaceItem> mGroceryList;
    private Context mContext;
    private LayoutInflater mInflater;
    CustomFonts customFonts;
    public static final int DISH_TYPE = 0;
    public static final int GROCERY_TYPE = 1;

    String userLat  = "0.0";
    String userLong = "0.0";
    String userAddress = "";

    TimeUtil timeUtil;

    public GroceryAdapter(List<PlaceItem> grocery, Context context, OnGroceryListener dishListener) {
        mGroceryList = grocery;
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mGroceryListener = dishListener;
        customFonts = new CustomFonts(context);
        timeUtil = new TimeUtil(context);

        userLat  = UserPreferenceUtil.getInstance(mContext).getLocationLat();
        userLong = UserPreferenceUtil.getInstance(mContext).getLocationLng();
        userAddress = UserPreferenceUtil.getInstance(mContext).getLocation();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_diet_dish, parent, false);
        switch (viewType) {
            case GROCERY_TYPE:
                return new GroceryViewHolder(v);
            case DISH_TYPE:
                return new DishViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final DecimalFormat decimalFormat = new DecimalFormat("#.#");

        final PlaceItem placeItem = mGroceryList.get(position);
        if(placeItem != null) {
            if(placeItem instanceof Grocery) {
//                if(position == 1){
//                    ((Grocery) placeItem).setMinQty("0.5");
//                }
                ((GroceryViewHolder)holder).dishName.setText(((Grocery) placeItem).getName());
                ((GroceryViewHolder)holder).dishDescrptn.setText(((Grocery) placeItem).getDescrptn());
                if (!((Grocery) placeItem).getPreBookingtime().equalsIgnoreCase("0")) {
                    ((GroceryViewHolder)holder).deliveryTime.setText(((Grocery) placeItem).getPreBookingtime() + "h");

                    ((GroceryViewHolder)holder).preBooking.setImageResource(R.drawable.timer);
                } else {
                    ((GroceryViewHolder)holder).deliveryTime.setText(((Grocery) placeItem).getDeliveryTime() + "m");
                }
                ((GroceryViewHolder)holder).resturantName.setText(((Grocery) placeItem).getSupplier_name());
                int price  = (int) Double.parseDouble(((Grocery) placeItem).getPrice());
                ((GroceryViewHolder)holder).dishPrice.setText(String.valueOf(price) + " " + ((Grocery) placeItem).getCurrency());
                ((GroceryViewHolder)holder).itemUnit.setText(((Grocery) placeItem).getPerUnit());


                String[] cal = ((Grocery) placeItem).getDishCal().split(",");
                if (cal.length > 0) {
                    if (cal[cal.length - 1].equalsIgnoreCase("") || cal[cal.length - 1].equalsIgnoreCase("NaN") || cal[cal.length - 1].equalsIgnoreCase("0.0") || cal[cal.length - 1].equalsIgnoreCase("0")) {
                        ((GroceryViewHolder)holder).caloriesLayout.setVisibility(View.GONE);
                    } else if (!cal[cal.length - 1].equalsIgnoreCase("")) {
                        ((GroceryViewHolder)holder).caloriesLayout.setVisibility(View.VISIBLE);
                        ((GroceryViewHolder)holder).calText.setText(cal[0].toString() + " Cal");
                        ((GroceryViewHolder)holder).carbsText.setText(cal[1].toString() + " Carbs");
                        ((GroceryViewHolder)holder).proText.setText(cal[2].toString() + " Pro");
                        ((GroceryViewHolder)holder).fatText.setText(cal[3].toString() + " Fat");
                    }

                }
                if (!((Grocery) placeItem).getImage_url().equalsIgnoreCase("")) {
                    Picasso.with(mContext).load(((Grocery) placeItem).getImage_url())
                            .resize(400, 200)
                            .centerCrop()
                            .error(R.drawable.placeholder)
                            .into( ((GroceryViewHolder)holder).dishImage);
                } else {
                    ((GroceryViewHolder)holder).dishImage.setImageResource(R.drawable.placeholder);
                }




                customFonts.setOpenSansSemiBold( ((GroceryViewHolder)holder).dishName);
                customFonts.setOpenSansRegulr( ((GroceryViewHolder)holder).dishDescrptn);
                customFonts.setOpenSansSemiBold( ((GroceryViewHolder)holder).deliveryTime);
                customFonts.setOpenSansSemiBold( ((GroceryViewHolder)holder).resturantName);
                customFonts.setOpenSansSemiBold( ((GroceryViewHolder)holder).dishPrice);

                customFonts.setOpenSansSemiBold( ((GroceryViewHolder)holder).calText);
                customFonts.setOpenSansSemiBold( ((GroceryViewHolder)holder).carbsText);
                customFonts.setOpenSansSemiBold( ((GroceryViewHolder)holder).proText);
                customFonts.setOpenSansSemiBold( ((GroceryViewHolder)holder).fatText);

                customFonts.setOpenSansSemiBold( ((GroceryViewHolder)holder).itemUnit);


                ((GroceryViewHolder)holder).linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mGroceryListener.onGroceryClick(((Grocery) placeItem));
                    }
                });

                if(timeUtil.isPlaceOpened(((Grocery) placeItem).getOpeningTime(), ((Grocery) placeItem).getClosingTime())) {
                    ((GroceryViewHolder)holder).cartLayout.setVisibility(View.VISIBLE);
                }else{
                    ((GroceryViewHolder)holder).cartLayout.setVisibility(View.GONE);
                }

            }else if(placeItem instanceof Dish){
                ((DishViewHolder)holder).dishName.setText(((Dish) placeItem).getDishName());
                ((DishViewHolder)holder).dishDescrptn.setText(((Dish) placeItem).getDescrptn());
                if (!((Dish) placeItem).getPreBookingtime().equalsIgnoreCase("0")) {
                    ((DishViewHolder)holder).deliveryTime.setText(((Dish) placeItem).getPreBookingtime() + "hrs");

                    ((DishViewHolder)holder).preBooking.setImageResource(R.drawable.timer);
                } else {
                    if(!((Dish)placeItem).getCustomDelivery().equals("")){
                        ((DishViewHolder)holder).deliveryTime.setText(((Dish) placeItem).getCustomDelivery());
                    }else {
                        ((DishViewHolder)holder).deliveryTime.setText(((Dish) placeItem).getDeliveryTime() + "m");
                    }
                }

                ((DishViewHolder)holder).resturantName.setText(((Dish) placeItem).getResturantName());
                int price  = (int) Double.parseDouble(((Dish) placeItem).getPrice());
                ((DishViewHolder)holder).dishPrice.setText(String.valueOf(price) + " " + ((Dish) placeItem).getCurrency());
                ((DishViewHolder)holder).itemUnit.setText("");



                String[] cal = ((Dish) placeItem).getDishCal().split(",");
                if (cal.length > 0) {
                    if (cal[cal.length - 1].equalsIgnoreCase("") || cal[cal.length - 1].equalsIgnoreCase("NaN") || cal[cal.length - 1].equalsIgnoreCase("0.0") || cal[cal.length - 1].equalsIgnoreCase("0")) {
                        ((DishViewHolder)holder).caloriesLayout.setVisibility(View.GONE);
                    } else if (!cal[cal.length - 1].equalsIgnoreCase("")) {
                        ((DishViewHolder)holder).caloriesLayout.setVisibility(View.VISIBLE);
                        ((DishViewHolder)holder).calText.setText(cal[0].toString() + " Cal");
                        ((DishViewHolder)holder).carbsText.setText(cal[1].toString() + " Carbs");
                        ((DishViewHolder)holder).proText.setText(cal[2].toString() + " Pro");
                        ((DishViewHolder)holder).fatText.setText(cal[3].toString() + " Fat");
                    }

                }
                if (!((Dish) placeItem).getImage_url().equalsIgnoreCase("")) {
                    Picasso.with(mContext).load(((Dish) placeItem).getImage_url())
                            .resize(400, 200)
                            .centerCrop()
                            .error(R.drawable.placeholder)
                            .into( ((DishViewHolder)holder).dishImage);
                } else {
                    ((DishViewHolder)holder).dishImage.setImageResource(R.drawable.placeholder);
                }





                customFonts.setOpenSansSemiBold( ((DishViewHolder)holder).dishName);
                customFonts.setOpenSansRegulr( ((DishViewHolder)holder).dishDescrptn);
                customFonts.setOpenSansSemiBold( ((DishViewHolder)holder).deliveryTime);
                customFonts.setOpenSansSemiBold( ((DishViewHolder)holder).resturantName);
                customFonts.setOpenSansSemiBold( ((DishViewHolder)holder).dishPrice);

                customFonts.setOpenSansSemiBold( ((DishViewHolder)holder).calText);
                customFonts.setOpenSansSemiBold( ((DishViewHolder)holder).carbsText);
                customFonts.setOpenSansSemiBold( ((DishViewHolder)holder).proText);
                customFonts.setOpenSansSemiBold( ((DishViewHolder)holder).fatText);

                customFonts.setOpenSansSemiBold( ((DishViewHolder)holder).itemUnit);


                ((DishViewHolder)holder).linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mGroceryListener.onDishClick(((Dish) placeItem));
                    }
                });

                if(timeUtil.isPlaceOpened(((Dish) placeItem).getOpeningTime(), ((Dish) placeItem).getClosingTime())) {
                    ((DishViewHolder)holder).cartLayout.setVisibility(View.VISIBLE);
                }else{
                    ((DishViewHolder)holder).cartLayout.setVisibility(View.GONE);
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return mGroceryList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mGroceryList != null) {
            PlaceItem objectItem = mGroceryList.get(position);
            if (objectItem instanceof Grocery) {
                return GROCERY_TYPE;
            }
        }
        return 0;
    }


    public static class GroceryViewHolder extends RecyclerView.ViewHolder {
        TextView dishDescrptn, dishPrice , deliveryTime, resturantName; //dishCal
        ImageView dishImage, preBooking;
        LinearLayout linearLayout, caloriesLayout, cartLayout;
        AutofitTextView calText, carbsText,proText, fatText, dishName, itemUnit;



        public GroceryViewHolder(View v) {
            super(v);
            dishImage = (ImageView) v.findViewById(R.id.item_diet_dish_image);
            preBooking = (ImageView) v.findViewById(R.id.item_diet_dish_prebooking);
            dishName = (AutofitTextView) v.findViewById(R.id.item_diet_dish_name);
            resturantName = (TextView) v.findViewById(R.id.item_diet_dish_resturnt_name);
            dishDescrptn = (TextView) v.findViewById(R.id.item_diet_dish_description);
            dishPrice = (TextView) v.findViewById(R.id.item_diet_dish_price);
            deliveryTime = (TextView) v.findViewById(R.id.item_diet_dish_deliver_time);
            linearLayout = (LinearLayout) v.findViewById(R.id.item_diet_dish_linearLayout);
            calText = (AutofitTextView) v.findViewById(R.id.calories_text_view);
            carbsText = (AutofitTextView) v.findViewById(R.id.carbs_text_view);
            proText = (AutofitTextView) v.findViewById(R.id.pro_text_view);
            fatText = (AutofitTextView) v.findViewById(R.id.fat_text_view);
            caloriesLayout = (LinearLayout) v.findViewById(R.id.calories_layout);

            itemUnit = (AutofitTextView) v.findViewById(R.id.item_unit);

            cartLayout = (LinearLayout) v.findViewById(R.id.cart_layout);



        }
    }

    public static class DishViewHolder extends RecyclerView.ViewHolder {
        TextView dishDescrptn, dishPrice , deliveryTime, resturantName; //dishCal
        ImageView dishImage, preBooking;
        LinearLayout linearLayout, caloriesLayout, cartLayout;
        AutofitTextView calText, carbsText,proText, fatText, dishName, itemUnit;



        public DishViewHolder(View v) {
            super(v);
            dishImage = (ImageView) v.findViewById(R.id.item_diet_dish_image);
            preBooking = (ImageView) v.findViewById(R.id.item_diet_dish_prebooking);
            dishName = (AutofitTextView) v.findViewById(R.id.item_diet_dish_name);
            resturantName = (TextView) v.findViewById(R.id.item_diet_dish_resturnt_name);
            dishDescrptn = (TextView) v.findViewById(R.id.item_diet_dish_description);
            dishPrice = (TextView) v.findViewById(R.id.item_diet_dish_price);
            deliveryTime = (TextView) v.findViewById(R.id.item_diet_dish_deliver_time);
            linearLayout = (LinearLayout) v.findViewById(R.id.item_diet_dish_linearLayout);
            calText = (AutofitTextView) v.findViewById(R.id.calories_text_view);
            carbsText = (AutofitTextView) v.findViewById(R.id.carbs_text_view);
            proText = (AutofitTextView) v.findViewById(R.id.pro_text_view);
            fatText = (AutofitTextView) v.findViewById(R.id.fat_text_view);
            caloriesLayout = (LinearLayout) v.findViewById(R.id.calories_layout);

            itemUnit = (AutofitTextView) v.findViewById(R.id.item_unit);

            cartLayout = (LinearLayout) v.findViewById(R.id.cart_layout);


        }
    }

    public interface OnGroceryListener {
        void onGroceryClick(Grocery grocery);
        void onDishClick(Dish dish);
        void onCartChanged();
        void onShowToast(int resID);
    }


}
