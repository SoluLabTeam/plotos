package com.o2.plotos.dietdetail.detox;

import com.o2.plotos.models.Dish;

import java.util.List;

/**
 * Created by Hassan on 14/02/2017.
 */

public interface IDetoxDataModel {
    void getDetoxDishRqust(String restaurant_id, String latitude, String longitude);
    interface OnGetDetoxDishRqustFinishLisener{
        void OnGetDetoxDishSuccess(List<Dish> dishList);
        void OnGetDetoxDishFailed(String error);
    }
}
