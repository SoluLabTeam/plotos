package com.o2.plotos.dietdetail;

import com.o2.plotos.models.ParentCategory;

import java.util.List;

/**
 * Created by Rania on 5/7/2017.
 */

public class GroceryCategoryViewModel implements ParentCategoryListener {

    ParentCategoryHelper mHelper;
    ParentCategoryViewListener mListener;

    public GroceryCategoryViewModel(ParentCategoryViewListener parentCategoryViewListener){
        mHelper = new ParentCategoryHelper(this);
        mListener = parentCategoryViewListener;
    }

    public void getParentCategories(String supplierID){
        mHelper.getParentCategory(supplierID);
    }

    @Override
    public void OnResponseSuccess(List<ParentCategory> parentCategoriesList) {
        if(mListener!=null)
        {
            mListener.onGetCategoriesSuccess(parentCategoriesList);
        }

    }

    @Override
    public void onResponseFailure(String error) {
        if(mListener != null) {
            mListener.onGetCategoriesFailure(error);
        }
    }

    public void onDestroy(){
        if(mListener != null){
            mListener = null;
        }
    }
}
