package com.o2.plotos.dietdetail;

import com.o2.plotos.models.Grocery;

import java.util.List;

/**
 * Created by Rania on 5/7/2017.
 */

public interface GroceryViewListener {

    void OnGroceryResponseSuccess(String catID, List<Grocery> groceryList);
    void OnGroceryResponseFailure(String catID, String error);
}
