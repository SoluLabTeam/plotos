package com.o2.plotos.dietdetail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.cart.CartActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.ParentCategory;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.InternetUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Rania on 5/7/2017.
 */

public class GroceryCategoryActivity extends AppCompatActivity implements ParentCategoryViewListener {


    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.activity_home_btn_cart)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.activity_home_txt_item)
    TextView txtCartItem;
    @BindView(R.id.text_no_dish)
    TextView textNoDish;
    @BindView(R.id.empty_view)
    LinearLayout emptyView;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.diet_detail_sharing)
    ImageButton dietDetailSharing;

    private ProgressDialog mProgressDialog;
    CustomFonts customFonts;
    GroceryCategoryViewModel mViewModel;
    private Unbinder mUnbinder;
    private GroceryCategoryAdapter mAdapter;
    private String mSupplierID = "";
    private StaggeredGridLayoutManager staggeredGridLayoutManagerVertical;
    public static String EXTRA_SUPPLIER = "supplier_id";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_category);
        mUnbinder = ButterKnife.bind(this);
        mViewModel = new GroceryCategoryViewModel(this);

        if (getIntent() != null && getIntent().getExtras() != null) {
            mSupplierID = getIntent().getExtras().getString(EXTRA_SUPPLIER);
        }
        initializeView();
    }

    private void initializeView() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait");
        customFonts = new CustomFonts(this);
//        customFonts.setOswaldBold(name);
        customFonts.setOswaldBold(title);


        staggeredGridLayoutManagerVertical =
                new StaggeredGridLayoutManager(
                        2, //The number of Columns in the grid
                        LinearLayoutManager.VERTICAL);

        //GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(staggeredGridLayoutManagerVertical);
        mAdapter = new GroceryCategoryAdapter(null, this, mSupplierID);
        mRecyclerView.setAdapter(mAdapter);

        if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
            mViewModel.getParentCategories(mSupplierID);
            mProgressDialog.show();
        } else {
            Toast.makeText(this, getString(R.string.error_internet_connection), Toast.LENGTH_LONG).show();
        }
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GroceryCategoryActivity.this, CartActivity.class));
            }
        });
        setCartFab();
    }

    /**
     * Show cart item from database
     */
    private void setCartFab() {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        int cartItemSize = dataBaseHelper.getCartItems().size();
        if (cartItemSize > 0) {
            floatingActionButton.show();
            txtCartItem.setVisibility(View.VISIBLE);
            txtCartItem.setText("" + cartItemSize);
        } else {
            txtCartItem.setVisibility(View.GONE);
            floatingActionButton.hide();
        }
    }

    @Override
    public void onGetCategoriesSuccess(List<ParentCategory> parentCategoryList) {
        mProgressDialog.hide();
        if (mAdapter != null) {
            emptyView.setVisibility(View.GONE);
            mAdapter.setItems(parentCategoryList);
            mAdapter.notifyDataSetChanged();
        } else {

        }
    }

    @Override
    public void onGetCategoriesFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        mProgressDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mViewModel != null) {
            mViewModel.onDestroy();
        }
        mUnbinder.unbind();
    }

    @OnClick(R.id.img_cross)
    public void closeScreen() {
        finish();
    }

    @OnClick(R.id.diet_detail_sharing)
    public void onViewClicked() {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Check out this app https://play.google.com/store/apps/details?id=com.o2.plotos&hl=en ");
        startActivity(Intent.createChooser(sharingIntent,"Share using"));
    }
}
