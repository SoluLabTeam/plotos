package com.o2.plotos.dietdetail.dietdish;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.dietdetail.DietDetailAdapter;
import com.o2.plotos.dietdetail.LowCrabActivity;
import com.o2.plotos.dietdetail.WithoutSpinnerActivity;
import com.o2.plotos.dietdetail.detox.DetoxActivity;
import com.o2.plotos.dishdetail.DishDetailActivity;
import com.o2.plotos.models.Dish;
import com.o2.plotos.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hassan on 8/03/2017.
 */

public class DietDishFragment extends Fragment implements DietDetailAdapter.OnDishListener {
    @BindView(R.id.fragment_diet_dish_recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.empty_view)
    LinearLayout emptyView;
    @BindView(R.id.text_no_dish)
    TextView emptyTextView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private ArrayList<Dish> mDishList;

    public DietDishFragment() {
        // Required empty public constructor
    }

    public static DietDishFragment newInstance(ArrayList<Dish> dishList) {
        DietDishFragment dishFragment = new DietDishFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("dishList", dishList);
        dishFragment.setArguments(bundle);
        return dishFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDishList = getArguments().getParcelableArrayList("dishList");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_diet_dish, container, false);
        ButterKnife.bind(this, view);
        setDishAdapter();
        return view;
    }

    @Override
    public void onDishClick(Dish dish) {
        Intent intent = new Intent(getActivity(), DishDetailActivity.class);
        intent.putExtra(DishDetailActivity.EXTRA_DISH, dish);
        startActivity(intent);
        //getActivity().overridePendingTransition(R.anim.slide_up, android.R.anim.fade_out);
    }

    @Override
    public void onCartChanged() {
        if(getActivity() instanceof DetoxActivity){
            ((DetoxActivity)getActivity()).setCartFab();
        }else if(getActivity() instanceof WithoutSpinnerActivity){
            ((WithoutSpinnerActivity)getActivity()).setCartFab();
        }else if(getActivity() instanceof LowCrabActivity){
            ((LowCrabActivity)getActivity()).setCartFab();
        }
    }

    @Override
    public void onShowToast(int resID) {
        Utils.showCartToast(getActivity(), resID);
    }

    private void setDishAdapter() {
        progressBar.setVisibility(View.GONE);
        if (mDishList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(linearLayoutManager);
            mRecyclerView.setVerticalFadingEdgeEnabled(true);
            DietDetailAdapter dishAdapter = new DietDetailAdapter(mDishList, getActivity(), this);
            mRecyclerView.setAdapter(dishAdapter);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyTextView.setVisibility(View.VISIBLE);
        }
    }
}