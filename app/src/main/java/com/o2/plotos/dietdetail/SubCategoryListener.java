package com.o2.plotos.dietdetail;

import com.o2.plotos.models.SubCategory;

import java.util.List;

/**
 * Created by Rania on 5/7/2017.
 */

public interface SubCategoryListener {

    void OnResponseSuccess(List<SubCategory> subCategoryList);
    void onResponseFailure(String error);
}
