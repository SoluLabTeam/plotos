package com.o2.plotos.dietdetail;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.models.ParentCategory;
import com.o2.plotos.utils.CustomFonts;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Rania on 5/7/2017.
 */

public class GroceryCategoryAdapter extends RecyclerView.Adapter<GroceryCategoryAdapter.ViewHolder> {

    private List<ParentCategory> items;
    private CustomFonts customFonts;
    private Context mContext;
    private String mSupplierID;

    public GroceryCategoryAdapter(List<ParentCategory> parentCategoryList, Context context, String supplierID) {
        items = parentCategoryList;
        mContext = context;
        customFonts = new CustomFonts(mContext);
        mSupplierID = supplierID;
    }

    public void setItems(List<ParentCategory> parentCategoryList) {
        this.items = parentCategoryList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_grocery_categories, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ParentCategory parentCategory = items.get(position);
        holder.mName.setText(parentCategory.getName());
        customFonts.setOpenSansBold(holder.mName);

        if (!parentCategory.getImage_url().equalsIgnoreCase("")) {
            Picasso.with(mContext).load(parentCategory.getImage_url())
                    .resize(400,400)
                    .centerCrop()
                    .error(R.drawable.placeholder)
                    .into(holder.mImage);
        } else {
            holder.mImage.setImageResource(R.drawable.placeholder);
        }

        holder.mContainerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GroceryDetailsActivity.class);
                intent.putExtra("cat_Id", parentCategory.getId());
                intent.putExtra("cat_name", parentCategory.getName());
                intent.putExtra("cat_desc", parentCategory.getDescription());
                intent.putExtra(GroceryCategoryActivity.EXTRA_SUPPLIER, mSupplierID);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items!= null?items.size():0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_diet_image)
        ImageView mImage;
        @BindView(R.id.item_diet_text_name)
        TextView mName;
        @BindView(R.id.containerLayout)
        LinearLayout mContainerLayout;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
