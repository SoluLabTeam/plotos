package com.o2.plotos.dietdetail;

import com.o2.plotos.models.ParentCategory;

import java.util.List;

/**
 * Created by Rania on 5/7/2017.
 */

public interface ParentCategoryViewListener {

    void onGetCategoriesSuccess(List<ParentCategory> parentCategoryList);
    void onGetCategoriesFailure(String message);
}
