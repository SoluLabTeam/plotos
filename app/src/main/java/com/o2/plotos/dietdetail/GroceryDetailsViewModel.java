package com.o2.plotos.dietdetail;

import com.o2.plotos.home.OfferChildCategoriesListener;
import com.o2.plotos.home.OfferHelper;
import com.o2.plotos.models.Dish;
import com.o2.plotos.models.Grocery;
import com.o2.plotos.models.SubCategory;

import java.util.List;

/**
 * Created by Rania on 5/7/2017.
 */

public class GroceryDetailsViewModel implements SubCategoryListener, GroceryListener, OfferChildCategoriesListener{

    SubCategoryViewListener mListener;
    SubCategoryHelper mHelper;
    GroceryViewListener mGroceryListener;
    OfferHelper mOfferHelper;
    OfferChildCategoriesListener mOfferListener;

    public GroceryDetailsViewModel(SubCategoryViewListener subCategoryViewListener,
                                   GroceryViewListener groceryViewListener, OfferChildCategoriesListener offerChildCategoriesListener){
        mListener = subCategoryViewListener;
        mHelper = new SubCategoryHelper(this, this);
        mGroceryListener = groceryViewListener;
        mOfferHelper = new OfferHelper(this);
        mOfferListener = offerChildCategoriesListener;
    }

    public void getSubCategories(String parentCatID, String supplierID){
        mHelper.getSubCategory(parentCatID, supplierID);
    }

    public void getGroceryByCatID(String catID, String supplierID, String latitude, String longitude){
        mHelper.getGroceryByCategory(catID, supplierID, latitude, longitude);
    }

    public void getOfferSubCategories(String offerID){
        mOfferHelper.getOfferChildCategories(offerID);
    }

    public void getOfferDishByCatID(String catID, String latitude, String longitude){
        mOfferHelper.getOfferDishes(catID, latitude, longitude);
    }

    public void getOfferGroceryByCatID(String catID, String latitude, String longitude){
        mOfferHelper.getOfferGrocery(catID, latitude, longitude);
    }

    @Override
    public void OnResponseSuccess(List<SubCategory> subCategoryList) {
        mListener.onGetCategoriesSuccess(subCategoryList);
    }

    @Override
    public void onResponseFailure(String error) {
        mListener.onGetCategoriesFailure(error);
    }

    public void onDestroy(){
        if(mListener != null){
            mListener = null;
        }
        if(mGroceryListener != null){
            mGroceryListener = null;
        }
    }

    @Override
    public void OnGroceryResponseSuccess(String catID, List<Grocery> groceryList) {

        if(mGroceryListener!=null && groceryList!=null)
        {
            mGroceryListener.OnGroceryResponseSuccess(catID, groceryList);
        }

    }

    @Override
    public void OnGroceryResponseFailure(String CatID, String error) {
        mGroceryListener.OnGroceryResponseFailure(CatID, error);
    }

    @Override
    public void onGetOfferChildCategoriesSuccessfully(List<SubCategory> offerChildCategories) {
        mOfferListener.onGetOfferChildCategoriesSuccessfully(offerChildCategories);
    }

    @Override
    public void onGetOfferChildCategoriesFailure(String message) {
        mOfferListener.onGetOfferChildCategoriesFailure(message);
    }

    @Override
    public void onGetOfferDishesSuccess(List<Dish> dishes, String catID) {
        mOfferListener.onGetOfferDishesSuccess(dishes, catID);
    }

    @Override
    public void onGetOfferDishesFailure(String message, String catID) {
        mOfferListener.onGetOfferDishesFailure(message, catID);
    }

    @Override
    public void onGetOfferGrocerySuccess(List<Grocery> dishes, String catID) {
        mOfferListener.onGetOfferGrocerySuccess(dishes, catID);
    }

    @Override
    public void onGetOfferGroceryFailure(String message, String catID) {
        mOfferListener.onGetOfferGroceryFailure(message, catID);
    }
}
