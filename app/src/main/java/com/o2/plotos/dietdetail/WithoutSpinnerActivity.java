package com.o2.plotos.dietdetail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.adapters.ViewPagerAdapter;
import com.o2.plotos.cart.CartActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.dietdetail.dietdish.DietDishFragment;
import com.o2.plotos.models.Dish;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WithoutSpinnerActivity extends AppCompatActivity implements IDietDetailView {

    @BindView(R.id.restuarant_detail_viewpager)
    ViewPager viewPager;
    @BindView(R.id.restuarant_detail_tablayout)
    TabLayout tabLayout;
    @BindView(R.id.empty_view)
    LinearLayout emptyView;
    //    @BindView(R.id.restaurant_detail_restaurantName)
//    TextView restaurantNameTextView;
    @BindView(R.id.activity_resturant_detail_diet_name)
    TextView dietTitle;
    @BindView(R.id.restaurant_detail_description)
    TextView resturantDescription;
    @BindView(R.id.restaurant_detail_open_close_time)
    TextView openCloseTime;
    CustomFonts customFonts;
    @BindView(R.id.restuarant_detail_img_cross)
    ImageButton imageButton;
    @BindView(R.id.restuarant_detail_image_cover)
    ImageView imageViewCover;
    @BindView(R.id.no_location_layout)
    RelativeLayout mNoLocationLayout;
    @BindView(R.id.no_data_title)
    TextView mNoDataTile;
    @BindView(R.id.no_data_message)
    TextView mNoDataMessage;
    @BindView(R.id.activity_home_btn_cart)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.activity_home_txt_item)
    TextView txtCartItem;


    String categryName;
    String category_Id;
    Resources resources;
    @BindView(R.id.diet_detail_sharing)
    ImageButton dietDetailSharing;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.no_data_image)
    ImageView noDataImage;
    @BindView(R.id.cart_layout)
    LinearLayout cartLayout;

    private String REQUEST_TYPE = "get_dish_by_category";

    private ProgressDialog mProgressDialog;
    private ArrayList<Dish> mDishList = new ArrayList<>();

    private ArrayList<Dish> breakFastDishList = new ArrayList<>();
    private ArrayList<Dish> launcDishList = new ArrayList<>();
    private ArrayList<Dish> dinnerDishList = new ArrayList<>();
    private ArrayList<Dish> snackDishList = new ArrayList<>();
    private ArrayList<Dish> allDayDishList = new ArrayList<>();

    int[] images = {R.drawable.calorie_controlled, R.drawable.healthy_balance, R.drawable.detox,
            R.drawable.paleo, R.drawable.nutri_vegan, R.drawable.nutri_vegetarian, R.drawable.wheat_free, R.drawable.nutri_low_carb};

    private IDietDetailPrsntr mDietDetailPrsntr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restuarant_detail);
        ButterKnife.bind(this);

        categryName = getIntent().getExtras().getString("catName");
        category_Id = getIntent().getExtras().getString("cat_Id");
        resources = getResources();


        mDietDetailPrsntr = new DietDetailPrsntrImpl();
        mDietDetailPrsntr.bindView(this);
        initializeView();
        setDietDetails();

        if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
            String userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
            String userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
            if (!userLat.equals("0.0") && !userLong.equals("0.0")) {
                mDietDetailPrsntr.sendDietDishRqust(REQUEST_TYPE, "0", category_Id, "0", userLat, userLong);
            } else {
                Toast.makeText(this, getString(R.string.error_no_location), Toast.LENGTH_LONG).show();
            }
        } else {
            setupViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    private void initializeView() {
        ButterKnife.bind(this);
        mProgressDialog = new ProgressDialog(this);
        dietTitle.setText(categryName);
        mProgressDialog.setMessage("Please wait");
        customFonts = new CustomFonts(this);
//        customFonts.setOswaldBold(restaurantNameTextView);
        customFonts.setOswaldBold(dietTitle);
        customFonts.setOpenSansSemiBold(resturantDescription);
        customFonts.setOpenSansBold(mNoDataTile);
        customFonts.setOpenSansRegulr(mNoDataMessage);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WithoutSpinnerActivity.this, CartActivity.class));
            }
        });
        setCartFab();
    }

    /**
     * Show cart item from database
     */
    public void setCartFab() {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        int cartItemSize = dataBaseHelper.getCartItems().size();
        if (cartItemSize > 0) {
            floatingActionButton.show();
            txtCartItem.setVisibility(View.VISIBLE);
            txtCartItem.setText("" + cartItemSize);
        } else {
            txtCartItem.setVisibility(View.GONE);
            floatingActionButton.hide();
        }
    }

    private void setDietDetails() {


        String[] diet = resources.getStringArray(R.array.dietdescrption);
//        restaurantNameTextView.setText(categryName);
        openCloseTime.setVisibility(View.GONE);
        imageViewCover.setImageResource(images[(Integer.valueOf(category_Id)) - 1]);
        resturantDescription.setText(diet[(Integer.valueOf(category_Id)) - 1]);
    }


    @Override
    protected void onPause() {
        super.onPause();
        mDietDetailPrsntr.unBindView();
    }

    private void setupViewPager(ViewPager viewPager) {

        int currentItem = 0;

        for (int i = 0; i < mDishList.size(); i++) {
            if (mDishList.get(i).getBreakFast().equalsIgnoreCase("1")) {
                breakFastDishList.add(mDishList.get(i));
            } else {
                if (mDishList.get(i).getDinner().equalsIgnoreCase("1")) {
                    dinnerDishList.add(mDishList.get(i));
                } else {
                    if (mDishList.get(i).getLaunch().equalsIgnoreCase("1")) {
                        launcDishList.add(mDishList.get(i));
                    } else {
                        if (mDishList.get(i).getSnacks().equalsIgnoreCase("1")) {
                            snackDishList.add(mDishList.get(i));
                        } else {
                            allDayDishList.add(mDishList.get(i));
                        }
                    }
                }
            }
        }

        //Toast.makeText(this, mDishList.size(), Toast.LENGTH_SHORT).show();

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (breakFastDishList.size() > 0) {
            adapter.addFrag(DietDishFragment.newInstance(breakFastDishList), "BreakFast");
            currentItem++;
        }
        if (launcDishList.size() > 0) {
            adapter.addFrag(DietDishFragment.newInstance(launcDishList), "Lunch");
            currentItem++;
        }
        if (dinnerDishList.size() > 0) {
            adapter.addFrag(DietDishFragment.newInstance(dinnerDishList), "Dinner");
            currentItem++;
        }
        if (snackDishList.size() > 0) {
            adapter.addFrag(DietDishFragment.newInstance(snackDishList), "Snacks");
            currentItem++;
        }
        if (allDayDishList.size() > 0) {
            adapter.addFrag(DietDishFragment.newInstance(allDayDishList), "All Day");
            currentItem++;
        }

        if (currentItem > 0) {
            emptyView.setVisibility(View.GONE);
            mNoLocationLayout.setVisibility(View.GONE);
        } else {
            mNoLocationLayout.setVisibility(View.VISIBLE);
        }

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(currentItem);
    }

    @OnClick(R.id.restuarant_detail_img_cross)
    public void closeScreen() {
        finish();
        //overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
    }

    @Override
    public void showProgress() {
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog.hide();
    }*/

    @Override
    public void onGetDietDishSuccess(List<Dish> dishList) {
        mDishList = (ArrayList<Dish>) dishList;
        setupViewPager(viewPager);
        //Toast.makeText(this, "Success..", Toast.LENGTH_SHORT).show();
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onGetDietDishFailed(String error) {
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        //Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.diet_detail_sharing)
    public void onViewClicked() {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Check out this app https://play.google.com/store/apps/details?id=com.o2.plotos&hl=en ");
        startActivity(Intent.createChooser(sharingIntent,"Share using"));
    }
}