package com.o2.plotos.dietdetail.detox;

/**
 * Created by Hassan on 14/02/2017.
 */

public interface IDetoxPrsntr {
    void sendDetoxDishRqust(String restaurant_id, String latitude, String longitude);
    void bindView(Object view);
    void unBindView();
}
