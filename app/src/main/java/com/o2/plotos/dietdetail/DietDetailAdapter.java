package com.o2.plotos.dietdetail;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.Cart;
import com.o2.plotos.models.Dish;
import com.o2.plotos.models.User;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.TimeUtil;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;

import me.grantland.widget.AutofitTextView;

/**
 * Created by Hassan on 9/02/2017.
 */

public class DietDetailAdapter extends RecyclerView.Adapter<DietDetailAdapter.DietDetailViewHolder> {
    private OnDishListener mDishListener;
    private List<Dish> mDishList;
    private Context mContext;
    private LayoutInflater mInflater;
    CustomFonts customFonts;
    String userLat  = UserPreferenceUtil.getInstance(mContext).getLocationLat();
    String userLong = UserPreferenceUtil.getInstance(mContext).getLocationLng();
    String userAddress = UserPreferenceUtil.getInstance(mContext).getLocation();
    TimeUtil timeUtil;

    public DietDetailAdapter(List<Dish> dishes, Context context, OnDishListener dishListener) {
        mDishList = dishes;
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mDishListener = dishListener;
        customFonts = new CustomFonts(context);
        timeUtil = new TimeUtil(context);
    }

    @Override
    public DietDetailAdapter.DietDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_diet_dish, parent, false);
        DietDetailViewHolder viewHolder = new DietDetailViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DietDetailAdapter.DietDetailViewHolder holder, final int position) {

        final DecimalFormat decimalFormat = new DecimalFormat("#.#");

        final Dish dish = mDishList.get(position);
        holder.dishName.setText(dish.getDishName());
        holder.dishDescrptn.setText(dish.getDescrptn());
        if (!dish.getPreBookingtime().equalsIgnoreCase("0")){
            holder.deliveryTime.setText(dish.getPreBookingtime() + "hrs");
          //  holder.deliveryTime.setTextColor(Color.rgb(246,122,64));
            holder.preBooking.setImageResource(R.drawable.timer);
        } else {
            if(!dish.getCustomDelivery().equals("")) {
                holder.deliveryTime.setText(dish.getCustomDelivery());
            }else{
                holder.deliveryTime.setText(dish.getDeliveryTime() + "min");
            }
        }
        holder.resturantName.setText(dish.getResturantName());
        NumberFormat format = NumberFormat.getNumberInstance();
        try {
            Number number = format.parse(dish.getPrice());

       // String str = dish.getPrice().replaceAll(", $", "");
        Double price  = Double.parseDouble(number.toString());
        holder.dishPrice.setText(decimalFormat.format(price) + " " + dish.getCurrency());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.itemUnit.setText("");



        String[] cal = dish.getDishCal().split(",");
        if (cal.length > 0) {
            if (cal[cal.length - 1].equalsIgnoreCase("") || cal[cal.length - 1].equalsIgnoreCase("NaN") || cal[cal.length - 1].equalsIgnoreCase("0.0") || cal[cal.length - 1].equalsIgnoreCase("0")) {
                holder.caloriesLayout.setVisibility(View.GONE);
            } else if (!cal[cal.length - 1].equalsIgnoreCase("")) {
                holder.caloriesLayout.setVisibility(View.VISIBLE);
                holder.calText.setText(cal[0].toString() + " Cal");
                holder.carbsText.setText(cal[1].toString() + " Carbs");
                holder.proText.setText(cal[2].toString() + " Pro");
                holder.fatText.setText(cal[3].toString() + " Fat");
            }

        }
        if (!dish.getImage_url().equalsIgnoreCase("")) {
            Picasso.with(mContext).load(dish.getImage_url())
                    .resize(400, 200)
                    .centerCrop()
                    .error(R.drawable.placeholder)
                    .into(holder.dishImage);
        } else {
            holder.dishImage.setImageResource(R.drawable.placeholder);
        }




        if(timeUtil.isPlaceOpened(dish.getOpeningTime(), dish.getClosingTime())) {
            holder.cartLayout.setVisibility(View.VISIBLE);
        }else{
            holder.cartLayout.setVisibility(View.GONE);
        }

        customFonts.setOpenSansSemiBold(holder.dishName);
        customFonts.setOpenSansRegulr(holder.dishDescrptn);
        customFonts.setOpenSansSemiBold(holder.deliveryTime);
        customFonts.setOpenSansSemiBold(holder.resturantName);
        customFonts.setOpenSansSemiBold(holder.dishPrice);

        customFonts.setOpenSansSemiBold(holder.calText);
        customFonts.setOpenSansSemiBold(holder.carbsText);
        customFonts.setOpenSansSemiBold(holder.proText);
        customFonts.setOpenSansSemiBold(holder.fatText);


        customFonts.setOpenSansSemiBold(holder.itemUnit);


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDishListener.onDishClick(dish);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDishList.size();
    }



    public static class DietDetailViewHolder extends RecyclerView.ViewHolder {
        TextView dishDescrptn, dishPrice, deliveryTime, resturantName;
        ImageView dishImage, preBooking;
        LinearLayout linearLayout, caloriesLayout, cartLayout;
        AutofitTextView calText, carbsText,proText, fatText, dishName, itemUnit;



        public DietDetailViewHolder(View v) {
            super(v);
            dishImage = (ImageView) v.findViewById(R.id.item_diet_dish_image);
            preBooking = (ImageView) v.findViewById(R.id.item_diet_dish_prebooking);
            dishName = (AutofitTextView) v.findViewById(R.id.item_diet_dish_name);
            resturantName = (TextView) v.findViewById(R.id.item_diet_dish_resturnt_name);
            dishDescrptn = (TextView) v.findViewById(R.id.item_diet_dish_description);
            dishPrice = (TextView) v.findViewById(R.id.item_diet_dish_price);
            deliveryTime = (TextView) v.findViewById(R.id.item_diet_dish_deliver_time);
            linearLayout = (LinearLayout) v.findViewById(R.id.item_diet_dish_linearLayout);
            calText = (AutofitTextView) v.findViewById(R.id.calories_text_view);
            carbsText = (AutofitTextView) v.findViewById(R.id.carbs_text_view);
            proText = (AutofitTextView) v.findViewById(R.id.pro_text_view);
            fatText = (AutofitTextView) v.findViewById(R.id.fat_text_view);
            caloriesLayout = (LinearLayout) v.findViewById(R.id.calories_layout);

            itemUnit = (AutofitTextView) v.findViewById(R.id.item_unit);

            cartLayout = (LinearLayout) v.findViewById(R.id.cart_layout);


        }
    }

    public interface OnDishListener {
        void onDishClick(Dish dish);
        void onCartChanged();
        void onShowToast(int resID);
    }


}
