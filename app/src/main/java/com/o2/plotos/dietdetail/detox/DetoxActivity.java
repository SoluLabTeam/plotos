package com.o2.plotos.dietdetail.detox;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.cart.CartActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.dietdetail.DietDetailAdapter;
import com.o2.plotos.dishdetail.DishDetailActivity;
import com.o2.plotos.models.Dish;
import com.o2.plotos.models.Restaurant;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetoxActivity extends AppCompatActivity implements IDetoxView,
        SwipeRefreshLayout.OnRefreshListener, DietDetailAdapter.OnDishListener {

    @BindView(R.id.content_diet_swipeRefresh)
    SwipeRefreshLayout refreshDietLayout;
    @BindView(R.id.content_diet_recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.empty_view)
    LinearLayout emptyView;
    @BindView(R.id.diet_detail_img_cross)
    ImageButton imageCross;
    @BindView(R.id.diet_detail_image_cover)
    ImageView coverImage;
    @BindView(R.id.diet_detail_txt_title)
    TextView txtTitle;
    @BindView(R.id.detox_activity_decrptn)
    TextView descrptnTxt;
    @BindView(R.id.activity_diet_detail_category_name)
    TextView textCategory;
    @BindView(R.id.diet_detail_text)
    TextView textSelectCategory;
    @BindView(R.id.diet_detail_list_view)
    ListView listCategory;
    @BindView(R.id.diet_detait_drop_listcard)
    CardView cardDropDown;
    @BindView(R.id.diet_detail_image_drop_down)
    ImageView imgDropDown;
    @BindView(R.id.no_location_layout)
    RelativeLayout mNoLocationLayout;
    @BindView(R.id.no_data_title)
    TextView mNoDataTile;
    @BindView(R.id.no_data_message)
    TextView mNoDataMessage;
    @BindView(R.id.activity_home_btn_cart)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.activity_home_txt_item)
    TextView txtCartItem;

    private ProgressDialog mProgressDialog;
    CustomFonts customFonts;
    private List<DetoxOptions> detoxOptionList;

    public static final String EXTRA_DIET = "resturant_id";
    private int userSelection = 1;
    private Restaurant mResturant;
    private List<Dish> mDetoxList;

    private List<Dish> day1 = new ArrayList<>();
    private List<Dish> day3 = new ArrayList<>();
    private List<Dish> day5 = new ArrayList<>();
    private IDetoxPrsntr mDetoxPrsntr;
    private DietDetailAdapter mDietDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diet_detail);
        ButterKnife.bind(this);
        initializeView();
        mDetoxList = new ArrayList<>();
        mDetoxPrsntr = new DetoxPrsntrImpl();
        mDetoxPrsntr.bindView(this);

        detoxOptionList = new ArrayList<>();
        makeSpinner();
        setResturantDetail();
        getDishesList();
        refreshDietLayout.setOnRefreshListener(this);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DetoxActivity.this, CartActivity.class));
            }
        });
        setCartFab();
    }

    /**
     * Show cart item from database
     */
    public void setCartFab() {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        int cartItemSize = dataBaseHelper.getCartItems().size();
        if (cartItemSize > 0) {
            floatingActionButton.show();
            txtCartItem.setVisibility(View.VISIBLE);
            txtCartItem.setText("" + cartItemSize);
        } else {
            txtCartItem.setVisibility(View.GONE);
            floatingActionButton.hide();
        }
    }

    private void getDishesList() {
        if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
            String userLat  = UserPreferenceUtil.getInstance(this).getLocationLat();
            String userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
            if(!userLat.equals("0.0") && !userLong.equals("0.0")) {
                mDetoxPrsntr.sendDetoxDishRqust(mResturant.getId(), userLat, userLong);
            }else{
                Toast.makeText(this, getString(R.string.error_no_location), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setResturantDetail() {
        mResturant = getIntent().getExtras().getParcelable(EXTRA_DIET);
        textCategory.setText(mResturant.getName());
        if (!mResturant.getImage_url().equalsIgnoreCase("")) {
            Picasso.with(this).load(mResturant.getImage_url())
                    .resize(400, 200)
                    .centerCrop()
                    .error(R.drawable.placeholder)
                    .into(coverImage);
        } else {
            coverImage.setImageResource(R.drawable.placeholder);
        }
    }

    private void initializeView() {
        ButterKnife.bind(this);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait");
        coverImage.setImageResource(R.drawable.detox);
        customFonts = new CustomFonts(this);
        customFonts.setOswaldBold(txtTitle);
        customFonts.setOswaldBold(textCategory);
        customFonts.setOpenSansSemiBold(descrptnTxt);
        customFonts.setOpenSansBold(mNoDataTile);
        customFonts.setOpenSansRegulr(mNoDataMessage);
    }


    @OnClick(R.id.diet_detail_img_cross)
    public void closeScreen() {
        finish();
        //overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
    }*/

    @Override
    public void showProgress() {
        refreshDietLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        refreshDietLayout.setRefreshing(false);
    }

    @Override
    public void onGetDetoxDishSuccess(List<Dish> dishList) {
        if(dishList != null && dishList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            mNoLocationLayout.setVisibility(View.GONE);
            setAdapter(dishList);
        }else{
            mNoLocationLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGetDetoxDishFailed(String error) {
        mRecyclerView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        mNoLocationLayout.setVisibility(View.VISIBLE);
    }


    @Override
    public void onDishClick(Dish dish) {
        Intent intent = new Intent(DetoxActivity.this, DishDetailActivity.class);
        intent.putExtra(DishDetailActivity.EXTRA_DISH, dish);
        startActivity(intent);
        //DetoxActivity.this.overridePendingTransition(R.anim.slide_up, android.R.anim.fade_out);
    }

    @Override
    public void onCartChanged() {
        setCartFab();
    }

    @Override
    public void onShowToast(int resID) {
        Utils.showCartToast(this, resID);
    }

    @Override
    public void onRefresh() {
        if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
            day1.clear();
            day3.clear();
            day5.clear();
//            mDetoxPrsntr.sendDetoxDishRqust(mResturant.getId());
            getDishesList();
        }
    }

    public void makeSpinner() {
        DetoxOptions opt1 = new DetoxOptions();
        opt1.setTitle("Detox 1 Day");
        opt1.setDscrption(getString(R.string.detox_one_descrption));
        opt1.setSelectedOption(1);
        detoxOptionList.add(opt1);
        DetoxOptions opt2 = new DetoxOptions();
        opt2.setTitle("Detox 3 Days");
        opt2.setDscrption(getString(R.string.detox_three_descrption));
        opt2.setSelectedOption(3);
        detoxOptionList.add(opt2);
        DetoxOptions opt3 = new DetoxOptions();
        opt3.setTitle("Detox 5 Days");
        opt3.setDscrption(getString(R.string.detox_five_descrption));
        opt3.setSelectedOption(5);
        detoxOptionList.add(opt3);
        textSelectCategory.setText(detoxOptionList.get(0).getTitle());
        descrptnTxt.setText(detoxOptionList.get(0).getDscrption());
        txtTitle.setText(detoxOptionList.get(0).getTitle());

        final SpinnerCustomdapter adapter = new SpinnerCustomdapter();
        listCategory.setAdapter(adapter);

        imgDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cardDropDown.getVisibility() == View.GONE) {
                    cardDropDown.setVisibility(View.VISIBLE);
                    imgDropDown.setImageResource(R.drawable.ic_arrow_up);
                } else {
                    cardDropDown.setVisibility(View.GONE);
                    imgDropDown.setImageResource(R.drawable.ic_arrow_down);
                }

            }
        });
        listCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                textSelectCategory.setText(detoxOptionList.get(i).getTitle());
                descrptnTxt.setText(detoxOptionList.get(i).getDscrption());
                txtTitle.setText(detoxOptionList.get(i).getTitle());
                imgDropDown.setImageResource(R.drawable.ic_arrow_down);
                for (int j = 0; j < detoxOptionList.size(); j++) {
                    detoxOptionList.get(j).setSelected(false);
                }
                detoxOptionList.get(i).setSelected(true);
                adapter.notifyDataSetChanged();
                cardDropDown.setVisibility(View.GONE);
                userSelection = detoxOptionList.get(i).getSelectedOption();
                if (InternetUtil.getInstance(DetoxActivity.this).isNetWorkAvailable()) {
                    day1.clear();
                    day3.clear();
                    day5.clear();
//                    mDetoxPrsntr.sendDetoxDishRqust(mResturant.getId());
                    getDishesList();
                }
            }
        });
    }

    class DetoxOptions {
        private String title;
        private boolean isSelected;
        private int selectedOption;
        private String dscrption;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public int getSelectedOption() {
            return selectedOption;
        }

        public void setSelectedOption(int selectedOption) {
            this.selectedOption = selectedOption;
        }

        public String getDscrption() {
            return dscrption;
        }

        public void setDscrption(String dscrption) {
            this.dscrption = dscrption;
        }
    }


    class SpinnerCustomdapter extends BaseAdapter {
        LayoutInflater layoutInfalter;

        public SpinnerCustomdapter() {
            layoutInfalter = LayoutInflater.from(DetoxActivity.this);
        }

        @Override
        public int getCount() {
            return detoxOptionList.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            ViewHolder v;
            if (view == null) {
                view = layoutInfalter.inflate(R.layout.item_spinner_cal, viewGroup, false);
                v = new ViewHolder();
                v.title = (TextView) view.findViewById(R.id.item_cal_text_spinner);
                v.imageCheck = (ImageView) view.findViewById(R.id.item_cal_check_spinner);

                view.setTag(v);
            } else {
                v = (ViewHolder) view.getTag();
            }
            v.title.setText(detoxOptionList.get(i).getTitle());

            if (detoxOptionList.get(i).isSelected()) {
                v.imageCheck.setVisibility(View.VISIBLE);
            } else {
                v.imageCheck.setVisibility(View.INVISIBLE);
            }

            return view;
        }

        class ViewHolder {
            public TextView title;
            public ImageView imageCheck;
        }
    }

    public void setAdapter(List<Dish> dishList) {

        for (int i = 0; i < dishList.size(); i++) {
            if (dishList.get(i).getLevel().equalsIgnoreCase("1")) {
                day1.add(dishList.get(i));
            }
            if (dishList.get(i).getLevel().equalsIgnoreCase("3")) {
                day3.add(dishList.get(i));
            }
            if (dishList.get(i).getLevel().equalsIgnoreCase("5")) {
                day5.add(dishList.get(i));
            }
        }
        if (userSelection == 1) {
            mDetoxList = day1;
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mDietDetailAdapter = new DietDetailAdapter(mDetoxList, this, this);
            mRecyclerView.setAdapter(mDietDetailAdapter);
        }
        if (userSelection == 3) {
            mDetoxList = day3;
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mDietDetailAdapter = new DietDetailAdapter(mDetoxList, this, this);
            mRecyclerView.setAdapter(mDietDetailAdapter);
        }
        if (userSelection == 5) {
            mDetoxList = day5;
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mDietDetailAdapter = new DietDetailAdapter(mDetoxList, this, this);
            mRecyclerView.setAdapter(mDietDetailAdapter);
        }
    }
}