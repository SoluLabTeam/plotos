package com.o2.plotos.dietdetail.detox;

import com.o2.plotos.models.Dish;

import java.util.List;

/**
 * Created by Hassan on 14/02/2017.
 */

public class DetoxPrsntrImpl implements IDetoxPrsntr,IDetoxDataModel.OnGetDetoxDishRqustFinishLisener {

    IDetoxView iDetoxView;
    IDetoxDataModel mDetoxDataModl;

    public DetoxPrsntrImpl(){
        mDetoxDataModl = new DetoxDataModlImpl(this);
    }

    @Override
    public void OnGetDetoxDishSuccess(List<Dish> dishList) {
        if(iDetoxView!=null){
            iDetoxView.hideProgress();
            iDetoxView.onGetDetoxDishSuccess(dishList);
        }
    }

    @Override
    public void OnGetDetoxDishFailed(String error) {
        if(iDetoxView!=null){
            iDetoxView.hideProgress();
            iDetoxView.onGetDetoxDishFailed(error);
        }
    }

    @Override
    public void sendDetoxDishRqust(String restaurant_id, String latitude, String longitude) {
        if (iDetoxView != null) {
            iDetoxView.showProgress();
            mDetoxDataModl.getDetoxDishRqust(restaurant_id, latitude, longitude);
        }
    }

    @Override
    public void bindView(Object view) {
        iDetoxView = (IDetoxView) view;
    }

    @Override
    public void unBindView() {
        iDetoxView = null;
    }
}
