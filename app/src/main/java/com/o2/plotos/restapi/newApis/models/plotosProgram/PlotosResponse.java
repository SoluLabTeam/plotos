package com.o2.plotos.restapi.newApis.models.plotosProgram;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

import java.util.List;

public class PlotosResponse{

    @Expose
    @SerializedName("result")
    private List<PlotosResult> result;
    @Expose
    @SerializedName("response")
    private String response;

    public List<PlotosResult> getResult() {
        return result;
    }

    public void setResult(List<PlotosResult> result) {
        this.result = result;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
