package com.o2.plotos.restapi.newApis.models.promo;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

public class PromoResponse extends BaseApiModel {
    @SerializedName("discount")
    private Double discount;

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }
}
