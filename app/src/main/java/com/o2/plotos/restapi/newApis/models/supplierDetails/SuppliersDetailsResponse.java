package com.o2.plotos.restapi.newApis.models.supplierDetails;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;
import com.o2.plotos.restapi.newApis.models.grocery.SupplierResponse;

public class SuppliersDetailsResponse extends BaseApiModel {
    @SerializedName("result")
    private SupplierResponse supplierData;

    public SupplierResponse getSupplierData() {
        return supplierData;
    }

    public void setSupplierData(SupplierResponse supplierData) {
        this.supplierData = supplierData;
    }
}
