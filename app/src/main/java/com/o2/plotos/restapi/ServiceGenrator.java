package com.o2.plotos.restapi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.o2.plotos.utils.ConstantUtil.BASE_URL;

/**
 * Created by Hassan on 1/28/2017.
 */

public class ServiceGenrator {


//    private static OkHttpClient okHttpClient = new OkHttpClient();

    public static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(100000 , TimeUnit.SECONDS)
            .connectTimeout(100000 , TimeUnit.SECONDS)
            .build();


    static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson));

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(okHttpClient).build();
        return retrofit.create(serviceClass);
    }

}
