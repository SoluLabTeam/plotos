package com.o2.plotos.restapi.newApis.models.dishdetails;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.ItemDetais;

import java.util.List;

public class DishDetailsResult {
    @SerializedName("dishes")
    private List<ItemDetais> listDishDetails;
    @SerializedName("addons")
    private List<AddOns> listAddons;

    public List<ItemDetais> getListDishDetails() {
        return listDishDetails;
    }

    public void setListDishDetails(List<ItemDetais> listDishDetails) {
        this.listDishDetails = listDishDetails;
    }

    public List<AddOns> getListAddons() {
        return listAddons;
    }

    public void setListAddons(List<AddOns> listAddons) {
        this.listAddons = listAddons;
    }
}
