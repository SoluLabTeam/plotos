package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.DeleteOrderResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Rania on 4/3/2017.
 */
public interface DeleteOrderRequest {

    @GET(ConstantUtil.ApiUrl.Delete_ORDER)
    Call<DeleteOrderResponse> DeleteRequest(@Path("ORDER_ID") String order_id);
}
