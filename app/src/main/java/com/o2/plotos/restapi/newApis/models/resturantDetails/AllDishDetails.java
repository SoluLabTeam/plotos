package com.o2.plotos.restapi.newApis.models.resturantDetails;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.ItemDetais;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class AllDishDetails {
    @SerializedName("breakfast")
    List<ItemDetais> list_breakfast;
    @SerializedName("snacks")
    List<ItemDetais> list_snack;
    @SerializedName("appetizers")
    List<ItemDetais> list_appetizers;
    @SerializedName("mains")
    List<ItemDetais> listMains;
    @SerializedName("beverages")
    List<ItemDetais> list_beverages;

    public List<ItemDetais> getList_breakfast() {
        return list_breakfast;
    }

    public void setList_breakfast(List<ItemDetais> list_breakfast) {
        this.list_breakfast = list_breakfast;
    }

    public List<ItemDetais> getList_snack() {
        return list_snack;
    }

    public void setList_snack(List<ItemDetais> list_snack) {
        this.list_snack = list_snack;
    }

    public List<ItemDetais> getList_appetizers() {
        return list_appetizers;
    }

    public void setList_appetizers(List<ItemDetais> list_appetizers) {
        this.list_appetizers = list_appetizers;
    }

    public List<ItemDetais> getListMains() {
        return listMains;
    }

    public void setListMains(List<ItemDetais> listMains) {
        this.listMains = listMains;
    }

    public List<ItemDetais> getList_beverages() {
        return list_beverages;
    }

    public void setList_beverages(List<ItemDetais> list_beverages) {
        this.list_beverages = list_beverages;
    }
}
