package com.o2.plotos.restapi.newApis.models.resturantDetails;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

import java.util.List;

public class ResturandDetailsResponse extends BaseApiModel {

    @SerializedName("result")
    private List<ResturantDetailsResult> listResturantResult;

    public List<ResturantDetailsResult> getListResturantResult() {
        return listResturantResult;
    }

    public void setListResturantResult(List<ResturantDetailsResult> listResturantResult) {
        this.listResturantResult = listResturantResult;
    }
}
