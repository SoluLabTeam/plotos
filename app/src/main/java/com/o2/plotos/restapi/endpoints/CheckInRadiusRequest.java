package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.InRadiusResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by rony on 3/22/2017.
 */
public interface CheckInRadiusRequest {

    @GET(ConstantUtil.ApiUrl.GET_RESTAURANTS_IN_RADIUS)
    Call<InRadiusResponse> InRadiusRequest(
            @Path("REST_ID") String resturant_id, @Path("USER_LAT") String user_lat, @Path("USER_LNG") String user_lang
    );

    @GET(ConstantUtil.ApiUrl.GET_SUPPLIER_IN_RADIUS)
    Call<InRadiusResponse> InRadiusSupplierRequest(
            @Path("supplier_id") String supplierID, @Path("latitude") String user_lat, @Path("longitude") String user_lang
    );
}
