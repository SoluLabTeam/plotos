package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.VerifiedResponse;
import com.o2.plotos.restapi.responses.VerifyNumResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by snveiga on 14/03/17.
 */

public interface VerifiedRequest {
    @GET(ConstantUtil.ApiUrl.VERIFIED)
    Call<VerifiedResponse> update(
            @Path("user_id") String user_id
    );
}
