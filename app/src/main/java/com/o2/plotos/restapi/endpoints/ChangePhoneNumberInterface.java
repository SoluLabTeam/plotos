package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.ChnageMobileResponse;
import com.o2.plotos.restapi.responses.ForgotResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by dell on 1/30/2018.
 */

public interface ChangePhoneNumberInterface {

    @POST(ConstantUtil.ApiUrl.CHANGE_PHONE)
    Call<ChnageMobileResponse> ChangePhoneRequest(
            @Query("user_id") String user_id, @Query("phone") String phone);
}
