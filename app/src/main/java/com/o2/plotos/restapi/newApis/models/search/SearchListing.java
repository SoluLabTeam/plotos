package com.o2.plotos.restapi.newApis.models.search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchListing {
    @SerializedName("restaurants")
    private List<SearchData> list_restaurant;
    @SerializedName("dishes")
    private List<SearchData> list_dishes;
    @SerializedName("suppliers")
    private List<SearchData> list_suppliers;
    @SerializedName("program")
    private List<SearchData> list_program;
    @SerializedName("grocery")
    private List<SearchData> list_grocery;
    @SerializedName("signature")
    private List<SearchData> list_signature;

    public List<SearchData> getList_restaurant() {
        return list_restaurant;
    }

    public void setList_restaurant(List<SearchData> list_restaurant) {
        this.list_restaurant = list_restaurant;
    }

    public List<SearchData> getList_dishes() {
        return list_dishes;
    }

    public void setList_dishes(List<SearchData> list_dishes) {
        this.list_dishes = list_dishes;
    }

    public List<SearchData> getList_suppliers() {
        return list_suppliers;
    }

    public void setList_suppliers(List<SearchData> list_suppliers) {
        this.list_suppliers = list_suppliers;
    }

    public List<SearchData> getList_program() {
        return list_program;
    }

    public void setList_program(List<SearchData> list_program) {
        this.list_program = list_program;
    }

    public List<SearchData> getList_grocery() {
        return list_grocery;
    }

    public void setList_grocery(List<SearchData> list_grocery) {
        this.list_grocery = list_grocery;
    }

    public List<SearchData> getList_signature() {
        return list_signature;
    }

    public void setList_signature(List<SearchData> list_signature) {
        this.list_signature = list_signature;
    }
}
