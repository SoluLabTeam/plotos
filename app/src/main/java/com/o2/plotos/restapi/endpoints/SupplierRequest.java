package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.SupplierRequestResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by RANIA on 9/05/2017.
 */

public interface SupplierRequest {
    @GET(ConstantUtil.ApiUrl.GET_SUPPLIERS)
    Call<SupplierRequestResponse> supplierRequest(
            @Path("lat") String latitude,
            @Path("lng") String longitude
    );
}
