package com.o2.plotos.restapi.newApis.models.grocery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryResponse {

    @Expose
    @SerializedName("main_id")
    private String main_id;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("sub_category")
    private List<SubCategoryList> sub_category;

    public String getMain_id() {
        return main_id;
    }

    public void setMain_id(String main_id) {
        this.main_id = main_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SubCategoryList> getSub_category() {
        return sub_category;
    }

    public void setSub_category(List<SubCategoryList> sub_category) {
        this.sub_category = sub_category;
    }
}
