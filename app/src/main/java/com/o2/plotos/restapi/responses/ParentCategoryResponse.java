package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.ParentCategory;

import java.util.List;

/**
 * Created by Rania on 5/7/2017.
 */

public class ParentCategoryResponse {

    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
    @SerializedName("result")
    public List<ParentCategory> parentCategoryList;
}
