package com.o2.plotos.restapi.newApis.models.youMayAlsoLike;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

import java.util.ArrayList;
import java.util.List;

public class YouMayAlsoLikeResponse extends BaseApiModel {
    @SerializedName("result")
    private List<ItemDetais> listItems=new ArrayList<>();

    public List<ItemDetais> getListItems() {
        return listItems;
    }

    public void setListItems(List<ItemDetais> listItems) {
        this.listItems = listItems;
    }
}
