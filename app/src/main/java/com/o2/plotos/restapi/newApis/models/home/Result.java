package com.o2.plotos.restapi.newApis.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {
    @Expose
    @SerializedName("new_restaurent")
    private List<String> new_restaurent;
    @Expose
    @SerializedName("near_restaurent")
    private List<String> near_restaurent;
    @Expose
    @SerializedName("category")
    private List<String> category;
}
