package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.User;

/**
 * Created by Hassan on 1/28/2017.
 */

public class SignInRquestResponse {
    @SerializedName("response")
    public String response ;
    @SerializedName("msg")
    public String message;
    @SerializedName("verified")
    public String verified;
    @SerializedName("profile")
    public User user;
}
