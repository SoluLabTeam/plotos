package com.o2.plotos.restapi.newApis.models.orders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatingResponse {


    @Expose
    @SerializedName("result")
    private String result;
    @Expose
    @SerializedName("response")
    private String response;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
