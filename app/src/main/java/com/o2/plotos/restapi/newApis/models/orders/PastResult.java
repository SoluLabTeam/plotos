package com.o2.plotos.restapi.newApis.models.orders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.ItemDetais;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class PastResult {


    @Expose
    @SerializedName("scheduled_date")
    String scheduled_date;

    @Expose
    @SerializedName("status")
    String status;

    @Expose
    @SerializedName("supplier_name")
    String supplier_name;

    @Expose
    @SerializedName("supplier_image")
    String supplier_image;

    @Expose
    @SerializedName("restaurant_image")
    String restaurant_image;

    @Expose
    @SerializedName("restaurant_name")
    String restaurant_name;

    @Expose
    @SerializedName("user_number")
    String user_number;
    @Expose
    @SerializedName("user_address")
    String user_address;
    @Expose
    @SerializedName("user_longitude")
    String user_longitude;
    @Expose
    @SerializedName("user_latitude")
    String user_latitude;
    @Expose
    @SerializedName("receiver_name")
    String receiver_name;
    @Expose
    @SerializedName("user_id")
    String user_id;
    @Expose
    @SerializedName("add_on_id")
    String add_on_id;
    @Expose
    @SerializedName("delivery_id")
    String delivery_id;
    @Expose
    @SerializedName("parent_id")
    String parent_id;
    @Expose
    @SerializedName("item_id")
    String item_id;
    @Expose
    @SerializedName("dish_id")
    String dish_id;
    @Expose
    @SerializedName("supplier_id")
    String supplier_id;
    @Expose
    @SerializedName("restaurant_id")
    String restaurant_id;
    @Expose
    @SerializedName("order_id")
    String order_id;

    @Expose
    @SerializedName("child")
    List<ItemDetais> child;

    public String getScheduled_date() {
        return scheduled_date;
    }

    public void setScheduled_date(String scheduled_date) {
        this.scheduled_date = scheduled_date;
    }

    public String getStatus() {
        return status;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getSupplier_image() {
        return supplier_image;
    }

    public void setSupplier_image(String supplier_image) {
        this.supplier_image = supplier_image;
    }

    public String getRestaurant_image() {
        return restaurant_image;
    }

    public void setRestaurant_image(String restaurant_image) {
        this.restaurant_image = restaurant_image;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getUser_number() {
        return user_number;
    }

    public void setUser_number(String user_number) {
        this.user_number = user_number;
    }

    public String getUser_address() {
        return user_address;
    }

    public void setUser_address(String user_address) {
        this.user_address = user_address;
    }

    public String getUser_longitude() {
        return user_longitude;
    }

    public void setUser_longitude(String user_longitude) {
        this.user_longitude = user_longitude;
    }

    public String getUser_latitude() {
        return user_latitude;
    }

    public void setUser_latitude(String user_latitude) {
        this.user_latitude = user_latitude;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAdd_on_id() {
        return add_on_id;
    }

    public void setAdd_on_id(String add_on_id) {
        this.add_on_id = add_on_id;
    }

    public String getDelivery_id() {
        return delivery_id;
    }

    public void setDelivery_id(String delivery_id) {
        this.delivery_id = delivery_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getDish_id() {
        return dish_id;
    }

    public void setDish_id(String dish_id) {
        this.dish_id = dish_id;
    }

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public List<ItemDetais> getChild() {
        return child;
    }

    public void setChild(List<ItemDetais> child) {
        this.child = child;
    }
}
