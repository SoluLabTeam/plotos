package com.o2.plotos.restapi.newApis.models.grocery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

public class SupplierModel {

    @Expose
    @SerializedName("result")
    private List<SupplierResponse> result;
    @Expose
    @SerializedName("response")
    private String response;

    public List<SupplierResponse> getResult() {
        return result;
    }

    public void setResult(List<SupplierResponse> result) {
        this.result = result;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
