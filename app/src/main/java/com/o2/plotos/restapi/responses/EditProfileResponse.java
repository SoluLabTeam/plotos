package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rania on 4/27/2017.
 */

public class EditProfileResponse {

    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
    @SerializedName("user_id")
    public String user_id;
}
