package com.o2.plotos.restapi.newApis.models.register;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

public class RegisterResponse extends BaseApiModel {
    @SerializedName("result")
    private RegisterData registerData;

    public RegisterData getRegisterData() {
        return registerData;
    }

    public void setRegisterData(RegisterData registerData) {
        this.registerData = registerData;
    }
}
