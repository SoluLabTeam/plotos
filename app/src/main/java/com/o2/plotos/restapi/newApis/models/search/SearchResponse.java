package com.o2.plotos.restapi.newApis.models.search;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

public class SearchResponse extends BaseApiModel {

    @SerializedName("result")
    private SearchListing searchListing;

    public SearchListing getSearchListing() {
        return searchListing;
    }

    public void setSearchListing(SearchListing searchListing) {
        this.searchListing = searchListing;
    }
}
