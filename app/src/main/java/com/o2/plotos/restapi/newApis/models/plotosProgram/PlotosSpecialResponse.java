package com.o2.plotos.restapi.newApis.models.plotosProgram;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;
import com.o2.plotos.restapi.newApis.models.resturantDetails.AllDishDetails;

import org.parceler.Parcel;

@Parcel
public class PlotosSpecialResponse extends BaseApiModel {

    @SerializedName("result")
    AllDishDetails allDishDetails;

    public AllDishDetails getAllDishDetails() {
        return allDishDetails;
    }

    public void setAllDishDetails(AllDishDetails allDishDetails) {
        this.allDishDetails = allDishDetails;
    }
}
