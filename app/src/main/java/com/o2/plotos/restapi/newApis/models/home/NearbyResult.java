package com.o2.plotos.restapi.newApis.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class NearbyResult {


    @Expose
    @SerializedName("is_deleted")
     String is_deleted;
    @Expose
    @SerializedName("updated_on")
     String updated_on;
    @Expose
    @SerializedName("added_on")
     String added_on;
    @Expose
    @SerializedName("delivery_areas")
     String delivery_areas;
    @Expose
    @SerializedName("delivery_polygon")
     String delivery_polygon;
    @Expose
    @SerializedName("radius")
     String radius;
    @Expose
    @SerializedName("delivery_longitude")
     String delivery_longitude;
    @Expose
    @SerializedName("delivery_latitude")
     String delivery_latitude;
    @Expose
    @SerializedName("longitude")
     String longitude;
    @Expose
    @SerializedName("latitude")
     String latitude;
    @Expose
    @SerializedName("restaurant_description")
     String restaurant_description;
    @Expose
    @SerializedName("end_price")
     String end_price;
    @Expose
    @SerializedName("start_price")
     String start_price;
    @Expose
    @SerializedName("days_schedule")
     String days_schedule;
    @Expose
    @SerializedName("days_open")
     String days_open;
    @Expose
    @SerializedName("delivery_time")
     String delivery_time;
    @Expose
    @SerializedName("pre_booking")
     String pre_booking;
    @Expose
    @SerializedName("closing_time")
     String closing_time;
    @Expose
    @SerializedName("opening_time")
     String opening_time;
    @Expose
    @SerializedName("delivery_time_end")
     String delivery_time_end;
    @Expose
    @SerializedName("delivery_time_start")
     String delivery_time_start;
    @Expose
    @SerializedName("delivery_path")
     String delivery_path;
    @Expose
    @SerializedName("plotos_delivery")
     String plotos_delivery;
    @Expose
    @SerializedName("vat")
     String vat;
    @Expose
    @SerializedName("commission")
     String commission;
    @Expose
    @SerializedName("cash_only")
     String cash_only;
    @Expose
    @SerializedName("min_order_amount")
     String min_order_amount;
    @Expose
    @SerializedName("banner_image")
     String banner_image;
    @Expose
    @SerializedName("image")
     String image;
    @Expose
    @SerializedName("address")
     String address;
    @Expose
    @SerializedName("is_new")
     String is_new;
    @Expose
    @SerializedName("status")
     String status;
    @Expose
    @SerializedName("number")
     String number;
    @Expose
    @SerializedName("email")
     String email;
    @Expose
    @SerializedName("last_name")
     String last_name;
    @Expose
    @SerializedName("first_name")
     String first_name;
    @Expose
    @SerializedName("restaurant_name")
     String restaurant_name;
    @Expose
    @SerializedName("user_id")
     String user_id;
    @Expose
    @SerializedName("restaurant_id")
     String restaurant_id;

    @Expose
    @SerializedName("dishes")
     List<DishDetail> dishes;

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getDelivery_areas() {
        return delivery_areas;
    }

    public void setDelivery_areas(String delivery_areas) {
        this.delivery_areas = delivery_areas;
    }

    public String getDelivery_polygon() {
        return delivery_polygon;
    }

    public void setDelivery_polygon(String delivery_polygon) {
        this.delivery_polygon = delivery_polygon;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getDelivery_longitude() {
        return delivery_longitude;
    }

    public void setDelivery_longitude(String delivery_longitude) {
        this.delivery_longitude = delivery_longitude;
    }

    public String getDelivery_latitude() {
        return delivery_latitude;
    }

    public void setDelivery_latitude(String delivery_latitude) {
        this.delivery_latitude = delivery_latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getRestaurant_description() {
        return restaurant_description;
    }

    public void setRestaurant_description(String restaurant_description) {
        this.restaurant_description = restaurant_description;
    }

    public String getEnd_price() {
        return end_price;
    }

    public void setEnd_price(String end_price) {
        this.end_price = end_price;
    }

    public String getStart_price() {
        return start_price;
    }

    public void setStart_price(String start_price) {
        this.start_price = start_price;
    }

    public String getDays_schedule() {
        return days_schedule;
    }

    public void setDays_schedule(String days_schedule) {
        this.days_schedule = days_schedule;
    }

    public String getDays_open() {
        return days_open;
    }

    public void setDays_open(String days_open) {
        this.days_open = days_open;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getPre_booking() {
        return pre_booking;
    }

    public void setPre_booking(String pre_booking) {
        this.pre_booking = pre_booking;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getDelivery_time_end() {
        return delivery_time_end;
    }

    public void setDelivery_time_end(String delivery_time_end) {
        this.delivery_time_end = delivery_time_end;
    }

    public String getDelivery_time_start() {
        return delivery_time_start;
    }

    public void setDelivery_time_start(String delivery_time_start) {
        this.delivery_time_start = delivery_time_start;
    }

    public String getDelivery_path() {
        return delivery_path;
    }

    public void setDelivery_path(String delivery_path) {
        this.delivery_path = delivery_path;
    }

    public String getPlotos_delivery() {
        return plotos_delivery;
    }

    public void setPlotos_delivery(String plotos_delivery) {
        this.plotos_delivery = plotos_delivery;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getCash_only() {
        return cash_only;
    }

    public void setCash_only(String cash_only) {
        this.cash_only = cash_only;
    }

    public String getMin_order_amount() {
        return min_order_amount;
    }

    public void setMin_order_amount(String min_order_amount) {
        this.min_order_amount = min_order_amount;
    }

    public String getBanner_image() {
        return banner_image;
    }

    public void setBanner_image(String banner_image) {
        this.banner_image = banner_image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIs_new() {
        return is_new;
    }

    public void setIs_new(String is_new) {
        this.is_new = is_new;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public List<DishDetail> getDishes() {
        return dishes;
    }

    public void setDishes(List<DishDetail> dishes) {
        this.dishes = dishes;
    }
}
