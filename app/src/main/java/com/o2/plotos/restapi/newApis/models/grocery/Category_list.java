package com.o2.plotos.restapi.newApis.models.grocery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class Category_list {
    @Expose
    @SerializedName("image_url")
    String image_url;
    @Expose
    @SerializedName("main_id")
    String main_id;
    @Expose
    @SerializedName("name")
    String name;

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getMain_id() {
        return main_id;
    }

    public void setMain_id(String main_id) {
        this.main_id = main_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
