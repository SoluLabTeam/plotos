package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.EditProfileResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by RANIA on 42/27/2017.
 */

public interface EditProfileRequest {
    @FormUrlEncoded
    @POST(ConstantUtil.ApiUrl.Edit_PROFILE)
    Call<EditProfileResponse> editProfile(@Field("profile_object") String jsonObject);
}
