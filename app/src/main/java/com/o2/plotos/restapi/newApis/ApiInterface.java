package com.o2.plotos.restapi.newApis;


import com.o2.plotos.models.Cart;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

import com.o2.plotos.restapi.newApis.models.NearByRestaurant.NearByResponse;
import com.o2.plotos.restapi.newApis.models.bannerImages.BannerImageResponse;
import com.o2.plotos.restapi.newApis.models.dishdetails.DishDetailsResponse;
import com.o2.plotos.restapi.newApis.models.editProfile.EditProfileModel;
import com.o2.plotos.restapi.newApis.models.editProfile.EditProfileResponse;
import com.o2.plotos.restapi.newApis.models.grocery.CategoryModel;
import com.o2.plotos.restapi.newApis.models.grocery.DishDetailModel;
import com.o2.plotos.restapi.newApis.models.grocery.SupplierModel;

import com.o2.plotos.restapi.newApis.models.home.HomeModel;

import com.o2.plotos.restapi.newApis.models.groceryDetails.GroceryDetailsResponse;

import com.o2.plotos.restapi.newApis.models.login.LoginResponce;
import com.o2.plotos.restapi.newApis.models.orders.OrderModel;
import com.o2.plotos.restapi.newApis.models.orders.OrdersResponse;
import com.o2.plotos.restapi.newApis.models.orders.RatingResponse;
import com.o2.plotos.restapi.newApis.models.plotosProgram.PlotosResponse;

import com.o2.plotos.restapi.newApis.models.plotosProgram.PlotosSpecialDetoxResponse;
import com.o2.plotos.restapi.newApis.models.plotosProgram.PlotosSpecialResponse;
import com.o2.plotos.restapi.newApis.models.promo.PromoResponse;
import com.o2.plotos.restapi.newApis.models.resturantDetails.ResturandDetailsResponse;

import com.o2.plotos.restapi.newApis.models.search.SearchResponse;
import com.o2.plotos.restapi.newApis.models.submitQuery.SubmitQueryResponse;
import com.o2.plotos.restapi.newApis.models.register.RegisterResponse;
import com.o2.plotos.restapi.newApis.models.supplierDetails.SuppliersDetailsResponse;
import com.o2.plotos.restapi.newApis.models.youMayAlsoLike.YouMayAlsoLikeResponse;
import com.o2.plotos.restapi.responses.SignUpRequestResponse;

import java.util.Map;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;


public interface ApiInterface {

    @POST("Usermaster/sign_in")
    @FormUrlEncoded
    Call<LoginResponce> getLoginResponse(@Field("email") String email, @Field("password") String password);

    @Multipart
    @POST("Usermaster/profileEdit")
    Call<EditProfileModel> getEditProfileResponse(
            @PartMap() Map<String, RequestBody> partMap
    );

    @Multipart
    @POST("Usermaster/profileEdit")
    Call<EditProfileModel> getEditProfileResponse(
            @PartMap() Map<String, RequestBody> partMap,
            @Part MultipartBody.Part file1
    );

    @GET("Usermaster/appQuery/{user_id}/{querytext}")
    Call<SubmitQueryResponse> callSubmitQueryResponse(@Path("user_id") String id, @Path("querytext") String query);

    @POST("Usermaster/sign_out/{user_id}/{token}")
    Call<BaseApiModel> callLogout(@Path("user_id") String id, @Path("token") String token);


    @POST("Usermaster/createUser")
    @FormUrlEncoded
    Call<RegisterResponse> doRegister(@FieldMap() Map<String, String> map);

    @POST("Usermaster/ForgetPassword")
    @FormUrlEncoded
    Call<BaseApiModel> doForgotPass(@Field("email") String email);

    @POST("Bannermaster/getBannerList/{latitude}/{longitude}")
    Call<BannerImageResponse> callGetBanner(@Path("latitude") String latitude,
                                            @Path("longitude") String longitude);


    @POST("Usermaster/verifyCode")
    @FormUrlEncoded
    Call<LoginResponce> verifyOTP(@Field("code") String code, @Field("user_id") String userid);

    @POST("Usermaster/mobileVerification")
    @FormUrlEncoded
    Call<BaseApiModel> getOTP(@Field("number") String number, @Field("user_id") String userid);

    @POST("Usermaster/phoneNumber")
    @FormUrlEncoded
    Call<BaseApiModel> changeMobile(@Field("number") String number, @Field("user_id") String userid);

    @POST("Searchmaster/search")
    @FormUrlEncoded
    Call<SearchResponse> getSearchResults(@Field("searchkey") String text, @Field("latitude") String lat, @Field("longitude") String lon);


    @POST("Supplier/getSuppliers/{id}/{latitude}/{longitude}")
    Call<SupplierModel> callSupplierList(
            @Path("id") String id,
            @Path("latitude") String latitude,
            @Path("longitude") String longitude);

    @POST("Supplier/getCategary/{latitude}/{longitude}")
    Call<CategoryModel> callCatList(
            @Path("latitude") String latitude,
            @Path("longitude") String longitude);

    @POST("Supplier/getSubcategoryItem/{suppid}/{mainid}/{latitude}/{longitude}")
    Call<DishDetailModel> callSupplierDishList(
            @Path("suppid") String suppid,
            @Path("mainid") String mainid,
            @Path("latitude") String latitude,
            @Path("longitude") String longitude);

    @POST("Homemaster/getCategories/{id}/{idc}/{latitude}/{longitude}")
    Call<HomeModel> callHomeList(
            @Path("id") String id,
            @Path("idc") String idc,
            @Path("latitude") String latitude,
            @Path("longitude") String longitude);

    @GET("Supplier/getItem/{id}")
    Call<GroceryDetailsResponse> getGroceryDetails(@Path("id") String id);

    @GET("Restaurantmaster/getDishById/{id}")
    Call<DishDetailsResponse> getDishDetails(@Path("id") String id);

    @GET("Homemaster/getProgramList")
    Call<PlotosResponse> getPlotosProgram();

    @POST("Restaurantmaster/get_restaurants/{id}/{latitude}/{longitude}")
    Call<NearByResponse> callNearbyRestaurantList(
            @Path("id") String id,
            @Path("latitude") String latitude,
            @Path("longitude") String longitude);

    @GET("Restaurantmaster/get_restaurants/{id}/{latitude}/{longitude}")
    Call<ResturandDetailsResponse> getResturandDetails(
            @Path("id") String idc,
            @Path("latitude") String latitude,
            @Path("longitude") String longitude);

    @GET("Restaurantmaster/getPlotosSpecial/{latitude}/{longitude}")
    Call<PlotosSpecialResponse> callPlotosSignature(
            @Path("latitude") String latitude,
            @Path("longitude") String longitude);


    @GET("Homemaster/programDish/{id}/{latitude}/{longitude}")
    Call<PlotosSpecialResponse> callPlotosSpecial(
            @Path("id") String id,
            @Path("latitude") String latitude,
            @Path("longitude") String longitude);

    @GET("Homemaster/programDish/{id}/{latitude}/{longitude}")
    Call<PlotosSpecialDetoxResponse> callPlotosSpecialDetox(
            @Path("id") String id,
            @Path("latitude") String latitude,
            @Path("longitude") String longitude);


    @GET("Supplier/getSupplierCategory/{id}")
    Call<SuppliersDetailsResponse> getSupplierDetails(
            @Path("id") String id);

    @GET("Ordermaster/getOrder/{user_id}/{order_id}")
    Call<OrderModel> getOrders(
            @Path("user_id") String user_id, @Path("order_id") String order_id);

    @GET("Restaurantmaster/rating/{user_id}/{restaurent_id}/{rating}")
    Call<RatingResponse> callRatingApi(
            @Path("user_id") String user_id,
            @Path("restaurent_id") String restaurent_id,
            @Path("rating") String rating);

    @POST("Restaurantmaster/alsoLikeDishes")
    @FormUrlEncoded
    Call<YouMayAlsoLikeResponse> getSimilarProducts(@FieldMap() Map<String, String> map);

    @POST("Usermaster/checkuseremail")
    @FormUrlEncoded
    Call<LoginResponce> getUserIsExist(@FieldMap() Map<String, String> map);

    @POST("Ordermaster/place_order")
    @FormUrlEncoded
    Call<BaseApiModel> doPlaceorder(@Field("order_object") String order_object);

    @GET("Ordermaster/check_code/{code}/{user_id}/{amount}/0/0")
    Call<PromoResponse> applyPromo(
            @Path("code") String code,
            @Path("user_id") String user_id,
            @Path("amount") String amount);

    @GET("Usermaster/deleteUser/{user_id}")
    Call<BaseApiModel> deleteAccount(
            @Path("user_id") String user_id);
}
