package com.o2.plotos.restapi.newApis.models.groceryDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroceryDetails {

    @Expose
    @SerializedName("itemId")
    private String itemId;
    @Expose
    @SerializedName("updated_on")
    private String updated_on;
    @Expose
    @SerializedName("added_on")
    private String added_on;
    @Expose
    @SerializedName("is_deleted")
    private String is_deleted;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("views")
    private String views;
    @Expose
    @SerializedName("other_details")
    private String other_details;
    @Expose
    @SerializedName("ingredients")
    private String ingredients;
    @Expose
    @SerializedName("custom_delivery")
    private String custom_delivery;
    @Expose
    @SerializedName("delivery_time")
    private String delivery_time;
    @Expose
    @SerializedName("min_qty")
    private String min_qty;
    @Expose
    @SerializedName("qty_unit")
    private String qty_unit;
    @Expose
    @SerializedName("per_unit")
    private String per_unit;
    @Expose
    @SerializedName("currency")
    private String currency;
    @Expose
    @SerializedName("price")
    private String price;
    @Expose
    @SerializedName("image")
    private String image;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("special_offer")
    private String special_offer;
    @Expose
    @SerializedName("nutritional_info")
    private String nutritional_info;
    @Expose
    @SerializedName("availability")
    private String availability;
    @Expose
    @SerializedName("level")
    private String level;
    @Expose
    @SerializedName("program")
    private String program;
    @Expose
    @SerializedName("meal")
    private String meal;
    @Expose
    @SerializedName("user_id")
    private String user_id;
    @Expose
    @SerializedName("category_id")
    private String category_id;
    @Expose
    @SerializedName("supplier_id")
    private String supplier_id;
    @Expose
    @SerializedName("id")
    private String id;

    private int quantity = 0;
    private Double totalPrice = 0.0;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getOther_details() {
        return other_details;
    }

    public void setOther_details(String other_details) {
        this.other_details = other_details;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getCustom_delivery() {
        return custom_delivery;
    }

    public void setCustom_delivery(String custom_delivery) {
        this.custom_delivery = custom_delivery;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getMin_qty() {
        return min_qty;
    }

    public void setMin_qty(String min_qty) {
        this.min_qty = min_qty;
    }

    public String getQty_unit() {
        return qty_unit;
    }

    public void setQty_unit(String qty_unit) {
        this.qty_unit = qty_unit;
    }

    public String getPer_unit() {
        return per_unit;
    }

    public void setPer_unit(String per_unit) {
        this.per_unit = per_unit;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecial_offer() {
        return special_offer;
    }

    public void setSpecial_offer(String special_offer) {
        this.special_offer = special_offer;
    }

    public String getNutritional_info() {
        return nutritional_info;
    }

    public void setNutritional_info(String nutritional_info) {
        this.nutritional_info = nutritional_info;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
