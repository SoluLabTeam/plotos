package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.Card;

/**
 * Created by Hassan on 2/16/2017.
 */

public class PlaceOrderResponse {
    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
    @SerializedName("order_id")
    public String orderId;
    @SerializedName("error_message")
    public String error_message;
    @SerializedName("card")
    public Card card;
}
