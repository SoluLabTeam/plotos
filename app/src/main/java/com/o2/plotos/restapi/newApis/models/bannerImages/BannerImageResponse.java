package com.o2.plotos.restapi.newApis.models.bannerImages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

import java.util.List;

public class BannerImageResponse extends BaseApiModel{

    @Expose
    @SerializedName("result")
    List<BannerImageResult> bannerImageResults;

    public List<BannerImageResult> getBannerImageResults() {
        return bannerImageResults;
    }

    public void setBannerImageResults(List<BannerImageResult> bannerImageResults) {
        this.bannerImageResults = bannerImageResults;
    }
}
