package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.SignInRquestResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Hassan on 1/28/2017.
 */

public interface SignInRequest {
    @GET(ConstantUtil.ApiUrl.SIGN_IN)
    Call<SignInRquestResponse> signInRequest(
            @Query("email") String email,
            @Query("password") String password,
            @Query("device_token") String  deviceToken);
}
