package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.SubCategoryResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Rania on 7/05/2017.
 */

public interface SubCategoriesRequest {
    @GET (ConstantUtil.ApiUrl.GET_SUB_CATEGORY)
    Call<SubCategoryResponse> getSubCategoriesRequest(
            @Path("parent_id") String parentId, @Path("supplier_id") String supplierId
    );
}
