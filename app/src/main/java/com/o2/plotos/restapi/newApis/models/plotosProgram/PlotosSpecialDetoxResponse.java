package com.o2.plotos.restapi.newApis.models.plotosProgram;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;
import com.o2.plotos.restapi.newApis.models.resturantDetails.AllDishDetails;

import org.parceler.Parcel;

import java.util.List;
import java.util.Map;

@Parcel
public class PlotosSpecialDetoxResponse extends BaseApiModel {

    @SerializedName("result")
    Map<String, List<ItemDetais>> allDishDetails;
//    AllDishDetails allDishDetails;

//    public AllDishDetails getAllDishDetails() {
//        return allDishDetails;
//    }
//
//    public void setAllDishDetails(AllDishDetails allDishDetails) {
//        this.allDishDetails = allDishDetails;
//    }


    public Map<String, List<ItemDetais>> getAllDishDetails() {
        return allDishDetails;
    }

    public void setAllDishDetails(Map<String, List<ItemDetais>> allDishDetails) {
        this.allDishDetails = allDishDetails;
    }
}
