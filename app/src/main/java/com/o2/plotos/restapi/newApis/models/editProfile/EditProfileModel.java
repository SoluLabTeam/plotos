package com.o2.plotos.restapi.newApis.models.editProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;
import com.o2.plotos.restapi.newApis.models.login.UserData;

import java.util.List;

public class EditProfileModel extends BaseApiModel {


    @Expose
    @SerializedName("user_data")
    UserData user_data;

    public UserData getUser_data() {
        return user_data;
    }

    public void setUser_data(UserData user_data) {
        this.user_data = user_data;
    }
}
