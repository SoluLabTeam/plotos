package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.Dish;

import java.util.List;

/**
 * Created by Hassan on 8/02/2017.
 */

public class DishesRequestResponse {
    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
    @SerializedName("result")
    public List<Dish> dish;
}
