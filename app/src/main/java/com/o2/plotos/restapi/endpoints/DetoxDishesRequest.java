package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.DetoxDshesRequestResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Hassan on 14/02/2017.
 */

public interface DetoxDishesRequest {
    @GET(ConstantUtil.ApiUrl.GET_DETOX_DISHES)
    Call<DetoxDshesRequestResponse> dishesRequest(
            @Path("restaurant_id") String restrnt_id,
            @Path("lat") String latitude,
            @Path("lng") String longitude
    );
}
