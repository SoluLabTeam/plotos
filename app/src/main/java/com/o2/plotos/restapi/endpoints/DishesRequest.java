package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.DishesRequestResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Hassan on 8/02/2017.
 */

public interface DishesRequest {
    @GET (ConstantUtil.ApiUrl.GET_DISHES)
    Call<DishesRequestResponse> dishesRequest(
            @Path("type") String type,
            @Path("restaurant_id") String restrnt_id,
            @Path("category") String category_id,
            @Path("calorie") String calorie,
            @Path("lat") String latitude,
            @Path("lng") String longitude
    );
}
