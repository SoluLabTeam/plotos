package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.GroceryResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Rania on 7/05/2017.
 */

public interface GroceryByCategoryRequest {
    @GET (ConstantUtil.ApiUrl.GET_GROCERY_BY_CATEGORY)
    Call<GroceryResponse> getGroceryByCategoryRequest(
            @Path("category_id") String parentId,
            @Path("supplier_id") String supplierId,
            @Path("lat") String latitude,
            @Path("lng") String longitude
    );
}
