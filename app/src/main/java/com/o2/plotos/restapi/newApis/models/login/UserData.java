package com.o2.plotos.restapi.newApis.models.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserData {


    @Expose
    @SerializedName("image")
    private String image;
    @Expose
    @SerializedName("email")
    private String email;
//    @Expose
//    @SerializedName("device_token_id")
//    private String device_token_id;
    @Expose
    @SerializedName("verified")
    private String verified;
    @Expose
    @SerializedName("delivery_location")
    private String delivery_location;
    @Expose
    @SerializedName("number")
    private String number;
    @Expose
    @SerializedName("last_name")
    private String last_name;
    @Expose
    @SerializedName("first_name")
    private String first_name;
    @Expose
    @SerializedName("user_id")
    private String user_id;
//    @Expose
//    @SerializedName("address")
//    private List<Address> list_address;

//    public List<Address> getList_address() {
//        return list_address;
//    }
//
//    public void setList_address(List<Address> list_address) {
//        this.list_address = list_address;
//    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    public String getDevice_token_id() {
//        return device_token_id;
//    }
//
//    public void setDevice_token_id(String device_token_id) {
//        this.device_token_id = device_token_id;
//    }



    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public String getDelivery_location() {
        return delivery_location;
    }

    public void setDelivery_location(String delivery_location) {
        this.delivery_location = delivery_location;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
