package com.o2.plotos.restapi.newApis.models.login;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;


public class LoginResponce extends BaseApiModel {
    @SerializedName("profile")
    private UserData userData;

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }
}
