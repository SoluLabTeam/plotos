package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.RestaurantRquestResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Hassan on 31/01/2017.
 */

public interface RestaurantRequest {
    @GET(ConstantUtil.ApiUrl.GET_RESTAURANTS)
    Call<RestaurantRquestResponse> restaurantResquest(
            @Path("resturant_type") String resturant_type,
            @Path("lat") String latitude,
            @Path("lng") String longitude
    );
}
