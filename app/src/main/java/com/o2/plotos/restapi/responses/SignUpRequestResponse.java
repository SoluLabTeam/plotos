package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.User;

/**
 * Created by Hassan on 30/01/2017.
 */

public class SignUpRequestResponse {
    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
    @SerializedName("verified")
    public String verified;
    @SerializedName("user_id")
    public String user_id;
    @SerializedName("profile")
    public User user;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
