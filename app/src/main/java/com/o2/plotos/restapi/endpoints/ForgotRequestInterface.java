package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.BannerRequestResponse;
import com.o2.plotos.restapi.responses.ForgotResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by dell on 12/21/2017.
 */

public interface ForgotRequestInterface {

    @POST(ConstantUtil.ApiUrl.FORGOT_EMAIL)
    Call<ForgotResponse> ForgotRequest(
            @Query("email") String email);
}
