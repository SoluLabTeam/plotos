package com.o2.plotos.restapi.responses;

/**
 * Created by dell on 12/26/2017.
 */

public class CheckUserEmailResponse {


        private String response;

        private String login_required;

        private String user_id;

        private String msg;

        private NewProfile profile;

    public String getResponse ()
    {
        return response;
    }

    public void setResponse (String response)
    {
        this.response = response;
    }

    public String getLogin_required ()
    {
        return login_required;
    }

    public void setLogin_required (String login_required)
    {
        this.login_required = login_required;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getMsg ()
    {
        return msg;
    }

    public void setMsg (String msg)
    {
        this.msg = msg;
    }

    public NewProfile getProfile ()
    {
        return profile;
    }

    public void setProfile (NewProfile profile)
    {
        this.profile = profile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [response = "+response+", login_required = "+login_required+", user_id = "+user_id+", msg = "+msg+", profile = "+profile+"]";
    }
}
