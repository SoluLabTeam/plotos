package com.o2.plotos.restapi.newApis.models.plotosProgram;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlotosResult {


    @Expose
    @SerializedName("updated_on")
    private String updated_on;
    @Expose
    @SerializedName("added_on")
    private String added_on;
    @Expose
    @SerializedName("is_deleted")
    private String is_deleted;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("image")
    private String image;
    @Expose
    @SerializedName("parent_id")
    private String parent_id;
    @Expose
    @SerializedName("program_description")
    private String program_description;
    @Expose
    @SerializedName("program_name")
    private String program_name;
    @Expose
    @SerializedName("program_id")
    private String program_id;


    public PlotosResult(String parent_id, String program_name, String program_id) {
        this.parent_id = parent_id;
        this.program_name = program_name;
        this.program_id = program_id;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getProgram_description() {
        return program_description;
    }

    public void setProgram_description(String program_description) {
        this.program_description = program_description;
    }

    public String getProgram_name() {
        return program_name;
    }

    public void setProgram_name(String program_name) {
        this.program_name = program_name;
    }

    public String getProgram_id() {
        return program_id;
    }

    public void setProgram_id(String program_id) {
        this.program_id = program_id;
    }
}
