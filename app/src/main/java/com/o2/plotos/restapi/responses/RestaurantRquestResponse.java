package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.Restaurant;

import java.util.List;

/**
 * Created by Hassan on 31/01/2017.
 */

public class RestaurantRquestResponse {
    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
    @SerializedName("result")
    public List<Restaurant> restaurant;
}
