package com.o2.plotos.restapi.newApis.models.grocery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class SupplierResponse {

    @Expose
    @SerializedName(value = "cat_list", alternate = {"category_list", "catagories"})
    List<Category_list> category_list;
    @Expose
    @SerializedName("supplier_description")
    String supplier_description;
    @Expose
    @SerializedName("end_price")
    String end_price;
    @Expose
    @SerializedName("start_price")
    String start_price;
    @Expose
    @SerializedName("pre_booking")
    String pre_booking;
    @Expose
    @SerializedName("min_order_amount")
    String min_order_amount;
    @Expose
    @SerializedName("image")
    String image;
    @Expose
    @SerializedName("supplier_name")
    String supplier_name;
    @Expose
    @SerializedName("user_id")
    String user_id;
    @Expose
    @SerializedName("supplier_id")
    String supplier_id;

    @Expose
    @SerializedName("opening_time")
    String opening_time;
    @Expose
    @SerializedName("banner_image")
    String banner_image;

    public String getBanner_image() {
        return banner_image;
    }

    public void setBanner_image(String banner_image) {
        this.banner_image = banner_image;
    }

    @Expose
    @SerializedName("closing_time")
    String closing_time;


    public List<Category_list> getCategory_list() {
        return category_list;
    }

    public void setCategory_list(List<Category_list> category_list) {
        this.category_list = category_list;
    }

    public String getSupplier_description() {
        return supplier_description;
    }

    public void setSupplier_description(String supplier_description) {
        this.supplier_description = supplier_description;
    }

    public String getEnd_price() {
        return end_price;
    }

    public void setEnd_price(String end_price) {
        this.end_price = end_price;
    }

    public String getStart_price() {
        return start_price;
    }

    public void setStart_price(String start_price) {
        this.start_price = start_price;
    }

    public String getPre_booking() {
        return pre_booking;
    }

    public void setPre_booking(String pre_booking) {
        this.pre_booking = pre_booking;
    }

    public String getMin_order_amount() {
        return min_order_amount;
    }

    public void setMin_order_amount(String min_order_amount) {
        this.min_order_amount = min_order_amount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }
}
