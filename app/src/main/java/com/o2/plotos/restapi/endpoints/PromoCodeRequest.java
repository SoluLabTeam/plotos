package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.PromoCodeResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by snveiga on 16/03/17.
 */

public interface PromoCodeRequest {
    @GET(ConstantUtil.ApiUrl.PROMO_CODE)
    Call<PromoCodeResponse> check_code(
            @Path("code") String restrnt_id,
            @Path("user_id") String user_id,
            @Path("amount") int amount

    );
}
