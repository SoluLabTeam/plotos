package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.BannerRequestResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Hassan on 31/01/2017.
 */

public interface BannerRequest {
    @GET(ConstantUtil.ApiUrl.GET_BANNERS)
    Call<BannerRequestResponse> bannerRequest();
}
