package com.o2.plotos.restapi.newApis.models.grocery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryModel {

    @Expose
    @SerializedName("result")
    private List<CategoryResponse> result;
    @Expose
    @SerializedName("response")
    private String response;

    public List<CategoryResponse> getResult() {
        return result;
    }

    public void setResult(List<CategoryResponse> result) {
        this.result = result;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
