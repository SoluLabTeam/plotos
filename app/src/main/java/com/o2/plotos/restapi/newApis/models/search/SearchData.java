package com.o2.plotos.restapi.newApis.models.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchData {

    @Expose
    @SerializedName("search_item_image")
    private String search_item_image;
    @Expose
    @SerializedName("search_item_name")
    private String search_item_name;
    @Expose
    @SerializedName("search_item_id")
    private String search_item_id;

    public String getSearch_item_image() {
        return search_item_image;
    }

    public void setSearch_item_image(String search_item_image) {
        this.search_item_image = search_item_image;
    }

    public String getSearch_item_name() {
        return search_item_name;
    }

    public void setSearch_item_name(String search_item_name) {
        this.search_item_name = search_item_name;
    }

    public String getSearch_item_id() {
        return search_item_id;
    }

    public void setSearch_item_id(String search_item_id) {
        this.search_item_id = search_item_id;
    }
}
