package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.DishesRequestResponse;
import com.o2.plotos.restapi.responses.GroceryResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Hassan on 8/02/2017.
 */

public interface OfferDataRequest {
    @GET (ConstantUtil.ApiUrl.GET_OFFER_DISH_BY_CATEGORY)
    Call<DishesRequestResponse> offerDishesRequest(
            @Path("child_category_id") String categoryID,
            @Path("lat") String latitude,
            @Path("lng") String longitude
    );

    @GET (ConstantUtil.ApiUrl.GET_OFFER_GROCERY_BY_CATEGORY)
    Call<GroceryResponse> offerGroceryRequest(
            @Path("child_category_id") String categoryID,
            @Path("lat") String latitude,
            @Path("lng") String longitude
    );
}
