package com.o2.plotos.restapi.endpoints;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rania on 4/4/2017.
 */
public class EditOrderGroceryRequestObj {

    @SerializedName("order_id")
    private String order_id ;

    @SerializedName("add_on_id")
    private String addOnId;

    @SerializedName("price")
    private String dishPrice;

    @SerializedName("item_order_count")
    private String itemOrderCount;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDishPrice() {
        return dishPrice;
    }

    public void setDishPrice(String dishPrice) {
        this.dishPrice = dishPrice;
    }

    public String getAddOnId() {
        return addOnId;
    }

    public void setAddOnId(String addOnId) {
        this.addOnId = addOnId;
    }

    public String getItemOrderCount() {
        return itemOrderCount;
    }

    public void setItemOrderCount(String itemOrderCount) {
        this.itemOrderCount = itemOrderCount;
    }

    @Override
    public String toString() {
        return "EditOrderRequestObj{" +
                "orderID=" + order_id +
                ", addOnId='" + addOnId + '\'' +
                ", dishPrice='" + dishPrice + '\'' +
                '}';
    }
}
