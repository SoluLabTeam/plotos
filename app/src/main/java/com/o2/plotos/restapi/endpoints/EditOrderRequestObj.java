package com.o2.plotos.restapi.endpoints;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rania on 4/4/2017.
 */
public class EditOrderRequestObj {

    @SerializedName("order_id")
    private String order_id ;

    @SerializedName("add_on_id")
    private String addOnId;

    @SerializedName("dish_order_count")
    private String item_count;

    @SerializedName("price")
    private String dishPrice;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getItem_count() {
        return item_count;
    }

    public void setItem_count(String item_count) {
        this.item_count = item_count;
    }

    public String getDishPrice() {
        return dishPrice;
    }

    public void setDishPrice(String dishPrice) {
        this.dishPrice = dishPrice;
    }

    public String getAddOnId() {
        return addOnId;
    }

    public void setAddOnId(String addOnId) {
        this.addOnId = addOnId;
    }

    @Override
    public String toString() {
        return "EditOrderRequestObj{" +
                "orderID=" + order_id +
                ", addOnId='" + addOnId + '\'' +
                ", item_count='" + item_count + '\'' +
                ", dishPrice='" + dishPrice + '\'' +
                '}';
    }
}
