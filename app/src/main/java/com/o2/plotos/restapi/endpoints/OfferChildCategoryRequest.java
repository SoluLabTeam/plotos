package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.OfferChildCategoryResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Rania on 20/05/2017.
 */

public interface OfferChildCategoryRequest {
    @GET(ConstantUtil.ApiUrl.GET_OFFER_CHILD_CATEGORIES)
    Call<OfferChildCategoryResponse> offerChildCategoryRequest(
            @Path("special_offer_id") String specialOfferId
    );
}
