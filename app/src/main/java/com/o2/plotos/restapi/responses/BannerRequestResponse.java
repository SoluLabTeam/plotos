package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.Banner;

import java.util.List;

/**
 * Created by Rania on 20/05/2017.
 */

public class BannerRequestResponse {
    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
    @SerializedName("result")
    public List<Banner> bannerList;
}
