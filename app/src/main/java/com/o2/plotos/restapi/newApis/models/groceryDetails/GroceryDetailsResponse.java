package com.o2.plotos.restapi.newApis.models.groceryDetails;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

import java.util.List;

public class GroceryDetailsResponse extends BaseApiModel {
    @SerializedName("result")
    private List<ItemDetais> listGroceryDetails;

    public List<ItemDetais> getListGroceryDetails() {
        return listGroceryDetails;
    }

    public void setListGroceryDetails(List<ItemDetais> listGroceryDetails) {
        this.listGroceryDetails = listGroceryDetails;
    }
}
