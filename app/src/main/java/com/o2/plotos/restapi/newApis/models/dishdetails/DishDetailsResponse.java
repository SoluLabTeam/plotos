package com.o2.plotos.restapi.newApis.models.dishdetails;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

public class DishDetailsResponse extends BaseApiModel {
    @SerializedName("result")
    private DishDetailsResult dishDetailsResult;

    public DishDetailsResult getDishDetailsResult() {
        return dishDetailsResult;
    }

    public void setDishDetailsResult(DishDetailsResult dishDetailsResult) {
        this.dishDetailsResult = dishDetailsResult;
    }
}
