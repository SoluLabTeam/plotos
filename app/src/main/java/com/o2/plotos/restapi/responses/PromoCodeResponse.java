package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by snveiga on 16/03/17.
 */

public class PromoCodeResponse {
    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
    @SerializedName("discount")
    public int discount;
    @SerializedName("ambassador")
    public int ambassador;
    @SerializedName("delivery_charge")
    public int delivery_charge;

}
