package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.VerifyNumResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Hassan on 28/02/2017.
 */

public interface VerifyNumRequest {
    @GET(ConstantUtil.ApiUrl.VERIFY_NUMBER)
    Call<VerifyNumResponse> numberResquest(
            @Path("number") String Number
    );
}
