package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.ParentCategoryResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Hassan on 31/01/2017.
 */

public interface ParentCategoryRequest {
    @GET(ConstantUtil.ApiUrl.GET_PARENT_CATEGORY)
    Call<ParentCategoryResponse> getParentCategoryRequest();

    @GET(ConstantUtil.ApiUrl.GET_GROCERY_BY_CATEGORY_BY_SUPPLIER)
    Call<ParentCategoryResponse> getParentCategoryBySupplierRequest( @Path("supplier_id") String supplierID);
}
