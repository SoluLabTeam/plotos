package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.DishesRequestResponse;
import com.o2.plotos.restapi.responses.GroceryResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by ZoMy on 4/3/2017.
 */

public interface DishRequest {
    @GET(ConstantUtil.ApiUrl.GET_DISHES_BY_ID)
    Call<DishesRequestResponse> dishRequest(
            @Path("dish_id") String dishId
    );

    @GET(ConstantUtil.ApiUrl.GET_GROCERY_BY_ID)
    Call<GroceryResponse> groceryRequest(
            @Path("item_id") String itemID
    );


}
