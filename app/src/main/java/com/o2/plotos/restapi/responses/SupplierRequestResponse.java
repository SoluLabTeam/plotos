package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.Supplier;

import java.util.List;

/**
 * Created by Hassan on 31/01/2017.
 */

public class SupplierRequestResponse {
    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
    @SerializedName("result")
    public List<Supplier> supplierList;
}
