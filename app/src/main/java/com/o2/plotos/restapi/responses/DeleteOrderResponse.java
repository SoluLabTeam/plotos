package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rania on 4/3/2017.
 */
public class DeleteOrderResponse {

    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
}
