package com.o2.plotos.restapi.newApis.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeModel {

    @Expose
    @SerializedName("result")
    private HomeResponse result;
    @Expose
    @SerializedName("response")
    private String response;

    public HomeResponse getResult() {
        return result;
    }

    public void setResult(HomeResponse result) {
        this.result = result;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
