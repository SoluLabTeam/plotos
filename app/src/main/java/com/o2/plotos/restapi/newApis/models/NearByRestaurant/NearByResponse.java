package com.o2.plotos.restapi.newApis.models.NearByRestaurant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.home.NearbyResult;

import java.util.List;

public class NearByResponse {

    @Expose
    @SerializedName("result")
    private List<NearbyResult> result;
    @Expose
    @SerializedName("response")
    private String response;

    public List<NearbyResult> getResult() {
        return result;
    }

    public void setResult(List<NearbyResult> result) {
        this.result = result;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
