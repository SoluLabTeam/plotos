package com.o2.plotos.restapi.newApis.models.resturantDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class DishData {

    @Expose
    @SerializedName("min_order_amount")
     String min_order_amount;
    @Expose
    @SerializedName("pre_booking")
     String pre_booking;
    @Expose
    @SerializedName("closing_time")
     String closing_time;
    @Expose
    @SerializedName("opening_time")
     String opening_time;
    @Expose
    @SerializedName("restaurant_name")
     String restaurant_name;
    @Expose
    @SerializedName("restaurants_image")
     String restaurants_image;
    @Expose
    @SerializedName("updated_on")
     String updated_on;
    @Expose
    @SerializedName("added_on")
     String added_on;
    @Expose
    @SerializedName("is_deleted")
     String is_deleted;
    @Expose
    @SerializedName("isSignatured")
     String isSignatured;
    @Expose
    @SerializedName("status")
     String status;
    @Expose
    @SerializedName("plotos_picks")
     String plotos_picks;
    @Expose
    @SerializedName("views")
     String views;
    @Expose
    @SerializedName("other_details")
     String other_details;
    @Expose
    @SerializedName("ingredients")
     String ingredients;
    @Expose
    @SerializedName("custom_delivery")
     String custom_delivery;
    @Expose
    @SerializedName("delivery_time")
     String delivery_time;
    @Expose
    @SerializedName("currency")
     String currency;
    @Expose
    @SerializedName("price")
     String price;
    @Expose
    @SerializedName("name")
     String name;
    @Expose
    @SerializedName("image")
     String image;
    @Expose
    @SerializedName("beverages")
     String beverages;
    @Expose
    @SerializedName("mains")
     String mains;
    @Expose
    @SerializedName("appetizers")
     String appetizers;
    @Expose
    @SerializedName("dinner")
     String dinner;
    @Expose
    @SerializedName("snacks")
     String snacks;
    @Expose
    @SerializedName("lunch")
     String lunch;
    @Expose
    @SerializedName("breakfast")
     String breakfast;
    @Expose
    @SerializedName("special_offer")
     String special_offer;
    @Expose
    @SerializedName("nutritional_info")
     String nutritional_info;
    @Expose
    @SerializedName("availability")
     String availability;
    @Expose
    @SerializedName("level")
     String level;
    @Expose
    @SerializedName("program")
     String program;
    @Expose
    @SerializedName("meal")
     String meal;
    @Expose
    @SerializedName("dish_categories_id")
     String dish_categories_id;
    @Expose
    @SerializedName("user_id")
     String user_id;
    @Expose
    @SerializedName("restaurant_id")
     String restaurant_id;

    public String getMin_order_amount() {
        return min_order_amount;
    }

    public void setMin_order_amount(String min_order_amount) {
        this.min_order_amount = min_order_amount;
    }

    public String getPre_booking() {
        return pre_booking;
    }

    public void setPre_booking(String pre_booking) {
        this.pre_booking = pre_booking;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getRestaurants_image() {
        return restaurants_image;
    }

    public void setRestaurants_image(String restaurants_image) {
        this.restaurants_image = restaurants_image;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getIsSignatured() {
        return isSignatured;
    }

    public void setIsSignatured(String isSignatured) {
        this.isSignatured = isSignatured;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPlotos_picks() {
        return plotos_picks;
    }

    public void setPlotos_picks(String plotos_picks) {
        this.plotos_picks = plotos_picks;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getOther_details() {
        return other_details;
    }

    public void setOther_details(String other_details) {
        this.other_details = other_details;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getCustom_delivery() {
        return custom_delivery;
    }

    public void setCustom_delivery(String custom_delivery) {
        this.custom_delivery = custom_delivery;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBeverages() {
        return beverages;
    }

    public void setBeverages(String beverages) {
        this.beverages = beverages;
    }

    public String getMains() {
        return mains;
    }

    public void setMains(String mains) {
        this.mains = mains;
    }

    public String getAppetizers() {
        return appetizers;
    }

    public void setAppetizers(String appetizers) {
        this.appetizers = appetizers;
    }

    public String getDinner() {
        return dinner;
    }

    public void setDinner(String dinner) {
        this.dinner = dinner;
    }

    public String getSnacks() {
        return snacks;
    }

    public void setSnacks(String snacks) {
        this.snacks = snacks;
    }

    public String getLunch() {
        return lunch;
    }

    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(String breakfast) {
        this.breakfast = breakfast;
    }

    public String getSpecial_offer() {
        return special_offer;
    }

    public void setSpecial_offer(String special_offer) {
        this.special_offer = special_offer;
    }

    public String getNutritional_info() {
        return nutritional_info;
    }

    public void setNutritional_info(String nutritional_info) {
        this.nutritional_info = nutritional_info;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public String getDish_categories_id() {
        return dish_categories_id;
    }

    public void setDish_categories_id(String dish_categories_id) {
        this.dish_categories_id = dish_categories_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getDish_id() {
        return dish_id;
    }

    public void setDish_id(String dish_id) {
        this.dish_id = dish_id;
    }

    @Expose
    @SerializedName("dish_id")

     String dish_id;
}
