package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by snveiga on 14/03/17.
 */

public class VerifiedResponse {
    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
    @SerializedName("code")
    public String code;
}
