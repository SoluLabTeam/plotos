package com.o2.plotos.restapi.newApis.models.bannerImages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BannerImageResult {


    @Expose
    @SerializedName("updated_on")
    private String updated_on;
    @Expose
    @SerializedName("added_on")
    private String added_on;
    @Expose
    @SerializedName("is_deleted")
    private String is_deleted;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("is_grocery")
    private String is_grocery;
    @Expose
    @SerializedName("type_value")
    private String type_value;
    @Expose
    @SerializedName("type")
    private String type;
    @Expose
    @SerializedName("image")
    private String image;
    @Expose
    @SerializedName("messages")
    private String messages;
    @Expose
    @SerializedName("id")
    private String id;

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIs_grocery() {
        return is_grocery;
    }

    public void setIs_grocery(String is_grocery) {
        this.is_grocery = is_grocery;
    }

    public String getType_value() {
        return type_value;
    }

    public void setType_value(String type_value) {
        this.type_value = type_value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
