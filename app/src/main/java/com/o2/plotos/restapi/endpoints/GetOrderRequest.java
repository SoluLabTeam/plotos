package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.GetOrderResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Hassan on 22/02/2017.
 */

public interface GetOrderRequest {
    @GET(ConstantUtil.ApiUrl.GET_ORDER)
    Call<GetOrderResponse> orderRequest(
            @Path("user_id") String userId
    );
}
