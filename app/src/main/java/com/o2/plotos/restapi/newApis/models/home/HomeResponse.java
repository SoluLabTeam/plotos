package com.o2.plotos.restapi.newApis.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class HomeResponse {

    @Expose
    @SerializedName("new_restaurent")
     List<NewRestResult> new_restaurent;
    @Expose
    @SerializedName("near_restaurent")
     List<NearbyResult> near_restaurent;
    @Expose
    @SerializedName("category")
     List<CategoryResult> category;

    public List<NewRestResult> getNew_restaurent() {
        return new_restaurent;
    }

    public void setNew_restaurent(List<NewRestResult> new_restaurent) {
        this.new_restaurent = new_restaurent;
    }

    public List<NearbyResult> getNear_restaurent() {
        return near_restaurent;
    }

    public void setNear_restaurent(List<NearbyResult> near_restaurent) {
        this.near_restaurent = near_restaurent;
    }

    public List<CategoryResult> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryResult> category) {
        this.category = category;
    }
}
