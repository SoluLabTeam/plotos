package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.PlaceOrderResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Hassan on 2/16/2017.
 */

public interface PlaceOrderRequest {

    @FormUrlEncoded
    @POST(ConstantUtil.ApiUrl.PLACE_ORDER)
    Call<PlaceOrderResponse> placeOrder(@Field("order_object") String jsonArray);

}
