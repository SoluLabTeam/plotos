package com.o2.plotos.restapi.newApis.models.dishdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class AddOns {

    @Expose
    @SerializedName("updated_on")
    String updated_on;
    @Expose
    @SerializedName("added_on")
    String added_on;
    @Expose
    @SerializedName("is_deleted")
    String is_deleted;
    @Expose
    @SerializedName("price")
    String price;
    @Expose
    @SerializedName("name")
    String name;
    @Expose
    @SerializedName("category_id")
    String category_id;
    @Expose
    @SerializedName("dish_id")
    String dish_id;
    @Expose
    @SerializedName("add_on_id")
    String add_on_id;

    boolean isSelected = false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getDish_id() {
        return dish_id;
    }

    public void setDish_id(String dish_id) {
        this.dish_id = dish_id;
    }

    public String getAdd_on_id() {
        return add_on_id;
    }

    public void setAdd_on_id(String add_on_id) {
        this.add_on_id = add_on_id;
    }
}
