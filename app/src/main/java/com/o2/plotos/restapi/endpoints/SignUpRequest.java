package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.SignUpRequestResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Hassan on 30/01/2017.
 */

public interface SignUpRequest {
    @GET(ConstantUtil.ApiUrl.SIGN_UP)
    Call<SignUpRequestResponse> signUpRequest(
            @Query("first_name")String firstName,
            @Query("last_name")String lastName,
            @Query("email")String email,
            @Query("password")String password,
            @Query("number")String number,
            @Query("facebook_id")String fb_id,
            @Query("device_token")String deviceToken
    );
}
