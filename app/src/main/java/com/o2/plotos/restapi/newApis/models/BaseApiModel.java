package com.o2.plotos.restapi.newApis.models;

import com.google.gson.annotations.SerializedName;

public class BaseApiModel {
    @SerializedName("response")
    String response;
    @SerializedName("msg")
    String msg;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
