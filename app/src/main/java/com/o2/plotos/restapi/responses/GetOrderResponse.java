package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.Order;

import java.util.List;

/**
 * Created by Hassan on 22/02/2017.
 */

public class GetOrderResponse {
    @SerializedName("response")
    public String response;
    @SerializedName("result")
    public List<Order> order;
}