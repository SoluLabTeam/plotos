package com.o2.plotos.restapi.newApis.models.orders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;

import java.util.List;

public class OrdersResponse{


    @Expose
    @SerializedName("past")
    private List<PastResult> past;

    @Expose
    @SerializedName("upcoming")
    private List<PastResult> upcoming;

    public List<PastResult> getPast() {
        return past;
    }

    public void setPast(List<PastResult> past) {
        this.past = past;
    }

    public List<PastResult> getUpcoming() {
        return upcoming;
    }

    public void setUpcoming(List<PastResult> upcoming) {
        this.upcoming = upcoming;
    }
}
