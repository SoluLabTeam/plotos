package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rony on 3/22/2017.
 */
public class InRadiusResponse {
    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
}
