package com.o2.plotos.restapi.endpoints;

import com.o2.plotos.restapi.responses.CheckUserEmailResponse;
import com.o2.plotos.restapi.responses.ForgotResponse;
import com.o2.plotos.utils.ConstantUtil;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by dell on 12/26/2017.
 */

public interface CheckUserInterface {

    @POST(ConstantUtil.ApiUrl.CHECK_NEW_EMAIL)
    Call<CheckUserEmailResponse> CheckEmailRequest(
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("delivery_address") String address
            );
}
