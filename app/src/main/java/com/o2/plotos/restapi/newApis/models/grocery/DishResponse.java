package com.o2.plotos.restapi.newApis.models.grocery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DishResponse {


    @Expose
    @SerializedName("parent_id")
    private String parent_id;
    @Expose
    @SerializedName("categoryImage")
    private String categoryImage;
    @Expose
    @SerializedName("categoryName")
    private String categoryName;
    @Expose
    @SerializedName("categoryId")
    private String categoryId;

    @Expose
    @SerializedName("item")
    private List<DishResult> item;

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<DishResult> getItem() {
        return item;
    }

    public void setItem(List<DishResult> item) {
        this.item = item;
    }
}
