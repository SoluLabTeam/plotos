package com.o2.plotos.restapi.newApis.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.o2.plotos.models.ItemDetais;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

@Parcel
public class CategoryResult {


    int typeView = 0;
    @Expose
    @SerializedName("updated_on")
    String updated_on;
    @Expose
    @SerializedName("added_on")
    String added_on;
    @Expose
    @SerializedName("is_deleted")
    String is_deleted;
    @Expose
    @SerializedName("isUsed")
    String isUsed;
    @Expose
    @SerializedName("pick_sequence")
    String pick_sequence;
    @Expose
    @SerializedName("name")
    String name;
    @Expose
    @SerializedName("pick_id")
    String pick_id;

    @Expose
    @SerializedName("dishes")
    List<ItemDetais> dishes;

    ArrayList<NearbyResult> nearByResults = new ArrayList<>();
    ArrayList<NewRestResult> newRestResults = new ArrayList<>();

    public int getTypeView() {
        return typeView;
    }

    public void setTypeView(int typeView) {
        this.typeView = typeView;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public String getPick_sequence() {
        return pick_sequence;
    }

    public void setPick_sequence(String pick_sequence) {
        this.pick_sequence = pick_sequence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPick_id() {
        return pick_id;
    }

    public void setPick_id(String pick_id) {
        this.pick_id = pick_id;
    }

    public List<ItemDetais> getDishes() {
        return dishes;
    }

    public void setDishes(List<ItemDetais> dishes) {
        this.dishes = dishes;
    }

    public ArrayList<NearbyResult> getNearByResults() {
        return nearByResults;
    }

    public void setNearByResults(ArrayList<NearbyResult> nearByResults) {
        this.nearByResults = nearByResults;
    }

    public ArrayList<NewRestResult> getNewRestResults() {
        return newRestResults;
    }

    public void setNewRestResults(ArrayList<NewRestResult> newRestResults) {
        this.newRestResults = newRestResults;
    }
}
