package com.o2.plotos.restapi.newApis.models.orders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;


@Parcel
public class ItemOrders {

    @Expose
    @SerializedName("addon")
     List<AddonsResult> addon;
    @Expose
    @SerializedName("min_order_amount")
     String min_order_amount;
    @Expose
    @SerializedName("end_price")
     String end_price;
    @Expose
    @SerializedName("start_price")
     String start_price;
    @Expose
    @SerializedName("days_open")
     String days_open;
    @Expose
    @SerializedName("closing_time")
     String closing_time;
    @Expose
    @SerializedName("opening_time")
     String opening_time;
    @Expose
    @SerializedName("name")
     String dish_name;
    @Expose
    @SerializedName("image")
     String dishImage;
    @Expose
    @SerializedName("restaurant_name")
     String restaurant_name;
    @Expose
    @SerializedName("updated_delivered")
     String updated_delivered;
    @Expose
    @SerializedName("updated_on_the_way")
     String updated_on_the_way;
    @Expose
    @SerializedName("updated_preparing")
     String updated_preparing;
    @Expose
    @SerializedName("added_on")
     String added_on;
    @Expose
    @SerializedName("is_deleted")
     String is_deleted;
    @Expose
    @SerializedName("item_order_count")
     String item_order_count;
    @Expose
    @SerializedName("dish_order_count")
     String dish_order_count;
    @Expose
    @SerializedName("is_new")
     String is_new;
    @Expose
    @SerializedName("status")
     String status;
    @Expose
    @SerializedName("promo_code")
     String promo_code;
    @Expose
    @SerializedName("special_note")
    String special_note;
    @Expose
    @SerializedName("note")
     String note;
    @Expose
    @SerializedName("delivery_charge")
     String delivery_charge;
    @Expose
    @SerializedName("scheduled_time")
     String scheduled_time;
    @Expose
    @SerializedName("scheduled_date")
     String scheduled_date;
    @Expose
    @SerializedName("currency")
     String currency;
    @Expose
    @SerializedName("discount")
     String discount;
    @Expose
    @SerializedName("price")
     String price;
    @Expose
    @SerializedName("payment_method")
     String payment_method;
    @Expose
    @SerializedName("user_number")
     String user_number;
    @Expose
    @SerializedName("user_address")
     String user_address;
    @Expose
    @SerializedName("user_longitude")
     String user_longitude;
    @Expose
    @SerializedName("user_latitude")
     String user_latitude;
    @Expose
    @SerializedName("receiver_name")
     String receiver_name;
    @Expose
    @SerializedName("user_id")
     String user_id;
    @Expose
    @SerializedName("add_on_id")
     String add_on_id;
    @Expose
    @SerializedName("delivery_id")
     String delivery_id;
    @Expose
    @SerializedName("parent_id")
     String parent_id;
    @Expose
    @SerializedName("item_id")
     String item_id;
    @Expose
    @SerializedName("dish_id")
     String dish_id;
    @Expose
    @SerializedName("supplier_id")
     String supplier_id;
    @Expose
    @SerializedName("restaurant_id")
     String restaurant_id;
    @Expose
    @SerializedName("order_id")
     String order_id;


    @Expose
    @SerializedName("supplier_name")
     String supplier_name;

    @Expose
    @SerializedName("supplier_image")
     String supplier_image;


    @Expose
    @SerializedName("restaurant_image")
     String restaurant_image;

    public List<AddonsResult> getAddon() {
        return addon;
    }

    public void setAddon(List<AddonsResult> addon) {
        this.addon = addon;
    }

    public String getMin_order_amount() {
        return min_order_amount;
    }

    public void setMin_order_amount(String min_order_amount) {
        this.min_order_amount = min_order_amount;
    }

    public String getEnd_price() {
        return end_price;
    }

    public void setEnd_price(String end_price) {
        this.end_price = end_price;
    }

    public String getStart_price() {
        return start_price;
    }

    public void setStart_price(String start_price) {
        this.start_price = start_price;
    }

    public String getDays_open() {
        return days_open;
    }

    public void setDays_open(String days_open) {
        this.days_open = days_open;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getDish_name() {
        return dish_name;
    }

    public void setDish_name(String dish_name) {
        this.dish_name = dish_name;
    }

    public String getDishImage() {
        return dishImage;
    }

    public void setDishImage(String dishImage) {
        this.dishImage = dishImage;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getUpdated_delivered() {
        return updated_delivered;
    }

    public void setUpdated_delivered(String updated_delivered) {
        this.updated_delivered = updated_delivered;
    }

    public String getUpdated_on_the_way() {
        return updated_on_the_way;
    }

    public void setUpdated_on_the_way(String updated_on_the_way) {
        this.updated_on_the_way = updated_on_the_way;
    }

    public String getUpdated_preparing() {
        return updated_preparing;
    }

    public void setUpdated_preparing(String updated_preparing) {
        this.updated_preparing = updated_preparing;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getItem_order_count() {
        return item_order_count;
    }

    public void setItem_order_count(String item_order_count) {
        this.item_order_count = item_order_count;
    }

    public String getDish_order_count() {
        return dish_order_count;
    }

    public void setDish_order_count(String dish_order_count) {
        this.dish_order_count = dish_order_count;
    }

    public String getIs_new() {
        return is_new;
    }

    public void setIs_new(String is_new) {
        this.is_new = is_new;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }

    public String getSpecial_note() {
        return special_note;
    }

    public void setSpecial_note(String special_note) {
        this.special_note = special_note;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(String delivery_charge) {
        this.delivery_charge = delivery_charge;
    }

    public String getScheduled_time() {
        return scheduled_time;
    }

    public void setScheduled_time(String scheduled_time) {
        this.scheduled_time = scheduled_time;
    }

    public String getScheduled_date() {
        return scheduled_date;
    }

    public void setScheduled_date(String scheduled_date) {
        this.scheduled_date = scheduled_date;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getUser_number() {
        return user_number;
    }

    public void setUser_number(String user_number) {
        this.user_number = user_number;
    }

    public String getUser_address() {
        return user_address;
    }

    public void setUser_address(String user_address) {
        this.user_address = user_address;
    }

    public String getUser_longitude() {
        return user_longitude;
    }

    public void setUser_longitude(String user_longitude) {
        this.user_longitude = user_longitude;
    }

    public String getUser_latitude() {
        return user_latitude;
    }

    public void setUser_latitude(String user_latitude) {
        this.user_latitude = user_latitude;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAdd_on_id() {
        return add_on_id;
    }

    public void setAdd_on_id(String add_on_id) {
        this.add_on_id = add_on_id;
    }

    public String getDelivery_id() {
        return delivery_id;
    }

    public void setDelivery_id(String delivery_id) {
        this.delivery_id = delivery_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getDish_id() {
        return dish_id;
    }

    public void setDish_id(String dish_id) {
        this.dish_id = dish_id;
    }

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }


    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getSupplier_image() {
        return supplier_image;
    }

    public void setSupplier_image(String supplier_image) {
        this.supplier_image = supplier_image;
    }

    public String getRestaurant_image() {
        return restaurant_image;
    }

    public void setRestaurant_image(String restaurant_image) {
        this.restaurant_image = restaurant_image;
    }
}
