package com.o2.plotos.restapi.newApis.models.orders;

import com.google.gson.annotations.SerializedName;

public class OrderModel {

    @SerializedName("response")
    private String response;

    @SerializedName("result")
    private OrdersResponse result;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public OrdersResponse getResult() {
        return result;
    }

    public void setResult(OrdersResponse result) {
        this.result = result;
    }
}
