package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dell on 1/30/2018.
 */

public class ChnageMobileResponse {

    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;
}
