package com.o2.plotos.restapi.newApis.models.grocery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DishResult {

    @Expose
    @SerializedName("itemDeliveryTime")
    private String itemDeliveryTime;
    @Expose
    @SerializedName("supplierDeliveryTime")
    private String supplierDeliveryTime;
    @Expose
    @SerializedName("delivery_areas")
    private String delivery_areas;
    @Expose
    @SerializedName("delivery_polygon")
    private String delivery_polygon;
    @Expose
    @SerializedName("radius")
    private String radius;
    @Expose
    @SerializedName("delivery_longitude")
    private String delivery_longitude;
    @Expose
    @SerializedName("delivery_latitude")
    private String delivery_latitude;
    @Expose
    @SerializedName("longitude")
    private String longitude;
    @Expose
    @SerializedName("latitude")
    private String latitude;
    @Expose
    @SerializedName("supplier_description")
    private String supplier_description;
    @Expose
    @SerializedName("end_price")
    private String end_price;
    @Expose
    @SerializedName("start_price")
    private String start_price;
    @Expose
    @SerializedName("days_schedule")
    private String days_schedule;
    @Expose
    @SerializedName("days_open")
    private String days_open;
    @Expose
    @SerializedName("pre_booking")
    private String pre_booking;
    @Expose
    @SerializedName("closing_time")
    private String closing_time;
    @Expose
    @SerializedName("opening_time")
    private String opening_time;
    @Expose
    @SerializedName("delivery_time_end")
    private String delivery_time_end;
    @Expose
    @SerializedName("delivery_time_start")
    private String delivery_time_start;
    @Expose
    @SerializedName("delivery_path")
    private String delivery_path;
    @Expose
    @SerializedName("plotos_delivery")
    private String plotos_delivery;
    @Expose
    @SerializedName("commission")
    private String commission;
    @Expose
    @SerializedName("cash_only")
    private String cash_only;
    @Expose
    @SerializedName("min_order_amount")
    private String min_order_amount;
    @Expose
    @SerializedName("banner_image")
    private String banner_image;
    @Expose
    @SerializedName("address")
    private String address;
    @Expose
    @SerializedName("number")
    private String number;
    @Expose
    @SerializedName("email")
    private String email;
    @Expose
    @SerializedName("last_name")
    private String last_name;
    @Expose
    @SerializedName("first_name")
    private String first_name;
    @Expose
    @SerializedName("supplier_name")
    private String supplier_name;
    @Expose
    @SerializedName("images")
    private String images;
    @Expose
    @SerializedName("updated_on")
    private String updated_on;
    @Expose
    @SerializedName("added_on")
    private String added_on;
    @Expose
    @SerializedName("is_deleted")
    private String is_deleted;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("views")
    private String views;
    @Expose
    @SerializedName("other_details")
    private String other_details;
    @Expose
    @SerializedName("ingredients")
    private String ingredients;
    @Expose
    @SerializedName("custom_delivery")
    private String custom_delivery;
    @Expose
    @SerializedName("delivery_time")
    private String delivery_time;
    @Expose
    @SerializedName("min_qty")
    private String min_qty;
    @Expose
    @SerializedName("qty_unit")
    private String qty_unit;
    @Expose
    @SerializedName("per_unit")
    private String per_unit;
    @Expose
    @SerializedName("currency")
    private String currency;
    @Expose
    @SerializedName("price")
    private String price;
    @Expose
    @SerializedName("image")
    private String image;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("special_offer")
    private String special_offer;
    @Expose
    @SerializedName("nutritional_info")
    private String nutritional_info;
    @Expose
    @SerializedName("availability")
    private String availability;
    @Expose
    @SerializedName("level")
    private String level;
    @Expose
    @SerializedName("program")
    private String program;
    @Expose
    @SerializedName("meal")
    private String meal;
    @Expose
    @SerializedName("user_id")
    private String user_id;
    @Expose
    @SerializedName("category_id")
    private String category_id;
    @Expose
    @SerializedName("supplier_id")
    private String supplier_id;
    @Expose
    @SerializedName("id")
    private String id;
    @SerializedName("item_id")
    private String item_id;

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getItemDeliveryTime() {
        return itemDeliveryTime;
    }

    public void setItemDeliveryTime(String itemDeliveryTime) {
        this.itemDeliveryTime = itemDeliveryTime;
    }

    public String getSupplierDeliveryTime() {
        return supplierDeliveryTime;
    }

    public void setSupplierDeliveryTime(String supplierDeliveryTime) {
        this.supplierDeliveryTime = supplierDeliveryTime;
    }

    public String getDelivery_areas() {
        return delivery_areas;
    }

    public void setDelivery_areas(String delivery_areas) {
        this.delivery_areas = delivery_areas;
    }

    public String getDelivery_polygon() {
        return delivery_polygon;
    }

    public void setDelivery_polygon(String delivery_polygon) {
        this.delivery_polygon = delivery_polygon;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getDelivery_longitude() {
        return delivery_longitude;
    }

    public void setDelivery_longitude(String delivery_longitude) {
        this.delivery_longitude = delivery_longitude;
    }

    public String getDelivery_latitude() {
        return delivery_latitude;
    }

    public void setDelivery_latitude(String delivery_latitude) {
        this.delivery_latitude = delivery_latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getSupplier_description() {
        return supplier_description;
    }

    public void setSupplier_description(String supplier_description) {
        this.supplier_description = supplier_description;
    }

    public String getEnd_price() {
        return end_price;
    }

    public void setEnd_price(String end_price) {
        this.end_price = end_price;
    }

    public String getStart_price() {
        return start_price;
    }

    public void setStart_price(String start_price) {
        this.start_price = start_price;
    }

    public String getDays_schedule() {
        return days_schedule;
    }

    public void setDays_schedule(String days_schedule) {
        this.days_schedule = days_schedule;
    }

    public String getDays_open() {
        return days_open;
    }

    public void setDays_open(String days_open) {
        this.days_open = days_open;
    }

    public String getPre_booking() {
        return pre_booking;
    }

    public void setPre_booking(String pre_booking) {
        this.pre_booking = pre_booking;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getDelivery_time_end() {
        return delivery_time_end;
    }

    public void setDelivery_time_end(String delivery_time_end) {
        this.delivery_time_end = delivery_time_end;
    }

    public String getDelivery_time_start() {
        return delivery_time_start;
    }

    public void setDelivery_time_start(String delivery_time_start) {
        this.delivery_time_start = delivery_time_start;
    }

    public String getDelivery_path() {
        return delivery_path;
    }

    public void setDelivery_path(String delivery_path) {
        this.delivery_path = delivery_path;
    }

    public String getPlotos_delivery() {
        return plotos_delivery;
    }

    public void setPlotos_delivery(String plotos_delivery) {
        this.plotos_delivery = plotos_delivery;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getCash_only() {
        return cash_only;
    }

    public void setCash_only(String cash_only) {
        this.cash_only = cash_only;
    }

    public String getMin_order_amount() {
        return min_order_amount;
    }

    public void setMin_order_amount(String min_order_amount) {
        this.min_order_amount = min_order_amount;
    }

    public String getBanner_image() {
        return banner_image;
    }

    public void setBanner_image(String banner_image) {
        this.banner_image = banner_image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getOther_details() {
        return other_details;
    }

    public void setOther_details(String other_details) {
        this.other_details = other_details;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getCustom_delivery() {
        return custom_delivery;
    }

    public void setCustom_delivery(String custom_delivery) {
        this.custom_delivery = custom_delivery;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getMin_qty() {
        return min_qty;
    }

    public void setMin_qty(String min_qty) {
        this.min_qty = min_qty;
    }

    public String getQty_unit() {
        return qty_unit;
    }

    public void setQty_unit(String qty_unit) {
        this.qty_unit = qty_unit;
    }

    public String getPer_unit() {
        return per_unit;
    }

    public void setPer_unit(String per_unit) {
        this.per_unit = per_unit;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecial_offer() {
        return special_offer;
    }

    public void setSpecial_offer(String special_offer) {
        this.special_offer = special_offer;
    }

    public String getNutritional_info() {
        return nutritional_info;
    }

    public void setNutritional_info(String nutritional_info) {
        this.nutritional_info = nutritional_info;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
