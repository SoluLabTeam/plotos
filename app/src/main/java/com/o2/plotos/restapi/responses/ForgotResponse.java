package com.o2.plotos.restapi.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dell on 12/21/2017.
 */

public class ForgotResponse {
    @SerializedName("response")
    public String response;
    @SerializedName("msg")
    public String message;

}
