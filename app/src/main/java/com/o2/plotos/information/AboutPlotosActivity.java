package com.o2.plotos.information;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2.plotos.R;

public class AboutPlotosActivity extends AppCompatActivity {

    ImageView img_back;
    WebView web;
    TextView tvrate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_plotos);
        web= (WebView) findViewById(R.id.web);
        web.loadUrl("file:///android_asset/about.html");
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setSaveFormData(true);
        web.getSettings().setBuiltInZoomControls(false);

        img_back = (ImageView)findViewById(R.id.img_back);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        tvrate= (TextView) findViewById(R.id.tvrate);
        tvrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
    }
}
