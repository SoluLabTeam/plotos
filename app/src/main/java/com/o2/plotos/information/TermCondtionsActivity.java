package com.o2.plotos.information;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;

import com.o2.plotos.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TermCondtionsActivity extends AppCompatActivity {

    @BindView(R.id.term_condition_webview)
    WebView termCondition;

    private ActionBar mActionbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_condtions);
        ButterKnife.bind(this);

        termCondition.loadUrl("file:///android_asset/term.html");

        mActionbar = getSupportActionBar();
        mActionbar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
