package com.o2.plotos.information;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.authentication.signin.SignInActivity;
import com.o2.plotos.cart.CartActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.User;
import com.o2.plotos.utils.UserPreferenceUtil;

import java.sql.SQLException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InformationActivity extends AppCompatActivity {

    @BindView(R.id.activity_information_about_plotos)
    TextView aboutPloto;
    @BindView(R.id.activity_information_term)
    TextView term;
    @BindView(R.id.activity_information_how_work)
    TextView howWorks;
    @BindView(R.id.activity_information_contact)
    TextView contact;

    private ActionBar mActionbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        mActionbar = getSupportActionBar();
        mActionbar.setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.activity_information_about_plotos)
    public void aboutPlotos(){
        startActivity(new Intent(InformationActivity.this,AboutPlotosActivity.class));
    }
    @OnClick(R.id.activity_information_contact)
    public void contact(){
        startActivity(new Intent(InformationActivity.this,ContactActivity.class));
    }
    @OnClick(R.id.activity_information_how_work)
    public void howWork(){
        startActivity(new Intent(InformationActivity.this,HowItWorks.class));
    }
    @OnClick(R.id.activity_information_term)
    public void termCondition(){
        startActivity(new Intent(InformationActivity.this,TermCondtionsActivity.class));
    }
}
