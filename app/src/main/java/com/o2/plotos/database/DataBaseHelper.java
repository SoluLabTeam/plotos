package com.o2.plotos.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.o2.plotos.models.Card;
import com.o2.plotos.models.Cart;
import com.o2.plotos.models.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hassan on 2/14/2017.
 */

public class DataBaseHelper extends OrmLiteSqliteOpenHelper {

    //Constants for db name and version
    private static final String DATABASE_NAME = "plotos.db";
    private static final int DATABASE_VERSION = 5;

    private Dao<Cart, Integer> cartIntegerDao = null;
    private Dao<User, Integer> userIntegerDao = null;
    private Dao<Card, Integer> cardIntegerDao = null;

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Card.class);
            TableUtils.createTable(connectionSource, Cart.class);
            TableUtils.createTable(connectionSource, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        if (oldVersion == 1 && newVersion == 2) {
            database.execSQL("DROP TABLE IF EXISTS cart");
            try {
                TableUtils.createTable(connectionSource, Cart.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else if(oldVersion == 2 && newVersion == 3) {
            database.execSQL("ALTER TABLE `user` ADD COLUMN email TEXT;");
        }else {
            database.execSQL("DROP TABLE IF EXISTS card");
            database.execSQL("DROP TABLE IF EXISTS cart");
            database.execSQL("DROP TABLE IF EXISTS user");

            try {
                TableUtils.createTable(connectionSource, Card.class);
                TableUtils.createTable(connectionSource, Cart.class);
                TableUtils.createTable(connectionSource, User.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Dao<Cart, Integer> getCartIntegerDao() throws SQLException {
        if (cartIntegerDao == null) {
            cartIntegerDao = getDao(Cart.class);
        }
        return cartIntegerDao;
    }

    public Dao<User, Integer> getUserIntegerDao() throws SQLException {
        if (userIntegerDao == null) {
            userIntegerDao = getDao(User.class);
        }
        return userIntegerDao;
    }

    public Dao<Card, Integer> getCardIntegerDao() throws SQLException {
        if (cardIntegerDao == null) {
            cardIntegerDao = getDao(Card.class);
        }
        return cardIntegerDao;
    }

    public void saveCartItem(Cart cart, boolean isEdit) throws SQLException {
        Dao<Cart, Integer> cartIntegerDao = getCartIntegerDao();
        if (isEdit) {
            cartIntegerDao.update(cart);
        } else {
            cartIntegerDao.create(cart);
        }
    }

    public List<Cart> getCartItems() {
        List<Cart> cartList = new ArrayList<>();
        try {
            Dao<Cart, Integer> dao = getCartIntegerDao();
            cartList = dao.queryForAll();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cartList;
    }

    public void deleteAllCartItems() {
        try {
            Dao<Cart, Integer> dao = getCartIntegerDao();
            dao.deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteCartItem(Cart cart) {
        try {
            Dao<Cart, Integer> cartIntegerDao = getCartIntegerDao();
            cartIntegerDao.delete(cart);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveUser(User user) {
        try {
            Dao<User, Integer> userIntegerDao = getUserIntegerDao();
            userIntegerDao.create(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void editUser(User user) {
        try {
            Dao<User, Integer> userIntegerDao = getUserIntegerDao();
            userIntegerDao.update(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUser(User user) {
        try {
            Dao<User, Integer> userIntegerDao = getUserIntegerDao();
            userIntegerDao.delete(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveCard(Card card) {
        try {
            Dao<Card, Integer> cardIntegerDao = getCardIntegerDao();
            cardIntegerDao.create(card);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteCard(Card card) {
        try {
            Dao<Card, Integer> cardIntegerDao = getCardIntegerDao();
            cardIntegerDao.delete(card);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> getUsersItems() {
        List<User> userList = new ArrayList<>();
        try {
            Dao<User, Integer> dao = getUserIntegerDao();
            userList = dao.queryForAll();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }
}
