package com.o2.plotos;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.places.DeliveryLocationActivity;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;

/**
 * Created by Rania on 9/26/2017.
 */

public class StartupActivity extends AppCompatActivity {

    @BindView(R.id.restaurant_text_view)
    TextView mRestaurantTextView;
    @BindView(R.id.grocery_text_view)
    TextView mGroceryTextView;
    @BindView(R.id.category_text_view)
    TextView mCategoryTextView;
    @BindView(R.id.closeButton)
    ImageView mCloseButton;

    CustomFonts customFonts;
    Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);
        unbinder = ButterKnife.bind(this);
        Utils.buttonTouchDelegate(mCloseButton);
        setCustomFonts();
        dialogLocation();
        String userID = UserPreferenceUtil.getInstance(this).UserId();
          /* We're logged in, we can register the user with Intercom */
        if(!userID.equals("0")) {
            Registration registration = Registration.create().withUserId(userID);
            Intercom.client().registerIdentifiedUser(registration);
            Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
        }else{
            Intercom.client().registerUnidentifiedUser();
            Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
        }

        UserPreferenceUtil.getInstance(this).setDeliveryDate("ASAP");
    }

    public void setCustomFonts() {
        customFonts = new CustomFonts(this);
        customFonts.setOpenSansBold(mRestaurantTextView);
        customFonts.setOpenSansBold(mGroceryTextView);
        customFonts.setOpenSansBold(mCategoryTextView);
    }

    public void dialogLocation()

    {
        final Dialog dialog2 = new Dialog(this);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setCancelable(true);
        dialog2.setContentView(R.layout.dialog_changelocation);
        dialog2.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_top_bootom;
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView txt_changelocation = (TextView) dialog2.findViewById(R.id.txt_changelocation);
        TextView txt_cancel = (TextView) dialog2.findViewById(R.id.txt_cancel);
        EditText edit_location = (EditText)dialog2.findViewById(R.id.edit_location);

        String location = UserPreferenceUtil.getInstance(this).getLocation();

        edit_location.setText(location);


        txt_changelocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(StartupActivity.this, DeliveryLocationActivity.class);
                startActivity(i);

                dialog2.dismiss();


            }
        });


        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog2.dismiss();
            }
        });

        dialog2.show();

    }
    @OnClick(R.id.closeButton)
    public void closeButtClick(){

        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("CurrentTab", 2);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        //finish();
    }

    @OnClick(R.id.restaurant_card)
    public void restaurantCardClick()
    {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("CurrentTab", 2);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.grocery_card)
    public void groceryCardClick()
    {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("CurrentTab", 2);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.category_card)
    public void categoryCardClick()
    {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("CurrentTab", 2);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
