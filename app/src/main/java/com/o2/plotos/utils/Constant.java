package com.o2.plotos.utils;

public class Constant {
    public static final String MOBILE = "mobile";

    public static final String GROCERYSUPPERLIERDETAILS = "grocerySupperlierDetails";
    public static final String NEARBYDETAILS = "nearbydetails";
    public static final String NEWPLOTOSDETAILS = "newplotosdetails";
    public static final String CATEGORYDETAILS = "categorydetails";
    public static final String SIGNATUREDETAILS = "signaturedetails";
    public static final String ID = "id";
    public static final String CAT = "cat";
    public static final String QUANTITY = "quantity";
    public static final String R_ID = "r_id";
    public static final String CAT_DISH = "Dishes";
    public static final String CAT_GROCERY = "Grocery";
    public static final String SUPPLIER_ID = "supplier_id";
    public static final String OBJ_PAST_ORDER = "list_past_order";

    /*public static final String PREFSTRINGGROCERY = "prefStringGrocery";
    public static final String PREFSTRINGDISH = "prefStringDish";*//*
    public static final String PREFLISTGROCERY = "prefListGrocery";
    public static final String PREFLISTDISH = "prefListDish";*/
    public static final String PREFITEMLIST = "prefItemList";

}
