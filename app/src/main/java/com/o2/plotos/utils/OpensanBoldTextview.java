package com.o2.plotos.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class OpensanBoldTextview extends android.support.v7.widget.AppCompatTextView {

    public OpensanBoldTextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public OpensanBoldTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OpensanBoldTextview(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/OpenSans-Bold.ttf");
        setTypeface(tf ,1);

    }

}
