package com.o2.plotos.utils;

import android.content.Context;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Hassan on 3/03/2017.
 */

public class TimeUtil {

    Context context;
    public TimeUtil(Context c){
        this.context = c;

    }

    public boolean isPlaceOpened(long opening, long closing){
        long currentTime = System.currentTimeMillis()/1000;
        Log.d("OpenClose", "time:" + opening +" - "+ closing +" - "+ currentTime );
        if(opening < currentTime && currentTime < closing){
            return true;
        }
        return false;
    }

//    public boolean isPlaceOpened(String opening, String closing){
//        boolean status = false;
//        String checkingAmPm = "";
//
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
//            //Opening time
//            String string1 = convertTime(opening);
//            String[] timeSlot = string1.split(":");
//            int localHour = Integer.parseInt(timeSlot[0].trim());
//            String[] minString = timeSlot[1].split(" ");
//            int localMin = Integer.parseInt(minString[0].trim());
//            String timetoOpen = localHour+":"+localMin;
//            Date theDate = sdf.parse(timetoOpen);
//            Calendar calTest = Calendar.getInstance();
//            calTest.setTime(theDate);
//
//            //Date time1 = new SimpleDateFormat("HH:mm a").parse(string1);
//            //Calendar calendar1 = Calendar.getInstance();
//            //calendar1.setTime(time1);
//
//            /*String openingAmPM = convertTime(string1);
//            Date openAmPM = new SimpleDateFormat("HH:mm a").parse(openingAmPM);
//            Calendar calOpenAmPm = Calendar.getInstance();
//            calOpenAmPm.setTime(openAmPM);*/
//
//
//
//            //Closing time
//            String string2 = convertTime(closing);
//            String[] closeSlot = string2.split(":");
//            int closeHour = Integer.parseInt(closeSlot[0].trim());
//            String[] closeminString = closeSlot[1].split(" ");
//            int closeMin = Integer.parseInt(closeminString[0].trim());
//            String timetoClose = closeHour+":"+closeMin;
//            Date closeDate = sdf.parse(timetoClose);
//            Calendar closeTest = Calendar.getInstance();
//            closeTest.setTime(closeDate);
//            if (closeminString[1].equalsIgnoreCase("AM")) {
//                checkingAmPm = "0";
//            } else {
//                checkingAmPm = "1";
//            }
//            if(checkingAmPm.equalsIgnoreCase("0")){
//                closeTest.add(Calendar.DATE, 1);
//            }
//
//           /*SimpleDateFormat cdf = new SimpleDateFormat("HH:mm");
//           Date time2 = new SimpleDateFormat("HH:mm a").parse(string2);
//            Calendar calendar2 = Calendar.getInstance();
//            calendar2.setTime(time2);
//            String closingAmPM = convertTime(string2);
//            Date closeAmPM = new SimpleDateFormat("HH:mm a").parse(closingAmPM);
//            Calendar calCloseAmPm = Calendar.getInstance();
//            calCloseAmPm.setTime(closeAmPM);
//            checkingAmPm = String.valueOf(calCloseAmPm.get(calCloseAmPm.AM_PM));*/
//
//            //Current Time
//            String someRandomTime = convertTime(getcurrentTime());
//            String[] currentSlot = someRandomTime.split(":");
//            int currentHour = Integer.parseInt(currentSlot[0].trim());
//            String[] currentMinString = currentSlot[1].split(" ");
//            int currentMin = Integer.parseInt(currentMinString[0].trim());
//            String currentTime = currentHour+":"+currentMin;
//            Date currentDate = sdf.parse(currentTime);
//            Calendar currentTest = Calendar.getInstance();
//            currentTest.setTime(currentDate);
//            if (currentMinString[1].equalsIgnoreCase("AM")) {
//                checkingAmPm = "0";
//            } else {
//                checkingAmPm = "1";
//            }
//            if(checkingAmPm.equalsIgnoreCase("0")) {
//                if (currentTest.before(calTest)) {
//                    currentTest.add(Calendar.DATE, 1);
//                }
//            }
//
//
//            /*String someRandomTime = getcurrentTime();
//            Date d = new SimpleDateFormat("HH:mm a").parse(someRandomTime);
//            Calendar calendar3 = Calendar.getInstance();
//            calendar3.setTime(d);
//            String currentAmPM = convertTime(someRandomTime);
//            Date currntAmPM = new SimpleDateFormat("HH:mm a").parse(currentAmPM);
//            Calendar calCurrntAmPM = Calendar.getInstance();
//            calCurrntAmPM.setTime(currntAmPM);
//            checkingAmPm = String.valueOf(calCurrntAmPM.get(calCurrntAmPM.AM_PM));
//            }
//
//            Date a = calendar1.getTime();
//            Date b = calendar2.getTime();
//            Date x = calendar3.getTime();*/
//            /*if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
//                status = true;
//                //checkes whether the current time is between 14:49:00 and 20:11:13.
//            }
//            else {
//                status = false;
//            }*/
//
//            Date a = calTest.getTime();
//            Date b = closeTest.getTime();
//            Date x = currentTest.getTime();
//            if (x.after(a) && x.before(b)) {
//                status = true;
//                //checkes whether the current time is between 14:49:00 and 20:11:13.
//            }
//            else {
//                status = false;
//            }
//        } catch (ParseException|NumberFormatException ex) {
//            ex.printStackTrace();
//        }
//        return status;
//    }

    public String getcurrentTime(){
        //TimeZone tz = TimeZone.getTimeZone("Asia/Dubai");
        TimeZone tz = TimeZone.getDefault();
        Calendar c = Calendar.getInstance(tz);
        String time = String.format("%02d" , c.get(Calendar.HOUR))+":"+
                String.format("%02d" , c.get(Calendar.MINUTE));

        String amPm ;
        String checking = String.format("%02d",c.get(Calendar.AM_PM));

        if(checking.equalsIgnoreCase("00")){
            amPm = "AM";
        } else {
            amPm = "PM";
        }
        String hardCodeTime = "08:46 PM";

        String currentTime = time +" "+amPm;
        return currentTime;
    }

    public String convertTime (String rawTime){
        String formattedDate= "";
        //String time = getDubaiTime();
        String time = rawTime;

        DateFormat readFormat = new SimpleDateFormat("hh:mm a");
        DateFormat writeFormat = new SimpleDateFormat("HH:mm a");
        Date date2 = null;
        try {
            date2 = readFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date2 != null) {
            formattedDate = writeFormat.format(date2);
            //Log.v("converted time ",formattedDate);
        }
        return formattedDate;
    }


    public static void getOrderDeliveredTime(String dateTime){

        String dateString = "07:44 PM Thu 9 Mar";
        SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a EEE d MMM");
        Calendar localTime = Calendar.getInstance(TimeZone.getDefault());
        TimeZone dubaiZone = TimeZone.getTimeZone("Asia/Dubai");
        TimeZone currentZone = TimeZone.getDefault();
        try {
            dateFormat.setTimeZone(dubaiZone);
            localTime.setTime(dateFormat.parse(dateString));

            localTime.setTimeZone(currentZone);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        DateFormat formatter = new SimpleDateFormat("h:mm a");

        System.out.println("Local:: "+formatter.format(localTime.getTime()));



        Calendar localCalender = Calendar.getInstance();
        localCalender.setTimeZone(currentZone);
               // localCalender.setTime(localTime.getTime());
      //  localCalender.set(Calendar.HOUR,1);
        localCalender.set(Calendar.MINUTE,45);

        Calendar orderTime = Calendar.getInstance(currentZone);
        orderTime.add(Calendar.HOUR , localCalender.get(Calendar.HOUR));
        orderTime.add(Calendar.MINUTE , localCalender.get(Calendar.MINUTE));


        String time = String.format("%02d" , orderTime.get(Calendar.HOUR))+":"+
                String.format("%02d" , orderTime.get(Calendar.MINUTE));


        System.out.println("Delivery:: "+time + " "+localCalender );
}


}
