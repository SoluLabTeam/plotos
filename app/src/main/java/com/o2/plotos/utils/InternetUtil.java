package com.o2.plotos.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Hassan on 1/28/2017.
 */

public class InternetUtil {
    private static InternetUtil sInstance;
    private static Context sContext;
    private InternetUtil(){
    }
    public static InternetUtil getInstance(Context context){
        if(sInstance ==null){
            sInstance = new InternetUtil();
        }
        sContext = context;
        return sInstance;
    }
    /**
     * Is Device connect with internet
     * @return true if connect else false
     */
    public boolean isNetWorkAvailable(){
        ConnectivityManager cm = (ConnectivityManager) sContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
