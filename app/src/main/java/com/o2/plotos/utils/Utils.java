package com.o2.plotos.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.view.Gravity;
import android.view.TouchDelegate;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.desai.vatsal.mydynamictoast.MyCustomToast;
import com.o2.plotos.home.resturants.AddToCartActivity;
import com.o2.plotos.home.resturants.GroceryCategoryDetailActivity;

import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restuarantdetail.ResturantDetailsActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rania on 5/21/2017.
 */

public class Utils {

    public static void buttonTouchDelegate(final View view) {
        final View parent = (View) view.getParent();
        parent.post(new Runnable() {
            @Override
            public void run() {
                final Rect r = new Rect();
                view.getHitRect(r);
                r.top -= 150;
                r.bottom += 150;
                r.left -= 150;
                r.right += 150;

                parent.setTouchDelegate(new TouchDelegate(r, view));
            }
        });
    }

    public static Spanned getAEDSpannable(double price) {
        //        String price = String.valueOf(Html.fromHtml(String.valueOf(listItemDetails.get(position).getBasicPrice() * listItemDetails.get(position).getQuantity()) + "<sup>AED</sup>"));
//        Html.fromHtml("price<sup>2</sup>"))
//        String s1 = String.valueOf(Html.fromHtml("" + price + " <sup>AED</sup"));
//        SpannableString ss = new SpannableString(s1);
//        int start = s1.indexOf(" ") + 1;
//        int end = s1.length();
//        ss.setSpan(new RelativeSizeSpan(0.5f), start, end, 0);
//        ss.toString();
//        String s1 = String.valueOf(Html.fromHtml("y<sup><small>2</small></sup>"));
        Spanned spanned = Html.fromHtml(price + "<sup><small> AED</small></sup>");
        return spanned;
    }

    public static void showCartToast(Context context, int restID) {
        MyCustomToast myCustomToast = new MyCustomToast(context);
//        myCustomToast.setCustomMessageText("custom toast message..");
        myCustomToast.setCustomMessageTextSize(18);
        myCustomToast.setCustomMessageTextColor(Color.WHITE);
        myCustomToast.setCustomMessageIcon(restID, MyCustomToast.POSITION_TOP);
//                    myCustomToast.setCustomMessageIconColor(Color.WHITE);
        myCustomToast.setCustomMessageBackgroundColor("#4AB173");
//        myCustomToast.setCustomMessageBackgroundDrawable(restID);
        myCustomToast.setCustomMessageDuration(MyCustomToast.LENGTH_LONG);
        myCustomToast.setGravity(Gravity.CENTER, 0, 0);
//                    myCustomToast.setCustomMessageTypeface("cambriai.ttf");
        myCustomToast.show();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus() != null && inputManager != null) {
                inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                inputManager.hideSoftInputFromInputMethod(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    public static void navigateToResturantDetailsAct(String r_id, Context context) {
        Intent intent = new Intent(context, ResturantDetailsActivity.class);
        intent.putExtra(Constant.R_ID, r_id);
        context.startActivity(intent);
    }

    public static void navigateToDishOrGroceryDetailsAct(String dish_id, Context context, String cat) {
        Intent intent = new Intent(context, AddToCartActivity.class);
        intent.putExtra(Constant.ID, dish_id);
        intent.putExtra(Constant.CAT, cat);
        context.startActivity(intent);
    }

    public static void navigateToDishOrGroceryDetailsActWithQuantity(String dish_id, Context context, String cat, int quantity) {
        Intent intent = new Intent(context, AddToCartActivity.class);
        intent.putExtra(Constant.ID, dish_id);
        intent.putExtra(Constant.CAT, cat);
        intent.putExtra(Constant.QUANTITY, quantity - 1);
        context.startActivity(intent);
    }

    public static void navigateToSuppAct(String sup_id, Context context) {
        Intent intent = new Intent(context, GroceryCategoryDetailActivity.class);
        intent.putExtra(Constant.SUPPLIER_ID, sup_id);
        context.startActivity(intent);
    }


    public static void storeDatainToCart(ItemDetais itemDetais, Context context, int quantity, Double basicPrice) {

        itemDetais.setQuantity(quantity);
        itemDetais.setBasicPrice(basicPrice);
        List<ItemDetais> listItems = UserPreferenceUtil.getInstance(context).getItemList(Constant.PREFITEMLIST);
        for (int i = 0; i < listItems.size(); i++) {
            if (itemDetais.getItem_id().equalsIgnoreCase(listItems.get(i).getItem_id())) {
                listItems.remove(i);
                break;
            }
        }
        if (itemDetais.getQuantity() > 0) {
            listItems.add(itemDetais);
        }
        UserPreferenceUtil.getInstance(context).storeListing(listItems, Constant.PREFITEMLIST);
    }


    public static int updateQuantity(int quantity, boolean needtoAdd) {
        if (needtoAdd) {
            quantity += 1;
        } else {
            if (quantity != 0) {
                quantity -= 1;
            }
        }
        return quantity;
    }

    public static String getFormattedTime(String dateStr, String strReadFormat, String strWriteFormat) {

        String formattedDate = dateStr;

        DateFormat readFormat = new SimpleDateFormat(strReadFormat, Locale.US);
        DateFormat writeFormat = new SimpleDateFormat(strWriteFormat, Locale.US);

        Date date = null;

        try {
            date = readFormat.parse(dateStr);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        if (date != null) {
            formattedDate = writeFormat.format(date);
        }

        return formattedDate;
    }

}
