package com.o2.plotos.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by Hassan on 4/02/2017.
 */

public class CustomFonts {
    Context context;
     public CustomFonts(Context c){
         this.context = c;

     }

    public void setOswaldBold(TextView textView){
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Oswald-Bold.ttf");
        textView.setTypeface(font);
    }
    public void setOswldRegulr(TextView textView){
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Oswald-Regular.ttf");
        textView.setTypeface(font);

    }
    public void setOswldLight(TextView textView){
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Oswald-Light.ttf");
        textView.setTypeface(font);

    }

    public void setOpenSansBold(TextView textView){
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Bold.ttf");
        textView.setTypeface(font);

    }
    public void setOpenSansItalic(TextView textView){
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Italic.ttf");
        textView.setTypeface(font);

    }
    public void setOpenSansLight(TextView textView){
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Light.ttf");
        textView.setTypeface(font);

    }
    public void setOpenSansRegulr(TextView textView){
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
        textView.setTypeface(font);

    }
    public void setOpenSansSemiBold(TextView textView){
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Semibold.ttf");
        textView.setTypeface(font);

    }
}
