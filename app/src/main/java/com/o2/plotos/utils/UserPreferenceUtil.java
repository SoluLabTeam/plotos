package com.o2.plotos.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.o2.plotos.models.CreditCard;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.models.login.UserData;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Save the user setting and info preference
 * Created by Hassan on 2/20/2017.
 */

public class UserPreferenceUtil {
    public static final String prefName = "ploto";
    public static final String PAYMENTTOKEN = "paymenttoken";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_NUMBER = "number";
    private static final String DTIME = "dtime";
    private static final String DTOTAL = "dtotal";
    private static final String KEY_LAT = "lat";
    private static final String KEY_LNG = "lng";
    private static final String KEY_CURRENT_LAT = "current_lat";
    private static final String KEY_CURRENT_LNG = "current_lng";

    private static final String KEY_AS_SOON_CHECK = "as_soon_as";
    private static final String KEY_CURRENT_CHECK = "is_current_check";
    private static final String KEY_FUTURE_CHECK = "future_check";
    private static final String KEY_LOCATION_CHECK = "is_location_check";

    private static final String KEY_CURRENT_LOCATION = "current_location";
    private static final String KEY_LOCATION = "location";
    private static final String KEY_DETAIL_ADD = "detail_add";
    private static final String KEY_DELIVERY_DATE = "delivery_date";
    private static final String KEY_DELIVERY_DATE2 = "delivery_date2";

    public static final String KEY_COUNTRY_CODE = "country_code";

    public static final String KEY_VERIFIED = "verified";

    public static final String KEY_PROFILE = "profile";

    public static final String KEY_RETURN_ACTIVITY = "return_activity";
    public static final String IS_INTRO_SEEN = "is_intro_seen";

    private static UserPreferenceUtil sInstance;
    private static Context sContext;
    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;


    private UserPreferenceUtil() {
        if (sContext != null) {
            sharedPref = sContext.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        }
    }

    public void clearPref(){
        editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }

    public void storeUserData(UserData userData) {
        editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(userData);
        editor.putString("User", json);
        editor.apply();
    }

    public void storeString(String key, String value) {
        editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getStoreString(String key) {
        String storeString = sharedPref.getString(key, "");
        return storeString;
    }

    public void storeListing(List list, String key) {
        editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        storeString(key, json);
        editor.putString(key, json);
        editor.apply();
    }

    public List<ItemDetais> getItemList(String key) {
        List<ItemDetais> listGrocery = new ArrayList<>();
        Gson gson = new Gson();
        Type type = new TypeToken<List<ItemDetais>>() {
        }.getType();
        if (gson.fromJson(getStoreString(key), type) != null) {
            listGrocery = gson.fromJson(getStoreString(key), type);
        }
        return listGrocery;
    }


    public UserData getUserData() {
        Gson gson = new Gson();
        String json = sharedPref.getString("User", "");
        UserData obj = gson.fromJson(json, UserData.class);
        return obj;
    }

    public void storeCreditCardData(CreditCard payData) {
        editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(payData);
        editor.putString("payment", json);
        editor.apply();
    }

    public CreditCard getCreditCardData() {
        Gson gson = new Gson();
        String json = sharedPref.getString("payment", "");
        CreditCard obj = gson.fromJson(json, CreditCard.class);
        return obj;
    }

    public static UserPreferenceUtil getInstance(Context context) {
        if (sInstance == null) {
            sContext = context;
            sInstance = new UserPreferenceUtil();
        }

        return sInstance;
    }

    public void setUserId(String userId) {
        editor = sharedPref.edit();
        editor.putString(KEY_USER_ID, userId);
        editor.apply();
    }

    public String UserId() {
        String userId = sharedPref.getString(KEY_USER_ID, "0");
        return userId;
    }

    public void setPaymentToken(String token) {
        editor = sharedPref.edit();
        editor.putString(PAYMENTTOKEN, token);
        editor.apply();
    }
    public String PaymentToken() {
        String token = sharedPref.getString(PAYMENTTOKEN, "");
        return token;
    }


    public void setNumber(String number) {
        editor = sharedPref.edit();
        editor.putString(KEY_NUMBER, number);
        editor.commit();
    }

    public String getNumber() {
        String number = sharedPref.getString(KEY_NUMBER, "");
        return number;
    }

    public void setDeliveryTime(String time) {
        editor = sharedPref.edit();
        editor.putString(DTIME, time);
        editor.commit();
    }

    public String getDeliveryTime() {
        String number = sharedPref.getString(DTIME, "");
        return number;
    }

    public void setIsIntroSeen(boolean time) {
        editor = sharedPref.edit();
        editor.putBoolean(IS_INTRO_SEEN, time);
        editor.commit();
    }

    public boolean getIsIntroSeen() {
        boolean number = sharedPref.getBoolean(IS_INTRO_SEEN, false);
        return number;
    }

    public void setDeliveryTotal(String time) {
        editor = sharedPref.edit();
        editor.putString(DTOTAL, time);
        editor.commit();
    }

    public String getDeliveryTotal() {
        String number = sharedPref.getString(DTOTAL, "");
        return number;
    }

    public void setprofilepic(String profile) {
        editor = sharedPref.edit();
        editor.putString(KEY_PROFILE, profile);
        editor.commit();
    }

    public String getProfile() {
        String number = sharedPref.getString(KEY_PROFILE, "");
        return number;
    }

    public void setCurrentLat(String lat) {
        editor = sharedPref.edit();
        editor.putString(KEY_CURRENT_LAT, lat);
        editor.commit();
    }

    public String getCurrentLat() {
        String lat = "0.0";
        if (sharedPref != null) {
            lat = sharedPref.getString(KEY_CURRENT_LAT, "0.0");
        }
        return lat;
    }

    public String getCurrentLng() {
        String lng = sharedPref.getString(KEY_CURRENT_LNG, "0.0");
        return lng;
    }

    public void setCurrentLng(String lng) {
        editor = sharedPref.edit();
        editor.putString(KEY_CURRENT_LNG, lng);
        editor.commit();
    }

    public void setLocation(String location) {
        editor = sharedPref.edit();
        editor.putString(KEY_LOCATION, location);
        editor.commit();
    }

    public String getLocation() {
        String location = sharedPref.getString(KEY_LOCATION, getCurrentLocation());
        return location;
    }


    public void setDetailAddress(String location) {
        editor = sharedPref.edit();
        editor.putString(KEY_DETAIL_ADD, location);
        editor.commit();
    }

    public String getDetailAddress() {
        String location = sharedPref.getString(KEY_DETAIL_ADD, "");
        return location;
    }


    public void setCurrentLocation(String currentLocation) {
        editor = sharedPref.edit();
        editor.putString(KEY_CURRENT_LOCATION, currentLocation);
        editor.commit();
    }

    public String getCurrentLocation() {
        String location = sharedPref.getString(KEY_CURRENT_LOCATION, "Unknown Location");
        return location;
    }


    public void setLocationLat(String lat) {
        editor = sharedPref.edit();
        editor.putString(KEY_LAT, lat);
        editor.commit();
    }

    public String getLocationLat() {
        String lat = sharedPref.getString(KEY_LAT, getCurrentLat());
        return lat;
    }

    public String getLocationLng() {
        String lng = sharedPref.getString(KEY_LNG, getCurrentLng());
        return lng;
    }

    public void setLocationLng(String lng) {
        editor = sharedPref.edit();
        editor.putString(KEY_LNG, lng);
        editor.commit();
    }

    public void setDeliveryDate(String date) {
        editor = sharedPref.edit();
        editor.putString(KEY_DELIVERY_DATE, date);
        editor.commit();
    }


    public void setDeliveryDate2(String date) {
        editor = sharedPref.edit();
        editor.putString(KEY_DELIVERY_DATE2, date);
        editor.commit();
    }

    public String getDeliveryDate() {
        String date = sharedPref.getString(KEY_DELIVERY_DATE, "ASAP");
        return date;
    }

    public String getDeliveryDate2() {
        String date = sharedPref.getString(KEY_DELIVERY_DATE2, "ASAP");
        return date;
    }


    public void setCountryCode(String countryCode) {
        editor = sharedPref.edit();
        editor.putString(KEY_COUNTRY_CODE, countryCode);
        editor.commit();
    }

    public String getCountryCode() {
        String code = sharedPref.getString(KEY_COUNTRY_CODE, "AE");
        return code;
    }


    public void setAsSoonCheck(boolean asSoonAs) {
        editor = sharedPref.edit();
        editor.putBoolean(KEY_AS_SOON_CHECK, asSoonAs);
        editor.commit();
    }

    public boolean getAsSoonCheck() {
        boolean asSoonCheck = sharedPref.getBoolean(KEY_AS_SOON_CHECK, true);
        return asSoonCheck;
    }


    public void setCurrentLocationCheck(boolean isCurrentCheck) {
        editor = sharedPref.edit();
        editor.putBoolean(KEY_CURRENT_CHECK, isCurrentCheck);
        editor.commit();
    }

    public boolean getCurrentLocationCheck() {
        boolean currentLocation = sharedPref.getBoolean(KEY_CURRENT_CHECK, true);
        return currentLocation;
    }


    public void setLocationCheck(boolean isCurrentCheck) {
        editor = sharedPref.edit();
        editor.putBoolean(KEY_LOCATION_CHECK, isCurrentCheck);
        editor.commit();
    }

    public boolean getLocationCheck() {
        boolean currentLocation = sharedPref.getBoolean(KEY_LOCATION_CHECK, true);
        return currentLocation;
    }

    public void setFutureCheck(boolean isCurrentCheck) {
        editor = sharedPref.edit();
        editor.putBoolean(KEY_FUTURE_CHECK, isCurrentCheck);
        editor.commit();
    }

    public boolean getFutureCheck() {
        boolean currentLocation = sharedPref.getBoolean(KEY_FUTURE_CHECK, true);
        return currentLocation;
    }


    /*
     * set user account verification status
     */
    public void setVerified(boolean verified) {
        editor = sharedPref.edit();
        editor.putBoolean(KEY_VERIFIED, verified);
        editor.commit();
    }

    /*
     * Get user account verification status
     */
    public boolean getVerified() {
        return sharedPref.getBoolean(KEY_VERIFIED, false);
    }


    /*
     * set user return activity
     */
    public void setReturnActivity(String activity) {
        editor = sharedPref.edit();
        editor.putString(KEY_RETURN_ACTIVITY, activity);
        editor.commit();
    }

    /*
     * Get user return activity
     */
    public String getReturnActivity() {
        return sharedPref.getString(KEY_RETURN_ACTIVITY, "");
    }

}
