package com.o2.plotos.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class OpensanTextview extends android.support.v7.widget.AppCompatTextView {

    public OpensanTextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public OpensanTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OpensanTextview(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/OpenSans-Regular.ttf");
        setTypeface(tf ,1);

    }

}
