package com.o2.plotos.utils;

import android.util.Log;

import com.o2.plotos.BuildConfig;

/**
 * Created by Hassan on 1/28/2017.
 */
public class LogCat {
    public static final boolean D =  BuildConfig.DEBUG;
    public static void LogInfo(String TAG, String msg) {
        if (D) {
            Log.i(TAG, msg);
        }
    }
    public static void LogWarrning(String TAG, String msg) {
        if (D) {
            Log.w(TAG, msg);
        }
    }
    public static void LogDebug(String TAG, String msg) {
        if (D) {
            Log.d(TAG, msg);
        }
    }
    public static void LogError(String TAG, String msg) {
        if (D) {
            Log.e(TAG, msg);
        }
    }
}
