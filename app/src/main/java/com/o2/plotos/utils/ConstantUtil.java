package com.o2.plotos.utils;


public class ConstantUtil {

    public static final String TAG = "Ploto";

    //Dev Url
//    public static final String BASE_URL = "http://dev.plotos.com/dashboard/api/index.php/";
    //Live Server
    public static final String BASE_URL = "https://plotos.com/dashboard/api/index.php/";

    public static class ApiUrl {
        public static final String SIGN_IN = "UserManagement/sign_in";
        public static final String SIGN_UP = "UserManagement/sign_up";
        public static final String VERIFY_NUMBER = "UserManagement/verify_number/{number}";
        public static final String VERIFIED = "UserManagement/verified/{user_id}";
        public static final String PROMO_CODE = "PromoManagement/check_code/{code}/{user_id}/{amount}";
        public static final String GET_RESTAURANTS = "RestaurantManagement/{resturant_type}/0/1/{lat}/{lng}";
        public static final String GET_RESTAURANTS_IN_RADIUS = "RestaurantManagement/in_radius/{REST_ID}/{USER_LAT}/{USER_LNG}";
        public static final String GET_SUPPLIER_IN_RADIUS = "SupplierManagement/in_radius/{supplier_id}/{latitude}/{longitude}";
        public static final String GET_DETOX_DISHES = "DishManagement/dishes_by_detox/{restaurant_id}/{lat}/{lng}";
        public static final String GET_DISHES = "DishManagement/{type}/" +
                "{restaurant_id}/{category}/{calorie}/{lat}/{lng}";
        public static final String GET_DISHES_BY_ID = "DishManagement/get_dishes/{dish_id}";
        public static final String GET_GROCERY_BY_ID = "GroceryItemManagement/get_groceryitems/{item_id}";

        public static final String PLACE_ORDER = "OrderManagement/place_order";
        public static final String CHECK_NEW_EMAIL = "UserManagement/checkuseremail";
        public static final String CHANGE_PHONE = "UserManagement/updatePhone";
        public static final String FORGOT_EMAIL = "UserManagement/request_password_reset";
        public static final String GET_ORDER = "OrderManagement/get_orders/{user_id}/user";
        public static final String Delete_ORDER = "OrderManagement/delete_order/{ORDER_ID}";
        public static final String Edit_ORDER = "OrderManagement/edit_order/order_object";
        public static final String Edit_PROFILE = "UserManagement/profile/profile_object";
        public static final String GET_PARENT_CATEGORY = "GroceryCategoryManagement/get_parent_categories";
        public static final String GET_SUB_CATEGORY = "GroceryCategoryManagement/get_child_categories/{parent_id}/{supplier_id}";
        public static final String GET_GROCERY_BY_CATEGORY = "GroceryItemManagement/get_groceryitems_by_category/{category_id}/{supplier_id}/{lat}/{lng}";
        public static final String GET_SUPPLIERS = "SupplierManagement/get_suppliers/0/1/{lat}/{lng}";
        public static final String GET_GROCERY_BY_CATEGORY_BY_SUPPLIER = "GroceryCategoryManagement/get_categories_by_supplier/{supplier_id}";
        public static final String GET_BANNERS = "BannerManagement/get_banner_list/0";
        public static final String GET_OFFER_CHILD_CATEGORIES = "SpecialOfferManagement/get_child_categories/{special_offer_id}";
        public static final String GET_OFFER_DISH_BY_CATEGORY = "DishManagement/get_dish_by_special_offer/{child_category_id}/{lat}/{lng}";
        public static final String GET_OFFER_GROCERY_BY_CATEGORY = "GroceryItemManagement/get_groceryitem_by_special_offer/{child_category_id}/{lat}/{lng}";
    }
}
