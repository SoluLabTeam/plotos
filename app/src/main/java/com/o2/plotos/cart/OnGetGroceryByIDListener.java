package com.o2.plotos.cart;

import com.o2.plotos.models.Grocery;

/**
 * Created by Rania on 5/14/2017.
 */

public interface OnGetGroceryByIDListener {

    void onGetGrocerySuccess(Grocery grocery);
    void onGetGeoceryFailed(String message);
}
