package com.o2.plotos.cart;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.checkout.CardValidator;
import com.checkout.CheckoutKit;
import com.checkout.exceptions.CardException;
import com.checkout.exceptions.CheckoutException;
import com.checkout.models.Card;
import com.checkout.models.CardToken;
import com.checkout.models.CardTokenResponse;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.o2.plotos.R;
import com.o2.plotos.authentication.phone.PhoneVerificationActivity;
import com.o2.plotos.authentication.signin.SignInActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.dishdetail.DishDetailActivity;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.models.Cart;
import com.o2.plotos.models.Dish;
import com.o2.plotos.models.Grocery;
import com.o2.plotos.models.User;
import com.o2.plotos.places.DeliveryLocationActivity;
import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.ChangePhoneNumberInterface;
import com.o2.plotos.restapi.endpoints.CheckUserInterface;
import com.o2.plotos.restapi.endpoints.PlaceOrderRequest;
import com.o2.plotos.restapi.endpoints.PromoCodeRequest;
import com.o2.plotos.restapi.responses.CheckUserEmailResponse;
import com.o2.plotos.restapi.responses.ChnageMobileResponse;
import com.o2.plotos.restapi.responses.NewProfile;
import com.o2.plotos.restapi.responses.PlaceOrderResponse;
import com.o2.plotos.restapi.responses.PromoCodeResponse;
import com.o2.plotos.restuarantdetail.IResturntDetalDataModl;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.LogCat;
import com.o2.plotos.utils.TimeUtil;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.wx.wheelview.widget.WheelView;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class CartActivity extends AppCompatActivity implements CartAdapter.OnActionMenuClicked,
        IResturntDetalDataModl.OnGetResturntDishRqustFinishLisener, OnGetGroceryByIDListener {


    @BindView(R.id.content_cart_recyclerview1)
    RecyclerView mRecyclerView;
    @BindView(R.id.content_cart_total_price)
    TextView textTotalPrice;
    @BindView(R.id.content_cart_grand_total_price)
    TextView textGrandTotalPrice;
    @BindView(R.id.content_cart_grandtotal_text)
    TextView textGrandTotal;
    @BindView(R.id.content_cart_address)
    TextView address;
    @BindView(R.id.content_cart_delivery_text)
    TextView deliverytext;
    @BindView(R.id.content_cart_delivery_price)
    TextView deliveryPrice;
    @BindView(R.id.content_cart_promo_applied)
    View viewPromoApplied;

    @BindView(R.id.content_cart_total_text)
    TextView textTotal;

    @BindView(R.id.content_cart_invalid_promo_code)
    TextView textInvalidPromoCode;

    @BindView(R.id.content_cart_discount)
    TextView textDiscount;

    @BindView(R.id.content_cart_phone_number)
    EditText editTextPhoneNumber;

    @BindView(R.id.content_cart_promo_code)
    EditText editTextPromoCode;

    @BindView(R.id.content_cart_note)
    EditText editTextNote;

    @BindView(R.id.activity_cart_textInputLayout_phone_number)
    TextInputLayout textInputLayoutPhone;

    @BindView(R.id.activity_cart_textInputLayout_note)
    TextInputLayout textInputLayoutNote;

    @BindView(R.id.day_time_range)
    View dayTimeRange;

    @BindView(R.id.saved_card)
    View mSavedCard;

    @BindView(R.id.new_card)
    View mNewCard;

    @BindView(R.id.options_card)
    View mOptionsCard;

    @BindView(R.id.radio_card)
    RadioButton mRadioCard;

    @BindView(R.id.radio_cash)
    RadioButton mRadioCash;

    @BindView(R.id.save_card)
    CheckBox mSaveCard;

    @BindView(R.id.btn_cancel)
    Button mBtnCancel;

    @BindView(R.id.card_digits)
    TextView mCardDigits;

    @BindView(R.id.month_range)
    HorizontalScrollView monthRange;

    @BindView(R.id.year_range)
    HorizontalScrollView yearRange;

    @BindView(R.id.day_range)
    HorizontalScrollView dayRange;

    @BindView(R.id.time_range)
    HorizontalScrollView timeRange;

    @BindView(R.id.wheelviewDate)
    WheelView wheelViewDate;

    @BindView(R.id.wheelviewTime)
    WheelView wheelViewTime;

    @BindView(R.id.activity_delivery_layout_dailog)
    LinearLayout layoutDialog;

    @BindView(R.id.activity_delivery_btn_set_time)
    Button btnSelectDateTime;

    @BindView(R.id.delivery_location_txt_select_date)
    TextView txtDeliveryDateTime;

    @BindView(R.id.delivery_location_future_date)
    ImageView imgCheckFutureDate;

    @BindView(R.id.delivery_location_as_soon_check)
    ImageView imgCheckAsSoonAs;

    @BindView(R.id.change_phone_button)
    Button changePhoneButton;

    @BindView(R.id.content_cart_promo_container)
    LinearLayout promoContainer;
    @BindView(R.id.content_cart_email)
    EditText contentCartEmail;
    @BindView(R.id.activity_cart_textInputLayout_email)
    TextInputLayout activityCartTextInputLayoutEmail;
    @BindView(R.id.content_cart_promo_code_text)
    TextView contentCartPromoCodeText;
    @BindView(R.id.content_cart_btn_promo_code)
    Button contentCartBtnPromoCode;

    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.text_input_name)
    TextInputLayout textInputName;
    @BindView(R.id.number)
    EditText number;
    @BindView(R.id.text_input_number)
    TextInputLayout textInputNumber;
    @BindView(R.id.cvv)
    EditText cvv;
    @BindView(R.id.text_input_cvv)
    TextInputLayout textInputCvv;
    @BindView(R.id.expire_range)
    LinearLayout expireRange;
    @BindView(R.id.content_cart_placeorder_button)
    Button contentCartPlaceorderButton;

    static Context context;

    String user_id = "0";
    @BindView(R.id.content_cart_delivery_add)
    EditText contentCartDeliveryAdd;
    @BindView(R.id.content_cart_change_btn)
    Button contentCartChangeBtn;
    @BindView(R.id.btn_change)
    Button btnChange;
    @BindView(R.id.linlay_email)
    LinearLayout linlayEmail;
    @BindView(R.id.txt_deleiverystar)
    TextView txtDeleiverystar;
    @BindView(R.id.txt_mobilestar)
    TextView txtMobilestar;
    @BindView(R.id.txt_emailstar)
    TextView txtEmailstar;
    @BindView(R.id.txt_paymentstar)
    TextView txtPaymentstar;

    /*@BindView(R.id.restuarant_detail_text_time)
    TextView txtArriveTime;*/

    //private String publicKey = "pk_test_83d75e1e-6734-47b0-a2a2-01d3a41193da";
    private String publicKey = "pk_8fa20311-cdeb-4bea-aad1-019ee87b8fff";

    CustomFonts customFonts;
    Double totalPrice = 0.0;
    int discount = 0;
    int ambassador = 0;
    int deliveryCharge = 5;
    private List<Cart> cartList;
    private ProgressDialog mProgressDialog;
    private Cart mCartOnEdit;

    //  private GoogleMap mGoogleMap;
    private String promoCode;
    private String deliveryDay, deliveryTime, deliveryDateTime;
    private String selectedDateTime;
    private String dateFormate[] = new String[2];

    private com.o2.plotos.models.Card savedCard;
    private String savedCardId;
    private String savedCustomerId;

    private String ccExpireMonth;
    private String ccExpireYear;

    boolean check_no = false;

    TimeUtil timeUtil;

    private List<String> tempTimeList;
    private List<String> timeList;


    private Calendar mCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;
    private TimePickerDialog.OnTimeSetListener time;


    private final int VERIFICATION = 0;

    //    Double totalGroceryPrice = 0.0;
//    boolean orderHasGrocery = false;
    boolean cashOnly = false;
    final DecimalFormat decimalFormat = new DecimalFormat("#.#");
    HashMap<String, Double> restaurantHashMap = new HashMap<>();
    HashMap<String, Double> supplierHashMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ButterKnife.bind(this);
        context = this;
        editTextPhoneNumber.setClickable(false);
        editTextPhoneNumber.setEnabled(false);
//        Fabric.with(this, new Crashlytics());
//        forceCrash();
        //   editTextPromoCode.setText("FeelTheLove");


        DataBaseHelper dataBaseHelper = new DataBaseHelper(CartActivity.this);
        try {
            List<User> users = dataBaseHelper.getUserIntegerDao().queryForAll();
            if (users.size() == 0) {
                UserPreferenceUtil.getInstance(this).setReturnActivity("CART");

                //  user_id = users.get(0).getId();
                contentCartEmail.setVisibility(View.VISIBLE);
                linlayEmail.setVisibility(View.VISIBLE);

                //Intent intent = new Intent(CartActivity.this, SignInActivity.class);
                // startActivity(intent);
                //finish();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        /*SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.activity_cart_map);

        mapFragment.getMapAsync(this);*/

        timeUtil = new TimeUtil(this);

        try {
            savedCard = dataBaseHelper.getCardIntegerDao().queryBuilder().queryForFirst();
            if (savedCard != null) {
                if (savedCard.getId() != null) {
                    //Snackbar.make(monthRange, savedCard.getId(), Snackbar.LENGTH_LONG).show();
                    savedCardId = savedCard.getId();
                    savedCustomerId = savedCard.getCustomerId();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ccExpireMonth = "";
        ccExpireYear = "";

        setProgressDialog();
        initializeXml();
        makeCartList();
        setCustomFonts();
        // applyPromoCode();

       /* com.o2.plotos.models.Card mycard = new com.o2.plotos.models.Card();

        mycard.setId("11");
        mycard.setCustomerId("11");
        mycard.setExpiryMonth("01");
        mycard.setExpiryYear("21");
        mycard.setLastDigits("2489");
        mycard.setPaymentMethod("Card");

        dataBaseHelper.saveCard(mycard);*/

    }

    @Override
    protected void onResume() {
        super.onResume();



        String _address = UserPreferenceUtil.getInstance(this).getLocation();
        if (_address != null) {
            _address.replace("\n", ", ");
            address.setText(_address);
            // contentCartDeliveryAdd.setText(_address);
            // contentCartDeliveryAdd.clearFocus();
        }

        if (UserPreferenceUtil.getInstance(this).getCurrentLocationCheck()) {

            contentCartDeliveryAdd.getText().clear();
            contentCartDeliveryAdd.clearFocus();


        } else {

            String detail_address = UserPreferenceUtil.getInstance(this).getDetailAddress();
            contentCartDeliveryAdd.setText(detail_address);
            contentCartDeliveryAdd.clearFocus();
            //imgCheckLocation.setVisibility(View.VISIBLE);
        }


        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }

        // get phone number from shared pref as it can be saved from address activity
        String phone = UserPreferenceUtil.getInstance(this).getNumber();
        if (phone != null) {
            editTextPhoneNumber.setText(phone);
        }
        user_id = UserPreferenceUtil.getInstance(getApplicationContext()).UserId();

        if (user_id.equals("0")) {
            contentCartEmail.setVisibility(View.VISIBLE);
            linlayEmail.setVisibility(View.VISIBLE);

        }
        String star = "*";

        String text = "<font color=#00000>Your Delivery location details</font> <font color=#4AB173>" + star + "</font>";
        String text1 = "<font color=#00000>Mobile</font> <font color=#4AB173>" + star + "</font>";
        String text2 = "<font color=#00000>Payment Method</font> <font color=#4AB173>" + star + "</font>";
        String text3 = "<font color=#00000>Email</font> <font color=#4AB173>" + star + "</font>";

        txtDeleiverystar.setText(Html.fromHtml(text));
        txtMobilestar.setText(Html.fromHtml(text1));
        txtPaymentstar.setText(Html.fromHtml(text2));
        txtEmailstar.setText(Html.fromHtml(text3));
        // txtAddress.setText(Html.fromHtml(text));

        //UserPreferenceUtil.getInstance(this).

        //  String email = UserPreferenceUtil.getInstance(this).get

        /*User user = getUserDetail();

        //if(!user.getNumber().equals("")) {
        if( user.getNumber() != null ) {
            editTextPhoneNumber.setText(user.getNumber());
            editTextPhoneNumber.setEnabled(false);
        }*/

        /*if (mGoogleMap != null) {
            setLocation();
        }*/
    }

    private User getUserDetail() {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        User user = null;
        try {
            user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    public void forceCrash() {
        throw new RuntimeException("This is a crash");
    }


    private void initializeXml() {
        ButterKnife.bind(this);

        setUserPreferenceDate();

        if (savedCard != null) {
            mSavedCard.setVisibility(View.VISIBLE);
            mNewCard.setVisibility(View.GONE);
            mCardDigits.setText("****" + savedCard.getLastDigits());
        } else {
            mSavedCard.setVisibility(View.VISIBLE);
            mCardDigits.setVisibility(View.GONE);
            btnChange.setVisibility(View.GONE);
            mNewCard.setVisibility(View.VISIBLE);
        }

        dayRange.setHorizontalFadingEdgeEnabled(true);
        timeRange.setHorizontalFadingEdgeEnabled(true);
        monthRange.setHorizontalFadingEdgeEnabled(true);
        yearRange.setHorizontalFadingEdgeEnabled(true);

        initDayRangeView(dayRange);
        initTimeRangeView(timeRange, 8, 23, 30, true);

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, 0);
        int year = cal.get(Calendar.YEAR);

        initDateRangeView(monthRange, cal, 1, 12, Calendar.MONTH, "MMM", "MM");

        cal.set(Calendar.YEAR, year);
        initDateRangeView(yearRange, cal, year, year + 10, Calendar.YEAR, "yyyy", "yyyy");
    }

    /*
     * Select delivery time
     */
    public void selectDeliveryTime(View v) {

        TextView textView = (TextView) v.findViewById(R.id.label);
        Context context = getApplicationContext();
        LinearLayout parent = (LinearLayout) v.getParent();

        for (int index = 0; index < parent.getChildCount(); index++) {
            LinearLayout _view = (LinearLayout) parent.getChildAt(index);
            TextView _textView = (TextView) _view.getChildAt(0);
            _view.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
            _textView.setTextColor(ContextCompat.getColor(context, R.color.textGray));
        }

        v.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
        textView.setTextColor(Color.WHITE);

        if (deliveryDay != null && deliveryTime != null) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = format.parse(deliveryDay);
                SimpleDateFormat sdfDisplay = new SimpleDateFormat("EEE, MMM d", Locale.UK);

                selectedDateTime = sdfDisplay.format(date.getTime()) + " " + deliveryTime;
                txtDeliveryDateTime.setText(selectedDateTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            imgCheckAsSoonAs.setVisibility(View.GONE);
            imgCheckFutureDate.setVisibility(View.VISIBLE);
        }
    }

    /*
     * Select expire date
     */
    public void selectExpireDate(View v) {

        TextView textView = (TextView) v.findViewById(R.id.label);
        Context context = getApplicationContext();
        LinearLayout parent = (LinearLayout) v.getParent();

        for (int index = 0; index < parent.getChildCount(); index++) {
            LinearLayout _view = (LinearLayout) parent.getChildAt(index);
            TextView _textView = (TextView) _view.getChildAt(0);
            _view.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
            _textView.setTextColor(ContextCompat.getColor(context, R.color.textGray));
        }

        v.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
        textView.setTextColor(Color.WHITE);
        String value = (String) v.getTag();
        if (value.length() == 4) {
            ccExpireYear = value;
        } else {
            ccExpireMonth = value;
        }
        //Snackbar.make(v, ccExpireYear+" "+ccExpireMonth, Snackbar.LENGTH_LONG).show();
    }

    /*
     * Init time range scroll view
     */
    public void initDayRangeView(HorizontalScrollView container) {

        Context context = getApplicationContext();
        LayoutInflater inflater = getLayoutInflater();
        LinearLayout parent = new LinearLayout(context);

        SimpleDateFormat sdfDisplay = new SimpleDateFormat("EEE, MMM d", Locale.UK);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

        Calendar cal = Calendar.getInstance();

        for (int i = 0; i <= 3; i++) {

            View view = inflater.inflate(R.layout.item_range, null);
            TextView textView = (TextView) view.findViewById(R.id.label);

            textView.setText(sdfDisplay.format(cal.getTime()));

            view.setTag(sdf.format(cal.getTime()));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deliveryDay = (String) v.getTag();
                    selectDeliveryTime(v);
                }
            });

            parent.addView(view);

            cal.add(Calendar.DATE, 1);
        }

        container.addView(parent);

    }

    /*
     * Init time range scroll view
     */
    public void initDateRangeView(final HorizontalScrollView container, Calendar cal, int start, int end, int step, String display, String data) {

        Context context = getApplicationContext();
        LayoutInflater inflater = getLayoutInflater();
        LinearLayout parent = new LinearLayout(context);

        SimpleDateFormat sdfDisplay = new SimpleDateFormat(display, Locale.UK);
        SimpleDateFormat sdf = new SimpleDateFormat(data, Locale.UK);

        for (int i = start; i <= end; i++) {

            View view = inflater.inflate(R.layout.item_range, null);
            TextView textView = (TextView) view.findViewById(R.id.label);

            textView.setText(sdfDisplay.format(cal.getTime()));

            view.setTag(sdf.format(cal.getTime()));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectExpireDate(v);
                }
            });

            parent.addView(view);

            cal.add(step, 1);
        }

        container.addView(parent);

    }

    /*
     * Init time range scroll view
     */
    public void initTimeRangeView(HorizontalScrollView container, int start, int end, int step, boolean format) {

        Context context = getApplicationContext();
        LayoutInflater inflater = getLayoutInflater();
        final LinearLayout parent = new LinearLayout(context);

        String mFormat = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(mFormat, Locale.UK);

        // add 1 hour to start time
        //start += 1;

        // subtract 1 hour from end time
        //end += 1;

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, start);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        //cal.add(Calendar.MINUTE, -step);

        String label;
        do {
            cal.add(Calendar.MINUTE, step);
            label = sdf.format(cal.getTime());

            cal.add(Calendar.MINUTE, step);
            label += " - " + sdf.format(cal.getTime());

            cal.add(Calendar.MINUTE, -step);

            View view = inflater.inflate(R.layout.item_range, null);
            TextView textView = (TextView) view.findViewById(R.id.label);

            textView.setText(label);

            textView.setTag(textView.getTextSize());

            view.setId(cal.get(Calendar.HOUR_OF_DAY));

            view.setTag(label);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deliveryTime = (String) v.getTag();
                    selectDeliveryTime(v);
                }
            });

            parent.addView(view);

        } while (!(cal.get(Calendar.HOUR_OF_DAY) == end && (int) cal.get(Calendar.MINUTE) == 30));

        container.addView(parent);

    }

    /**
     * Set the user preference
     */
    private void setUserPreferenceDate() {
        //txtDeliveryDateTime.setText(UserPreferenceUtil.getInstance(this).getDeliveryDate());
        if (UserPreferenceUtil.getInstance(this).getAsSoonCheck()) {
            imgCheckAsSoonAs.setVisibility(View.VISIBLE);
            imgCheckFutureDate.setVisibility(View.GONE);
            dayTimeRange.setVisibility(View.GONE);
        } else {
            imgCheckAsSoonAs.setVisibility(View.GONE);
            imgCheckFutureDate.setVisibility(View.VISIBLE);
            dayTimeRange.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Get the list of times difference from string.xml file
     *
     * @return
     */
    private List<String> getTimesList() {
        List<String> timesList = new ArrayList<>();
        String[] timesArray = getResources().getStringArray(R.array.timesArray);

        for (int i = 0; i < timesArray.length; i++) {
            timesList.add(timesArray[i]);
        }
        return timesList;
    }

    private void setProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(true);
    }


    private void makeCartList() {

        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        cartList = dataBaseHelper.getCartItems();
        boolean isThereDishInOrder = false;
        totalPrice = 0.0;
        for (int i = 0; i < cartList.size(); i++) {
            Cart cart = cartList.get(i);
            try {
                double priceDouble = Double.parseDouble(cart.getDishPrice());
                totalPrice = totalPrice + priceDouble;
                if (cart.getItemGroceryID() != null && !cart.getItemGroceryID().equals("")) {
                    if (cart.getMinOrderAmount() != null && !cart.getMinOrderAmount().equals("0.00")) {
                        if (supplierHashMap.get(cart.getSupplierID()) != null) {
                            Double price = supplierHashMap.get(cart.getSupplierID()) + priceDouble;
                            supplierHashMap.put(cart.getSupplierID(), price);
                        } else {
                            supplierHashMap.put(cart.getSupplierID(), priceDouble);
                        }
                    }
                } else { //Restaurants
                    if (restaurantHashMap.get(cart.getResturantId()) != null) {
                        Double price = restaurantHashMap.get(cart.getResturantId()) + priceDouble;
                        restaurantHashMap.put(cart.getResturantId(), price);
                    } else {
                        restaurantHashMap.put(cart.getResturantId(), priceDouble);
                    }
                }
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }

            if (promoCode != null && i == 0) {
                cart.setPromoCode(promoCode);
            } else {
                cart.setPromoCode("");
            }
            if (cart.getResturantId() != null) {
                isThereDishInOrder = true;
            }
            if (cart.getCashOnly() != null && !cart.getCashOnly().equals("0")) {
                cashOnly = true;
            }
        }

        if (cashOnly) {
            mRadioCash.setChecked(true);
            mRadioCard.setChecked(false);
            mRadioCard.setVisibility(View.GONE);
            mOptionsCard.setVisibility(View.GONE);
            mRadioCash.setVisibility(View.VISIBLE);
        } else {
            mRadioCard.setChecked(true);
            mRadioCash.setChecked(false);
            mRadioCard.setVisibility(View.VISIBLE);
            mOptionsCard.setVisibility(View.VISIBLE);
            mRadioCash.setVisibility(View.VISIBLE);
        }

        Double grandTotal = 0.0;
        if (cartList.size() > 0) {
            if (discount > 0) {

                BigDecimal bd2 = new BigDecimal(discount);
//      bd.setScale(2, BigDecimal.ROUND_HALF_UP);   bd.setScale does not change bd
                bd2 = bd2.setScale(2, BigDecimal.ROUND_HALF_UP);

                textDiscount.setText("-" + String.valueOf(bd2));
                viewPromoApplied.setVisibility(View.VISIBLE);
            } else {
                viewPromoApplied.setVisibility(View.GONE);
            }

            BigDecimal bd2 = new BigDecimal(totalPrice);
//      bd.setScale(2, BigDecimal.ROUND_HALF_UP);   bd.setScale does not change bd
            bd2 = bd2.setScale(2, BigDecimal.ROUND_HALF_UP);

            textTotalPrice.setText(String.valueOf(bd2));
            grandTotal = totalPrice - discount + deliveryCharge;

            BigDecimal bd = new BigDecimal(grandTotal);
//      bd.setScale(2, BigDecimal.ROUND_HALF_UP);   bd.setScale does not change bd
            bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
            textGrandTotalPrice.setText(String.valueOf(bd));


        } else {
            textTotalPrice.setText("0.00");
            textDiscount.setText("0.00");
            textGrandTotalPrice.setText("0.00");
            deliveryPrice.setText("0.00");
        }

        if (grandTotal <= 0) {
            mRadioCard.setVisibility(View.GONE);
            mOptionsCard.setVisibility(View.GONE);
            mRadioCash.setVisibility(View.GONE);
            mRadioCash.setChecked(false);
            mRadioCard.setChecked(false);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        CartAdapter cartAdapter = new CartAdapter(this, cartList, this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(cartAdapter);
    }

    @OnClick(R.id.change_phone_button)
    public void changePhoneButton() {


        if (check_no == false) {

            editTextPhoneNumber.setSelection(editTextPhoneNumber.getText().length());
            changePhoneButton.setText("SAVE");
            changePhoneButton.setTextColor(getResources().getColor(R.color.white));
            changePhoneButton.setBackgroundResource(R.drawable.change_address_back);
            editTextPhoneNumber.setClickable(true);
            editTextPhoneNumber.setEnabled(true);
            //requestFocus(editTextPhoneNumber);
            editTextPhoneNumber.setFocusable(true);

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editTextPhoneNumber, InputMethodManager.SHOW_IMPLICIT);

            //  editTextPhoneNumber.setEnabled(!editTextPhoneNumber.isEnabled());

            check_no = true;


        } else {

            check_no = false;
            changePhoneButton.setTextColor(getResources().getColor(R.color.colorAccent));
            changePhoneButton.setText("CHANGE MOBILE");
            changePhoneButton.setBackgroundResource(R.drawable.change_mobile_back);
            editTextPhoneNumber.setClickable(false);
            editTextPhoneNumber.setEnabled(false);
            if (editTextPhoneNumber.getText().toString().isEmpty()) {
                editTextPhoneNumber.setError("Please Enter Mobile No.");
            } else {

                changeMobile(user_id, editTextPhoneNumber.getText().toString(), mProgressDialog);
            }

        }


    }

    @OnClick(R.id.activity_cart_img_cross)
    public void closeScreen() {
        finish();
    }

    @OnClick(R.id.content_cart_btn_promo_code)
    public void onClickPromoCode() {

        if (contentCartBtnPromoCode.getText().toString().equals("REMOVE")) {


            editTextPromoCode.setEnabled(true);
            editTextPromoCode.getText().clear();
            contentCartBtnPromoCode.setText("APPLY");
            discount = 0;
            deliveryCharge = 5;
            makeCartList();
        } else {


            applyPromoCode();
        }


    }

    @OnClick(R.id.content_cart_placeorder_button)
    public void onClick() {


        if(contentCartDeliveryAdd.getText().toString().isEmpty())
        {


            // contentCartDeliveryAdd.setError("");
            Toast.makeText(context, "please enter detail delivery address", Toast.LENGTH_SHORT).show();
        }
        else
        {
            if (user_id.equals("0")) {
                if (validateEmail() && validatePhoneNumber()) {

                    mProgressDialog.show();
                    if (contentCartDeliveryAdd.getText().toString().isEmpty()) {
                        // contentCartDeliveryAdd.setError("Enter Please  Delivery Address ");
                        Toast.makeText(context, "please enter detail delivery address", Toast.LENGTH_SHORT).show();

                    } else {
                        checkUserEmail(contentCartEmail.getText().toString(), editTextPhoneNumber.getText().toString(), address.getText().toString());

                    }


                }

            } else {
                if (validatePhoneNumber()) {


                    if (contentCartDeliveryAdd.getText().toString().isEmpty()) {
                        Toast.makeText(context, "please enter detail delivery address", Toast.LENGTH_SHORT).show();
                        //  contentCartDeliveryAdd.setError("Enter Please  Delivery Address ");
                    } else {
                        mProgressDialog.show();

                        if (mRadioCard.isChecked() && savedCardId == null) {
                            try {
                                new ConnectionTask().execute("");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            if (savedCardId != null) {
                                sendOrderRequest(null, savedCardId);
                            } else {

                   /*     if(contentCartDeliveryAdd.getText().toString().isEmpty())
                        {
                            contentCartDeliveryAdd.setError("Enter Please  Delivery Address ");
                        }*/
                                //  else
                                {
                                    sendOrderRequest();
                                }

                            }
                        }


                    }
                }
            }

        }



    }

    @OnClick(R.id.radio_cash)
    public void onClickCashOnDelivery() {
        mOptionsCard.setVisibility(View.GONE);
        mSavedCard.setVisibility(View.VISIBLE);
        mRadioCash.setChecked(true);
        mRadioCard.setChecked(false);
    }

    @OnClick(R.id.radio_card)
    public void onClickCreditCard() {


        mOptionsCard.setVisibility(View.VISIBLE);
        mRadioCard.setChecked(true);
        mSavedCard.setVisibility(View.VISIBLE);

        mBtnCancel.setVisibility(View.GONE);
        mRadioCash.setChecked(false);

        if (savedCard != null) {
            mNewCard.setVisibility(View.GONE);
        } else

        {
            mNewCard.setVisibility(View.VISIBLE);
        }


    }

    @OnClick(R.id.btn_change)
    public void onClickChangeCreditCard() {

        if (mRadioCard.isChecked()) {
            mSavedCard.setVisibility(View.VISIBLE);
            mNewCard.setVisibility(View.VISIBLE);
            mBtnCancel.setVisibility(View.VISIBLE);
        }


    }

    @OnClick(R.id.btn_cancel)
    public void onClickCancelCreditCard() {
        mSavedCard.setVisibility(View.VISIBLE);
        mNewCard.setVisibility(View.GONE);
    }

    public void setCustomFonts() {
        customFonts = new CustomFonts(this);
        customFonts.setOpenSansBold(textDiscount);
        customFonts.setOpenSansBold(textTotalPrice);
        customFonts.setOpenSansBold(textTotal);
        customFonts.setOpenSansBold(textGrandTotalPrice);
        customFonts.setOpenSansBold(textGrandTotal);
        customFonts.setOpenSansRegulr(deliverytext);
        customFonts.setOpenSansRegulr(deliveryPrice);
    }


    private int getMaximumDishTime() {

        int tempTime = 0;

        for (int i = 0; i < cartList.size(); i++) {
            Cart cart = cartList.get(i);
            int currentTime = Integer.parseInt(cart.getTime());
            if (currentTime > tempTime) {
                tempTime = currentTime;
            }
        }
        return tempTime;
    }

    private boolean validatePhoneNumber() {
        if (editTextPhoneNumber.getText().toString().trim().isEmpty()) {
            editTextPhoneNumber.setError(getString(R.string.error_empty_phone_order));
            return false;
        } else {
            //  editTextPhoneNumber.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateEmail() {
        final String email_pattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (contentCartEmail.getText().toString().trim().isEmpty()) {
            contentCartEmail.setError(getString(R.string.error_empty_email));
            return false;
        } else if (!contentCartEmail.getText().toString().matches(email_pattern)) {
            contentCartEmail.setError(getString(R.string.error_empty_email));
            return false;
        } else {
            activityCartTextInputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }

    private void applyPromoCode() {

        if (cartList.size() == 0) {
            Toast.makeText(context, "please add item in cart before apply promo code", Toast.LENGTH_SHORT).show();
        } else {
            final String code = editTextPromoCode.getText().toString();

            if (!code.isEmpty()) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editTextPromoCode.getWindowToken(), 0);

                mProgressDialog.show();

                String restaurant_ids = "";
                for (int i = 0; i < cartList.size(); i++) {
                    if (cartList.get(i).getResturantId() != null) {
                        if (i == 0) {
                            restaurant_ids = cartList.get(i).getResturantId();
                        } else {
                            restaurant_ids = restaurant_ids + "," + cartList.get(i).getResturantId();
                        }
                    }
                }

                PromoCodeRequest promoCodeRequest = ServiceGenrator.createService(PromoCodeRequest.class);

                Call<PromoCodeResponse> responseCall = promoCodeRequest.check_code(code,
                        UserPreferenceUtil.getInstance(getApplicationContext()).UserId(),
                        (int) Math.floor(totalPrice));
                responseCall.enqueue(new Callback<PromoCodeResponse>() {
                    @Override
                    public void onResponse(Call<PromoCodeResponse> call, Response<PromoCodeResponse> response) {
                        Log.d(ConstantUtil.TAG, "check promo code response " + response.raw());
                        mProgressDialog.hide();
                        if (response.code() == 200) {
                            PromoCodeResponse apiResponse = response.body();
                            String responseCode = apiResponse.response;
                            contentCartBtnPromoCode.setText("REMOVE");

                            if (responseCode.equalsIgnoreCase("0")) {
                                editTextPromoCode.setEnabled(false);
                                textInvalidPromoCode.setVisibility(View.GONE);
                                discount = Integer.valueOf(apiResponse.discount);
                                ambassador = Integer.valueOf(apiResponse.ambassador);
                                if (ambassador == 1) {
//                                deliveryCharge = Integer.valueOf(apiResponse.delivery_charge);
                                    deliveryCharge = 0;
                                }

                                BigDecimal bd = new BigDecimal(deliveryCharge);
//      bd.setScale(2, BigDecimal.ROUND_HALF_UP);   bd.setScale does not change bd
                                bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
                                textGrandTotalPrice.setText(String.valueOf(bd));

                                deliveryPrice.setText(String.valueOf(bd));

                                promoCode = code;
                                makeCartList();
                            } else if (responseCode.equalsIgnoreCase("2")) {
                                textInvalidPromoCode.setVisibility(View.VISIBLE);

                                deliveryCharge = 5;
                                BigDecimal bd = new BigDecimal(deliveryCharge);
//      bd.setScale(2, BigDecimal.ROUND_HALF_UP);   bd.setScale does not change bd
                                bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
                                //textGrandTotalPrice.setText(String.valueOf(bd));

                                deliveryPrice.setText(String.valueOf(bd));
                                promoCode = null;
                                if (discount > 0) {
                                    discount = 0;
                                    makeCartList();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PromoCodeResponse> call, Throwable t) {
                        mProgressDialog.hide();
                        Log.d(ConstantUtil.TAG, "onFailure " + t.getMessage());
                    }
                });

            } else {
                textInvalidPromoCode.setVisibility(View.VISIBLE);
            }
        }


    }

    /**
     * Set the user preference
     */
    private String setUserPreferenceTime() {

        String user_id = UserPreferenceUtil.getInstance(getApplicationContext()).UserId();

        String time;

        if (user_id.equals("0")) {

            time = "ASAP";
        } else {
            if (UserPreferenceUtil.getInstance(this).getAsSoonCheck()) {
                time = "ASAP";
            } else {

                time = UserPreferenceUtil.getInstance(this).getDeliveryDate();


            }
        }


        return time;
    }

    private void sendOrderRequest() {
        sendOrderRequest(null, null);
    }

    private void sendOrderRequest(String cardToken, String cardId) {




        if (restaurantHashMap.size() > 0 || supplierHashMap.size() > 0) {
            String message = "";
            if (restaurantHashMap.size() > 0) {
                Iterator myIterator = restaurantHashMap.keySet().iterator();
                while (myIterator.hasNext()) {
                    String key = (String) myIterator.next();
                    Double price = restaurantHashMap.get(key);
                    String restaurantName = "";
                    for (Cart cart : cartList) {
                        if (cart.getMinOrderAmount() == null) {
                            continue;
                        }
                        Double minOrderAmount = Double.parseDouble(cart.getMinOrderAmount());
                        if (cart.getResturantId() != null && cart.getResturantId().equals(key) && price < minOrderAmount) {
                            restaurantName = cart.getRestaurantName();
                            if (message.equals("")) {
                                message = restaurantName + " minimum order price should be more than " + minOrderAmount;
                            } else {
                                message = message + " And " + restaurantName + " minimum order price should be more than " + minOrderAmount;
                            }
                            break;
                        }
                    }


                }
            }

            if (supplierHashMap.size() > 0) {
                Iterator myIterator = supplierHashMap.keySet().iterator();
                while (myIterator.hasNext()) {
                    String key = (String) myIterator.next();
                    Double price = supplierHashMap.get(key);
                    String supplierName = "";
                    for (Cart cart : cartList) {
                        if (cart.getMinOrderAmount() == null) {
                            continue;
                        }
                        Double minOrderAmount = Double.parseDouble(cart.getMinOrderAmount());
                        if (cart.getSupplierID() != null && cart.getSupplierID().equals(key) && price < minOrderAmount) {
                            supplierName = cart.getSupplierName();
                            if (message.equals("")) {
                                message = supplierName + " minimum order price should be more than " + minOrderAmount;
                            } else {
                                message = message + " And " + supplierName + " minimum order price should be more than " + minOrderAmount;
                            }
                            break;
                        }
                    }
                }
            }

            if (!message.equals("")) {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                mProgressDialog.hide();
                return;
            }
        }

//        if(true)
//        return;

        // validate fields
        /*if( !validateAddress() ) {
            return;
        }*/
        /*if( !validatePhoneNumber() ) {
            return;
        }*/

        // check if user has been verified
        boolean verified = true; //UserPreferenceUtil.getInstance(getApplicationContext()).getVerified();

        if (verified) {
            final PlaceOrderRequest placeOrderRequest = ServiceGenrator.createService(PlaceOrderRequest.class);

            DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
            User user = null;

            try {
                user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Gson gson = new Gson();

            UserPreferenceUtil.getInstance(getApplicationContext()).setNumber(editTextPhoneNumber.getText().toString());

            // add address and phone number
//            Cart cart = cartList.get(0);
            int cartItemIndex = 0;
            for (Cart cart : cartList) {

                cart.setDeliveryCharge(String.valueOf(deliveryCharge));
                cart.setAmbassador(String.valueOf(ambassador));
                cart.setNote(editTextNote.getText().toString());
                cart.setUserNumber(editTextPhoneNumber.getText().toString());
                cart.setUserAddress(contentCartDeliveryAdd.getText().toString() + "," + UserPreferenceUtil.getInstance(getApplicationContext()).getLocation());
                cart.setUserLat(UserPreferenceUtil.getInstance(getApplicationContext()).getLocationLat());
                cart.setUserLng(UserPreferenceUtil.getInstance(getApplicationContext()).getLocationLng());
                cart.setAddOnId(cart.getAddOnId());

                // set user for when user logs in at checkout
                if (user != null) {
                    cart.setUserId(user.getId());
                    cart.setReceiver_name(user.getFirstName() + " " + user.getLastName());
                    cart.setUserNumber(user.getNumber());
                } else {
                    cart.setUserId(UserPreferenceUtil.getInstance(getApplicationContext()).UserId());
                    cart.setReceiver_name("guest");

                }

                if (promoCode != null) {
                    cart.setPromoCode(promoCode);
                }

                cart.setDiscount(discount);

              /*  if (imgCheckFutureDate.getVisibility() == View.VISIBLE &&
                        deliveryDay != null && deliveryTime != null) {
                    cart.setTime(deliveryDay + " " + deliveryTime.substring(0, 5));
                }
*/
                String time =UserPreferenceUtil.getInstance(this).getDeliveryDate2();
                cart.setTime(time);

                if (cardId != null) {
                    cart.setCardToken(null);
                    cart.setCardId(cardId);
                    //cart.setCardCustomerId(customerId);
                } else if (cardToken != null) {
                    cart.setCardToken(cardToken);
                    cart.setCardId(null);
                } else if (cardId == null && cardToken == null) {
                    cart.setCardNumber("Cash on delivery");
                }

                if (cart.getAddOnId() == null) {
                    cart.setAddOnId("");
                }
                cart.setDeliveryCharge(String.valueOf(deliveryCharge));

                if (cart.getDishId() != null && !cart.getDishId().equals("")) {
                    double itemCountDouble = Double.parseDouble(cart.getItem_count());
                    int itemCount = (int) itemCountDouble;
                    cart.setItem_count(String.valueOf(itemCount));
                } else {
                    double itemGroceryCountDouble = Double.parseDouble(cart.getItemOrderCount());
                    int itemGroceryCount = (int) itemGroceryCountDouble;
                    cart.setItemOrderCount(String.valueOf(itemGroceryCount));
                }

                Log.d("cart", cart.toString());

                cartList.set(cartItemIndex, cart);
                cartItemIndex++;
            }

            JsonElement element = gson.toJsonTree(cartList, new TypeToken<List<Cart>>() {
            }.getType());

            if (!element.isJsonArray()) {
                return;
            }

            JsonArray jsonArray = element.getAsJsonArray();
            String jsonString = jsonArray.toString();

            Call<PlaceOrderResponse> responseCall = placeOrderRequest.placeOrder(jsonString);
            //mProgressDialog.show();
            responseCall.enqueue(new Callback<PlaceOrderResponse>() {
                @Override
                public void onResponse(Call<PlaceOrderResponse> call, Response<PlaceOrderResponse> response) {
                    Log.v(ConstantUtil.TAG, "placeOrder -> " + response.raw());
                    //mProgressDialog.hide();
                    PlaceOrderResponse placeOrderResponse = response.body();
                    Log.v(ConstantUtil.TAG, "placeOrder body -> " + response.toString());
                    String code = placeOrderResponse.response;

                    if (code.equalsIgnoreCase("0")) {
                        DataBaseHelper dataBaseHelper = new DataBaseHelper(CartActivity.this);
                        if (placeOrderResponse.card != null) {
                            if (mNewCard.getVisibility() != View.GONE) {
                                if (savedCard != null) {
                                    dataBaseHelper.deleteCard(savedCard);
                                }
                                if (mSaveCard.isChecked()) {
                                    savedCard = placeOrderResponse.card;
                                    dataBaseHelper.saveCard(savedCard);
                                    savedCardId = savedCard.getId();
                                }
                            }
                        }
                        dataBaseHelper.deleteAllCartItems();

                        Intent intent = new Intent(CartActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("EXIT", true);
                        startActivity(intent);
                        finish();

                        Toast.makeText(CartActivity.this, "Your order has been placed.", Toast.LENGTH_LONG).show();
                    } else { //if (code.equalsIgnoreCase("1")) {
                        Toast.makeText(CartActivity.this, "We could not place your order. Please try again.", Toast.LENGTH_LONG).show();
                        if (mProgressDialog != null) {
                            mProgressDialog.hide();
                        }
                    }
                }

                @Override
                public void onFailure(Call<PlaceOrderResponse> call, Throwable t) {
                    Log.v(ConstantUtil.TAG, "placeOrder onFailer " + t.getMessage());
                    Toast.makeText(CartActivity.this, "We couldn't register your order. Please try again.", Toast.LENGTH_LONG).show();
                    mProgressDialog.hide();
                }
            });
        } else {
            // start activity for result
            Intent intent = new Intent(CartActivity.this, PhoneVerificationActivity.class);
            startActivityForResult(intent, VERIFICATION);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case VERIFICATION:
                    if (data.getBooleanExtra("verified", false)) {
                        sendOrderRequest();
                    } else {
                        Toast.makeText(CartActivity.this, "Please verify your account before placing your order.", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

  /*  @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        setLocation();
    }


    public void setLocation() {

        Double lat;
        Double lng;
        lat = Double.parseDouble(UserPreferenceUtil.getInstance(this).getLocationLat());
        lng = Double.parseDouble(UserPreferenceUtil.getInstance(this).getLocationLng());

        LatLng currentLocation = new LatLng(lat, lng);
        MarkerOptions marker = new MarkerOptions().position(currentLocation)
                .title("Delivery location");
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin_sm));

        mGoogleMap.addMarker(marker);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16f));
        mGoogleMap.getUiSettings().setAllGesturesEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);

    }*/


    @OnClick(R.id.delivery_location_txt_select_date)
    public void selectDateTime() {

        if (dayTimeRange.getVisibility() != View.VISIBLE) {
            dayTimeRange.setVisibility(View.VISIBLE);

            Calendar cal = Calendar.getInstance();
            int h = cal.get(Calendar.HOUR_OF_DAY);

            final View view = timeRange.findViewWithTag(h + ":00 - " + h + ":30");
            if (view != null) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        timeRange.scrollTo((int) view.getX(), 0);
                    }
                }, 100);
            }
        } else {
            dayTimeRange.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.delivery_location_layout_as_soon)
    public void selectAsSoon() {
        imgCheckAsSoonAs.setVisibility(View.VISIBLE);
        imgCheckFutureDate.setVisibility(View.GONE);
        dayTimeRange.setVisibility(View.GONE);
        UserPreferenceUtil.getInstance(this).setAsSoonCheck(true);
        UserPreferenceUtil.getInstance(this).setFutureCheck(false);
    }

    @OnClick(R.id.content_cart_change_btn)
    public void onClickChnge() {
        startActivity(new Intent(this, DeliveryLocationActivity.class));
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    @Override
    public void onEdit(int position) {
        mCartOnEdit = cartList.get(position);
        CartViewModel cartViewModel = new CartViewModel(this, this);
        if (mCartOnEdit.getDishId() != null && !mCartOnEdit.getDishId().equals("")) {
            cartViewModel.getDishByID(mCartOnEdit.getDishId());
        } else if (mCartOnEdit.getItemGroceryID() != null && !mCartOnEdit.getItemGroceryID().equals("")) {
            cartViewModel.getGroceryByID(mCartOnEdit.getItemGroceryID());
        }
        //Log.d("test2", "onEdit: " + mCartOnEdit.toString());
        finish();
    }

    @Override
    public void onDelete(final int position) {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Are you sure you want to delete this item?");
        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                DataBaseHelper dataBaseHelper = new DataBaseHelper(CartActivity.this);
                dataBaseHelper.deleteCartItem(cartList.get(position));
                makeCartList();

                if (!editTextPromoCode.getText().toString().isEmpty()) {

                    if (cartList.size() != 0) {

                        applyPromoCode();
                    } else

                    {
                        contentCartBtnPromoCode.setText("APPLY");
                        editTextPromoCode.getText().clear();
                    }

                }


                dialog.dismiss();

            }
        });
        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        alert.show();
    }

    public void checkUserEmail(String email, String phone, String address) {
        CheckUserInterface forgotRequest = ServiceGenrator.createService(CheckUserInterface.class);
        Call<CheckUserEmailResponse> forgotcall = forgotRequest.CheckEmailRequest(email, phone, address);
        forgotcall.enqueue(new Callback<CheckUserEmailResponse>() {
            @Override
            public void onResponse(Call<CheckUserEmailResponse> call, Response<CheckUserEmailResponse> response) {
                if (response.code() == 200) {
                    CheckUserEmailResponse apiResponse = response.body();

                    if (apiResponse.getLogin_required().equalsIgnoreCase("1")) {
                        Intent intent = new Intent(CartActivity.this, SignInActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        UserPreferenceUtil.getInstance(context).setUserId(apiResponse.getUser_id());

                        User user = new User();
                        NewProfile newProfile = apiResponse.getProfile();
                        user.setFirstName(newProfile.getFirst_name());
                        user.setLastName(newProfile.getLast_name());
                        user.setEmail(newProfile.getEmail());
                        user.setNumber(newProfile.getNumber());
                        user.setId(newProfile.getUser_id());


                        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
                        dataBaseHelper.saveUser(user);


                        if (mRadioCard.isChecked() && savedCardId == null) {
                            try {
                                new ConnectionTask().execute("");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            if (savedCardId != null) {
                                sendOrderRequest(null, savedCardId);
                            } else {


                                sendOrderRequest();
                            }
                        }

                        // NewProfile  newProfile= apiResponse.getProfile();
                        //UserPreferenceUtil.getInstance(context).setNumber(newProfile.getNumber());

                    }


                    // Toast.makeText(context,apiResponse.message,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckUserEmailResponse> call, Throwable t) {
                LogCat.LogDebug(ConstantUtil.TAG, "Banner ap onFailure " + t.getMessage());
            }
        });
    }

    @Override
    public void OnGetResturntDishSuccess(List<Dish> dishList) {
        Intent intent = new Intent(CartActivity.this, DishDetailActivity.class);
        intent.putExtra(DishDetailActivity.EXTRA_EDIT_DISH, true);
        intent.putExtra(DishDetailActivity.EXTRA_DISH, dishList.get(0));
        intent.putExtra(DishDetailActivity.EXTRA_EDIT_CART, mCartOnEdit);
        startActivity(intent);
        CartActivity.this.overridePendingTransition(R.anim.slide_up, android.R.anim.fade_out);
    }

    @Override
    public void OnGetResturntDishFailed(String error) {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetGrocerySuccess(Grocery grocery) {
        Intent intent = new Intent(CartActivity.this, DishDetailActivity.class);
        intent.putExtra(DishDetailActivity.EXTRA_EDIT_DISH, true);
        intent.putExtra(DishDetailActivity.EXTRA_DISH, grocery);
        intent.putExtra(DishDetailActivity.EXTRA_EDIT_CART, mCartOnEdit);
        startActivity(intent);
        CartActivity.this.overridePendingTransition(R.anim.slide_up, android.R.anim.fade_out);
    }

    @Override
    public void onGetGeoceryFailed(String message) {
        Toast.makeText(this, "Can not get grocery details", Toast.LENGTH_SHORT).show();
    }


    class ConnectionTask extends AsyncTask<String, Void, String> {

        final EditText nameField = (EditText) findViewById(R.id.name);
        final EditText numberField = (EditText) findViewById(R.id.number);
        final EditText cvvField = (EditText) findViewById(R.id.cvv);

        final int errorColor = Color.rgb(255, 215, 152);

        final String name = nameField.getText().toString();
        final String number = numberField.getText().toString();
        final String cvv = cvvField.getText().toString();
        final String month = ccExpireMonth;
        final String year = ccExpireYear;


        private boolean validateCardFields(final String number, final String month, final String year, final String cvv) {
            boolean error = false;
            clearFieldsError();

            if (!CardValidator.validateCardNumber(number)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        numberField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                    }
                });
                error = true;
            }
            if (!CardValidator.validateExpiryDate(month, year)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        monthRange.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                        yearRange.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                    }
                });
                error = true;
            }
            if (cvv.equals("")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cvvField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                    }
                });
                error = true;
            }
            return !error;
        }

        private void clearFieldsError() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "please enter valid  card details", Toast.LENGTH_SHORT).show();
                    mProgressDialog.hide();
                    cvvField.getBackground().clearColorFilter();
                    numberField.getBackground().clearColorFilter();
                    monthRange.getBackground().clearColorFilter();
                    yearRange.getBackground().clearColorFilter();
                }
            });
        }

        @Override
        protected String doInBackground(String... urls) {

            //mProgressDialog.hide();
            Log.d("doInBackground", "Trying to...");
            if (validateCardFields(number, month, year, cvv)) {
                clearFieldsError();
                try {
                    Card card = new Card(number, name, month, year, cvv);
                    CheckoutKit ck = CheckoutKit.getInstance(publicKey, CheckoutKit.Environment.LIVE);
                    final com.checkout.httpconnector.Response<CardTokenResponse> resp = ck.createCardToken(card);
                    if (resp.hasError) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //goToError();
                                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        CardToken ct = resp.model.getCard();
                        sendOrderRequest(resp.model.getCardToken(), null);

                        Log.d("token", resp.model.getJson());
                        //Snackbar.make(cvvField, resp.model.getJson(), Snackbar.LENGTH_LONG).show();
                        return resp.model.getCardToken();
                    }
                } catch (final CardException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (e.getType().equals(CardException.CardExceptionType.INVALID_CVV)) {
                                cvvField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            } else if (e.getType().equals(CardException.CardExceptionType.INVALID_EXPIRY_DATE)) {
                                monthRange.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                                yearRange.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            } else if (e.getType().equals(CardException.CardExceptionType.INVALID_NUMBER)) {
                                numberField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            }
                        }
                    });
                } catch (CheckoutException | IOException e2) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("IOException", e2.toString());
                            //goToError();
                        }
                    });
                }
            }
            return "";
        }

    }


    public void changeMobile(String user_id, final String mobile, final ProgressDialog mProgressDialog) {

        mProgressDialog.show();
        ChangePhoneNumberInterface changemobilerequest = ServiceGenrator.createService(ChangePhoneNumberInterface.class);
        Call<ChnageMobileResponse> forgotcall = changemobilerequest.ChangePhoneRequest(user_id, mobile);
        forgotcall.enqueue(new Callback<ChnageMobileResponse>() {
            @Override
            public void onResponse(Call<ChnageMobileResponse> call, Response<ChnageMobileResponse> response) {
                if (response.code() == 200) {
                    mProgressDialog.hide();
                    ChnageMobileResponse apiResponse = response.body();
                    UserPreferenceUtil.getInstance(context).setNumber(mobile);
                    //editTextPhoneNumber.setClickable(false);
                    // editTextPhoneNumber.setEnabled(false);

                    InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm2.hideSoftInputFromWindow(editTextPhoneNumber.getWindowToken(), 0);

                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    Toast.makeText(context, apiResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ChnageMobileResponse> call, Throwable t) {
                mProgressDialog.hide();
                LogCat.LogDebug(ConstantUtil.TAG, "Banner ap onFailure " + t.getMessage());
            }
        });
    }


}
