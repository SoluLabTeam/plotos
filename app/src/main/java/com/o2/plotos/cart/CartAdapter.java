package com.o2.plotos.cart;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.models.Cart;
import com.o2.plotos.utils.CustomFonts;

import java.math.BigDecimal;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hassan on 6/02/2017.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CardViewHolder> {




    private List<Cart> mCardlist;
    private LayoutInflater mInflater;
    Context mContext;
    CustomFonts customFonts;
    OnActionMenuClicked mOnActionMenuClicked;

    public CartAdapter(Context context, List<Cart> orderList, OnActionMenuClicked onActionMenuClicked) {
        mInflater = LayoutInflater.from(context);
        mCardlist = orderList;
        mContext = context;
        customFonts = new CustomFonts(mContext);
        mOnActionMenuClicked = onActionMenuClicked;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_cart, parent, false);
        CardViewHolder cardViewHolder = new CardViewHolder(v);
        return cardViewHolder;
    }

    @Override
    public void onBindViewHolder(final CardViewHolder holder, final int position) {
        //holder.textCount.setText(mCardlist.get(position).getItem_count());
        holder.textCount.setText("" + (position + 1));
        holder.textName.setText(mCardlist.get(position).getDishName());

        BigDecimal bd2 = new BigDecimal(Double.parseDouble(mCardlist.get(position).getDishPrice()));
//      bd.setScale(2, BigDecimal.ROUND_HALF_UP);   bd.setScale does not change bd
        bd2 = bd2.setScale(2, BigDecimal.ROUND_HALF_UP);
        holder.textPrice.setText(String.valueOf(bd2));
         holder.txtCurrency.setText(mCardlist.get(position).getCurrenyCode());
        customFonts.setOpenSansRegulr(holder.textName);
        customFonts.setOpenSansSemiBold(holder.textPrice);
        customFonts.setOpenSansRegulr(holder.textCount);
        customFonts.setOpenSansRegulr(holder.deleteText);
        customFonts.setOpenSansRegulr(holder.editText);

        holder.menuLayout.setTag(position);
        holder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.menuLayout.setVisibility(View.VISIBLE);
            }
        });

        holder.closeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.menuLayout.setVisibility(View.GONE);
            }
        });

        holder.editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnActionMenuClicked.onEdit(position);
                holder.menuLayout.setVisibility(View.GONE);
            }
        });

        holder.deleteText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnActionMenuClicked.onDelete(position);
                holder.menuLayout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCardlist.size();
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_cart_dish_name)
        TextView textName;
        @BindView(R.id.item_cart_count)
        TextView textCount;
        @BindView(R.id.item_cart_price)
        TextView textPrice;
        @BindView(R.id.menu_button)
        ImageButton menuButton;
        @BindView(R.id.menu_layout)
        LinearLayout menuLayout;
        @BindView(R.id.close_menu)
        ImageButton closeMenu;
        @BindView(R.id.edit_text)
        TextView editText;
        @BindView(R.id.delete_text)
        TextView deleteText;
        @BindView(R.id.txt_currency)
        TextView txtCurrency;

        public CardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnActionMenuClicked {
        void onEdit(int position);

        void onDelete(int position);
    }
}
