package com.o2.plotos.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.o2.plotos.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NutrisionAdp extends RecyclerView.Adapter<NutrisionAdp.NutrisionHolder> {

    private List<Drawable> list;
    private Context context;

    public NutrisionAdp(List<Drawable> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public NutrisionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_images, parent, false);
        return new NutrisionHolder(view);
    }

    @Override
    public void onBindViewHolder(NutrisionHolder holder, int position) {
        holder.img.setImageDrawable(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class NutrisionHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img)
        ImageView img;

        public NutrisionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
