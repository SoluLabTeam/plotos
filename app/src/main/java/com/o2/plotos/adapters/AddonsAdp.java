package com.o2.plotos.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.models.dishdetails.AddOns;
import com.o2.plotos.utils.OpensanTextview;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddonsAdp extends RecyclerView.Adapter<AddonsAdp.AddonsHolder> {
    private List<AddOns> list;
    private Context context;

    public interface addOnClickListner {
        void onClickOfAddons(boolean b, float price,int pos);
    }

    private addOnClickListner listner;

    public AddonsAdp(List<AddOns> list, Context context, addOnClickListner listner) {
        this.list = list;
        this.context = context;
        this.listner = listner;
    }

    @Override
    public AddonsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_addons, parent, false);
        return new AddonsHolder(view);
    }

    @Override
    public void onBindViewHolder(final AddonsHolder holder, final int position) {
        holder.radioAddons.setText(list.get(position).getName());
        holder.price.setText(list.get(position).getPrice() + " " + context.getString(R.string.currency));
        holder.radioAddons.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                listner.onClickOfAddons(b, Float.parseFloat(list.get(position).getPrice()),position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class AddonsHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.radio_addons)
        CheckBox radioAddons;
        @BindView(R.id.price)
        OpensanTextview price;

        public AddonsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
