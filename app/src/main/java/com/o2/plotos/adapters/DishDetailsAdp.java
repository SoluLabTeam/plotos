package com.o2.plotos.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.o2.plotos.R;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.models.resturantDetails.DishData;
import com.o2.plotos.utils.OpensanTextview;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.grantland.widget.AutofitTextView;

public class DishDetailsAdp extends RecyclerView.Adapter<DishDetailsAdp.DishDetialsHolder> {
    public interface onclickOfDishLister {
        void onClickofWholeView(int pos);

        void onClickOfPlusinCart(int pos);

        void onClickOfMinusinCart(int pos);
    }

    private onclickOfDishLister lister;
    private Context context;
    private List<ItemDetais> listDish;

    public DishDetailsAdp(List<ItemDetais> listDish, Context context, onclickOfDishLister lister) {
        this.lister = lister;
        this.context = context;
        this.listDish = listDish;
    }

    @Override
    public DishDetialsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_cat, parent, false);
        return new DishDetialsHolder(view);
    }

    @Override
    public void onBindViewHolder(DishDetialsHolder holder, int position) {
        if (listDish.get(position).getImage().length() > 0) {
            Picasso.with(context)
                    .load(listDish.get(position).getImage())
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.dishImage);
        }
        holder.cartShowDigit.setText(String.valueOf(listDish.get(position).getQuantity()));
        holder.itemDietDishName.setText(listDish.get(position).getName());
        holder.itemDietDishDescription.setText(listDish.get(position).getIngredients());
        holder.itemDietDishPrice.setText(listDish.get(position).getPrice() +" "+ context.getString(R.string.currency));
        if (!listDish.get(position).getDelivery_time().equals("0")) {
            holder.itemDietDishDeliverTime.setText(listDish.get(position).getDelivery_time() + " Mins");
        }
        if (listDish.get(position).getOther_details() != null && !listDish.get(position).getOther_details().equals("")) {
            String details = listDish.get(position).getOther_details();
            String[] dArray = details.split(",");
            if (dArray.length >= 1) {
                if (!dArray[0].trim().equals("NaN")&&!dArray[0].trim().equals("0")) {
                    holder.tvkcal1.setVisibility(View.VISIBLE);
                    holder.tvkcal1.setText(dArray[0] + " kcal");
                }
            }
            if (dArray.length >= 2) {
                if (!dArray[1].trim().equals("NaN")&&!dArray[1].trim().equals("0")) {
                    holder.tvkcal2.setVisibility(View.VISIBLE);
                    holder.tvkcal2.setText(dArray[1] + " Cho");
                }
            }
            if (dArray.length >= 3) {
                if (!dArray[2].trim().equals("NaN")&&!dArray[2].trim().equals("0")) {
                    holder.tvkcal3.setVisibility(View.VISIBLE);
                    holder.tvkcal3.setText(dArray[2] + " Pro");
                }
            }
            if (dArray.length >= 4) {
                if (!dArray[3].trim().equals("NaN")&&!dArray[3].trim().equals("0")) {
                    holder.tvkcal4.setVisibility(View.VISIBLE);
                    holder.tvkcal4.setText(dArray[3] + " Fat");
                }
            }
            if (dArray.length >= 5) {
                if (!dArray[4].trim().equals("NaN")&&!dArray[4].trim().equals("0")) {
                    holder.tvkcal5.setVisibility(View.VISIBLE);
                    holder.tvkcal5.setText(dArray[4] + " Ccal");
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return listDish.size();
    }

    public class DishDetialsHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.dish_image)
        ImageView dishImage;
        @BindView(R.id.item_diet_dish_name)
        OpensanTextview itemDietDishName;
        @BindView(R.id.item_diet_dish_deliver_time)
        OpensanTextview itemDietDishDeliverTime;
        @BindView(R.id.tvkcal1)
        AutofitTextView tvkcal1;
        @BindView(R.id.tvkcal2)
        AutofitTextView tvkcal2;
        @BindView(R.id.tvkcal3)
        AutofitTextView tvkcal3;
        @BindView(R.id.tvkcal4)
        AutofitTextView tvkcal4;
        @BindView(R.id.tvkcal5)
        AutofitTextView tvkcal5;
        @BindView(R.id.calories_layout)
        LinearLayout caloriesLayout;
        @BindView(R.id.item_diet_dish_description)
        OpensanTextview itemDietDishDescription;
        @BindView(R.id.item_diet_dish_price)
        OpensanTextview itemDietDishPrice;
        @BindView(R.id.item_unit)
        AutofitTextView itemUnit;
        @BindView(R.id.cart_minus)
        OpensanTextview cartMinus;
        @BindView(R.id.cart_showDigit)
        OpensanTextview cartShowDigit;
        @BindView(R.id.cart_plus)
        OpensanTextview cartPlus;
        @BindView(R.id.cart_layout)
        LinearLayout cartLayout;
        @BindView(R.id.item_diet_dish_linearLayout)
        LinearLayout itemDietDishLinearLayout;

        public DishDetialsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.cart_minus, R.id.cart_plus, R.id.item_diet_dish_linearLayout})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.cart_minus:
                    lister.onClickOfMinusinCart(getAdapterPosition());
                    break;
                case R.id.cart_plus:
                    lister.onClickOfPlusinCart(getAdapterPosition());
                    break;
                case R.id.item_diet_dish_linearLayout:
                    lister.onClickofWholeView(getAdapterPosition());
                    break;
            }
        }
    }
}
