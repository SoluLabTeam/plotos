package com.o2.plotos.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.o2.plotos.R;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.models.dishdetails.DishDetails;
import com.o2.plotos.restapi.newApis.models.groceryDetails.GroceryDetails;
import com.o2.plotos.utils.OpensanTextview;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CartListingAdp extends RecyclerView.Adapter<CartListingAdp.CartHolder> {

    private List<ItemDetais> list;
    private Context context;
    private CartPlusAndMinusListner listner;

    public interface CartPlusAndMinusListner {
        void onClickOfPlus(int pos);

        void onClickOfMinus(int pos);
    }

    public CartListingAdp(List<ItemDetais> list, Context context, CartPlusAndMinusListner listner) {
        this.list = list;
        this.context = context;
        this.listner = listner;
    }

    @Override
    public CartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_item, parent, false);
        return new CartHolder(view);
    }

    @Override
    public void onBindViewHolder(CartHolder holder, int position) {
        ItemDetais itemDetais = list.get(position);
        holder.setData(itemDetais.getImage(), itemDetais.getName()
                , itemDetais.getIngredients(), itemDetais.getQuantity(), itemDetais.getBasicPrice() * itemDetais.getQuantity());

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class CartHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_item)
        ImageView imgItem;
        @BindView(R.id.cart_item_name)
        OpensanTextview cartItemName;
        @BindView(R.id.cart_item_description)
        OpensanTextview cartItemDescription;
        @BindView(R.id.cart_item_price)
        OpensanTextview cartItemPrice;
        @BindView(R.id.img_minus)
        OpensanTextview imgMinus;
        @BindView(R.id.tv_value)
        OpensanTextview tvValue;
        @BindView(R.id.img_plus)
        OpensanTextview imgPlus;
        @BindView(R.id.rl_plusminus)
        RelativeLayout rlPlusminus;

        public CartHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.img_minus, R.id.img_plus})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.img_minus:
                    if (list.get(getAdapterPosition()).getQuantity() != 1) {
                        listner.onClickOfMinus(getAdapterPosition());
                    }
                    break;
                case R.id.img_plus:
                    listner.onClickOfPlus(getAdapterPosition());
                    break;
            }
        }

        private void setData(String imgUrl, String name, String descri, int count, Double price) {
            Picasso.with(context).load(imgUrl)
                    .placeholder(ContextCompat.getDrawable(context, R.drawable.placeholder))
                    .error(ContextCompat.getDrawable(context, R.drawable.placeholder))
                    .into(imgItem);
            cartItemName.setText(name);
            cartItemDescription.setText(descri);
            tvValue.setText(String.valueOf(count));
            cartItemPrice.setText(String.valueOf(price)+" AED");
        }
    }
}
