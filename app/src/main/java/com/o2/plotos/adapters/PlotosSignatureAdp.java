package com.o2.plotos.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.home.homeTab.NearbyDishDetailAdp;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.models.home.DishDetail;
import com.o2.plotos.restapi.newApis.models.home.NearbyResult;
import com.o2.plotos.restapi.newApis.models.resturantDetails.DishData;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlotosSignatureAdp extends RecyclerView.Adapter<PlotosSignatureAdp.SupplierHolder> implements PSignatureDetailAdp.ImageClickListner {


    private List<List<ItemDetais>> list;
    private List<String> listTitle;
    private Context context;
    private ClickListnerOfPlotosSignature listner;


    @Override
    public void onClickOfImage(int pos, int parentPosition) {
        Utils.navigateToDishOrGroceryDetailsAct(list.get(parentPosition).get(pos).getItem_id(), context, Constant.CAT_DISH);
    }


    public interface ClickListnerOfPlotosSignature {
        void onClickPlotosSignature(int pos);
    }

    public PlotosSignatureAdp(List<List<ItemDetais>> list, List<String> listTitle, Context context, ClickListnerOfPlotosSignature listner) {
        this.list = list;
        this.listTitle = listTitle;
        this.context = context;
        this.listner = listner;

    }

    @Override
    public SupplierHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grocery_detail, parent, false);
        return new SupplierHolder(view);
    }

    @Override
    public void onBindViewHolder(SupplierHolder holder, int position) {
        holder.tvTitle.setText(listTitle.get(position));
        holder.ivNext.setVisibility(View.VISIBLE);
        holder.rv_grocery_cat.setHasFixedSize(true);
        holder.rv_grocery_cat.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        PSignatureDetailAdp mAdapter = new PSignatureDetailAdp(list.get(position), context, this, position);
        holder.rv_grocery_cat.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SupplierHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_grocery_cat)
        RecyclerView rv_grocery_cat;
        @BindView(R.id.iv_next)
        ImageView ivNext;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.iv_rest_image)
        ImageView iv_rest_image;


        public SupplierHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick(R.id.iv_next)
        public void onViewClicked() {
            listner.onClickPlotosSignature(getAdapterPosition());
        }
    }
}
