package com.o2.plotos.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.o2.plotos.R;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.models.home.DishDetail;
import com.o2.plotos.restapi.newApis.models.resturantDetails.DishData;
import com.o2.plotos.utils.OpensanTextview;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.grantland.widget.AutofitTextView;

public class SignatureDetailAdp extends RecyclerView.Adapter<SignatureDetailAdp.SupplierHolder> {

    private List<ItemDetais> list;
    private Context context;
    private ClickListnerOfSignature listner;


    public interface ClickListnerOfSignature {
        void onClickCatSignature(int pos);

        void onClickOfPlusinCartSign(int pos);

        void onClickOfMinusinCartSign(int pos);
    }

    public SignatureDetailAdp(List<ItemDetais> list, Context context, ClickListnerOfSignature listner) {
        this.list = list;
        this.context = context;
        this.listner = listner;

    }

    @Override
    public SupplierHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_cat, parent, false);
        return new SupplierHolder(view);
    }

    @Override
    public void onBindViewHolder(SupplierHolder holder, int position) {
        if (list.get(position).getImage() != null && !list.get(position).getImage().equals("")) {
            Picasso.with(context).load(list.get(position).getImage())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.dishImage);
        }
        holder.cart_showDigit.setText(String.valueOf(list.get(position).getQuantity()));
        holder.itemDietDishName.setText(list.get(position).getName());
        holder.itemDietDishDeliverTime.setText(list.get(position).getDelivery_time() + " min");
        holder.itemDietDishDescription.setText(list.get(position).getIngredients());
        holder.itemDietDishPrice.setText(list.get(position).getPrice() + " AED");
        if (list.get(position).getOther_details() != null && !list.get(position).getOther_details().equals("")) {
            String details = list.get(position).getOther_details();
            String[] dArray = details.split(",");
            if (dArray.length >= 1) {
                if (!dArray[0].trim().equals("NaN")&&!dArray[0].trim().equals("0")) {
                    holder.tvkcal1.setVisibility(View.VISIBLE);
                    holder.tvkcal1.setText(dArray[0] + " kcal");
                }
            }
            if (dArray.length >= 2) {
                if (!dArray[1].trim().equals("NaN")&&!dArray[1].trim().equals("0")) {
                    holder.tvkcal2.setVisibility(View.VISIBLE);
                    holder.tvkcal2.setText(dArray[1] + " Cho");
                }
            }
            if (dArray.length >= 3) {
                if (!dArray[2].trim().trim().equals("NaN")&&!dArray[2].trim().equals("0")) {
                    holder.tvkcal3.setVisibility(View.VISIBLE);
                    holder.tvkcal3.setText(dArray[2] + " Pro");
                }
            }
            if (dArray.length >= 4) {
                if (!dArray[3].trim().equals("NaN")&&!dArray[3].trim().equals("0")) {
                    holder.tvkcal4.setVisibility(View.VISIBLE);
                    holder.tvkcal4.setText(dArray[3] + " Fat");
                }
            }
            if (dArray.length >= 5) {
                if (!dArray[4].trim().equals("NaN")&&!dArray[4].trim().equals("0")) {
                    holder.tvkcal5.setVisibility(View.VISIBLE);
                    holder.tvkcal5.setText(dArray[4] + " Ccal");
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SupplierHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.dish_image)
        ImageView dishImage;
        @BindView(R.id.item_diet_dish_name)
        OpensanTextview itemDietDishName;
        @BindView(R.id.item_diet_dish_deliver_time)
        OpensanTextview itemDietDishDeliverTime;
        @BindView(R.id.tvkcal1)
        AutofitTextView tvkcal1;
        @BindView(R.id.tvkcal2)
        AutofitTextView tvkcal2;
        @BindView(R.id.tvkcal3)
        AutofitTextView tvkcal3;
        @BindView(R.id.tvkcal4)
        AutofitTextView tvkcal4;
        @BindView(R.id.tvkcal5)
        AutofitTextView tvkcal5;
        @BindView(R.id.item_diet_dish_description)
        OpensanTextview itemDietDishDescription;
        @BindView(R.id.item_diet_dish_price)
        OpensanTextview itemDietDishPrice;
        @BindView(R.id.item_unit)
        AutofitTextView itemUnit;

        @BindView(R.id.cart_minus)
        OpensanTextview cart_minus;

        @BindView(R.id.cart_showDigit)
        OpensanTextview cart_showDigit;

        @BindView(R.id.cart_plus)
        OpensanTextview cart_plus;

        @BindView(R.id.item_diet_dish_linearLayout)
        LinearLayout ll_item;


        public SupplierHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick({R.id.item_diet_dish_linearLayout,R.id.cart_plus,R.id.cart_minus})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.item_diet_dish_linearLayout:
                    listner.onClickCatSignature(getAdapterPosition());
                    break;
                case R.id.cart_plus:
                    listner.onClickOfPlusinCartSign(getAdapterPosition());
                    break;
                case R.id.cart_minus:
                    listner.onClickOfMinusinCartSign(getAdapterPosition());
                    break;
            }
        }
    }
}
