package com.o2.plotos.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.OpensanTextview;
import com.o2.plotos.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConformOrderItemDetailsAdp extends RecyclerView.Adapter<ConformOrderItemDetailsAdp.ItemDetailsHolder> {

    private List<ItemDetais> listItemDetails;
    private Context context;
    private ClickListnerOfDelete clickListnerOfDelete;
    private int parentPosition;

    public ConformOrderItemDetailsAdp(List<ItemDetais> listItemDetails, Context context, ClickListnerOfDelete clickListnerOfDelete, int parentPosition) {
        this.listItemDetails = listItemDetails;
        this.context = context;
        this.clickListnerOfDelete = clickListnerOfDelete;
        this.parentPosition = parentPosition;
    }

    public interface ClickListnerOfDelete {
        void onClickDelete(int pos, int parentPosition);
    }

    @Override
    public ItemDetailsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_confirm_order_detail, parent, false);
        return new ItemDetailsHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemDetailsHolder holder, final int position) {
        holder.tvDishName.setText(listItemDetails.get(position).getName());
        holder.tvQuantity.setText(String.valueOf(listItemDetails.get(position).getQuantity()));
//        String price = String.valueOf(Html.fromHtml(String.valueOf(listItemDetails.get(position).getBasicPrice() * listItemDetails.get(position).getQuantity()) + "<sup>AED</sup>"));
        ;
        Spanned spanned=Html.fromHtml(String.valueOf(listItemDetails.get(position).getBasicPrice() * listItemDetails.get(position).getQuantity())  +"<sup><small> AED</small></sup>");
        holder.tvPrice.setText(spanned);
//        holder.tvPrice.setText(price);
        holder.menuLayout.setTag(position);
        holder.ivSetting.setTag(holder.menuLayout);
        holder.closeMenu.setTag(holder.menuLayout);
        holder.deleteText.setTag(holder.menuLayout);
        holder.editText.setTag(holder.menuLayout);
        holder.ivSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((View) v.getTag()).setVisibility(View.VISIBLE);
            }
        });

        holder.closeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((View) v.getTag()).setVisibility(View.GONE);
            }
        });

        holder.editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((View) v.getTag()).setVisibility(View.GONE);
                if (listItemDetails.get(position).isGrocery()) {
                    Utils.navigateToDishOrGroceryDetailsActWithQuantity(listItemDetails.get(position).getItem_id(), context, Constant.CAT_GROCERY,listItemDetails.get(position).getQuantity());
                } else {
                    Utils.navigateToDishOrGroceryDetailsActWithQuantity(listItemDetails.get(position).getItem_id(), context, Constant.CAT_DISH,listItemDetails.get(position).getQuantity());
                }
            }
        });

        holder.deleteText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((View) v.getTag()).setVisibility(View.GONE);
                clickListnerOfDelete.onClickDelete(position, parentPosition);

            }
        });

    }

    @Override
    public int getItemCount() {
        return listItemDetails.size();
    }

    public class ItemDetailsHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvQuantity)
        OpensanTextview tvQuantity;
        @BindView(R.id.tvDishName)
        OpensanTextview tvDishName;
        @BindView(R.id.tvPrice)
        OpensanTextview tvPrice;
        @BindView(R.id.ivSetting)
        ImageView ivSetting;

        @BindView(R.id.menu_layout)
        LinearLayout menuLayout;

        @BindView(R.id.close_menu)
        ImageButton closeMenu;

        @BindView(R.id.edit_text)
        TextView editText;

        @BindView(R.id.delete_text)
        TextView deleteText;


        public ItemDetailsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
