package com.o2.plotos.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.o2.plotos.R;
import com.o2.plotos.models.ConfirmOrder;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.utils.OpensanTextview;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderListingAdp extends RecyclerView.Adapter<OrderListingAdp.OrderHolder> implements ConformOrderItemDetailsAdp.ClickListnerOfDelete {

    private List<ConfirmOrder> listConfirmOrder;
    private Context context;
    private ClickListnerOfDelete clickListnerOfDelete;

    public OrderListingAdp(List<ConfirmOrder> listConfirmOrder, Context context, ClickListnerOfDelete clickListnerOfDelete) {
        this.listConfirmOrder = listConfirmOrder;
        this.context = context;
        this.clickListnerOfDelete = clickListnerOfDelete;
    }

    public interface ClickListnerOfDelete {
        void onClickDelete(int pos, int parentPos);
    }

    @Override
    public OrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_confirm_order, parent, false);
        return new OrderHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderHolder holder, int position) {
        Picasso.with(context).load(listConfirmOrder.get(position).getProvider_img())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.restImage);

        holder.restName.setText(listConfirmOrder.get(position).getProvider_name());
        if (listConfirmOrder.get(position).getPre_booking().trim().equals("0")) {
            holder.tvRestHours.setText("60 MINS " + " | " + "Min Order AED " + listConfirmOrder.get(position).getMin_order_amount());
        } else {
            holder.tvRestHours.setText(listConfirmOrder.get(position).getPre_booking() + " hours" + " | " + "Min Order AED " + listConfirmOrder.get(position).getMin_order_amount());
        }
//        holder.tvRestTimings.setText("Open at 11:00 AM - 1.30 PM");
        Spanned spanned = Html.fromHtml("5<sup><small> AED</small></sup>");
        holder.tvDeliberyFees.setText(spanned);
        holder.tvTotal.setText(Utils.getAEDSpannable(getTotal(listConfirmOrder.get(position).getItemDetais())));
        holder.rvDishes.setLayoutManager(new LinearLayoutManager(context));
        holder.rvDishes.setAdapter(new ConformOrderItemDetailsAdp(listConfirmOrder.get(position).getItemDetais(), context, this, position));
    }

    private Double getTotal(List<ItemDetais> list) {
        Double totalPriceOfSingleRest = 5.0;
        for (int i = 0; i < list.size(); i++) {
            totalPriceOfSingleRest += (list.get(i).getBasicPrice() * list.get(i).getQuantity());
        }
        Double totalfinal = 0.0;
        Double totalPref = 0.0;
        String total=UserPreferenceUtil.getInstance(context).getDeliveryTotal();
        if (total!=null && !total.equals("")) {
            totalPref = Double.valueOf(total);
        }
        totalfinal = totalPref + totalPriceOfSingleRest;
        UserPreferenceUtil.getInstance(context).setDeliveryTotal(String.valueOf(totalfinal));
        EventBus.getDefault().post(UserPreferenceUtil.getInstance(context).getDeliveryTotal());
        return totalPriceOfSingleRest;
    }

    @Override
    public int getItemCount() {
        return listConfirmOrder.size();
    }

    @Override
    public void onClickDelete(int pos, int parentPosition) {
        clickListnerOfDelete.onClickDelete(pos, parentPosition);
    }

    public class OrderHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rest_image)
        ImageView restImage;
        @BindView(R.id.rest_name)
        OpensanTextview restName;
        @BindView(R.id.tv_rest_hours)
        OpensanTextview tvRestHours;
        @BindView(R.id.tv_rest_timings)
        OpensanTextview tvRestTimings;
        @BindView(R.id.rv_dishes)
        RecyclerView rvDishes;
        @BindView(R.id.tvDeliberyFees)
        OpensanTextview tvDeliberyFees;
        @BindView(R.id.tvTotal)
        OpensanTextview tvTotal;

        public OrderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
