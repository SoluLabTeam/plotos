package com.o2.plotos;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.o2.plotos.authentication.signin.SignInActivityNew;
import com.o2.plotos.utils.PlayGifView;

import java.util.List;

public class BaseActivity extends AppCompatActivity {
    private Dialog dialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showProgress() {
        if (dialog == null) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progress_dialog);
            ImageView imahe= (ImageView) dialog.findViewById(R.id.imahe);
            Glide.with(this).load(R.drawable.loading).into(imahe);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
        }
        dialog.show();
    }

    public void showTryAgainMsg() {
        Toast.makeText(this, getString(R.string.msg_please_try_again), Toast.LENGTH_SHORT).show();
    }

    public void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public boolean isAllFieldFill(List<EditText> list) {
        for (int i = 0; i < list.size(); i++) {
            EditText editText = list.get(i);
            String data = editText.getText().toString().trim();
            if (data.isEmpty()) {
                Toast.makeText(this, getString(R.string.msg_plz_fill), Toast.LENGTH_SHORT).show();
                return false;

            }
        }
        return true;
    }

    public boolean isEdittextfilled(EditText editText) {
        if (TextUtils.isEmpty(editText.getText().toString().trim())) {
            return false;
        }
        return true;
    }

    public void hideProgress() {
        dialog.dismiss();
    }

    public String getDataFromEditText(EditText editText) {
        return editText.getText().toString().trim();
    }

    public boolean isValidemail(String email) {
        if (!(!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
            Toast.makeText(this, getString(R.string.error_empty_email), Toast.LENGTH_SHORT).show();
        }
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
