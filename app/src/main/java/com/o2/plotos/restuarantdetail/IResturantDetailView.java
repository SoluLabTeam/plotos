package com.o2.plotos.restuarantdetail;

import com.o2.plotos.models.Dish;

import java.util.List;

/**
 * Created by Hassan on 8/02/2017.
 */

public interface IResturantDetailView {
    //void showProgress();
    //void hideProgress();
    void onGetRestaurantDishSuccess(List<Dish> dishList);
    void onGetRestaurantDishFailed(String error);
}
