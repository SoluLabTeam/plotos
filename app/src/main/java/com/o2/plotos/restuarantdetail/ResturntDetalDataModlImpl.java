package com.o2.plotos.restuarantdetail;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.DishesRequest;
import com.o2.plotos.restapi.responses.DishesRequestResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hassan on 8/02/2017.
 */

public class ResturntDetalDataModlImpl implements IResturntDetalDataModl{

    OnGetResturntDishRqustFinishLisener mLisener;

    public ResturntDetalDataModlImpl(OnGetResturntDishRqustFinishLisener lisener){
        mLisener = lisener;
    }

    @Override
    public void getResturntDishRqust(String type,String restaurant_id, String category_id, String calorie, String latitude, String longitude) {
        DishesRequest resturntDishRqust = ServiceGenrator.createService(DishesRequest.class);
        Call<DishesRequestResponse> resturantDishRequestCall =
                resturntDishRqust.dishesRequest(type,restaurant_id,category_id,calorie, latitude, longitude);
        resturantDishRequestCall.enqueue(new Callback<DishesRequestResponse>() {
            @Override
            public void onResponse(Call<DishesRequestResponse> call, Response<DishesRequestResponse> response) {
                LogCat.LogDebug(ConstantUtil.TAG," -> ResturandResult Request api response "+response.raw());
                if(response.code()== 200){
                    DishesRequestResponse apiResponse =  response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        mLisener.OnGetResturntDishSuccess(apiResponse.dish);

                    }else if(responseCode.equalsIgnoreCase("1")){
                        mLisener.OnGetResturntDishFailed("Resturant ResturandResult request Failed(1)");
                    }else if(responseCode.equalsIgnoreCase("2")){
                        mLisener.OnGetResturntDishFailed("Resturant ResturandResult request Failed(2)");
                    }
                }
            }
            @Override
            public void onFailure(Call<DishesRequestResponse> call, Throwable t) {
                mLisener.OnGetResturntDishFailed("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"Resturant dish ap onFailure "+t.getMessage());
            }
        });

    }
}
