package com.o2.plotos.restuarantdetail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.adapters.ViewPagerAdapter;
import com.o2.plotos.cart.CartActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.Dish;
import com.o2.plotos.models.Restaurant;
import com.o2.plotos.restuarantdetail.dishtab.DishFragment;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RestuarantDetailActivity extends AppCompatActivity implements IResturantDetailView {

    @BindView(R.id.restuarant_detail_viewpager)
    ViewPager viewPager;
    @BindView(R.id.restuarant_detail_tablayout)
    TabLayout tabLayout;

    @BindView(R.id.empty_view)
    LinearLayout emptyView;
    @BindView(R.id.restaurant_detail_restaurantName)
    TextView restaurantNameTextView;
    @BindView(R.id.restaurant_detail_description)
    TextView resturantDescription;
    @BindView(R.id.restaurant_detail_open_close_time)
    TextView openCloseTime;
    @BindView(R.id.restuarant_detail_image_cover)
    ImageView imageViewCover;
    @BindView(R.id.text_no_dish)
    TextView emptyTextView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.no_location_layout)
    RelativeLayout mNoLocationLayout;
    @BindView(R.id.no_data_title)
    TextView mNoDataTile;
    @BindView(R.id.no_data_message)
    TextView mNoDataMessage;
    @BindView(R.id.activity_home_btn_cart)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.activity_home_txt_item)
    TextView txtCartItem;

    CustomFonts customFonts;
    @BindView(R.id.diet_detail_sharing)
    ImageButton dietDetailSharing;
    @BindView(R.id.img_restorent)
    ImageView imgRestorent;
    @BindView(R.id.activity_resturant_name)
    TextView activityResturantName;
    @BindView(R.id.no_data_image)
    ImageView noDataImage;
    @BindView(R.id.cart_layout)
    LinearLayout cartLayout;

    private ProgressDialog mProgressDialog;
    private String REQUEST_TYPE = "get_dishes_by_restaurant";
    public static final String EXTRA_RESTURANT = "resturant_id";
    private ArrayList<Dish> mDishList = new ArrayList<>();
    private ArrayList<Dish> breakFastDishList = new ArrayList<>();
    private ArrayList<Dish> launcDishList = new ArrayList<>();
    private ArrayList<Dish> dinnerDishList = new ArrayList<>();
    private ArrayList<Dish> snackDishList = new ArrayList<>();
    private ArrayList<Dish> allDayDishList = new ArrayList<>();
    private Restaurant mResturant;
    private IResturantDetalPrsnter mRestaurantPresenter;
    DataBaseHelper dataBaseHelper = new DataBaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_detail_new);
        ButterKnife.bind(this);

        mRestaurantPresenter = new ResturntDetalPrsntrImpl();
        mRestaurantPresenter.bindView(this);
        initializeView();
        setResturantDetail();

        if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
            String userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
            String userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
            if (!userLat.equals("0.0") && !userLong.equals("0.0")) {
                mRestaurantPresenter.sendRestrntDishRqust(REQUEST_TYPE, mResturant.getId(), "0", "0", userLat, userLong);
            } else {
                Toast.makeText(this, getString(R.string.error_no_location), Toast.LENGTH_LONG).show();
            }
        } else {
            setupViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    private void initializeView() {
        ButterKnife.bind(this);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait");

        customFonts = new CustomFonts(this);
        customFonts.setOswaldBold(restaurantNameTextView);
        customFonts.setOpenSansSemiBold(resturantDescription);
        customFonts.setOpenSansRegulr(openCloseTime);
        customFonts.setOpenSansRegulr(activityResturantName);
        customFonts.setOpenSansBold(mNoDataTile);
        customFonts.setOpenSansRegulr(mNoDataMessage);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RestuarantDetailActivity.this, CartActivity.class));
            }
        });
        setCartFab();
    }

    /**
     * Show cart item from database
     */
    public void setCartFab() {
        int cartItemSize = dataBaseHelper.getCartItems().size();
        if (cartItemSize > 0) {
            floatingActionButton.show();
            txtCartItem.setVisibility(View.VISIBLE);
            txtCartItem.setText("" + cartItemSize);
        } else {
            txtCartItem.setVisibility(View.GONE);
            floatingActionButton.hide();
        }
    }

    private void setResturantDetail() {
        mResturant = getIntent().getExtras().getParcelable(EXTRA_RESTURANT);
        restaurantNameTextView.setText(mResturant.getName());
        resturantDescription.setText(mResturant.getResturntDescrptn());
        activityResturantName.setText(mResturant.getName());
        openCloseTime.setText(mResturant.getPreBooking() + " hours |" + "Min Order " + mResturant.getStartPrice() + " | " + "Open at " + mResturant.getResturantOpenTime() + " - " + mResturant.getResturantCloseTime());

  /*      if(!mResturant.getPreBooking().equalsIgnoreCase("0")){
            openCloseTime.setText(mResturant.getPreBooking() + " Hours notice.");
            openCloseTime.setTextColor(Color.rgb(246,122,64));
        }else {
            openCloseTime.setText(mResturant.getResturantOpenTime()+ " - "+mResturant.getResturantCloseTime());
        }
*/
        if (!mResturant.getImage_url().equalsIgnoreCase("")) {
            Picasso.with(this).load(mResturant.getImage_url())
                    .resize(400, 200)
                    .centerCrop()
                    .error(R.drawable.placeholder)
                    .into(imageViewCover);

            Picasso.with(this).load(mResturant.getImage_url())
                    .error(R.drawable.placeholder)
                    .into(imgRestorent);
        } else {
            imgRestorent.setImageResource(R.drawable.placeholder);
            imageViewCover.setImageResource(R.drawable.placeholder);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        mRestaurantPresenter.unBindView();
    }

    private void setupViewPager(ViewPager viewPager) {

        int currentItem = 0;


        for (int i = 0; i < mDishList.size(); i++) {
            if (mDishList.get(i).getBreakFast().equalsIgnoreCase("1")) {
                breakFastDishList.add(mDishList.get(i));
            }
            if (mDishList.get(i).getDinner().equalsIgnoreCase("1")) {
                dinnerDishList.add(mDishList.get(i));
            }
            if (mDishList.get(i).getLaunch().equalsIgnoreCase("1")) {
                launcDishList.add(mDishList.get(i));
            }

            if (mDishList.get(i).getSnacks().equalsIgnoreCase("1")) {
                snackDishList.add(mDishList.get(i));
            } else {
                allDayDishList.add(mDishList.get(i));
            }


        }

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (breakFastDishList.size() > 0) {
            adapter.addFrag(DishFragment.newInstance(breakFastDishList), "BreakFast");
            currentItem++;
        }
        if (launcDishList.size() > 0) {
            adapter.addFrag(DishFragment.newInstance(launcDishList), "Lunch");
            currentItem++;
        }
        if (dinnerDishList.size() > 0) {
            adapter.addFrag(DishFragment.newInstance(dinnerDishList), "Dinner");
            currentItem++;
        }
        if (snackDishList.size() > 0) {
            adapter.addFrag(DishFragment.newInstance(snackDishList), "Snacks");
            currentItem++;
        }
        if (allDayDishList.size() > 0) {
            adapter.addFrag(DishFragment.newInstance(allDayDishList), "ALL");
            currentItem++;
        }

        if (currentItem > 0) {
            emptyView.setVisibility(View.GONE);
            mNoLocationLayout.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
            mNoLocationLayout.setVisibility(View.GONE);
        }

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(currentItem);
    }

    @OnClick(R.id.restuarant_detail_img_cross)
    public void closeScreen() {
        finish();
        //overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
    }

    @Override
    public void showProgress() {
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog.hide();
    }*/

    @Override
    public void onGetRestaurantDishSuccess(List<Dish> dishList) {

        progressBar.setVisibility(View.GONE);

        mDishList = (ArrayList<Dish>) dishList;
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        //Toast.makeText(this, "loaded", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetRestaurantDishFailed(String error) {

        progressBar.setVisibility(View.GONE);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        emptyTextView.setVisibility(View.VISIBLE);

        //Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.diet_detail_sharing)
    public void onViewClicked() {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Check out this app https://play.google.com/store/apps/details?id=com.o2.plotos&hl=en ");
        startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }
}
