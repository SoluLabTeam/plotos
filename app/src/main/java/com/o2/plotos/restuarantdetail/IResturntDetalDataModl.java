package com.o2.plotos.restuarantdetail;

import com.o2.plotos.models.Dish;

import java.util.List;

/**
 * Created by Hassan on 8/02/2017.
 */

public interface IResturntDetalDataModl {
    void getResturntDishRqust(String type,String restaurant_id, String category_id, String calorie, String latitude, String longitude);
    interface OnGetResturntDishRqustFinishLisener{
        void OnGetResturntDishSuccess(List<Dish> dishList);
        void OnGetResturntDishFailed(String error);
    }
}
