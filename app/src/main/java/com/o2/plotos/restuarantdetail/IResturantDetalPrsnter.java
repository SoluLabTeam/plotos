package com.o2.plotos.restuarantdetail;

/**
 * Created by Hassan on 8/02/2017.
 */

public interface IResturantDetalPrsnter {
    void sendRestrntDishRqust(String type,String restaurant_id, String category_id, String calorie, String latitude, String longitude);
    void bindView(Object view);
    void unBindView();
}
