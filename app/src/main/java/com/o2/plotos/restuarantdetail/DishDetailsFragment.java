package com.o2.plotos.restuarantdetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.adapters.DishDetailsAdp;
import com.o2.plotos.home.order.DeliverToActivity;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DishDetailsFragment extends Fragment implements DishDetailsAdp.onclickOfDishLister {

    @BindView(R.id.fragment_breakfast_recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.empty_view)
    LinearLayout emptyView;
    @BindView(R.id.text_no_dish)
    TextView emptyTextView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;


    private List<ItemDetais> mDishList;
    DishDetailsAdp dishAdapter;

    public DishDetailsFragment() {
        // Required empty public constructor
    }

    public static DishDetailsFragment newInstance(List<ItemDetais> dishList) {
        DishDetailsFragment dishFragment = new DishDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("dishList", Parcels.wrap(dishList));
        dishFragment.setArguments(bundle);
        return dishFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDishList = Parcels.unwrap(getArguments().getParcelable("dishList"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_break_fast, container, false);
        ButterKnife.bind(this, view);
        setDishAdapter();


        return view;
    }


    /*@Override
    public void onDishClick(Dish dish) {
        Intent intent = new Intent(getActivity(), DishDetailActivity.class);
        intent.putExtra(DishDetailActivity.EXTRA_DISH, dish);
        startActivity(intent);
        //getActivity().overridePendingTransition(R.anim.slide_up,android.R.anim.fade_out);
    }

    @Override
    public void onCartChanged() {
        ((RestuarantDetailActivity) getActivity()).setCartFab();
    }

    @Override
    public void onShowToast(int resID) {
        Utils.showCartToast(getActivity(), resID);
    }*/

    private void setDishAdapter() {
        progressBar.setVisibility(View.GONE);
        if (mDishList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(linearLayoutManager);
//            RestaurantDetailAdapter dishAdapter = new RestaurantDetailAdapter(mDishList, getActivity(), this);
            dishAdapter = new DishDetailsAdp(mDishList, getActivity(), this);
            mRecyclerView.setAdapter(dishAdapter);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyTextView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onClickofWholeView(int pos) {
        Utils.navigateToDishOrGroceryDetailsAct(mDishList.get(pos).getItem_id(), getContext(), Constant.CAT_DISH);

    }

    @Override
    public void onClickOfPlusinCart(int pos) {
        ItemDetais itemDetais = mDishList.get(pos);
        itemDetais.setQuantity(Utils.updateQuantity(itemDetais.getQuantity(), true));
        mDishList.set(pos, itemDetais);
        dishAdapter.notifyItemChanged(pos);
        Utils.storeDatainToCart(itemDetais, getActivity(), itemDetais.getQuantity(), Double.valueOf(itemDetais.getPrice()));
        EventBus.getDefault().post("");
    }

    @Override
    public void onClickOfMinusinCart(int pos) {
        ItemDetais itemDetais = mDishList.get(pos);
        itemDetais.setQuantity(Utils.updateQuantity(itemDetais.getQuantity(), false));
        mDishList.set(pos, itemDetais);
        dishAdapter.notifyItemChanged(pos);
        if (itemDetais.getQuantity() > 0) {
            Utils.storeDatainToCart(itemDetais, getActivity(), itemDetais.getQuantity(), Double.valueOf(itemDetais.getPrice()));
        }
        EventBus.getDefault().post("");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

}
