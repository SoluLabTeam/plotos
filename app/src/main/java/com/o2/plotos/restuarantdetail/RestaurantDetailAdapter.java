package com.o2.plotos.restuarantdetail;

/**
 * Created by Hassan on 3/02/2017.
 */

//public class RestaurantDetailAdapter extends RecyclerView.Adapter<RestaurantDetailAdapter.RestaurantViewHolder> implements CheckInRadiusListener {
//    private OnDishListener mDishListener;
//    private ArrayList<ResturandResult> mDishList;
//    private Context mContext;
//    private LayoutInflater mInflater;
//    CustomFonts customFonts;
//    CheckIfRestaurantInRadiusHelper radiusHelper;
//    private int mCartDishIndex = 0;
//    private int mCartItemCount = 0;
//    String userLat  = UserPreferenceUtil.getInstance(mContext).getLocationLat();
//    String userLong = UserPreferenceUtil.getInstance(mContext).getLocationLng();
//    String userAddress = UserPreferenceUtil.getInstance(mContext).getLocation();
//
//    public RestaurantDetailAdapter(ArrayList<ResturandResult> dishes, Context context, OnDishListener dishListener) {
//        mDishList = dishes;
//        mInflater = LayoutInflater.from(context);
//        mContext = context;
//        mDishListener = dishListener;
//        customFonts = new CustomFonts(context);
//        radiusHelper = new CheckIfRestaurantInRadiusHelper(this);
//    }
//
//
//    @Override
//    public RestaurantDetailAdapter.RestaurantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View v = mInflater.inflate(R.layout.item_dish, parent, false);
//        RestaurantViewHolder viewHolder = new RestaurantViewHolder(v);
//        return viewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(final RestaurantDetailAdapter.RestaurantViewHolder holder, final int position) {
//        final ResturandResult dish = mDishList.get(position);
//        holder.dishName.setText(dish.getDishName());
//        holder.dishDescrptn.setText(dish.getDescrptn());
//        if (!dish.getPreBookingtime().equalsIgnoreCase("0")){
//            holder.deliveryTime.setText(dish.getPreBookingtime() + " Hours notice.");
//            holder.deliveryTime.setTextColor(Color.rgb(246,122,64));
//            holder.preBooking.setImageResource(R.drawable.timer);
//        } else {
//            holder.deliveryTime.setText(dish.getDeliveryTime() + " Min");
//        }
//        holder.dishPrice.setText(dish.getPrice() + " " + dish.getCurrency());
//        String[] cal = dish.getDishCal().split(",");
//        if (cal.length > 0) {
//            if (cal[cal.length - 1].equalsIgnoreCase("") || cal[cal.length - 1].equalsIgnoreCase("NaN") || cal[cal.length - 1].equalsIgnoreCase("0.0") || cal[cal.length - 1].equalsIgnoreCase("0")) {
//                holder.caloriesLayout.setVisibility(View.GONE);
//            } else if (!cal[cal.length - 1].equalsIgnoreCase("")) {
//                holder.caloriesLayout.setVisibility(View.VISIBLE);
//                holder.calText.setText(cal[0].toString() + " Cal");
//                holder.carbsText.setText(cal[1].toString() + " Carbs");
//                holder.proText.setText(cal[2].toString() + " Pro");
//                holder.fatText.setText(cal[3].toString() + " Fat");
//            }
//
//        }
//        if (!dish.getImage_url().equalsIgnoreCase("")) {
//            Picasso.with(mContext).load(dish.getImage_url())
//                    .resize(400, 200)
//                    .centerCrop()
//                    .error(R.drawable.placeholder)
//                    .into(holder.dishImage);
//        } else {
//            holder.dishImage.setImageResource(R.drawable.placeholder);
//        }
//
//        holder.minus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int count = Integer.parseInt( holder.digit.getText().toString());
//                if(count > 0){
//                    count = count - 1;
//                    holder.digit.setText(String.valueOf(count));
//                }
//            }
//        });
//
//        holder.plus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int count = Integer.parseInt( holder.digit.getText().toString());
//                count = count +1;
//                holder.digit.setText(String.valueOf(count));
//            }
//        });
//
//        holder.addToCartText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mCartDishIndex = position;
//                mCartItemCount = Integer.parseInt( holder.digit.getText().toString());
//                if(mCartItemCount > 0) {
//                    radiusHelper.checkInRadiusRequest(dish.getResturantId(), userLat, userLong, true);
//                }
//            }
//        });
//
//        customFonts.setOpenSansSemiBold(holder.dishName);
//        customFonts.setOpenSansRegulr(holder.dishDescrptn);
//        customFonts.setOpenSansSemiBold(holder.deliveryTime);
//        customFonts.setOpenSansSemiBold(holder.dishPrice);
//
//        customFonts.setOpenSansSemiBold(holder.calText);
//        customFonts.setOpenSansSemiBold(holder.carbsText);
//        customFonts.setOpenSansSemiBold(holder.proText);
//        customFonts.setOpenSansSemiBold(holder.fatText);
//
//        customFonts.setOpenSansSemiBold(holder.digit);
//        customFonts.setOpenSansSemiBold(holder.addToCartText);
//
//        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mDishListener.onDishClick(dish);
//            }
//        });
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return mDishList.size();
//    }
//
//    @Override
//    public void OnResponseSuccess() {
//        ResturandResult dish = mDishList.get(mCartDishIndex);
//        DataBaseHelper dataBaseHelper = new DataBaseHelper(mContext);
//        Cart cart = null;
//        User user = null;
//        boolean isEdit = false;
//        try {
//            cart  = dataBaseHelper.getCartIntegerDao().queryBuilder().where().eq("dishId", dish.getDishId()).queryForFirst();
//            user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();
//            int grandTotal = (int) Double.parseDouble(dish.getPrice()) * mCartItemCount;
//            int totalCount = mCartItemCount;
//            if (cart == null){
//                cart = new Cart();
//            }else{
//                isEdit = true;
//                totalCount = mCartItemCount + Integer.parseInt(cart.getItem_count());
//                grandTotal = (int) Double.parseDouble(dish.getPrice()) * totalCount;
//            }
//            cart.setUserLat(userLat);
//            cart.setUserLng(userLong);
//            cart.setUserAddress(userAddress);
//            cart.setDishId(dish.getDishId());
//            cart.setDishName(dish.getDishName());
//            cart.setResturantId(dish.getResturantId());
//            cart.setCurrenyCode(dish.getCurrency());
//            cart.setItem_count(String.valueOf(totalCount));
//            cart.setStatusCode("1");
//            cart.setSpecialNote("");
//            cart.setPromoCode("0");
//            cart.setCardNumber("0");
//            cart.setDishPrice("" + grandTotal);
//            if( user != null ) {
//                cart.setUserId(user.getId());
//                cart.setReceiver_name(user.getFirstName() + " " + user.getLastName());
//                cart.setUserNumber(user.getNumber());
//            }
//            dataBaseHelper.saveCartItem(cart, isEdit);
//        } catch (SQLException e) {
//            e.printStackTrace();
//            Toast.makeText(mContext, "Hey! Sorry, something went wrong, please try again later." , Toast.LENGTH_LONG).show();
//        }
//        mCartItemCount = 0;
//        mCartDishIndex = 0;
//        Toast.makeText(mContext, "Item added successfully." , Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onResponseFailure(String error) {
//        mCartItemCount = 0;
//        mCartDishIndex = 0;
//        Toast.makeText(mContext, "Hey! Sorry, but this restaurant doesn't deliver currently to your area, choose another one or change your delivery location!" , Toast.LENGTH_LONG).show();
//    }
//
//    public static class RestaurantViewHolder extends RecyclerView.ViewHolder {
//        TextView dishDescrptn, dishPrice, deliveryTime;
//        AutofitTextView calText, carbsText,proText, fatText, dishName;
//        ImageView dishImage, preBooking;
//        LinearLayout linearLayout, caloriesLayout;
//        ImageButton minus, plus;
//        TextView digit, addToCartText;
//
//        public RestaurantViewHolder(View v) {
//            super(v);
//            dishImage = (ImageView) v.findViewById(R.id.item_dish_image);
//            preBooking = (ImageView) v.findViewById(R.id.item_dish_prebooking);
//            dishName = (AutofitTextView) v.findViewById(R.id.item_dish_name);
//            dishDescrptn = (TextView) v.findViewById(R.id.item_dish_description);
//            dishPrice = (TextView) v.findViewById(R.id.item_dish_price);
//            deliveryTime = (TextView) v.findViewById(R.id.item_dish_deliver_time);
//            linearLayout = (LinearLayout) v.findViewById(R.id.item_dish_linearLayout);
//            calText = (AutofitTextView) v.findViewById(R.id.calories_text_view);
//            carbsText = (AutofitTextView) v.findViewById(R.id.carbs_text_view);
//            proText = (AutofitTextView) v.findViewById(R.id.pro_text_view);
//            fatText = (AutofitTextView) v.findViewById(R.id.fat_text_view);
//            caloriesLayout = (LinearLayout) v.findViewById(R.id.calories_layout);
//
//            minus = (ImageButton) v.findViewById(R.id.cart_minus);
//            plus = (ImageButton) v.findViewById(R.id.cart_plus);
//            digit = (TextView) v.findViewById(R.id.cart_showDigit);
//            addToCartText  = (TextView)v.findViewById(R.id.add_to_card_text);
//
//            Utils.buttonTouchDelegate(minus);
//            Utils.buttonTouchDelegate(plus);
//        }
//    }
//
//    public interface OnDishListener {
//        void onDishClick(ResturandResult dish);
//    }
//
//}