package com.o2.plotos.restuarantdetail;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.adapters.ViewPagerAdapter;
import com.o2.plotos.home.order.DeliverToActivity;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.resturantDetails.ResturandDetailsResponse;
import com.o2.plotos.restapi.newApis.models.resturantDetails.ResturantDetailsResult;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResturantDetailsActivity extends BaseActivity {
    @BindView(R.id.activity_resturant_name)
    TextView activityResturantName;
    @BindView(R.id.diet_detail_sharing)
    ImageButton dietDetailSharing;
    @BindView(R.id.restuarant_detail_image_cover)
    ImageView restuarantDetailImageCover;
    @BindView(R.id.img_restorent)
    ImageView imgRestorent;
    @BindView(R.id.card_view_notification)
    CardView cardViewNotification;
    @BindView(R.id.restaurantName)
    TextView restaurantName;
    @BindView(R.id.restaurant_detail_open_close_time)
    TextView restaurantDetailOpenCloseTime;
    @BindView(R.id.restaurant_detail_description)
    TextView restaurantDetailDescription;
    @BindView(R.id.restuarant_detail_tablayout)
    TabLayout restuarantDetailTablayout;
    @BindView(R.id.restuarant_detail_viewpager)
    ViewPager restuarantDetailViewpager;
    CustomFonts customFonts;
    @BindView(R.id.img_back)
    ImageView imgBack;
    private int currentItem;
    private String r_id;


    @BindView(R.id.cartLayout)
    RelativeLayout cartLayout;
    @BindView(R.id.tvItemCount)
    TextView tvItemCount;
    @BindView(R.id.tvTotal)
    TextView tvTotal;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resturant_details);
        ButterKnife.bind(this);
        initializeView();
        if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
            r_id = getIntent().getStringExtra(Constant.R_ID);
            callResturantDetailsApi(r_id);
        } else {
            Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
        }
        setCartFab();
    }

    private void callResturantDetailsApi(String id) {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResturandDetailsResponse> call = apiInterface.getResturandDetails(id, UserPreferenceUtil.getInstance(this).getLocationLat(), UserPreferenceUtil.getInstance(this).getLocationLng());
        call.enqueue(new Callback<ResturandDetailsResponse>() {
            @Override
            public void onResponse(Call<ResturandDetailsResponse> call, Response<ResturandDetailsResponse> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    setupView(response.body().getListResturantResult().get(0));
                } else {

                }
            }

            @Override
            public void onFailure(Call<ResturandDetailsResponse> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }

    @SuppressLint("StringFormatMatches")
    private void setupView(ResturantDetailsResult resturantDetailsResult) {
        activityResturantName.setText(resturantDetailsResult.getRestaurant_name());
        if (resturantDetailsResult.getBanner_image().length() > 0) {
            Picasso.with(this)
                    .load(resturantDetailsResult.getBanner_image())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(restuarantDetailImageCover);
        } else {
            restuarantDetailImageCover.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.placeholder));
        }
        if (resturantDetailsResult.getImage().length() > 0) {
            Picasso.with(this)
                    .load(resturantDetailsResult.getImage())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(imgRestorent);
        } else {
            imgRestorent.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.placeholder));
        }
        restaurantName.setText(resturantDetailsResult.getRestaurant_name());
        String bookingTime=resturantDetailsResult.getPre_booking();
        if (bookingTime.equals("0")){
            restaurantDetailOpenCloseTime.setText("Min Order AED " + resturantDetailsResult.getMin_order_amount()
                    + " | Open at " + resturantDetailsResult.getOpening_time());
        }else {
            restaurantDetailOpenCloseTime.setText(
                    bookingTime +
                    " hours | Min Order AED " + resturantDetailsResult.getMin_order_amount()
                    + " | Open at " + resturantDetailsResult.getOpening_time());
        }
        restaurantDetailDescription.setText(resturantDetailsResult.getRestaurant_description());
        setupViewPager(restuarantDetailViewpager, resturantDetailsResult);
        restuarantDetailTablayout.setupWithViewPager(restuarantDetailViewpager);

    }

    private void setupViewPager(ViewPager viewPager, ResturantDetailsResult resturantDetailsResult) {
        currentItem = 0;
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        Breakfast, Appetizer, Mains, Snacks& Dessert & Beverages
        if (resturantDetailsResult.getAllDishDetails().getList_breakfast().size() > 0) {
            adapter.addFrag(DishDetailsFragment.newInstance(resturantDetailsResult.getAllDishDetails().getList_breakfast()), "BreakFast");
            currentItem++;
        }
        if (resturantDetailsResult.getAllDishDetails().getList_appetizers().size() > 0) {
            adapter.addFrag(DishDetailsFragment.newInstance(resturantDetailsResult.getAllDishDetails().getList_appetizers()), "Appetizers");
            currentItem++;

        }

        if (resturantDetailsResult.getAllDishDetails().getListMains().size() > 0) {
            adapter.addFrag(DishDetailsFragment.newInstance(resturantDetailsResult.getAllDishDetails().getListMains()), "Mains");
            currentItem++;

        }
        if (resturantDetailsResult.getAllDishDetails().getList_snack().size() > 0) {
            adapter.addFrag(DishDetailsFragment.newInstance(resturantDetailsResult.getAllDishDetails().getList_snack()), "Snacks");
            currentItem++;

        }


        if (resturantDetailsResult.getAllDishDetails().getList_beverages().size() > 0) {
            adapter.addFrag(DishDetailsFragment.newInstance(resturantDetailsResult.getAllDishDetails().getList_beverages()), "Beverages");
            currentItem++;
        }

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(currentItem);
    }


    private void initializeView() {
        customFonts = new CustomFonts(this);
        customFonts.setOswaldBold(restaurantName);
        customFonts.setOpenSansSemiBold(restaurantDetailDescription);
        customFonts.setOpenSansRegulr(restaurantDetailOpenCloseTime);
        customFonts.setOpenSansRegulr(activityResturantName);
    }

    private void setCartFab() {
        int cartItemSize = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST).size();
        if (cartItemSize > 0) {
            cartLayout.setVisibility(View.VISIBLE);
            tvItemCount.setText("" + cartItemSize);
            makeList();
        } else {
            cartLayout.setVisibility(View.GONE);
        }
    }

    private void makeList() {
        Double tootal = 0.00;
        List<ItemDetais> listOfCartItems = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST);
        for (int i = 0; i < listOfCartItems.size(); i++) {
            ItemDetais itemDetais = listOfCartItems.get(i);
            tootal = tootal + (itemDetais.getQuantity() * itemDetais.getBasicPrice());
        }
        tvTotal.setText(tootal + " AED");
    }



    @OnClick({R.id.img_back,R.id.cartLayout,R.id.diet_detail_sharing})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.cartLayout:
                startActivity(new Intent(this, DeliverToActivity.class));
                break;
            case R.id.diet_detail_sharing:
                String link="https://play.google.com/store/apps/details?id=\"" + getPackageName();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Check out the app :"+link);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String s) {
        setCartFab();
    };

    @Override
    public void onBackPressed() {
        finish();
    }
}
