package com.o2.plotos.restuarantdetail;

import com.o2.plotos.models.Dish;

import java.util.List;

/**
 * Created by Hassan on 8/02/2017.
 */

public class ResturntDetalPrsntrImpl implements IResturantDetalPrsnter,
        IResturntDetalDataModl.OnGetResturntDishRqustFinishLisener {

    IResturantDetailView iResturantDetailView;
    IResturntDetalDataModl mResturntDetalDataModl;

    public ResturntDetalPrsntrImpl(){
        mResturntDetalDataModl = new ResturntDetalDataModlImpl(this);
    }

    @Override
    public void sendRestrntDishRqust(String type,String restaurant_id, String category_id, String calorie, String latitude, String longitude) {
        if(iResturantDetailView!=null){
            //iResturantDetailView.showProgress();
            mResturntDetalDataModl.getResturntDishRqust(type,restaurant_id,category_id,calorie, latitude, longitude);
        }
    }

    @Override
    public void bindView(Object view) {
        iResturantDetailView = (IResturantDetailView) view;
    }

    @Override
    public void unBindView() {
        iResturantDetailView = null;
    }

    @Override
    public void OnGetResturntDishSuccess(List<Dish> dishList) {
        if(iResturantDetailView!=null){
            //iResturantDetailView.hideProgress();
            iResturantDetailView.onGetRestaurantDishSuccess(dishList);
        }
    }

    @Override
    public void OnGetResturntDishFailed(String error) {
        if(iResturantDetailView!=null){
            //iResturantDetailView.hideProgress();
            iResturantDetailView.onGetRestaurantDishFailed(error);
        }
    }
}
