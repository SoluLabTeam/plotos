package com.o2.plotos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Hassan on 3/02/2017.
 */

public class Dish extends PlaceItem implements Parcelable {
    @SerializedName("dish_id")
    private String dishId;
    @SerializedName("restaurant_id")
    private String resturantId;
    @SerializedName("restaurant_name")
    private String resturantName;
    @SerializedName("pre_booking")
    private String preBookingtime;
    @SerializedName("name")
    private String dishName;
    @SerializedName("ingredients")
    private String descrptn;
    @SerializedName("price")
    private String price;
    @SerializedName("currency")
    String currency;
    @SerializedName("delivery_time")
    String deliveryTime;
    @SerializedName("image_url")
    private String image_url;
    @SerializedName("image")
    private String image;
    @SerializedName("other_details")
    private String dishCal;
    @SerializedName("nutritional_info")
    private String nutritions;
    @SerializedName("is_deleted")
    String isdeleted;
    @SerializedName("level")
    String level;
    @SerializedName("vegan")
    private String vegan;
    @SerializedName("gluten")
    private String gluten;
    @SerializedName("dairy")
    private String dairy;
    @SerializedName("raw")
    private String raw;
    @SerializedName("breakfast")
    private String breakFast;
    @SerializedName("restaurant_opening_time")
    private String rstrntOpeningTime;
    @SerializedName("restaurant_closing_time")
    private String rstrntClosingTime;
    @SerializedName("lunch")
    private String launch;
    @SerializedName("snacks")
    private String snacks;
    @SerializedName("dinner")
    private String dinner;
    @SerializedName("categories")
    private List<Category> categoryList;
    @SerializedName("opening_time")
    private long openingTime;
    @SerializedName("closing_time")
    private long closingTime;
    @SerializedName("custom_delivery")
    private String customDelivery;
    @SerializedName("cash_only")
    private String cashOnly;
    @SerializedName("min_order_amount")
    private String minOrderAmount;

    private double count;

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public Dish() {
    }


    protected Dish(Parcel in) {
        dishId = in.readString();
        resturantId = in.readString();
        resturantName = in.readString();
        preBookingtime = in.readString();
        dishName = in.readString();
        descrptn = in.readString();
        price = in.readString();
        currency = in.readString();
        deliveryTime = in.readString();
        image_url = in.readString();
        image = in.readString();
        dishCal = in.readString();
        nutritions = in.readString();
        isdeleted = in.readString();
        level = in.readString();
        vegan = in.readString();
        gluten = in.readString();
        dairy = in.readString();
        raw = in.readString();
        breakFast = in.readString();
        rstrntOpeningTime = in.readString();
        rstrntClosingTime = in.readString();
        launch = in.readString();
        snacks = in.readString();
        dinner = in.readString();
        categoryList = in.createTypedArrayList(Category.CREATOR);
        openingTime = in.readLong();
        closingTime = in.readLong();
        customDelivery =  in.readString();
        cashOnly = in.readString();
        minOrderAmount = in.readString();
    }

    public static final Creator<Dish> CREATOR = new Creator<Dish>() {
        @Override
        public Dish createFromParcel(Parcel in) {
            return new Dish(in);
        }

        @Override
        public Dish[] newArray(int size) {
            return new Dish[size];
        }
    };

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public String getDescrptn() {
        return descrptn;
    }

    public void setDescrptn(String descrptn) {
        this.descrptn = descrptn;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String imageId) {
        this.image = imageId;
    }

    public String getDishCal() {
        return dishCal;
    }

    public void setDishCal(String dishCal) {
        this.dishCal = dishCal;
    }

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(String isdeleted) {
        this.isdeleted = isdeleted;
    }

    public String getBreakFast() {
        return breakFast;
    }

    public void setBreakFast(String breakFast) {
        this.breakFast = breakFast;
    }

    public String getLaunch() {
        return launch;
    }

    public void setLaunch(String launch) {
        this.launch = launch;
    }

    public String getSnacks() {
        return snacks;
    }

    public void setSnacks(String snacks) {
        this.snacks = snacks;
    }

    public String getDinner() {
        return dinner;
    }

    public void setDinner(String dinner) {
        this.dinner = dinner;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public String getVegan() {
        return vegan;
    }

    public void setVegan(String vegan) {
        this.vegan = vegan;
    }

    public String getGluten() {
        return gluten;
    }

    public void setGluten(String gluten) {
        this.gluten = gluten;
    }

    public String getDairy() {
        return dairy;
    }

    public void setDairy(String dairy) {
        this.dairy = dairy;
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getResturantId() {
        return resturantId;
    }

    public void setResturantId(String resturantId) {
        this.resturantId = resturantId;
    }

    public String getNutritions() {
        return nutritions;
    }

    public void setNutritions(String nutritions) {
        this.nutritions = nutritions;
    }

    public String getRstrntOpeningTime() {
        return rstrntOpeningTime;
    }

    public void setRstrntOpeningTime(String rstrntOpeningTime) {
        this.rstrntOpeningTime = rstrntOpeningTime;
    }

    public String getRstrntClosingTime() {
        return rstrntClosingTime;
    }

    public void setRstrntClosingTime(String rstrntClosingTime) {
        this.rstrntClosingTime = rstrntClosingTime;
    }

    public String getPreBookingtime() {
        return preBookingtime;
    }

    public void setPreBookingtime(String preBookingtime) {
        this.preBookingtime = preBookingtime;
    }

    public String getResturantName() {
        return resturantName;
    }

    public void setResturantName(String resturantName) {
        this.resturantName = resturantName;
    }

    public long getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(long openingTime) {
        this.openingTime = openingTime;
    }

    public long getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(long closingTime) {
        this.closingTime = closingTime;
    }

    public String getCustomDelivery() {
        return customDelivery;
    }

    public void setCustomDelivery(String customDelivery) {
        this.customDelivery = customDelivery;
    }

    public String getCashOnly() {
        return cashOnly;
    }

    public void setCashOnly(String cashOnly) {
        this.cashOnly = cashOnly;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    public String getMinOrderAmount() {
        return minOrderAmount;
    }

    public void setMinOrderAmount(String minOrderAmount) {
        this.minOrderAmount = minOrderAmount;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dishId);
        dest.writeString(resturantId);
        dest.writeString(resturantName);
        dest.writeString(preBookingtime);
        dest.writeString(dishName);
        dest.writeString(descrptn);
        dest.writeString(price);
        dest.writeString(currency);
        dest.writeString(deliveryTime);
        dest.writeString(image_url);
        dest.writeString(image);
        dest.writeString(dishCal);
        dest.writeString(nutritions);
        dest.writeString(isdeleted);
        dest.writeString(level);
        dest.writeString(vegan);
        dest.writeString(gluten);
        dest.writeString(dairy);
        dest.writeString(raw);
        dest.writeString(breakFast);
        dest.writeString(rstrntOpeningTime);
        dest.writeString(rstrntClosingTime);
        dest.writeString(launch);
        dest.writeString(snacks);
        dest.writeString(dinner);
        dest.writeTypedList(categoryList);
        dest.writeLong(openingTime);
        dest.writeLong(closingTime);
        dest.writeString(customDelivery);
        dest.writeString(cashOnly);
        dest.writeString(minOrderAmount);
    }
}
