package com.o2.plotos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rania on 5/20/2017.
 */

public class Banner implements Parcelable {

    @SerializedName("id")
    private String mID;

    @SerializedName("messages")
    private String mMessages;

    @SerializedName("image_url")
    private String mImage;

    @SerializedName("type")
    private String mType;

    @SerializedName("type_value")
    private String mTypeValue;

    @SerializedName("is_grocery")
    private String mIsGrocery;

    @SerializedName("status")
    private String mStatus;

    @SerializedName("is_deleted")
    private String mIsDeleted;


    public Banner() {
    }


    public String getID() {
        return mID;
    }

    public void setID(String mID) {
        this.mID = mID;
    }

    public String getMessages() {
        return mMessages;
    }

    public void setMessages(String mMessages) {
        this.mMessages = mMessages;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String mImage) {
        this.mImage = mImage;
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    public String getTypeValue() {
        return mTypeValue;
    }

    public void setTypeValue(String mTypeValue) {
        this.mTypeValue = mTypeValue;
    }

    public String getIsGrocery() {
        return mIsGrocery;
    }

    public void setIsGrocery(String mIsGrocery) {
        this.mIsGrocery = mIsGrocery;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getIsDeleted() {
        return mIsDeleted;
    }

    public void setIsDeleted(String mIsDeleted) {
        this.mIsDeleted = mIsDeleted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mID);
        dest.writeString(this.mMessages);
        dest.writeString(this.mImage);
        dest.writeString(this.mType);
        dest.writeString(this.mTypeValue);
        dest.writeString(this.mIsGrocery);
        dest.writeString(this.mStatus);
        dest.writeString(this.mIsDeleted);
    }

    protected Banner(Parcel in) {
        this.mID = in.readString();
        this.mMessages = in.readString();
        this.mImage = in.readString();
        this.mType = in.readString();
        this.mTypeValue = in.readString();
        this.mIsGrocery = in.readString();
        this.mStatus = in.readString();
        this.mIsDeleted = in.readString();
    }

    public static final Parcelable.Creator<Banner> CREATOR = new Parcelable.Creator<Banner>() {
        @Override
        public Banner createFromParcel(Parcel source) {
            return new Banner(source);
        }

        @Override
        public Banner[] newArray(int size) {
            return new Banner[size];
        }
    };
}
