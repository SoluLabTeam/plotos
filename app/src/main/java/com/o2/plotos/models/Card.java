package com.o2.plotos.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by snveiga on 27/04/17.
 */

@DatabaseTable( tableName = "card")
public class Card {
    @DatabaseField(columnName = "id" , id = true) //PK
    @SerializedName("id")
    private String id;

    @DatabaseField(columnName = "customer_id")
    @SerializedName("customer_id")
    private String customerId;

    @DatabaseField(columnName = "expiry_month")
    @SerializedName("expiry_month")
    private String expiryMonth;

    @DatabaseField(columnName = "expiry_year")
    @SerializedName("expiry_year")
    private String expiryYear;

    @DatabaseField(columnName = "last_digits")
    @SerializedName("last_digits")
    private String lastDigits;

    @DatabaseField(columnName = "payment_method")
    @SerializedName("payment_method")
    private String paymentMethod;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }

    public String getLastDigits() {
        return lastDigits;
    }

    public void setLastDigits(String lastDigits) {
        this.lastDigits = lastDigits;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}

