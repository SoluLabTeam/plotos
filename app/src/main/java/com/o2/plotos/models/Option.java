package com.o2.plotos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Option implements Parcelable {

    @SerializedName("opt_id")
    private String id;
    @SerializedName("add_on_id")
    private String addOnId;
    @SerializedName("name")
    private String name;
    @SerializedName("price")
    private String price;

    public Option() {

    }

    protected Option(Parcel in) {
        id = in.readString();
        addOnId = in.readString();
        name = in.readString();
        price = in.readString();
    }

    public static final Creator<Option> CREATOR = new Creator<Option>() {
        @Override
        public Option createFromParcel(Parcel in) {
            return new Option(in);
        }

        @Override
        public Option[] newArray(int size) {
            return new Option[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddOnId() {
        return addOnId;
    }

    public void setAddOnId(String addOnId) {
        this.addOnId = addOnId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(addOnId);
        parcel.writeString(name);
        parcel.writeString(price);
    }
}
