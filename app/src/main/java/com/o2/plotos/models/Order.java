package com.o2.plotos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.util.List;

/**
 * Created by Hassan on 2/02/2017.
 */

public class Order implements Parcelable {

    @SerializedName("order_id")
    private String orderId;
    @SerializedName("dish_id")
    private String dishId;
    @SerializedName("restaurant_id")
    private String resturantId;
    @SerializedName("restaurant_name")
    private String restaurantname;
    @SerializedName("price")

    private String price;
    @SerializedName("currency")
    private String currency;
    @SerializedName("note")
    private String note;
    @SerializedName("status")
    private String orderStatus;
    @SerializedName("dish_name")
    private String dishName;
    @SerializedName("order_time")
    private String dateTime;
    @SerializedName("delivery_time")
    private String deliveryTime;
    @SerializedName("dish_ingredients")
    private String dishIngredients;
    @SerializedName("dish_other_details")
    private String calories;
    @SerializedName("dish_image")
    private String dishImageUrl;
    @SerializedName("add_ons")
    private List<Option> optionList;
    @SerializedName("dish_order_count")
    private String dishCount;

    @SerializedName("user_id")
    @DatabaseField
    private String userId;
    @SerializedName("receiver_name")
    @DatabaseField
    private String receiver_name;

    @SerializedName("user_number")
    @DatabaseField
    private String userNumber;

    @SerializedName("user_latitude")
    @DatabaseField
    private String userLat;

    @SerializedName("user_longitude")
    @DatabaseField
    private String userLng;

    @SerializedName("user_address")
    @DatabaseField
    private String userAddress;

    @SerializedName("cc_number")
    @DatabaseField
    private String cardNumber;

    @SerializedName("promo_code")
    @DatabaseField
    private String promoCode;

    @SerializedName("item_id")
    private String itemGroceryID;
    @SerializedName("supplier_id")
    private String supplierID;
    @SerializedName("item_order_count")
    private String itemOrderCount;
    @SerializedName("item_name")
    private String itemGroceryName;

    public Order() {
    }

    protected Order(Parcel in) {
        orderId = in.readString();
        dishId = in.readString();
        resturantId = in.readString();
        price = in.readString();
        currency = in.readString();
        note = in.readString();
        orderStatus = in.readString();
        dishName = in.readString();
        dateTime = in.readString();
        deliveryTime = in.readString();
        dishIngredients = in.readString();
        calories = in.readString();
        dishImageUrl = in.readString();
        optionList = in.createTypedArrayList(Option.CREATOR);
        userId = in.readString();
        receiver_name = in.readString();
        userNumber = in.readString();
        userLat = in.readString();
        userLng = in.readString();
        userAddress = in.readString();
        cardNumber = in.readString();
        promoCode = in.readString();
        dishCount = in.readString();
        this.itemGroceryID = in.readString();
        this.itemOrderCount = in.readString();
        this.supplierID = in.readString();
        itemGroceryName = in.readString();
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    public String getDishCount() {
        return dishCount;
    }

    public void setDishCount(String dishCount) {
        this.dishCount = dishCount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public String getResturantId() {
        return resturantId;
    }

    public void setResturantId(String resturantId) {
        this.resturantId = resturantId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getDishIngredients() {
        return dishIngredients;
    }

    public void setDishIngredients(String dishIngredients) {
        this.dishIngredients = dishIngredients;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public String getDishImageUrl() {
        return dishImageUrl;
    }

    public void setDishImageUrl(String dishImageUrl) {
        this.dishImageUrl = dishImageUrl;
    }

    public List<Option> getOptionList() {
        return optionList;
    }

    public void setOptionList(List<Option> optionList) {
        this.optionList = optionList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getUserLat() {
        return userLat;
    }

    public void setUserLat(String userLat) {
        this.userLat = userLat;
    }

    public String getUserLng() {
        return userLng;
    }

    public void setUserLng(String userLng) {
        this.userLng = userLng;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getItemGroceryID() {
        return itemGroceryID;
    }

    public void setItemGroceryID(String itemGroceryID) {
        this.itemGroceryID = itemGroceryID;
    }

    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    public String getItemOrderCount() {
        return itemOrderCount;
    }

    public void setItemOrderCount(String itemOrderCount) {
        this.itemOrderCount = itemOrderCount;
    }

    public String getItemGroceryName() {
        return itemGroceryName;
    }

    public void setItemGroceryName(String itemGroceryName) {
        this.itemGroceryName = itemGroceryName;
    }
    public String getRestaurantname() {
        return restaurantname;
    }

    public void setRestaurantname(String restaurantname) {
        this.restaurantname = restaurantname;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderId);
        dest.writeString(dishId);
        dest.writeString(resturantId);
        dest.writeString(price);
        dest.writeString(currency);
        dest.writeString(note);
        dest.writeString(orderStatus);
        dest.writeString(dishName);
        dest.writeString(dateTime);
        dest.writeString(deliveryTime);
        dest.writeString(dishIngredients);
        dest.writeString(calories);
        dest.writeString(dishImageUrl);
        dest.writeTypedList(optionList);
        dest.writeString(userId);
        dest.writeString(receiver_name);
        dest.writeString(userNumber);
        dest.writeString(userLat);
        dest.writeString(userLng);
        dest.writeString(userAddress);
        dest.writeString(restaurantname);
        dest.writeString(cardNumber);
        dest.writeString(promoCode);
        dest.writeString(dishCount);
        dest.writeString(this.itemGroceryID);
        dest.writeString(this.itemOrderCount);
        dest.writeString(this.supplierID);
        dest.writeString(this.itemGroceryName);
    }
}
