package com.o2.plotos.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rania on 5/11/2017.
 */

public class PlaceItem implements Parcelable {

    int is_dish;

    public int getIs_dish() {
        return is_dish;
    }

    public void setIs_dish(int is_dish) {
        this.is_dish = is_dish;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.is_dish);
    }

    public PlaceItem() {
    }

    protected PlaceItem(Parcel in) {
        this.is_dish = in.readInt();
    }

    public static final Parcelable.Creator<PlaceItem> CREATOR = new Parcelable.Creator<PlaceItem>() {
        @Override
        public PlaceItem createFromParcel(Parcel source) {
            return new PlaceItem(source);
        }

        @Override
        public PlaceItem[] newArray(int size) {
            return new PlaceItem[size];
        }
    };
}
