package com.o2.plotos.models;

/**
 * Created by Hassan on 2/1/2017.
 */

public class Diet {
    private String name;
    private int imageId;
    private int description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getDescription() {
        return description;
    }

    public void setDescription(int description) {
        this.description = description;
    }
}
