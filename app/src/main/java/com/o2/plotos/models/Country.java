package com.o2.plotos.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hassan on 28/02/2017.
 */

public class Country{
    @SerializedName("name")
    private String name;
    @SerializedName("code")
    private String code;
    @SerializedName("dial_code")
    private String dialCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDialCode() {
        return dialCode;
    }

    public void setDialCode(String dialCode) {
        this.dialCode = dialCode;
    }
}