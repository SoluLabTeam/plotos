package com.o2.plotos.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rania on 5/8/2017.
 */

public class Supplier extends Place implements Parcelable {
    private String delivery_time;

    private String delivery_time_start;

    private String person_name;

    private String supplier_closing_time;

    private String delivery_latitude;

    private String statusText;

    private String delivery_areas;

    private String first_name;

    private String pre_booking;

    private String delivery_longitude;

    private long opening_time;

    private String image_url;

    private String in_radius;

    private String user_id;

    private String longitude;

    private String supplier_description;

//    private String[][] days_schedule;

    private String supplier_id;

    private String supplier_opening_time;

    private String status;

    private String end_price;

    private String supplier_name;

    private long closing_time;

//    private String image;

    private String number;

//    private String plotos_delivery;

    private String is_deleted;

//    private String days_open;

//    private String delivery_path;

    private String address;

    private String email;

    private String delivery_time_end;

    private String last_name;

    private String radius;

    private String latitude;

    private String start_price;

    public Supplier(){}

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getDelivery_time_start() {
        return delivery_time_start;
    }

    public void setDelivery_time_start(String delivery_time_start) {
        this.delivery_time_start = delivery_time_start;
    }

    public String getPerson_name() {
        return person_name;
    }

    public void setPerson_name(String person_name) {
        this.person_name = person_name;
    }

    public String getSupplier_closing_time() {
        return supplier_closing_time;
    }

    public void setSupplier_closing_time(String supplier_closing_time) {
        this.supplier_closing_time = supplier_closing_time;
    }

    public String getDelivery_latitude() {
        return delivery_latitude;
    }

    public void setDelivery_latitude(String delivery_latitude) {
        this.delivery_latitude = delivery_latitude;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getDelivery_areas() {
        return delivery_areas;
    }

    public void setDelivery_areas(String delivery_areas) {
        this.delivery_areas = delivery_areas;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getPre_booking() {
        return pre_booking;
    }

    public void setPre_booking(String pre_booking) {
        this.pre_booking = pre_booking;
    }

    public String getDelivery_longitude() {
        return delivery_longitude;
    }

    public void setDelivery_longitude(String delivery_longitude) {
        this.delivery_longitude = delivery_longitude;
    }

    public long getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(long opening_time) {
        this.opening_time = opening_time;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getIn_radius() {
        return in_radius;
    }

    public void setIn_radius(String in_radius) {
        this.in_radius = in_radius;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSupplier_description() {
        return supplier_description;
    }

    public void setSupplier_description(String supplier_description) {
        this.supplier_description = supplier_description;
    }

//    public String[][] getDays_schedule() {
//        return days_schedule;
//    }
//
//    public void setDays_schedule(String[][] days_schedule) {
//        this.days_schedule = days_schedule;
//    }

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getSupplier_opening_time() {
        return supplier_opening_time;
    }

    public void setSupplier_opening_time(String supplier_opening_time) {
        this.supplier_opening_time = supplier_opening_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEnd_price() {
        return end_price;
    }

    public void setEnd_price(String end_price) {
        this.end_price = end_price;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public long getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(long closing_time) {
        this.closing_time = closing_time;
    }

//    public String getImage() {
//        return image;
//    }
//
//    public void setImage(String image) {
//        this.image = image;
//    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

//    public String getPlotos_delivery() {
//        return plotos_delivery;
//    }
//
//    public void setPlotos_delivery(String plotos_delivery) {
//        this.plotos_delivery = plotos_delivery;
//    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

//    public String getDays_open() {
//        return days_open;
//    }
//
//    public void setDays_open(String days_open) {
//        this.days_open = days_open;
//    }

//    public String getDelivery_path() {
//        return delivery_path;
//    }
//
//    public void setDelivery_path(String delivery_path) {
//        this.delivery_path = delivery_path;
//    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDelivery_time_end() {
        return delivery_time_end;
    }

    public void setDelivery_time_end(String delivery_time_end) {
        this.delivery_time_end = delivery_time_end;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getStart_price() {
        return start_price;
    }

    public void setStart_price(String start_price) {
        this.start_price = start_price;
    }



    @Override
    public String toString() {
        return "ClassPojo [delivery_time = " + delivery_time + ", delivery_time_start = " + delivery_time_start +
                ", person_name = " + person_name + ", supplier_closing_time = " + supplier_closing_time +
                ", delivery_latitude = " + delivery_latitude + ", statusText = " + statusText + ", delivery_areas = " +
                delivery_areas + ", first_name = " + first_name +
                ", pre_booking = " + pre_booking +
                ", delivery_longitude = " + delivery_longitude + ", opening_time = " + opening_time +
                ", image_url = " + image_url + ", in_radius = " + in_radius + ", user_id = " + user_id +
                ", longitude = " + longitude + ", supplier_description = " + supplier_description +
//                ", days_schedule = " + days_schedule +
                ", supplier_id = " + supplier_id + ", supplier_opening_time = " + supplier_opening_time +
                ", status = " + status + ", end_price = " + end_price + ", supplier_name = " + supplier_name +
                ", closing_time = " + closing_time +
//                + ", image = " + image + ", plotos_delivery = " + plotos_delivery +
                ", number = " + number +  ", is_deleted = " + is_deleted +
                 // ", days_open = " + days_open + ", delivery_path = " + delivery_path +
                ", address = " + address + ", email = " + email + ", delivery_time_end = " + delivery_time_end +
                ", last_name = " + last_name + ", radius = " + radius + ", latitude = " + latitude +
                ", start_price = " + start_price + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(supplier_id);
        dest.writeString(user_id);
        dest.writeString(supplier_name);
        dest.writeString(person_name);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(email);
        dest.writeString(number);
        dest.writeString(statusText);
        dest.writeString(status);
        dest.writeString(address);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(delivery_latitude);
        dest.writeString(delivery_longitude);
        dest.writeString(radius);
        dest.writeString(delivery_areas);
        dest.writeString(image_url);
        dest.writeLong(closing_time);
        dest.writeString(supplier_closing_time);
        dest.writeLong(opening_time);
        dest.writeString(supplier_opening_time);
        dest.writeString(start_price);
        dest.writeString(end_price);
        dest.writeString(supplier_description);
        dest.writeString(is_deleted);
        dest.writeString(in_radius);
        dest.writeString(pre_booking);
    }

    protected Supplier(Parcel in) {
        supplier_id = in.readString();
        user_id = in.readString();
        supplier_name = in.readString();
        person_name = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        email = in.readString();
        number = in.readString();
        statusText = in.readString();
        status = in.readString();
        address = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        delivery_latitude = in.readString();
        delivery_longitude = in.readString();
        radius = in.readString();
        delivery_areas = in.readString();
        image_url = in.readString();
        closing_time = in.readLong();
        supplier_closing_time = in.readString();
        opening_time = in.readLong();
        supplier_opening_time = in.readString();
        start_price = in.readString();
        end_price = in.readString();
        supplier_description = in.readString();
        is_deleted = in.readString();
        in_radius = in.readString();
        pre_booking = in.readString();
    }

    public static final Creator<Supplier> CREATOR = new Creator<Supplier>() {
        @Override
        public Supplier createFromParcel(Parcel in) {
            return new Supplier(in);
        }

        @Override
        public Supplier[] newArray(int size) {
            return new Supplier[size];
        }
    };
}