package com.o2.plotos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hassan on 31/01/2017.
 */

public class Restaurant extends Place implements Parcelable {
    @SerializedName("restaurant_id")
    private String id;
    @SerializedName("restaurant_name")
    private String name;
    @SerializedName("level")
    private String level;
    @SerializedName("number")
    private String number;
    @SerializedName("statusText")
    private String statusText;
    @SerializedName("status")
    private String status;
    @SerializedName("address")
    private String address;
    @SerializedName("latitutde")
    private String lat;
    @SerializedName("longitude")
    private String lng;
    @SerializedName("radius")
    private String radius;
    @SerializedName("image_url")
    private String image_url;
    @SerializedName("image")
    private String image;
    @SerializedName("start_price")
    private String startPrice;
    @SerializedName("end_price")
    private String endPrice;

    @SerializedName("pre_booking")
    private String preBooking;
    @SerializedName("restaurant_opening_time")
    private String resturantOpenTime;
    @SerializedName("restaurant_closing_time")
    private String resturantCloseTime;
    @SerializedName("restaurant_description")
    private String resturntDescrptn;
    @SerializedName("is_deleted")
    private String isDeleted;
    @SerializedName("opening_time")
    private long openingTime;
    @SerializedName("closing_time")
    private long closingTime;


    protected Restaurant(Parcel in) {
        id = in.readString();
        name = in.readString();
        level = in.readString();
        number = in.readString();
        statusText = in.readString();
        status = in.readString();
        address = in.readString();
        lat = in.readString();
        lng = in.readString();
        radius = in.readString();
        image_url = in.readString();
        image = in.readString();
        startPrice = in.readString();
        endPrice = in.readString();
        preBooking = in.readString();
        resturantOpenTime = in.readString();
        resturantCloseTime = in.readString();
        resturntDescrptn = in.readString();
        isDeleted = in.readString();
        openingTime = in.readLong();
        closingTime = in.readLong();
    }

    public static final Creator<Restaurant> CREATOR = new Creator<Restaurant>() {
        @Override
        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        @Override
        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(String startPrice) {
        this.startPrice = startPrice;
    }

    public String getResturntDescrptn() {
        return resturntDescrptn;
    }

    public void setResturntDescrptn(String resturntDescrptn) {
        this.resturntDescrptn = resturntDescrptn;
    }

    public String getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(String endPrice) {
        this.endPrice = endPrice;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getResturantOpenTime() {
        return resturantOpenTime;
    }

    public void setResturantOpenTime(String resturantOpenTime) {
        this.resturantOpenTime = resturantOpenTime;
    }

    public String getResturantCloseTime() {
        return resturantCloseTime;
    }

    public void setResturantCloseTime(String resturantCloseTime) {
        this.resturantCloseTime = resturantCloseTime;

    }

    public String getPreBooking() {
        return preBooking;
    }

    public void setPreBooking(String preBooking) {
        this.preBooking = preBooking;
    }

    public long getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(long openingTime) {
        this.openingTime = openingTime;
    }

    public long getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(long closingTime) {
        this.closingTime = closingTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(level);
        dest.writeString(number);
        dest.writeString(statusText);
        dest.writeString(status);
        dest.writeString(address);
        dest.writeString(lat);
        dest.writeString(lng);
        dest.writeString(radius);
        dest.writeString(image_url);
        dest.writeString(image);
        dest.writeString(startPrice);
        dest.writeString(endPrice);
        dest.writeString(preBooking);
        dest.writeString(resturantOpenTime);
        dest.writeString(resturantCloseTime);
        dest.writeString(resturntDescrptn);
        dest.writeString(isDeleted);
        dest.writeLong(openingTime);
        dest.writeLong(closingTime);
    }
}
