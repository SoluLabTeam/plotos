package com.o2.plotos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RANIA on 5/07/2017.
 */

public class Grocery extends PlaceItem implements Parcelable {
    @SerializedName("id")
    private String id;
    @SerializedName("supplier_id")
    private String supplier_id;
    @SerializedName("supplier_name")
    private String supplier_name;
    @SerializedName("pre_booking")
    private String preBookingtime;
    @SerializedName("name")
    private String name;
    @SerializedName("ingredients")
    private String descrptn;
    @SerializedName("price")
    private String price;
    @SerializedName("currency")
    String currency;
    @SerializedName("delivery_time")
    String deliveryTime;
    @SerializedName("image_url")
    private String image_url;
    @SerializedName("image")
    private String image;
    @SerializedName("other_details")
    private String dishCal;
    @SerializedName("nutritional_info")
    private String nutritions;
    @SerializedName("is_deleted")
    String isdeleted;
    @SerializedName("level")
    String level;
    @SerializedName("vegan")
    private String vegan;
    @SerializedName("gluten")
    private String gluten;
    @SerializedName("dairy")
    private String dairy;
    @SerializedName("raw")
    private String raw;
    @SerializedName("breakfast")
    private String breakFast;
    @SerializedName("supplier_opening_time")
    private String supplier_opening_time;
    @SerializedName("supplier_closing_time")
    private String supplier_closing_time;
    @SerializedName("lunch")
    private String launch;
    @SerializedName("snacks")
    private String snacks;
    @SerializedName("dinner")
    private String dinner;
    @SerializedName("opening_time")
    private long openingTime;
    @SerializedName("closing_time")
    private long closingTime;
    @SerializedName("category_id")
    private String category_id;
    @SerializedName("category_name")
    private String category_name;

    @SerializedName("per_unit")
    private String perUnit;
    @SerializedName("qty_unit")
    private String qtyUnit;
    @SerializedName("min_order_amount")
    private String minOrderAmount;
    @SerializedName("cash_only")
    private String cashOnly;
    @SerializedName("min_qty")
    private String minQty;

    private double count;

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }


    public Grocery() {
    }


    protected Grocery(Parcel in) {
        id = in.readString();
        supplier_id = in.readString();
        supplier_name = in.readString();
        preBookingtime = in.readString();
        name = in.readString();
        descrptn = in.readString();
        price = in.readString();
        currency = in.readString();
        deliveryTime = in.readString();
        image_url = in.readString();
        image = in.readString();
        dishCal = in.readString();
        nutritions = in.readString();
        isdeleted = in.readString();
        level = in.readString();
        vegan = in.readString();
        gluten = in.readString();
        dairy = in.readString();
        raw = in.readString();
        breakFast = in.readString();
        supplier_opening_time = in.readString();
        supplier_closing_time = in.readString();
        launch = in.readString();
        snacks = in.readString();
        dinner = in.readString();
        openingTime = in.readLong();
        closingTime = in.readLong();
        category_id = in.readString();
        category_name = in.readString();
        perUnit = in.readString();
        qtyUnit = in.readString();
        minOrderAmount = in.readString();
        cashOnly = in.readString();
        minQty = in.readString();
    }

    public static final Creator<Grocery> CREATOR = new Creator<Grocery>() {
        @Override
        public Grocery createFromParcel(Parcel in) {
            return new Grocery(in);
        }

        @Override
        public Grocery[] newArray(int size) {
            return new Grocery[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSupplier_opening_time() {
        return supplier_opening_time;
    }

    public void setSupplier_opening_time(String supplier_opening_time) {
        this.supplier_opening_time = supplier_opening_time;
    }

    public String getSupplier_closing_time() {
        return supplier_closing_time;
    }

    public void setSupplier_closing_time(String supplier_closing_time) {
        this.supplier_closing_time = supplier_closing_time;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getDescrptn() {
        return descrptn;
    }

    public void setDescrptn(String descrptn) {
        this.descrptn = descrptn;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String imageId) {
        this.image = imageId;
    }

    public String getDishCal() {
        return dishCal;
    }

    public void setDishCal(String dishCal) {
        this.dishCal = dishCal;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(String isdeleted) {
        this.isdeleted = isdeleted;
    }

    public String getBreakFast() {
        return breakFast;
    }

    public void setBreakFast(String breakFast) {
        this.breakFast = breakFast;
    }

    public String getLaunch() {
        return launch;
    }

    public void setLaunch(String launch) {
        this.launch = launch;
    }

    public String getSnacks() {
        return snacks;
    }

    public void setSnacks(String snacks) {
        this.snacks = snacks;
    }

    public String getDinner() {
        return dinner;
    }

    public void setDinner(String dinner) {
        this.dinner = dinner;
    }

    public String getVegan() {
        return vegan;
    }

    public void setVegan(String vegan) {
        this.vegan = vegan;
    }

    public String getGluten() {
        return gluten;
    }

    public void setGluten(String gluten) {
        this.gluten = gluten;
    }

    public String getDairy() {
        return dairy;
    }

    public void setDairy(String dairy) {
        this.dairy = dairy;
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getNutritions() {
        return nutritions;
    }

    public void setNutritions(String nutritions) {
        this.nutritions = nutritions;
    }

    public String getPreBookingtime() {
        return preBookingtime;
    }

    public void setPreBookingtime(String preBookingtime) {
        this.preBookingtime = preBookingtime;
    }

    public long getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(long openingTime) {
        this.openingTime = openingTime;
    }

    public long getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(long closingTime) {
        this.closingTime = closingTime;
    }

    public String getPerUnit() {
        return perUnit;
    }

    public void setPerUnit(String perUnit) {
        this.perUnit = perUnit;
    }

    public String getQtyUnit() {
        return qtyUnit;
    }

    public void setQtyUnit(String qtyUnit) {
        this.qtyUnit = qtyUnit;
    }

    public String getMinOrderAmount() {
        return minOrderAmount;
    }

    public void setMinOrderAmount(String minOrderAmount) {
        this.minOrderAmount = minOrderAmount;
    }

    public String getCashOnly() {
        return cashOnly;
    }

    public void setCashOnly(String cashOnly) {
        this.cashOnly = cashOnly;
    }

    public String getMinQty() {
        return minQty;
    }

    public void setMinQty(String minQty) {
        this.minQty = minQty;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(supplier_id);
        dest.writeString(supplier_name);
        dest.writeString(preBookingtime);
        dest.writeString(name);
        dest.writeString(descrptn);
        dest.writeString(price);
        dest.writeString(currency);
        dest.writeString(deliveryTime);
        dest.writeString(image_url);
        dest.writeString(image);
        dest.writeString(dishCal);
        dest.writeString(nutritions);
        dest.writeString(isdeleted);
        dest.writeString(level);
        dest.writeString(vegan);
        dest.writeString(gluten);
        dest.writeString(dairy);
        dest.writeString(raw);
        dest.writeString(breakFast);
        dest.writeString(supplier_opening_time);
        dest.writeString(supplier_closing_time);
        dest.writeString(launch);
        dest.writeString(snacks);
        dest.writeString(dinner);
        dest.writeLong(openingTime);
        dest.writeLong(closingTime);
        dest.writeString(category_id);
        dest.writeString(category_name);
        dest.writeString(perUnit);
        dest.writeString(qtyUnit);
        dest.writeString(minOrderAmount);
        dest.writeString(cashOnly);
        dest.writeString(minQty);
    }
}
