package com.o2.plotos.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.o2.plotos.restapi.newApis.models.dishdetails.AddOns;

import org.parceler.Parcel;

import java.util.List;


@Parcel
public class ItemDetais {
    @Expose
    @SerializedName(value = "item_id", alternate = {"dish_id", "id"})
    String item_id;


    @Expose
    @SerializedName(value = "provider_id", alternate = {"supplier_id", "restaurant_id"})
    String provider_id;

    @Expose
    @SerializedName("nutritional_info")
    String nutritional_info;

    @Expose
    @SerializedName("order_id")
    String order_id;

    @Expose
    @SerializedName("add_on_id")
    String add_on_id;

    @Expose
    @SerializedName(value = "item_count", alternate = {"dish_order_count", "item_order_count"})
    String item_count;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getAdd_on_id() {
        return add_on_id;
    }

    public void setAdd_on_id(String add_on_id) {
        this.add_on_id = add_on_id;
    }

    public String getItem_count() {
        return item_count;
    }

    public void setItem_count(String item_count) {
        this.item_count = item_count;
    }

    @Expose
    @SerializedName("name")
    String name;

    @Expose
    @SerializedName("image")
    String image;

    @Expose
    @SerializedName("price")
    String price;

    @Expose
    @SerializedName("currency")
    String currency;

    @Expose
    @SerializedName("pre_booking")
    String pre_booking;

    @SerializedName("addons")
    List<AddOns> list_addons;

    @Expose
    @SerializedName("special_note")
    String special_note;

    public String getSpecial_note() {
        return special_note;
    }

    public void setSpecial_note(String special_note) {
        this.special_note = special_note;
    }

    public List<AddOns> getList_addons() {
        return list_addons;
    }

    public void setList_addons(List<AddOns> list_addons) {
        this.list_addons = list_addons;
    }

    @Expose
    @SerializedName(value = "provider_name", alternate = {"restaurant_name", "supplier_name"})
    String provider_name;

    @Expose
    @SerializedName(value = "provider_img", alternate = {"restaurant_image", "supplier_image"})
    String provider_img;

    @Expose
    @SerializedName("delivery_time")
    String delivery_time;

    @Expose
    @SerializedName("ingredients")
    String ingredients;
    @Expose
    @SerializedName("other_details")
    String other_details;
    @Expose
    @SerializedName("min_order_amount")
    String min_order_amount;
    @Expose
    @SerializedName("opening_time")
    String opening_time;
    @Expose
    @SerializedName("closing_time")
    String closing_time;

    @Expose
    @SerializedName("restaurant_description")
    String restaurant_description;

    @Expose
    @SerializedName("start_price")
    String start_price;


    @Expose
    @SerializedName("end_price")
    String end_price;


    public String getItem_note() {
        return item_note;
    }

    public void setItem_note(String item_note) {
        this.item_note = item_note;
    }

    String item_note = "";

    public String getPre_booking() {
        return pre_booking;
    }

    public void setPre_booking(String pre_booking) {
        this.pre_booking = pre_booking;
    }


    public String getMin_order_amount() {
        return min_order_amount;
    }

    public void setMin_order_amount(String min_order_amount) {
        this.min_order_amount = min_order_amount;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    public String getOther_details() {
        return other_details;
    }

    public void setOther_details(String other_details) {
        this.other_details = other_details;
    }

    int quantity = 0;
    Double totalPrice = 0.0;

    public Double getBasicPrice() {
        return basicPrice;
    }

    public void setBasicPrice(Double basicPrice) {
        this.basicPrice = basicPrice;
    }

    Double basicPrice = 0.0;
    boolean isGrocery;

    public boolean isGrocery() {
        return isGrocery;
    }

    public void setGrocery(boolean grocery) {
        isGrocery = grocery;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getNutritional_info() {
        return nutritional_info;
    }

    public void setNutritional_info(String nutritional_info) {
        this.nutritional_info = nutritional_info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public String getProvider_img() {
        return provider_img;
    }

    public void setProvider_img(String provider_img) {
        this.provider_img = provider_img;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getRestaurant_description() {
        return restaurant_description;
    }

    public void setRestaurant_description(String restaurant_description) {
        this.restaurant_description = restaurant_description;
    }

    public String getStart_price() {
        return start_price;
    }

    public void setStart_price(String start_price) {
        this.start_price = start_price;
    }

    public String getEnd_price() {
        return end_price;
    }

    public void setEnd_price(String end_price) {
        this.end_price = end_price;
    }

}
