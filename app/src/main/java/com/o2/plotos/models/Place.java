package com.o2.plotos.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rania on 5/8/2017.
 */

public class Place implements Parcelable{

    int is_restaurant;

    public Place(){}

    public int is_restaurant() {
        return is_restaurant;
    }

    public void setIs_restaurant(int is_restaurant) {
        this.is_restaurant = is_restaurant;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(is_restaurant);
    }

    protected Place(Parcel in) {
        is_restaurant = in.readInt();
    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };
}
