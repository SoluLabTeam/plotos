package com.o2.plotos.models;

import java.util.ArrayList;
import java.util.List;

public class ConfirmOrder {

    String provider_id;
    String provider_name;
    String provider_img;
    String pre_booking;
    String min_order_amount;
    String opening_time;
    String closing_time;
    private double final_total=0.0;

    public double getFinal_total() {
        return final_total;
    }

    public void setFinal_total(double final_total) {
        this.final_total = final_total;
    }

    private List<ItemDetais> itemDetais = new ArrayList<>();

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public String getProvider_img() {
        return provider_img;
    }

    public void setProvider_img(String provider_img) {
        this.provider_img = provider_img;
    }

    public String getPre_booking() {
        return pre_booking;
    }

    public void setPre_booking(String pre_booking) {
        this.pre_booking = pre_booking;
    }

    public String getMin_order_amount() {
        return min_order_amount;
    }

    public void setMin_order_amount(String min_order_amount) {
        this.min_order_amount = min_order_amount;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    public List<ItemDetais> getItemDetais() {
        return itemDetais;
    }

    public void setItemDetais(List<ItemDetais> itemDetais) {
        this.itemDetais = itemDetais;
    }
}
