package com.o2.plotos.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreditCard {

    @Expose
    @SerializedName("cvv")
    private String cvv;
    @Expose
    @SerializedName("month")
    private String month;
    @Expose
    @SerializedName("number")
    private String number;
    @Expose
    @SerializedName("name")
    private String name;

    public CreditCard(String cvv, String month, String number, String name) {
        this.cvv = cvv;
        this.month = month;
        this.number = number;
        this.name = name;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
