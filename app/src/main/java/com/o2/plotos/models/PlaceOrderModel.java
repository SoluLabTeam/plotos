package com.o2.plotos.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaceOrderModel {


    @Expose
    @SerializedName("receiver_name")
    private String receiver_name = "";
    @Expose
    @SerializedName("add_on_id")
    private String add_on_id = "";
    @Expose
    @SerializedName("order_delivery_time")
    private String order_delivery_time = "";
    @Expose
    @SerializedName("dish_order_count")
    private String dish_order_count = "0";
    @Expose
    @SerializedName("price")
    private String price = "";
    @Expose
    @SerializedName("user_number")
    private String user_number = "";
    @Expose
    @SerializedName("note")
    private String note = "";
    @Expose
    @SerializedName("dish_name")
    private String dish_name = "";
    @Expose
    @SerializedName("restaurant_id")
    private String restaurant_id = "0";
    @Expose
    @SerializedName("user_longitude")
    private String user_longitude = "";
    @Expose
    @SerializedName("special_note")
    private String special_note = "";
    @Expose
    @SerializedName("cc_number")
    private String cc_number = "";
    @Expose
    @SerializedName("cc_token")
    private String cc_token = "";

    private String discount = "";

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getCc_token() {
        return cc_token;
    }

    public void setCc_token(String cc_token) {
        this.cc_token = cc_token;
    }

    @Expose
    @SerializedName("currency")
    private String currency = "AED";
    @Expose
    @SerializedName("user_latitude")
    private String user_latitude = "";
    @Expose
    @SerializedName("user_id")
    private String user_id = "";
    @Expose
    @SerializedName("dish_id")
    private String dish_id = "0";
    @Expose
    @SerializedName("item_id")
    private String item_id = "0";
    @Expose
    @SerializedName("supplier_id")
    private String supplier_id = "0";
    @Expose
    @SerializedName("item_order_count")
    private String item_order_count = "0";
    @Expose
    @SerializedName("time")
    private String time = "";
    @Expose
    @SerializedName("status")
    private String status = "1";
    @Expose
    @SerializedName("user_address")
    private String user_address = "";


    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getItem_order_count() {
        return item_order_count;
    }

    public void setItem_order_count(String item_order_count) {
        this.item_order_count = item_order_count;
    }

    @Expose
    @SerializedName("delivery_charge")

    private String delivery_charge = "5";

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getAdd_on_id() {
        return add_on_id;
    }

    public void setAdd_on_id(String add_on_id) {
        this.add_on_id = add_on_id;
    }

    public String getOrder_delivery_time() {
        return order_delivery_time;
    }

    public void setOrder_delivery_time(String order_delivery_time) {
        this.order_delivery_time = order_delivery_time;
    }

    public String getDish_order_count() {
        return dish_order_count;
    }

    public void setDish_order_count(String dish_order_count) {
        this.dish_order_count = dish_order_count;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUser_number() {
        return user_number;
    }

    public void setUser_number(String user_number) {
        this.user_number = user_number;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDish_name() {
        return dish_name;
    }

    public void setDish_name(String dish_name) {
        this.dish_name = dish_name;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getUser_longitude() {
        return user_longitude;
    }

    public void setUser_longitude(String user_longitude) {
        this.user_longitude = user_longitude;
    }

    public String getSpecial_note() {
        return special_note;
    }

    public void setSpecial_note(String special_note) {
        this.special_note = special_note;
    }

    public String getCc_number() {
        return cc_number;
    }

    public void setCc_number(String cc_number) {
        this.cc_number = cc_number;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getUser_latitude() {
        return user_latitude;
    }

    public void setUser_latitude(String user_latitude) {
        this.user_latitude = user_latitude;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDish_id() {
        return dish_id;
    }

    public void setDish_id(String dish_id) {
        this.dish_id = dish_id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_address() {
        return user_address;
    }

    public void setUser_address(String user_address) {
        this.user_address = user_address;
    }

    public String getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(String delivery_charge) {
        this.delivery_charge = delivery_charge;
    }
}
