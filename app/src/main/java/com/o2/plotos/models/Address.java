package com.o2.plotos.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hassan on 2/16/2017.
 */

public class Address {
    @SerializedName("address")
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
