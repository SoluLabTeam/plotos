package com.o2.plotos.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rania on 5/7/2017.
 */

public class SubCategory implements Parcelable {

    private String id;
    private String parent_id;
    private String name;
    private String description;
    private String image;
    private String image_url;
    private String status;
    private String statusText;
    private String parent_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getParent_name() {
        return parent_name;
    }

    public void setParent_name(String parent_name) {
        this.parent_name = parent_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        id = dest.readString();
        parent_id = dest.readString();
        name = dest.readString();
        description = dest.readString();
        image = dest.readString();
        image_url = dest.readString();
        status = dest.readString();
        statusText = dest.readString();
        parent_name = dest.readString();
    }
    public SubCategory(){}

    protected SubCategory(Parcel in) {
        id = in.readString();
        parent_id = in.readString();
        name = in.readString();
        description = in.readString();
        image = in.readString();
        image_url = in.readString();
        status = in.readString();
        statusText = in.readString();
        parent_name = in.readString();
    }

    public static final Creator<SubCategory> CREATOR = new Creator<SubCategory>() {
        @Override
        public SubCategory createFromParcel(Parcel in) {
            return new SubCategory(in);
        }

        @Override
        public SubCategory[] newArray(int size) {
            return new SubCategory[size];
        }
    };
}
