package com.o2.plotos.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Hassan on 1/28/2017.
 */
@DatabaseTable( tableName = "user")
public class User {
    @DatabaseField(columnName = "user_id" , id = true) //PK
    @SerializedName("user_id")
    private String id ;

    @DatabaseField(columnName = "first_name")
    @SerializedName("first_name")
    private String firstName;

    @DatabaseField(columnName = "last_name")
    @SerializedName("last_name")
    private String lastName;

    @DatabaseField(columnName = "image")
    @SerializedName("image")
    private String imageUrl;

    @DatabaseField(columnName = "number")
    @SerializedName("number")
    private String number;

    @DatabaseField(columnName = "latitutde")
    @SerializedName("latitutde")
    private String lat;

    @DatabaseField(columnName = "longitude")
    @SerializedName("longitude")
    private String lng;

    @DatabaseField(columnName = "timezone")
    @SerializedName("timezone")
    private String timeZone;

    @DatabaseField(columnName = "verified")
    @SerializedName("verified")
    private String verified;

    @DatabaseField(columnName = "email")
    @SerializedName("email")
    private String email;

    public User(){}


//    @SerializedName("address")
   // private List<Address> addressList;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageUrl() {
        return (imageUrl!=null) ? imageUrl : "";
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public boolean getVerified() {
        return (verified!=null)?verified.equals("1"):false;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

//    public List<Address> getAddressList() {
//        return addressList;
//    }
//
//    public void setAddressList(List<Address> addressList) {
//        this.addressList = addressList;
//    }
}
