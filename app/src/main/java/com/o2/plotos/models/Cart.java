package com.o2.plotos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Hassan on 6/02/2017.
 */
@DatabaseTable(tableName = "cart")
public class Cart implements Parcelable{

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private int itemId;

    @DatabaseField
    @SerializedName("restaurant_id")
    private String resturantId;

    @SerializedName("dish_id")
    @DatabaseField
    private String dishId;

    @SerializedName("user_id")
    @DatabaseField
    private String userId;

    @SerializedName("receiver_name")
    @DatabaseField
    private String receiver_name;

    @SerializedName("user_number")
    @DatabaseField
    private String userNumber;

    @SerializedName("add_on_id")
    @DatabaseField
    private String addOnId;

    @SerializedName("user_latitude")
    @DatabaseField
    private String userLat;

    @SerializedName("user_longitude")
    @DatabaseField
    private String userLng;

    @SerializedName("user_address")
    @DatabaseField
    private String userAddress;

    @SerializedName("cc_number")
    @DatabaseField
    private String cardNumber;

    @SerializedName("cc_token")
    @DatabaseField
    private String cardToken;

    @SerializedName("cc_id")
    @DatabaseField
    private String cardId;

    @SerializedName("cc_customer_id")
    @DatabaseField
    private String cardCustomerId;

    @SerializedName("promo_code")
    @DatabaseField
    private String promoCode;

    @SerializedName("note")
    @DatabaseField
    private String note;

    @SerializedName("special_note")
    @DatabaseField
    private String specialNote;

    @SerializedName("currency")
    @DatabaseField
    private String currenyCode;

    @SerializedName("status")
    @DatabaseField
    private String statusCode;

    @SerializedName("dish_order_count")
    @DatabaseField
    private String item_count;

    @SerializedName("dish_name")
    @DatabaseField
    private String dishName;

    @SerializedName("price")
    @DatabaseField
    private String dishPrice;

    @SerializedName("discount")
    private int discount;

    @DatabaseField
    private String time;

    @SerializedName("item_id")
    @DatabaseField
    private String itemGroceryID;

    @SerializedName("supplier_id")
    @DatabaseField
    private String supplierID;

    @SerializedName("item_order_count")
    @DatabaseField
    private String itemOrderCount;

    @DatabaseField
    private transient String cashOnly;

    @DatabaseField
    private String minOrderAmount;

    @DatabaseField
    private String restaurantName;
    @DatabaseField
    private String supplierName;

    @SerializedName("delivery_charge")
    private String deliveryCharge;

    @SerializedName("ambassador")
    private String ambassador;


    public String getAmbassador() {
        return ambassador;
    }

    public void setAmbassador(String ambassador) {
        this.ambassador = ambassador;
    }

    public String getItem_count() {
        return item_count;
    }

    public void setItem_count(String item_count) {
        this.item_count = item_count;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public String getDishPrice() {
        return dishPrice;
    }

    public void setDishPrice(String dishPrice) {
        this.dishPrice = dishPrice;
    }

    public String getResturantId() {
        return resturantId;
    }

    public void setResturantId(String resturantId) {
        this.resturantId = resturantId;
    }

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAddOnId() {
        return addOnId;
    }

    public void setAddOnId(String addOnId) {
        this.addOnId = addOnId;
    }

    public String getUserLat() {
        return userLat;
    }

    public void setUserLat(String userLat) {
        this.userLat = userLat;
    }

    public String getUserLng() {
        return userLng;
    }

    public void setUserLng(String userLng) {
        this.userLng = userLng;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardCustomerId() {
        return cardCustomerId;
    }

    public void setCardCustomerId(String cardCustomerId) {
        this.cardCustomerId = cardCustomerId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSpecialNote() {
        return specialNote;
    }

    public void setSpecialNote(String specialNote) {
        this.specialNote = specialNote;
    }

    public String getCurrenyCode() {
        return currenyCode;
    }

    public void setCurrenyCode(String currenyCode) {
        this.currenyCode = currenyCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    public String getItemOrderCount() {
        return itemOrderCount;
    }

    public void setItemOrderCount(String itemOrderCount) {
        this.itemOrderCount = itemOrderCount;
    }

    public String getItemGroceryID() {
        return itemGroceryID;
    }

    public void setItemGroceryID(String itemGroceryID) {
        this.itemGroceryID = itemGroceryID;
    }

    public String getCashOnly() {
        return cashOnly;
    }

    public void setCashOnly(String cashOnly) {
        this.cashOnly = cashOnly;
    }

    public String getMinOrderAmount() {
        return minOrderAmount;
    }

    public void setMinOrderAmount(String minOrderAmount) {
        this.minOrderAmount = minOrderAmount;
    }


    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "itemId=" + itemId +
                ", resturantId='" + resturantId + '\'' +
                ", dishId='" + dishId + '\'' +
                ", userId='" + userId + '\'' +
                ", receiver_name='" + receiver_name + '\'' +
                ", userNumber='" + userNumber + '\'' +
                ", addOnId='" + addOnId + '\'' +
                ", userLat='" + userLat + '\'' +
                ", userLng='" + userLng + '\'' +
                ", userAddress='" + userAddress + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", cardToken='" + cardToken + '\'' +
                ", cardId='" + cardId + '\'' +
                ", promoCode='" + promoCode + '\'' +
                ", note='" + note + '\'' +
                ", specialNote='" + specialNote + '\'' +
                ", currenyCode='" + currenyCode + '\'' +
                ", statusCode='" + statusCode + '\'' +
                ", item_count='" + item_count + '\'' +
                ", dishName='" + dishName + '\'' +
                ", dishPrice='" + dishPrice + '\'' +
                ", discount=" + discount +
                ", time='" + time + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.itemId);
        dest.writeString(this.resturantId);
        dest.writeString(this.dishId);
        dest.writeString(this.userId);
        dest.writeString(this.receiver_name);
        dest.writeString(this.userNumber);
        dest.writeString(this.addOnId);
        dest.writeString(this.userLat);
        dest.writeString(this.userLng);
        dest.writeString(this.userAddress);
        dest.writeString(this.cardNumber);
        dest.writeString(this.cardToken);
        dest.writeString(this.cardId);
        dest.writeString(this.promoCode);
        dest.writeString(this.note);
        dest.writeString(this.specialNote);
        dest.writeString(this.currenyCode);
        dest.writeString(this.statusCode);
        dest.writeString(this.item_count);
        dest.writeString(this.dishName);
        dest.writeString(this.dishPrice);
        dest.writeInt(this.discount);
        dest.writeString(this.time);
        dest.writeString(this.itemGroceryID);
        dest.writeString(this.itemOrderCount);
        dest.writeString(this.supplierID);
        dest.writeString(this.restaurantName);
        dest.writeString(this.supplierName);
        dest.writeString(this.deliveryCharge);
    }

    public Cart() {
    }

    protected Cart(Parcel in) {
        this.itemId = in.readInt();
        this.resturantId = in.readString();
        this.dishId = in.readString();
        this.userId = in.readString();
        this.receiver_name = in.readString();
        this.userNumber = in.readString();
        this.addOnId = in.readString();
        this.userLat = in.readString();
        this.userLng = in.readString();
        this.userAddress = in.readString();
        this.cardNumber = in.readString();
        this.cardToken = in.readString();
        this.cardId = in.readString();
        this.promoCode = in.readString();
        this.note = in.readString();
        this.specialNote = in.readString();
        this.currenyCode = in.readString();
        this.statusCode = in.readString();
        this.item_count = in.readString();
        this.dishName = in.readString();
        this.dishPrice = in.readString();
        this.discount = in.readInt();
        this.time = in.readString();
        this.itemGroceryID = in.readString();
        this.itemOrderCount = in.readString();
        this.supplierID = in.readString();
        this.restaurantName = in.readString();
        this.supplierName = in.readString();
        this.deliveryCharge = in.readString();
    }

    public static final Creator<Cart> CREATOR = new Creator<Cart>() {
        @Override
        public Cart createFromParcel(Parcel source) {
            return new Cart(source);
        }

        @Override
        public Cart[] newArray(int size) {
            return new Cart[size];
        }
    };
}
