package com.o2.plotos.dishdetail;

/**
 * Created by Rania on 3/22/2017.
 */
public class DishDetailViewModel implements CheckInRadiusListener{

    CheckIfRestaurantInRadiusHelper radiusHelper;
    CheckInRadiusResultsListener mListener;

    public DishDetailViewModel(CheckInRadiusResultsListener checkInRadiusResultsListener){
        radiusHelper = new CheckIfRestaurantInRadiusHelper(this);
        mListener = checkInRadiusResultsListener;
    }

    public void checkIfRestaurantInRadius(String restaurantID, String userLat, String userLang){
        radiusHelper.checkInRadiusRequest(restaurantID, userLat,userLang, true);
    }

    public void checkIfSupplierInRadius(String supplierID, String userLat, String userLang){
        radiusHelper.checkInRadiusRequest(supplierID, userLat,userLang, false);
    }

    @Override
    public void OnResponseSuccess() {
        mListener.onRestaurantInRadius();
    }

    @Override
    public void onResponseFailure(String error) {
        mListener.onRestaurantNotInRadius(error);
//        mListener.onRestaurantInRadius();
    }

    public void onDestroy(){
        if(mListener != null){
            mListener = null;
        }
    }
}
