package com.o2.plotos.dishdetail;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.CheckInRadiusRequest;
import com.o2.plotos.restapi.responses.InRadiusResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RANIA on 3/22/2017.
 */
public class CheckIfRestaurantInRadiusHelper {

    CheckInRadiusListener mListener;

    public CheckIfRestaurantInRadiusHelper(CheckInRadiusListener checkInRadiusListener){
        mListener = checkInRadiusListener;
    }

    public void checkInRadiusRequest(String placeID, String userLat, String userLang, boolean isRestaurant){
        CheckInRadiusRequest checkInRadiusRequest = ServiceGenrator.createService(CheckInRadiusRequest.class);
        Call<InRadiusResponse> inRadiusResponseCall;
        if(isRestaurant) {
            inRadiusResponseCall = checkInRadiusRequest.InRadiusRequest(placeID, userLat, userLang);
        }else{
            inRadiusResponseCall = checkInRadiusRequest.InRadiusSupplierRequest(placeID, userLat, userLang);
        }
        inRadiusResponseCall.enqueue(new Callback<InRadiusResponse>() {
            @Override
            public void onResponse(Call<InRadiusResponse> call, Response<InRadiusResponse> response) {
                LogCat.LogDebug(ConstantUtil.TAG," -> Restaurant api response "+response.raw());
                if(response.code()== 200) {
                    InRadiusResponse apiResponse = response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("1")){
                        mListener.OnResponseSuccess();
                    }else if(responseCode.equalsIgnoreCase("0")){
                        mListener.onResponseFailure("Restaurant is not in your radius");
                    }else{
                        mListener.onResponseFailure(apiResponse.message);
                    }
                }
            }

            @Override
            public void onFailure(Call<InRadiusResponse> call, Throwable t) {
                mListener.onResponseFailure("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"resturant ap onFailure "+t.getMessage());
            }
        });
    }
}
