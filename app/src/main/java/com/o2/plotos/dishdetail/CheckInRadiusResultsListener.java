package com.o2.plotos.dishdetail;

/**
 * Created by Rania on 3/23/2017.
 */
public interface CheckInRadiusResultsListener {

    void onRestaurantInRadius();
    void onRestaurantNotInRadius(String error);
}
