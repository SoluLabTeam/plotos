package com.o2.plotos.dishdetail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.o2.plotos.R;
import com.o2.plotos.cart.CartActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.models.Cart;
import com.o2.plotos.models.Category;
import com.o2.plotos.models.Dish;
import com.o2.plotos.models.Grocery;
import com.o2.plotos.models.Option;
import com.o2.plotos.models.Order;
import com.o2.plotos.models.PlaceItem;
import com.o2.plotos.models.User;
import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.EditOrderGroceryRequestObj;
import com.o2.plotos.restapi.endpoints.EditOrderRequest;
import com.o2.plotos.restapi.endpoints.EditOrderRequestObj;
import com.o2.plotos.restapi.responses.PlaceOrderResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.TimeUtil;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DishDetailActivity extends AppCompatActivity implements CheckInRadiusResultsListener {

    public static final String EXTRA_DISH = "dish";
    public static final String EXTRA_EDIT_CART = "cart";
    public static final String EXTRA_EDIT_DISH = "dish_edit";
    public static final String EXTRA_EDIT_ORDER = "order";
    public static final String EXTRA_IS_EDIT_ORDER = "order_edit";


    @BindView(R.id.activity_dish_detail_add_cart)
    Button cartButton;
    @BindView(R.id.activity_dish_detail_resturant_closed)
    FrameLayout closedView;
    @BindView(R.id.dish_detail_dishName)
    TextView dishName;
    @BindView(R.id.activity_dish_detail_description)
    TextView dishdescriptn;
    @BindView(R.id.activity_dish_detail_delivery_time)
    TextView deliveryTime;
    @BindView(R.id.activity_dish_detail_specialInstructions)
    TextView specialInstuctiontextView;
    @BindView(R.id.showDigit)
    TextView digit;
    @BindView(R.id.dish_detail_image_cover)
    ImageView coverImage;
    @BindView(R.id.activity_dish_detail_nutrition_container)
    LinearLayout nutritionLayout;
    @BindView(R.id.activity_dish_detail_minus)
    ImageButton minus;
    @BindView(R.id.activity_dish_detail_plus)
    ImageButton plus;
    @BindView(R.id.activity_dish_detail_optionLayout)
    LinearLayout layoutOption;
    @BindView(R.id.activity_dish_detail_edit_notes)
    EditText editTextNote;
    @BindView(R.id.activity_home_btn_cart)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.activity_home_txt_item)
    TextView txtCartItem;

    CustomFonts customFonts;
    TimeUtil timeUtil;

    @BindView(R.id.diet_detail_sharing)
    ImageButton dietDetailSharing;
    @BindView(R.id.cart_layout)
    LinearLayout cartLayout;
    private PlaceItem mDish;
    private int quantity = 1;
    //    private int totalPrice = 0;
//    private int calculatedPrice;
    private Double grandTotal;
    private int orginalPrice;
    private int addOnSum = 0;
    private DishDetailViewModel mViewModel;
    private boolean mIsDishEdit = false;
    private Cart mCart;
    private boolean mIsOrderEdit = false;
    private Order mEditOrder = null;

    private ProgressDialog mProgressDialog;

    int[] images = {R.drawable.one, R.drawable.two, R.drawable.three, R.drawable.four, R.drawable.five,
            R.drawable.six, R.drawable.seven, R.drawable.eight, R.drawable.nine, R.drawable.ten,
            R.drawable.eleven, R.drawable.twelve};
    String[] names = {"Vegan", "Vegetarian", "Gluten Free", "Dairy Free", "Contain Nuts", "Contain Peanuts", "Paleo", "Contain Eggs", "Egg Free", "Low Carb", "No Added Sugar", "Raw"};


    private HashMap<Integer, List<Option>> adsHashMap = new HashMap<>();
    private HashMap<Integer, List<Integer>> radioGroupHashMap = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_detail);
        ButterKnife.bind(this);
        mViewModel = new DishDetailViewModel(this);
        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE);
        if (getIntent().getExtras() != null) {
            mDish = getIntent().getExtras().getParcelable(EXTRA_DISH);
            mCart = getIntent().getExtras().getParcelable(EXTRA_EDIT_CART);
            mIsDishEdit = getIntent().getExtras().getBoolean(EXTRA_EDIT_DISH);
            mIsOrderEdit = getIntent().getExtras().getBoolean(EXTRA_IS_EDIT_ORDER);
            mEditOrder = getIntent().getExtras().getParcelable(EXTRA_EDIT_ORDER);
        }

        customFonts = new CustomFonts(this);
        timeUtil = new TimeUtil(this);
        setCustomFonts();
        setHeaderImage();
        if (mIsDishEdit) {
            editTextNote.setText(mCart.getSpecialNote());
            if (mCart.getDishId() != null && !mCart.getDishId().equals("")) {
                digit.setText(mCart.getItem_count());
            } else { // Grocery
                digit.setText(mCart.getItemOrderCount());
            }
        } else if (mIsOrderEdit) {
            if (mEditOrder.getDishId() != null && !mEditOrder.getDishId().equals("0")) {
                digit.setText(mEditOrder.getDishCount());
            } else {
                digit.setText(mEditOrder.getItemOrderCount());
            }
            editTextNote.setText(mEditOrder.getNote());
        } else {
            if (mDish instanceof Dish) {
                digit.setText("1");
            } else {
                if (((Grocery) mDish).getMinQty().equals("0.00")) {
                    digit.setText("1");
                } else {
                    digit.setText(((Grocery) mDish).getMinQty());
                }
            }
        }

        if (mDish instanceof Dish) {
            try {
                if (mIsDishEdit) {
                    grandTotal = (int) Double.parseDouble(((Dish) mDish).getPrice())
                            * Double.parseDouble(mCart.getItem_count());
                    quantity = Integer.parseInt(mCart.getItem_count());
                } else if (mIsOrderEdit) {

                    grandTotal = (int) Double.parseDouble(((Dish) mDish).getPrice())
                            * Double.parseDouble(mEditOrder.getDishCount());
                    quantity = Integer.parseInt(mEditOrder.getDishCount());

                } else {
                    grandTotal = Double.parseDouble(((Dish) mDish).getPrice());
                }
//                calculatedPrice =  Double.parseDouble(((ResturandResult)mDish).getPrice());
                setTotalPrice();
                orginalPrice = (int) Double.parseDouble(((Dish) mDish).getPrice());
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }
        } else if (mDish instanceof Grocery) {
            try {
                if (mIsDishEdit) {
                    grandTotal = (int) Double.parseDouble(((Grocery) mDish).getPrice())
                            * Double.parseDouble(mCart.getItemOrderCount());
                    quantity = Integer.parseInt(mCart.getItemOrderCount());
                } else if (mIsOrderEdit) {
                    grandTotal = (int) Double.parseDouble(((Grocery) mDish).getPrice())
                            * Double.parseDouble(mEditOrder.getItemOrderCount());
                    quantity = Integer.parseInt(mEditOrder.getItemOrderCount());
                } else {
                    if (((Grocery) mDish).getMinQty().equals("0.00")) {
                        quantity = 1;
                    } else {
                        quantity = Integer.parseInt(((Grocery) mDish).getMinQty());
                    }
                    grandTotal = quantity * Double.parseDouble(((Grocery) mDish).getPrice());
                }
//                calculatedPrice = (int) Double.parseDouble(((Grocery)mDish).getPrice());
                setTotalPrice();
                orginalPrice = (int) Double.parseDouble(((Grocery) mDish).getPrice());
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }
        }
        addOption();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.msg_please_wait));
        mProgressDialog.setCancelable(false);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DishDetailActivity.this, CartActivity.class));
            }
        });
        setCartFab();
    }

    /**
     * Show cart item from database
     */
    private void setCartFab() {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        int cartItemSize = dataBaseHelper.getCartItems().size();
        if (cartItemSize > 0) {
            floatingActionButton.show();
            txtCartItem.setVisibility(View.VISIBLE);
            txtCartItem.setText("" + cartItemSize);
        } else {
            txtCartItem.setVisibility(View.GONE);
            floatingActionButton.hide();
        }
    }

    private void setTotalPrice() {
        if (mIsDishEdit) {
            cartButton.setText("Save " + quantity + " to Cart( " + grandTotal + "AED)");
        } else if (mIsOrderEdit) {
            cartButton.setText("Save " + quantity + " to Order( " + grandTotal + "AED)");
        } else {
            cartButton.setText("Add " + quantity + " to Cart( " + grandTotal + "AED)");
        }
    }


    public void setHeaderImage() {
        if (mDish instanceof Dish) {
            if (timeUtil.isPlaceOpened(((Dish) mDish).getOpeningTime(), ((Dish) mDish).getClosingTime())) {
                closedView.setVisibility(View.GONE);
                cartButton.setVisibility(View.VISIBLE);
            } else {
                closedView.setVisibility(View.VISIBLE);
                cartButton.setVisibility(View.GONE);
            }
            dishdescriptn.setText(((Dish) mDish).getDescrptn());
            if (!((Dish) mDish).getPreBookingtime().equalsIgnoreCase("0")) {
                deliveryTime.setText(((Dish) mDish).getPreBookingtime() + " Hours notice.");
                deliveryTime.setTextColor(Color.rgb(246, 122, 64));
            } else {
                if (!((Dish) mDish).getCustomDelivery().equals("")) {
                    deliveryTime.setText(((Dish) mDish).getCustomDelivery());
                } else {
                    if (((Dish) mDish).getDeliveryTime() != null && !((Dish) mDish).getDeliveryTime().equals("")) {
                        deliveryTime.setText(((Dish) mDish).getDeliveryTime() + " Min");
                    }
                }
            }
            dishName.setText(((Dish) mDish).getDishName());

            if (!((Dish) mDish).getNutritions().equalsIgnoreCase("")) {
                String[] values = ((Dish) mDish).getNutritions().split(",");
                //added LayoutParams
                nutritionLayout.setOrientation(LinearLayout.HORIZONTAL);
                int layoutSize = getLayoutSize();
                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(layoutSize, LinearLayout.LayoutParams.WRAP_CONTENT);
                LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                int imageSize = getImageSize();
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(imageSize, imageSize);
                int repeatLoop = 0;
                if (values.length > 5) {
                    repeatLoop = 5;
                } else {
                    repeatLoop = values.length;
                }
                int textSize = 7;
                if (imageSize > 110) {
                    textSize = 8;
                }

                for (int i = 0; i < repeatLoop; i++) {

                    //add LinearLayout
                    LinearLayout linearLayout1 = new LinearLayout(this);
                    linearLayout1.setOrientation(LinearLayout.VERTICAL);
                    linearLayout1.setGravity(Gravity.CENTER_HORIZONTAL);
                    linearLayout1.setLayoutParams(params1);

                    ImageView imageView = new ImageView(this);
                    TextView textView = new TextView(this);
                    int id = Integer.valueOf(values[i]);
                    imageView.setImageResource(images[(id - 1)]);
                    imageView.setLayoutParams(layoutParams);
                    textView.setText(names[(id - 1)]);
                    textView.setMaxLines(1);
                    textView.setTextSize(textSize);
                    textView.setLayoutParams(params2);
                    textView.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);

                    linearLayout1.addView(imageView);
                    linearLayout1.addView(textView);

                    nutritionLayout.addView(linearLayout1);
                }
            }

            if (!((Dish) mDish).getImage_url().equalsIgnoreCase("")) {
                Picasso.with(this).load(((Dish) mDish).getImage_url())
                        .resize(400, 200)
                        .centerCrop()
                        .error(R.drawable.placeholder)
                        .into(coverImage);
            } else {
                coverImage.setImageResource(R.drawable.placeholder);
            }
        } else if (mDish instanceof Grocery) {
            //TODO
            if (timeUtil.isPlaceOpened(((Grocery) mDish).getOpeningTime(), ((Grocery) mDish).getClosingTime())) {
                closedView.setVisibility(View.GONE);
                cartButton.setVisibility(View.VISIBLE);
            } else {
                closedView.setVisibility(View.VISIBLE);
                cartButton.setVisibility(View.GONE);
            }
            dishdescriptn.setText(((Grocery) mDish).getDescrptn());
            if (!((Grocery) mDish).getPreBookingtime().equalsIgnoreCase("0")) {
                deliveryTime.setText(((Grocery) mDish).getPreBookingtime() + " Hours notice.");
                deliveryTime.setTextColor(Color.rgb(246, 122, 64));
            } else {
                deliveryTime.setText(((Grocery) mDish).getDeliveryTime() + " Min");
            }
            dishName.setText(((Grocery) mDish).getName());

            if (!((Grocery) mDish).getNutritions().equalsIgnoreCase("")) {
                String[] values = ((Grocery) mDish).getNutritions().split(",");
                //added LayoutParams
                nutritionLayout.setOrientation(LinearLayout.HORIZONTAL);
                int layoutSize = getLayoutSize();
                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(layoutSize, LinearLayout.LayoutParams.WRAP_CONTENT);
                LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                int imageSize = getImageSize();
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(imageSize, imageSize);
                int repeatLoop = 0;
                if (values.length > 5) {
                    repeatLoop = 5;
                } else {
                    repeatLoop = values.length;
                }
                int textSize = 7;
                if (imageSize > 110) {
                    textSize = 8;
                }

                for (int i = 0; i < repeatLoop; i++) {

                    //add LinearLayout
                    LinearLayout linearLayout1 = new LinearLayout(this);
                    linearLayout1.setOrientation(LinearLayout.VERTICAL);
                    linearLayout1.setGravity(Gravity.CENTER_HORIZONTAL);
                    linearLayout1.setLayoutParams(params1);

                    ImageView imageView = new ImageView(this);
                    TextView textView = new TextView(this);
                    int id = Integer.valueOf(values[i]);
                    imageView.setImageResource(images[(id - 1)]);
                    imageView.setLayoutParams(layoutParams);
                    textView.setText(names[(id - 1)]);
                    textView.setMaxLines(1);
                    textView.setTextSize(textSize);
                    textView.setLayoutParams(params2);
                    textView.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);

                    linearLayout1.addView(imageView);
                    linearLayout1.addView(textView);

                    nutritionLayout.addView(linearLayout1);
                }
            }

            if (!((Grocery) mDish).getImage_url().equalsIgnoreCase("")) {
                Picasso.with(this).load(((Grocery) mDish).getImage_url())
                        .resize(400, 200)
                        .centerCrop()
                        .error(R.drawable.placeholder)
                        .into(coverImage);
            } else {
                coverImage.setImageResource(R.drawable.placeholder);
            }
        }

    }

    public void setCustomFonts() {
        customFonts.setOswaldBold(dishName);
        customFonts.setOpenSansSemiBold(dishdescriptn);
        customFonts.setOpenSansBold(deliveryTime);
        customFonts.setOpenSansSemiBold(specialInstuctiontextView);
    }

    private void addOption() {

        LinearLayout.LayoutParams textPriceParams = new LinearLayout.
                LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams paramsContainer = new LinearLayout.
                LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        RadioGroup.LayoutParams radioGroupParams = new RadioGroup.
                LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        if (mDish instanceof Dish) {
            for (int i = 0; i < ((Dish) mDish).getCategoryList().size(); i++) {
                final Category category = ((Dish) mDish).getCategoryList().get(i);
                List<Option> addOnIdList = new ArrayList<>();
                try {
                    adsHashMap.put(Integer.parseInt(category.getCategoryId()), addOnIdList);
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                }
                //Title
                setItemCategoryTitle(i, category);

                final LinearLayout radioGroupLayout = new LinearLayout(this);
                try {
                    radioGroupLayout.setId(Integer.parseInt(category.getCategoryId()));
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                }
                radioGroupLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT
                        , LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));
                radioGroupLayout.setOrientation(LinearLayout.VERTICAL);

                LinearLayout linearLayoutVertical = new LinearLayout(this);
                linearLayoutVertical.setId(i + 10);
                linearLayoutVertical.setOrientation(LinearLayout.VERTICAL);
                linearLayoutVertical.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT
                        , LinearLayout.LayoutParams.WRAP_CONTENT, 3.0f));

                LinearLayout linearLayoutHorizontal = new LinearLayout(this);
                linearLayoutHorizontal.setId(i + 110);
                linearLayoutHorizontal.setOrientation(LinearLayout.HORIZONTAL);
                linearLayoutHorizontal.setWeightSum(4);
                linearLayoutHorizontal.setLayoutParams(paramsContainer);

                List<Integer> radioButtonList = new ArrayList<>();

                for (int j = 0; j < category.getOptionList().size(); j++) {
                    final Option options = category.getOptionList().get(j);

                    View line = new View(this);
                    line.setMinimumHeight(1);
                    line.setLayoutParams(textPriceParams);
                    line.setBackgroundResource(R.color.textMidGray);

                    //adding option radio buttons
                    final CheckBox checkBoxOption = new CheckBox(this);
                    try {
                        checkBoxOption.setId(Integer.parseInt(options.getAddOnId()));
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                    checkBoxOption.setButtonDrawable(R.drawable.radiobtn_states);
                    checkBoxOption.setHeight(110);
                    checkBoxOption.setText(options.getName());
                    checkBoxOption.setLayoutParams(radioGroupParams);
                    checkBoxOption.setTextColor(getResources().getColor(R.color.textGray));
                    radioButtonList.add(checkBoxOption.getId());
                    radioGroupLayout.addView(checkBoxOption);
                    radioGroupLayout.addView(line);

                    checkBoxOption.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final CheckBox checkBox = (CheckBox) view;
                            try {
                                List<Option> optionList = adsHashMap.get(Integer.parseInt(category.getCategoryId()));
                                if (optionList.contains(options)) {
                                    optionList.remove(options);
                                    addOnSum = addOnSum - (int) Double.parseDouble(options.getPrice());
                                    Double sum = makePrice(addOnSum, orginalPrice);
                                    grandTotal = sum;
                                } else {
                                    int price;
                                    if (optionList.size() > 0) {
                                        addOnSum = addOnSum - (int) Double.parseDouble(optionList.get(0).getPrice());
                                    }

                                    optionList.clear();
                                    optionList.add(options);
                                    int addOnPriceAdd = (int) Double.parseDouble(options.getPrice());
                                    addOnSum = addOnSum + addOnPriceAdd;
                                    Double sum = makePrice(addOnSum, orginalPrice);
                                    grandTotal = sum;

                                }
                            } catch (NumberFormatException ex) {
                                ex.printStackTrace();
                            }
                            setTotalPrice();
                            List<Integer> list = radioGroupHashMap.get(((LinearLayout) view.getParent()).getId());
                            for (int k = 0; k < list.size(); k++) {
                                if (list.get(k) != checkBox.getId()) {
                                    CheckBox r = (CheckBox) radioGroupLayout.findViewById(list.get(k));
                                    r.setChecked(false);

                                }
                            }
                        }
                    });


                    //adding option prices
                    TextView textPrice = new TextView(this);
                    textPrice.setText(options.getPrice() + " AED");
                    textPrice.setId(j + 200);
                    textPrice.setHeight(110);
                    textPrice.setGravity(Gravity.CENTER | Gravity.END);
                    textPrice.setTextColor(getResources().getColor(android.R.color.black));
                    textPrice.setLayoutParams(textPriceParams);

                    linearLayoutVertical.addView(textPrice);
                    View line2 = new View(this);
                    line2.setMinimumHeight(1);
                    line2.setLayoutParams(textPriceParams);
                    line2.setBackgroundResource(R.color.textMidGray);

                    linearLayoutVertical.addView(line2);

                    customFonts.setOpenSansRegulr(textPrice);

                }
                radioGroupHashMap.put(radioGroupLayout.getId(), radioButtonList);
                linearLayoutHorizontal.addView(radioGroupLayout);
                linearLayoutHorizontal.addView(linearLayoutVertical);

                layoutOption.addView(linearLayoutHorizontal);
            }
        } else if (mDish instanceof Grocery) {
            //TODO
        }

    }

    private void setItemCategoryTitle(int i, Category category) {
        TextView textTitle = new TextView(this);
        textTitle.setId(i);
        textTitle.setText(category.getCategoryName());
        textTitle.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        textTitle.setGravity(Gravity.CENTER);

        layoutOption.addView(textTitle);
        customFonts.setOpenSansSemiBold(textTitle);
    }

    @OnClick(R.id.dish_detail_img_cross)
    public void closeScreen() {
        finish();
        //overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
    }*/

    @OnClick(R.id.activity_dish_detail_add_cart)
    public void addToCart() {


        if(quantity >0 )
        {

            //  mProgressDialog.show();
            String userLat;
            String userLong;
        /*if (UserPreferenceUtil.getInstance(this).getAsSoonCheck()) {
            if (UserPreferenceUtil.getInstance(this).getCurrentLocationCheck()) {
                userLat = UserPreferenceUtil.getInstance(this).getCurrentLat();
                userLong = UserPreferenceUtil.getInstance(this).getCurrentLng();
            } else {
                userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
                userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
            }
        } else {
            if (UserPreferenceUtil.getInstance(this).getCurrentLocationCheck()) {
                userLat = UserPreferenceUtil.getInstance(this).getCurrentLat();
                userLong = UserPreferenceUtil.getInstance(this).getCurrentLng();
            } else {
                userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
                userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
            }
        }*/

            userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
            userLong = UserPreferenceUtil.getInstance(this).getLocationLng();

         /*   if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
                if (mDish instanceof ResturandResult) {
                    mViewModel.checkIfRestaurantInRadius(((ResturandResult) mDish).getResturantId(), userLat, userLong);
                } else if (mDish instanceof Grocery) {
                    mViewModel.checkIfSupplierInRadius(((Grocery) mDish).getSupplier_id(), userLat, userLong);
                } else {
                    Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                }
            }
*/



            String userAddress;

        /*if (UserPreferenceUtil.getInstance(this).getAsSoonCheck()) {
            if (UserPreferenceUtil.getInstance(this).getCurrentLocationCheck()) {
                userLat = UserPreferenceUtil.getInstance(this).getCurrentLat();
                userLong = UserPreferenceUtil.getInstance(this).getCurrentLng();
                userAddress = UserPreferenceUtil.getInstance(this).getCurrentLocation();
            } else {
                userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
                userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
                userAddress = UserPreferenceUtil.getInstance(this).getLocation();

            }
        } else {
            if (UserPreferenceUtil.getInstance(this).getCurrentLocationCheck()) {
                userLat = UserPreferenceUtil.getInstance(this).getCurrentLat();
                userLong = UserPreferenceUtil.getInstance(this).getCurrentLng();
                userAddress = UserPreferenceUtil.getInstance(this).getCurrentLocation();
            } else {
                userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
                userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
                userAddress = UserPreferenceUtil.getInstance(this).getLocation();
            }
        }*/

            userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
            userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
            userAddress = UserPreferenceUtil.getInstance(this).getLocation();

            DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
            Cart cart = null;
            User user;
            try {
                if (mIsDishEdit) {
                    cart = mCart;
                } else {
                    if (mDish instanceof Dish) {
                        cart = dataBaseHelper.getCartIntegerDao().queryBuilder().where().eq("dishId", ((Dish) mDish).getDishId()).queryForFirst();
                    } else {
                        cart = dataBaseHelper.getCartIntegerDao().queryBuilder().where().eq("itemGroceryID", ((Grocery) mDish).getId()).queryForFirst();
                    }
                }

                if (cart == null) {
                    cart = new Cart();
                } else {
                    mIsDishEdit = true;
                }
                cart.setUserLat(userLat);
                cart.setUserLng(userLong);
                cart.setUserAddress(userAddress);

                if (mDish instanceof Dish) {
                    cart.setDishId(((Dish) mDish).getDishId());
                    cart.setDishName(((Dish) mDish).getDishName());
                    cart.setResturantId(((Dish) mDish).getResturantId());
                    cart.setCurrenyCode(((Dish) mDish).getCurrency());
                    cart.setItem_count(String.valueOf(quantity));
                    String itemPrice = ((Dish) mDish).getPrice();
                    grandTotal = quantity * Double.parseDouble(itemPrice);
                    cart.setRestaurantName(((Dish) mDish).getResturantName());
                    cart.setCashOnly(((Dish) mDish).getCashOnly());
                    cart.setMinOrderAmount(((Dish) mDish).getMinOrderAmount());

                } else if (mDish instanceof Grocery) {
                    cart.setItemGroceryID(((Grocery) mDish).getId());
                    cart.setDishName(((Grocery) mDish).getName());
                    cart.setSupplierID(((Grocery) mDish).getSupplier_id());
                    cart.setCurrenyCode(((Grocery) mDish).getCurrency());
                    cart.setItemOrderCount(String.valueOf(quantity));
                    String itemPrice = ((Grocery) mDish).getPrice();
                    grandTotal = quantity * Double.parseDouble(itemPrice);
                    cart.setSupplierName(((Grocery) mDish).getSupplier_name());
                    cart.setCashOnly(((Grocery) mDish).getCashOnly());
                    cart.setMinOrderAmount(((Grocery) mDish).getMinOrderAmount());
                }

                cart.setStatusCode("1");
                //cart.setNote("" + editTextNote.getText().toString());
                cart.setSpecialNote(editTextNote.getText().toString());

                cart.setPromoCode("0");
                cart.setCardNumber("0");
                //cart.setTime(mDish.getDeliveryTime());

                Iterator it = adsHashMap.entrySet().iterator();
                String adOnIds = "";
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    List<Option> optionList = (List<Option>) pair.getValue();
                    if (optionList.size() > 0) {
                        Log.v(ConstantUtil.TAG, " pair " + optionList.get(0).getName());
                        adOnIds += optionList.get(0).getAddOnId() + ",";
                    }
                    it.remove(); // avoids a ConcurrentModificationException
                }
                if (adOnIds.length() > 0) {
                    adOnIds = adOnIds.substring(0, adOnIds.length() - 1);
                }
                cart.setDishPrice("" + grandTotal);
                cart.setAddOnId(adOnIds);

                user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();
                if (user != null) {
                    cart.setUserId(user.getId());
                    cart.setReceiver_name(user.getFirstName() + " " + user.getLastName());
                    cart.setUserNumber(user.getNumber());
                }
                if (!mIsOrderEdit) {
                    dataBaseHelper.saveCartItem(cart, mIsDishEdit);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                if (!mIsOrderEdit) {
                    try {
                        dataBaseHelper.saveCartItem(cart, mIsDishEdit);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                }
            }
            if (mIsDishEdit) {
                Intent cartIntent = new Intent(DishDetailActivity.this, CartActivity.class);
                startActivity(cartIntent);
                finish();
            } else if (mIsOrderEdit) {
                editOrder(cart);

            } else {
                Intent intent = new Intent(DishDetailActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                intent.putExtra("CurrentTab", 2);
                startActivity(intent);
                finish();
            }

        }else {
            Toast.makeText(this, "please add quantity", Toast.LENGTH_SHORT).show();

        }
    }

    @OnClick(R.id.activity_dish_detail_minus)
    public void minusClicked() {
        if (quantity > 0) {
            if (mDish instanceof Dish) {
                quantity--;
            } else {
                if (((Grocery) mDish).getMinQty().equals("0.00")) {
                    quantity--;
                } else {
                    quantity = quantity - Integer.parseInt(((Grocery) mDish).getMinQty());
                }
            }
            Double sum = makePrice(addOnSum, orginalPrice);
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(quantity);
            grandTotal = sum;
            String strI = sb.toString();
            digit.setText(strI);
            setTotalPrice();
        }
    }

    @OnClick(R.id.activity_dish_detail_plus)
    public void plusClicked() {
        if (mDish instanceof Dish) {
            quantity++;
        } else {
            if (((Grocery) mDish).getMinQty().equals("0.00")) {
                quantity++;
            } else {
                quantity = quantity + Integer.parseInt(((Grocery) mDish).getMinQty());
            }
        }
        Double sum = makePrice(addOnSum, orginalPrice);
        grandTotal = sum;
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(quantity);
        String strI = sb.toString();
        digit.setText(strI);
        setTotalPrice();
    }

    public Double makePrice(int addOn, int dish) {
        Double makeSum = (double) ((addOn + dish) * quantity);
        return makeSum;
    }

    private int getImageSize() {
        int density = getResources().getDisplayMetrics().densityDpi;

        int size = 70;

        if (density < 100) {
            size = 60;
        } else if (density >= 100 && density < 201) {
            size = 60;
        } else if (density >= 201 && density < 320) {
            size = 70;
        } else if (density >= 320 && density < 420) {
            size = 110;
        } else if (density >= 420 && density < 640) {
            size = 140;
        } else if (density >= 640) {
            size = 160;
        }

        return size;
    }

    private int getLayoutSize() {
        int density = getResources().getDisplayMetrics().densityDpi;
        int size = 70;
        if (density < 100) {
            size = 60;
        } else if (density >= 100 && density < 201) {
            size = 70;
        } else if (density >= 201 && density < 320) {
            size = 90;
        } else if (density >= 320 && density < 420) {
            size = 130;
        } else if (density >= 420) {
            size = 160;
        } else if (density >= 640) {
            size = 180;
        }

        return size;
    }

    @Override
    public void onRestaurantInRadius() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        String userLat;
        String userLong;
        String userAddress;

        /*if (UserPreferenceUtil.getInstance(this).getAsSoonCheck()) {
            if (UserPreferenceUtil.getInstance(this).getCurrentLocationCheck()) {
                userLat = UserPreferenceUtil.getInstance(this).getCurrentLat();
                userLong = UserPreferenceUtil.getInstance(this).getCurrentLng();
                userAddress = UserPreferenceUtil.getInstance(this).getCurrentLocation();
            } else {
                userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
                userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
                userAddress = UserPreferenceUtil.getInstance(this).getLocation();

            }
        } else {
            if (UserPreferenceUtil.getInstance(this).getCurrentLocationCheck()) {
                userLat = UserPreferenceUtil.getInstance(this).getCurrentLat();
                userLong = UserPreferenceUtil.getInstance(this).getCurrentLng();
                userAddress = UserPreferenceUtil.getInstance(this).getCurrentLocation();
            } else {
                userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
                userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
                userAddress = UserPreferenceUtil.getInstance(this).getLocation();
            }
        }*/

        userLat = UserPreferenceUtil.getInstance(this).getLocationLat();
        userLong = UserPreferenceUtil.getInstance(this).getLocationLng();
        userAddress = UserPreferenceUtil.getInstance(this).getLocation();

        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        Cart cart = null;
        User user;
        try {
            if (mIsDishEdit) {
                cart = mCart;
            } else {
                if (mDish instanceof Dish) {
                    cart = dataBaseHelper.getCartIntegerDao().queryBuilder().where().eq("dishId", ((Dish) mDish).getDishId()).queryForFirst();
                } else {
                    cart = dataBaseHelper.getCartIntegerDao().queryBuilder().where().eq("itemGroceryID", ((Grocery) mDish).getId()).queryForFirst();
                }
            }

            if (cart == null) {
                cart = new Cart();
            } else {
                mIsDishEdit = true;
            }
            cart.setUserLat(userLat);
            cart.setUserLng(userLong);
            cart.setUserAddress(userAddress);

            if (mDish instanceof Dish) {
                cart.setDishId(((Dish) mDish).getDishId());
                cart.setDishName(((Dish) mDish).getDishName());
                cart.setResturantId(((Dish) mDish).getResturantId());
                cart.setCurrenyCode(((Dish) mDish).getCurrency());
                cart.setItem_count(String.valueOf(quantity));
                String itemPrice = ((Dish) mDish).getPrice();
                grandTotal = quantity * Double.parseDouble(itemPrice);
                cart.setRestaurantName(((Dish) mDish).getResturantName());
                cart.setCashOnly(((Dish) mDish).getCashOnly());
                cart.setMinOrderAmount(((Dish) mDish).getMinOrderAmount());

            } else if (mDish instanceof Grocery) {
                cart.setItemGroceryID(((Grocery) mDish).getId());
                cart.setDishName(((Grocery) mDish).getName());
                cart.setSupplierID(((Grocery) mDish).getSupplier_id());
                cart.setCurrenyCode(((Grocery) mDish).getCurrency());
                cart.setItemOrderCount(String.valueOf(quantity));
                String itemPrice = ((Grocery) mDish).getPrice();
                grandTotal = quantity * Double.parseDouble(itemPrice);
                cart.setSupplierName(((Grocery) mDish).getSupplier_name());
                cart.setCashOnly(((Grocery) mDish).getCashOnly());
                cart.setMinOrderAmount(((Grocery) mDish).getMinOrderAmount());
            }

            cart.setStatusCode("1");
            //cart.setNote("" + editTextNote.getText().toString());
            cart.setSpecialNote(editTextNote.getText().toString());

            cart.setPromoCode("0");
            cart.setCardNumber("0");
            //cart.setTime(mDish.getDeliveryTime());

            Iterator it = adsHashMap.entrySet().iterator();
            String adOnIds = "";
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                List<Option> optionList = (List<Option>) pair.getValue();
                if (optionList.size() > 0) {
                    Log.v(ConstantUtil.TAG, " pair " + optionList.get(0).getName());
                    adOnIds += optionList.get(0).getAddOnId() + ",";
                }
                it.remove(); // avoids a ConcurrentModificationException
            }
            if (adOnIds.length() > 0) {
                adOnIds = adOnIds.substring(0, adOnIds.length() - 1);
            }
            cart.setDishPrice("" + grandTotal);
            cart.setAddOnId(adOnIds);

            user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();
            if (user != null) {
                cart.setUserId(user.getId());
                cart.setReceiver_name(user.getFirstName() + " " + user.getLastName());
                cart.setUserNumber(user.getNumber());
            }
            if (!mIsOrderEdit) {
                dataBaseHelper.saveCartItem(cart, mIsDishEdit);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            if (!mIsOrderEdit) {
                try {
                    dataBaseHelper.saveCartItem(cart, mIsDishEdit);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            }
        }
        if (mIsDishEdit) {
            Intent cartIntent = new Intent(DishDetailActivity.this, CartActivity.class);
            startActivity(cartIntent);
            finish();
        } else if (mIsOrderEdit) {
            editOrder(cart);

        } else {
            Intent homeIntent = new Intent(DishDetailActivity.this, HomeActivity.class);
            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
            finish();
        }
    }

    private void editOrder(Cart cart) {
        JsonElement element = null;
        Gson gson = new Gson();

        if (cart.getDishId() != null && !cart.getDishId().equals("0")) {
            EditOrderRequestObj editOrder = new EditOrderRequestObj();
            editOrder.setAddOnId(cart.getAddOnId());
            editOrder.setDishPrice(cart.getDishPrice());
            editOrder.setItem_count(cart.getItem_count());
            editOrder.setOrder_id(mEditOrder.getOrderId());

            List<EditOrderRequestObj> objLis = new ArrayList<>();
            objLis.add(editOrder);
            element = gson.toJsonTree(objLis, new TypeToken<List<EditOrderRequestObj>>() {
            }.getType());
        } else { //Grocery
            EditOrderGroceryRequestObj editOrder = new EditOrderGroceryRequestObj();
            editOrder.setAddOnId(cart.getAddOnId());
            editOrder.setDishPrice(cart.getDishPrice());
            editOrder.setOrder_id(mEditOrder.getOrderId());
            editOrder.setItemOrderCount(cart.getItemOrderCount());

            List<EditOrderGroceryRequestObj> objLis = new ArrayList<>();
            objLis.add(editOrder);
            element = gson.toJsonTree(objLis, new TypeToken<List<EditOrderGroceryRequestObj>>() {
            }.getType());
        }

        if (!element.isJsonArray()) {
            return;
        }
        JsonArray jsonArray = element.getAsJsonArray();
        String jsonString = jsonArray.toString();

        final EditOrderRequest editOrderRequest = ServiceGenrator.createService(EditOrderRequest.class);
        Call<PlaceOrderResponse> responseCall = editOrderRequest.editOrder(jsonString);
        mProgressDialog.show();
        responseCall.enqueue(new Callback<PlaceOrderResponse>() {
            @Override
            public void onResponse(Call<PlaceOrderResponse> call, Response<PlaceOrderResponse> response) {
                Log.v(ConstantUtil.TAG, "placeOrder -> " + response.raw());
                mProgressDialog.hide();
                PlaceOrderResponse placeOrderResponse = response.body();
                String code = placeOrderResponse.response;

                if (code.equalsIgnoreCase("0")) {
                    Intent intent = new Intent(DishDetailActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    intent.putExtra("CurrentTab", 3);
                    startActivity(intent);
                    finish();

                    //overridePendingTransition(android.R.anim.fade_in, R.anim.slide_down);
                    Toast.makeText(DishDetailActivity.this, "Your Order has been updated", Toast.LENGTH_SHORT).show();
                } else if (code.equalsIgnoreCase("1")) {
                    Toast.makeText(DishDetailActivity.this, "Some error while updating order", Toast.LENGTH_SHORT).show();
                } else if (code.equalsIgnoreCase("2")) {
                    Toast.makeText(DishDetailActivity.this, "Some error while updating order", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PlaceOrderResponse> call, Throwable t) {
                Log.v(ConstantUtil.TAG, "Update Order onFailer " + t.getMessage());
                mProgressDialog.hide();
            }
        });
    }

    @Override
    public void onRestaurantNotInRadius(String error) {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mViewModel != null) {
            mViewModel.onDestroy();
            mViewModel = null;
        }
    }

    @OnClick(R.id.diet_detail_sharing)
    public void onViewClicked() {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Check out this app https://play.google.com/store/apps/details?id=com.o2.plotos&hl=en ");
        startActivity(Intent.createChooser(sharingIntent,"Share using"));
    }
}
