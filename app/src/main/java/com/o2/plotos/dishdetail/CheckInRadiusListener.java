package com.o2.plotos.dishdetail;

/**
 * Created by Rania on 3/22/2017.
 */
public interface CheckInRadiusListener {
    void OnResponseSuccess();
    void onResponseFailure(String error);
}
