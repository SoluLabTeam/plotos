package com.o2.plotos.home.order;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.adapters.CartListingAdp;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.models.Order;
import com.o2.plotos.models.User;
import com.o2.plotos.places.DeliveryLocationActivity;
import com.o2.plotos.places.MapPinActivity;
import com.o2.plotos.places.PlaceAdapter;
import com.o2.plotos.places.PlaceAdapterDelivery;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.dishdetails.DishDetails;
import com.o2.plotos.restapi.newApis.models.groceryDetails.GroceryDetails;
import com.o2.plotos.restapi.newApis.models.youMayAlsoLike.YouMayAlsoLikeResponse;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.JSONArrayUtil;
import com.o2.plotos.utils.OpensanBoldTextview;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliverToActivity extends BaseActivity implements CartListingAdp.CartPlusAndMinusListner, SuggestionAdapter.onClickOfProductListner, PlaceAdapterDelivery.OnActionMenuClicked {

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.ivAsSoonAs)
    ImageView ivAsSoonAs;
    @BindView(R.id.ivCustomDelivery)
    ImageView ivCustomDelivery;
    @BindView(R.id.reclerview_suggest)
    RecyclerView reclerview_suggest;

    @BindView(R.id.btn_continue)
    Button btn_continue;

    @BindView(R.id.btnAddMore)
    Button btnAddMore;


    @BindView(R.id.linear_time)
    LinearLayout linear_time;

    @BindView(R.id.linear_location)
    LinearLayout linear_location;

    @BindView(R.id.lltime)
    LinearLayout lltime;

    @BindView(R.id.llAsSoonAs)
    LinearLayout llAsSoonAs;

    @BindView(R.id.lllocation)
    LinearLayout lllocation;

    @BindView(R.id.llSchedulDelivery)
    LinearLayout llSchedulDelivery;

    @BindView(R.id.btn_del_time)
    Button btn_del_time;

    @BindView(R.id.tvTimeToggle)
    TextView tvTimeToggle;
    @BindView(R.id.tvLocationToggle)
    TextView tvLocationToggle;

    @BindView(R.id.tvViewMap)
    TextView tvViewMap;
    @BindView(R.id.txt_title)
    OpensanBoldTextview txtTitle;
    @BindView(R.id.rv_cartList)
    RecyclerView rvCartList;

    @BindView(R.id.tvDelTime)
    TextView tvDelTime;

    @BindView(R.id.tvDeliveryTime)
    TextView tvDeliveryTime;

    @BindView(R.id.tvMins)
    TextView tvMins;

    @BindView(R.id.tvDeliveryLocation)
    TextView tvDeliveryLocation;

    @BindView(R.id.delivery_location_listView)
    ListView listView;

    SuggestionAdapter suggestionAdapter;

    private List<ItemDetais> listOfItems;
    private List<ItemDetais> listSimilarProduct;
    private CartListingAdp cartListingAdp;

    int mYear;
    int mMonth;
    int mDay;
    private JSONArray mAddresses;
    String strDeliveryTime = "asap";
    private SharedPreferences pref;
    private static int PLACE_PICKER_REQUEST = 1;
    PlaceAdapterDelivery mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliver_to);

        ButterKnife.bind(this);
        bindAdp();
        tvDeliveryLocation.setText(UserPreferenceUtil.getInstance(this).getLocation());

    }

    @Override
    protected void onResume() {
        super.onResume();
        getListingFromPref();
    }

    private void orderLocation() {
        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String name = "";
        String number = "";
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        User user;
        try {
            user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();
            if (user != null) {
                name = user.getFirstName() + " " + user.getLastName();
                number = user.getNumber();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        mAddresses = new JSONArray();
        JSONObject address = new JSONObject();

        try {
            address.put("location", "Deliver to my current location");
            address.put("name", name);
            address.put("number", number);
            address.put("address", UserPreferenceUtil.getInstance(this).getCurrentLocation());
            address.put("lat", UserPreferenceUtil.getInstance(this).getCurrentLat());
            address.put("lng", UserPreferenceUtil.getInstance(this).getCurrentLng());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String json = pref.getString("addresses", "");

        if (!json.isEmpty()) {
            try {
                mAddresses = new JSONArray(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            mAddresses.put(0, address);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mAdapter = new PlaceAdapterDelivery(DeliverToActivity.this, mAddresses, UserPreferenceUtil.getInstance(DeliverToActivity.this).getLocation(), this);

        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int i, long l) {

                JSONObject address = mAdapter.getItem(i);

                try {
                    UserPreferenceUtil.getInstance(DeliverToActivity.this).setNumber(address.getString("number"));
                    UserPreferenceUtil.getInstance(DeliverToActivity.this).setLocation(address.getString("address"));
                    UserPreferenceUtil.getInstance(DeliverToActivity.this).setLocationLat(String.valueOf(address.getDouble("lat")));
                    UserPreferenceUtil.getInstance(DeliverToActivity.this).setLocationLng(String.valueOf(address.getDouble("lng")));
                    lllocation.setVisibility(View.GONE);
                    tvLocationToggle.setRotation(90);
                    tvDeliveryLocation.setText(UserPreferenceUtil.getInstance(DeliverToActivity.this).getLocation());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void bindAdp() {
        listOfItems = new ArrayList<>();
        rvCartList.setNestedScrollingEnabled(false);
        cartListingAdp = new CartListingAdp(listOfItems, this, this);
        rvCartList.setLayoutManager(new LinearLayoutManager(this));
        rvCartList.setAdapter(cartListingAdp);

        listSimilarProduct = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        reclerview_suggest.setLayoutManager(linearLayoutManager);
        suggestionAdapter = new SuggestionAdapter(this, listSimilarProduct, this);
        reclerview_suggest.setAdapter(suggestionAdapter);
    }

    private void getListingFromPref() {
        listOfItems.clear();
        listOfItems.addAll(UserPreferenceUtil.getInstance(DeliverToActivity.this).getItemList(Constant.PREFITEMLIST));
        cartListingAdp.notifyDataSetChanged();
        getParamsFromCArtItem();
    }

    private void getParamsFromCArtItem() {
        StringBuilder rest_ids = new StringBuilder();
        StringBuilder supp_ids = new StringBuilder();
        StringBuilder dish_ids = new StringBuilder();
        StringBuilder item_ids = new StringBuilder();

        for (int i = 0; i < listOfItems.size(); i++) {
            ItemDetais itemDetais = listOfItems.get(i);
            if (itemDetais.isGrocery()) {
                supp_ids.append(itemDetais.getProvider_id() + ",");
                item_ids.append(itemDetais.getItem_id() + ",");
            } else {
                rest_ids.append(itemDetais.getProvider_id() + ",");
                dish_ids.append(itemDetais.getItem_id() + ",");
            }
        }

        Map<String, String> map = new HashMap<>();
        if (rest_ids.toString().length() > 0) {
            rest_ids.setLength(rest_ids.length() - 1);
            dish_ids.setLength(dish_ids.length() - 1);
            map.put("restaurant_id", rest_ids.toString());
            map.put("dish_id", dish_ids.toString());
        }
        if (supp_ids.toString().length() > 0) {
            supp_ids.setLength(supp_ids.length() - 1);
            item_ids.setLength(item_ids.length() - 1);
            map.put("supplier_id", supp_ids.toString());
            map.put("item_id", item_ids.toString());
        }
        callApiForSimilarProduct(map);
    }

    @OnClick({R.id.img_back, R.id.btn_continue, R.id.linear_location, R.id.linear_time, R.id.btn_del_time, R.id.btnAddMore, R.id.llSchedulDelivery, R.id.llAsSoonAs, R.id.tvViewMap})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_continue:
                UserPreferenceUtil.getInstance(this).setDeliveryTime(tvDeliveryTime.getText().toString().trim());
                startActivity(new Intent(this, ConfirmOrderActivity.class));
                break;
            case R.id.linear_location:
                orderLocation();
                if (lllocation.getVisibility() == View.VISIBLE) {
                    lllocation.setVisibility(View.GONE);
                    tvLocationToggle.setRotation(90);
                } else {
                    lllocation.setVisibility(View.VISIBLE);
                    lltime.setVisibility(View.GONE);
                    tvLocationToggle.setRotation(-90);
                    tvTimeToggle.setRotation(90);
                }
                break;
            case R.id.linear_time:
                if (tvDeliveryTime.getText().toString().trim().equalsIgnoreCase("asap")) {
                    ivAsSoonAs.setImageResource(R.drawable.check);
                    ivCustomDelivery.setImageResource(R.drawable.uncheck);
                } else {
                    ivCustomDelivery.setImageResource(R.drawable.check);
                    ivAsSoonAs.setImageResource(R.drawable.uncheck);
                }
                if (lltime.getVisibility() == View.VISIBLE) {
                    lltime.setVisibility(View.GONE);
                    tvTimeToggle.setRotation(90);
                    tvLocationToggle.setRotation(90);
                } else {
                    lltime.setVisibility(View.VISIBLE);
                    lllocation.setVisibility(View.GONE);
                    tvTimeToggle.setRotation(-90);
                }

                break;
            case R.id.btn_del_time:
                lltime.setVisibility(View.GONE);
                tvTimeToggle.setRotation(90);
                if (strDeliveryTime.equalsIgnoreCase("asap")) {
                    tvDeliveryTime.setText("ASAP          ");
                    tvMins.setVisibility(View.VISIBLE);
                } else {
                    tvDeliveryTime.setText(tvDelTime.getText().toString());
                    tvMins.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.btnAddMore:
                if (listOfItems.get(listOfItems.size() - 1).isGrocery()) {
                    Utils.navigateToSuppAct(listOfItems.get(listOfItems.size() - 1).getProvider_id(), this);
                } else {
                    Utils.navigateToResturantDetailsAct(listOfItems.get(listOfItems.size() - 1).getProvider_id(), this);
                }
                break;
            case R.id.llSchedulDelivery:
                datePicker();
                break;
            case R.id.llAsSoonAs:
                ivAsSoonAs.setImageResource(R.drawable.check);
                ivCustomDelivery.setImageResource(R.drawable.uncheck);
                strDeliveryTime = "asap";
                break;
            case R.id.tvViewMap:
                Intent intent = new Intent(DeliverToActivity.this, MapPinActivity.class);
                startActivityForResult(intent, PLACE_PICKER_REQUEST);
                break;
        }
    }

    private void datePicker() {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

//                        date_time = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        //*************Call Time Picker Here ********************
                        tiemPicker(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private void tiemPicker(final String date_time) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        ivCustomDelivery.setImageResource(R.drawable.check);
                        ivAsSoonAs.setImageResource(R.drawable.uncheck);
                        strDeliveryTime = "custom";
                        tvDelTime.setText(date_time + " " + String.format("%02d:%02d", hourOfDay, minute));
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @Override
    public void onClickOfPlus(int pos) {
        refreshDataAdd(pos, true);

    }

    @Override
    public void onClickOfMinus(int pos) {
        refreshDataMInus(pos, false);

    }

    private void refreshDataAdd(int pos, boolean needToadd) {
        ItemDetais itemDetais = listOfItems.get(pos);
        if (needToadd) {
            itemDetais.setQuantity(itemDetais.getQuantity() + 1);
        } else {
            itemDetais.setQuantity(itemDetais.getQuantity() - 1);
        }
        listOfItems.set(pos, itemDetais);
        cartListingAdp.notifyItemChanged(pos);

        Utils.storeDatainToCart(itemDetais, this, itemDetais.getQuantity(), Double.valueOf(itemDetais.getPrice()));


    }

    private void refreshDataMInus(int pos, boolean needToadd) {
        ItemDetais itemDetais = listOfItems.get(pos);
        if (needToadd) {
            itemDetais.setQuantity(itemDetais.getQuantity() + 1);
        } else {
            itemDetais.setQuantity(itemDetais.getQuantity() - 1);
        }
        listOfItems.set(pos, itemDetais);
        cartListingAdp.notifyItemChanged(pos);
        if (itemDetais.getQuantity() > 0) {
            Utils.storeDatainToCart(itemDetais, this, itemDetais.getQuantity(), Double.valueOf(itemDetais.getPrice()));
        }
    }


    private void callApiForSimilarProduct(Map<String, String> map) {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<YouMayAlsoLikeResponse> call = apiInterface.getSimilarProducts(map);
        call.enqueue(new Callback<YouMayAlsoLikeResponse>() {
            @Override
            public void onResponse(Call<YouMayAlsoLikeResponse> call, Response<YouMayAlsoLikeResponse> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    listSimilarProduct.clear();
                    listSimilarProduct.addAll(response.body().getListItems());
                    suggestionAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<YouMayAlsoLikeResponse> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }

    @Override
    public void onClickOfProduct(int pos) {
        Utils.navigateToDishOrGroceryDetailsAct(listSimilarProduct.get(pos).getItem_id(), this, Constant.CAT_DISH);
    }

    @Override
    public void onClickOfAddToCart(int pos) {
        Utils.storeDatainToCart(listSimilarProduct.get(pos), this, 1, Double.valueOf(listSimilarProduct.get(pos).getPrice()));
        Intent intent=new Intent(DeliverToActivity.this,DeliverToActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onEdit(int position) {

    }

    @Override
    public void onDelete(final int position) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Are you sure you want to delete this location?");
        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (mAddresses != null) {

                    // check if it's selected address
                    try {
                        if (mAddresses.getJSONObject(position).getString("address").equals(UserPreferenceUtil.getInstance(getApplicationContext()).getLocation())) {
                            listView.getChildAt(0).performClick();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //mAddresses.remove(position);
                    mAddresses = JSONArrayUtil.remove(mAddresses, position);

                    mAdapter.updateContent(mAddresses);

                    // save to user pref
                    SharedPreferences.Editor prefsEditor = pref.edit();
                    prefsEditor.putString("addresses", mAddresses.toString());
                    prefsEditor.commit();

                    final UserPreferenceUtil userPref = UserPreferenceUtil.getInstance(DeliverToActivity.this);

                    userPref.setLocation(UserPreferenceUtil.getInstance(DeliverToActivity.this).getCurrentLocation());


                }

                dialog.dismiss();

            }
        });
        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {

                JSONObject address = new JSONObject();

                try {
                    address.put("location", data.getStringExtra("location"));
                    address.put("number", data.getStringExtra("number"));
                    address.put("address", data.getStringExtra("address"));
                    address.put("lat", data.getDoubleExtra("lat", 0));
                    address.put("lng", data.getDoubleExtra("lng", 0));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (mAddresses == null) {
                    mAddresses = new JSONArray();
                }

                mAddresses.put(address);

                mAdapter.updateContent(mAddresses);


                final UserPreferenceUtil userPref = UserPreferenceUtil.getInstance(DeliverToActivity.this);

                try {
                    userPref.setNumber(address.getString("number"));
                    userPref.setLocation(address.getString("address"));
                    userPref.setLocationLat(String.valueOf(address.getDouble("lat")));
                    userPref.setLocationLng(String.valueOf(address.getDouble("lng")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // save to user pref
                SharedPreferences.Editor prefsEditor = pref.edit();
                prefsEditor.putString("addresses", mAddresses.toString());
                prefsEditor.commit();


              /*  Set<String> stationCodes = new HashSet<String>();
                JSONArray tempArray = new JSONArray();
                for (int i = 0; i < mAddresses.length(); i++) {
                    String stationCode = null;
                    try {
                        stationCode = mAddresses.getJSONObject(i).getString("address");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (stationCodes.contains(stationCode)) {
                        continue;
                    } else {
                        stationCodes.add(stationCode);
                        try {
                            tempArray.put(mAddresses.getJSONObject(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }


                mAddresses = tempArray; //assign temp to original*/


//                Intent i = new Intent(this, DeliveryLocationActivity.class);
//                startActivity(i);
//                finish();


                //imgCheckLocation.setVisibility(View.INVISIBLE);

            }
        }
    }
}
