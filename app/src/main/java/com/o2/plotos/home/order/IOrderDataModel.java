package com.o2.plotos.home.order;

import com.o2.plotos.models.Order;

import java.util.List;

/**
 * Created by Hassan on 22/02/2017.
 */

public interface IOrderDataModel {
    void getOrderRqust(String userId);
    interface OnGetorderRqustFinishLisener{
        void OnGetOrderSuccess(List<Order> orderList);
        void OnGetOrderFailed(String error);
    }
}
