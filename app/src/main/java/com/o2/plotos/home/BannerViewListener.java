package com.o2.plotos.home;

import com.o2.plotos.models.Banner;

import java.util.List;

/**
 * Created by Rania on 5/20/2017.
 */

public interface BannerViewListener {
    void onGetBannerSuccessfully(List<Banner> bannerList);
}
