package com.o2.plotos.home;

import com.o2.plotos.models.Dish;
import com.o2.plotos.models.Grocery;
import com.o2.plotos.models.SubCategory;

import java.util.List;

/**
 * Created by Rania on 5/20/2017.
 */

public interface OfferChildCategoriesListener {
    void onGetOfferChildCategoriesSuccessfully(List<SubCategory> offerChildCategories);
    void onGetOfferChildCategoriesFailure(String message);
    void onGetOfferDishesSuccess(List<Dish> dishes, String catID);
    void onGetOfferDishesFailure(String message, String catID);
    void onGetOfferGrocerySuccess(List<Grocery> groceries, String catID);
    void onGetOfferGroceryFailure(String message, String catID);
}
