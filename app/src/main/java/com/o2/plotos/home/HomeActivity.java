package com.o2.plotos.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.o2.plotos.LocationBaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.StartupActivity;
import com.o2.plotos.authentication.signin.SignInActivity;
import com.o2.plotos.authentication.signin.SignInActivityNew;
import com.o2.plotos.cart.CartActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.home.homeTab.HomeFragment;
import com.o2.plotos.home.menu.Disclaimer2Activity;
import com.o2.plotos.home.order.DeliverToActivity;
import com.o2.plotos.home.order.OrderFragment;
import com.o2.plotos.home.order.Orderdetail;
import com.o2.plotos.home.order.UpdateOrderListListener;
import com.o2.plotos.home.resturants.GroceryFragment;
import com.o2.plotos.home.resturants.ResturantFragment;
import com.o2.plotos.home.search.SearchActivity;
import com.o2.plotos.home.search.SearchFragment;
import com.o2.plotos.home.menu.MenuFragment;
import com.o2.plotos.models.ConfirmOrder;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import it.sephiroth.android.library.tooltip.Tooltip;

public class HomeActivity extends LocationBaseActivity implements Orderdetail.OnReOrderListener, UpdateOrderListListener {


    /**
     * Selected Tab icons list
     */
    int[] iconsSelected = {R.drawable.grocery_selected, R.drawable.search_selected, R.drawable.home_selected,
            R.drawable.myorder_selected, R.drawable.menu_selected};
    /**
     * Unselected tab icons list
     */
    int[] iconsUnSelected = {R.drawable.grocery, R.drawable.search,
            R.drawable.home, R.drawable.myorder, R.drawable.menu};


    int[] TextTabDefualt = {Color.parseColor("#808080"), Color.parseColor("#808080"),
            Color.parseColor("#808080"), Color.parseColor("#808080"), Color.parseColor("#808080")};

    /*  int[] TextTabDefualt = {R.color.colorPrimaryDark, R.color.colorPrimaryDark,
              R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark};
     */
    int[] TextTabColor = {Color.parseColor("#4AB173"), Color.parseColor("#4AB173"),
            Color.parseColor("#4AB173"), Color.parseColor("#4AB173"), Color.parseColor("#4AB173")};


    /**
     * Tabs Title Name
     */
    String[] tabsTitle = {"Grocery", "Search",
            "Home", "Orders", "Menu"};
    @BindView(R.id.activity_home_bottom_tab)
    public TabLayout tabLayout;
    @BindView(R.id.activity_home_viewpager)
    public ViewPager viewPager;
    @BindView(R.id.activity_home_btn_cart)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.activity_home_txt_item)
    TextView txtCartItem;
    @BindView(R.id.activity_home_bottom_layout)
    public LinearLayout bottomLayout;

    @BindView(R.id.cartLayout)
    RelativeLayout cartLayout;
    @BindView(R.id.tvItemCount)
    TextView tvItemCount;
    @BindView(R.id.tvTotal)
    TextView tvTotal;
    Geocoder geocoder;
    List<Address> addresses;
    int currentTab = 2;
    DataBaseHelper dataBaseHelper = new DataBaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        /*dataBaseHelper = new DataBaseHelper(this);
        int usersItemSize = dataBaseHelper.getUsersItems().size();
        if (usersItemSize == 0) {
            UserPreferenceUtil.getInstance(this).setUserId("0");
        }*/
//        String userID = UserPreferenceUtil.getInstance(this).UserId();
//        We're logged in, we can register the user with Intercom */
//        if (!userID.equals("0")) {
//            Registration registration = Registration.create().withUserId(userID);
//            Intercom.client().registerIdentifiedUser(registration);
//            Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
//        } else {
//            Intercom.client().registerUnidentifiedUser();
//            Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
//        }

        initializeView();

        cartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, DeliverToActivity.class));
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        setCartFab();
    }

    /**
     * Show cart item from database
     */
    private void setCartFab() {
        int cartItemSize = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST).size();
        if (cartItemSize > 0) {
            cartLayout.setVisibility(View.VISIBLE);
            tvItemCount.setText("" + cartItemSize);
            makeList();
        } else {
            txtCartItem.setVisibility(View.GONE);
            floatingActionButton.hide();
            cartLayout.setVisibility(View.GONE);
        }
    }

    private void makeList() {
        Double tootal = 0.00;
        List<ItemDetais> listOfCartItems = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST);
        for (int i = 0; i < listOfCartItems.size(); i++) {
            ItemDetais itemDetais = listOfCartItems.get(i);
            tootal = tootal + (itemDetais.getQuantity() * itemDetais.getBasicPrice());
        }
        tvTotal.setText(tootal + " AED");
    }


    private void initializeView() {
        ButterKnife.bind(this);
        setupViewPager(viewPager);


        tabLayout.setupWithViewPager(viewPager);
        currentTab = getIntent().getIntExtra("CurrentTab", 2);
        setupTabIcons();
        tabLayout.setSelectedTabIndicatorHeight(0);


        tabLayout.getTabAt(currentTab);
        viewPager.setCurrentItem(currentTab);


        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(getBaseContext(),"test",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(HomeActivity.this, DeliverToActivity.class));
            }
        });

//        BalloonPopup bp = BalloonPopup.Builder(getApplicationContext(), tabLayout)
//                .text("Hey! checkout our new &" +
//                        "\n fresh grocery section")
//                .shape(BalloonPopup.BalloonShape.rounded_square)
//                .bgColor(Color.parseColor("#4AB173"))
//                .fgColor(Color.WHITE)
////                .fgColor(Color.parseColor("#4AB173"))
//                .timeToLive(60000)
//                .textSize(14)
////                .drawable(R.drawable.ballon)
//                .gravity(BalloonPopup.BalloonGravity.center)
////                .offsetX(-200)
//                .offsetY(-100)
//
//                .show();


//        MyCustomToast myCustomToast = new MyCustomToast(HomeActivity.this);
//        myCustomToast.setCustomMessageText("Hey! checkout our new & \n" +
//                                            "fresh grocery section");
//        myCustomToast.setCustomMessageTextSize(18);
//        myCustomToast.setCustomMessageTextColor(Color.WHITE);
////        myCustomToast.setCustomMessageIcon(null, MyCustomToast.POSITION_LEFT);
////        myCustomToast.setCustomMessageIconColor(Color.WHITE);
//        myCustomToast.setCustomMessageBackgroundColor("#4AB173");
////        myCustomToast.setCustomMessageBackgroundDrawable(R.drawable.info_message_background);
//        myCustomToast.setCustomMessageDuration(60000);
//        myCustomToast.setGravity(Gravity.BOTTOM, -120, -100);
////        myCustomToast.setCustomMessageTypeface("cambriai.ttf");
//        myCustomToast.show();

//        Tooltip tooltip = new Tooltip.Builder(tabLayout)
//                .setText("Hey! checkout our new & \n" +
//                                            "fresh grocery section")
//                .setBackgroundColor(Color.parseColor("#4AB173"))
//                .show();

        final ViewGroup root = (ViewGroup) tabLayout.getChildAt(0);
        final View tab = root.getChildAt(1);

        if (currentTab == 0) {
            Tooltip.ClosePolicy mClosePolicy = Tooltip.ClosePolicy.TOUCH_ANYWHERE_CONSUME;
//
            Tooltip.make(this,
                    new Tooltip.Builder(101)
                            .anchor(tab, Tooltip.Gravity.TOP)
                            .closePolicy(mClosePolicy, 20000)
                            .activateDelay(800)
                            .showDelay(300)
                            .text("Hey! checkout our new & \n" +
                                    "fresh grocery section")
                            .maxWidth(500)
                            .withArrow(true)
                            .withOverlay(true)
//                        .typeface(mYourCustomFont)
                            .floatingAnimation(Tooltip.AnimationBuilder.SLOW)
                            .build()
            ).show();

        }

    }

    private void setupTabIcons() {
      /*  //int tabUnseletedColor = ContextCompat.getColor(HomeActivity.this, android.R.color.darker_gray);
        RelativeLayout tabOne = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.item_tabs, null);
        ImageView icon = (ImageView) tabOne.findViewById(R.id.item_tab_title);
        TextView item_text = (TextView)tabOne.findViewById(R.id.item_text);
        item_text.setText("Grocery");
        icon.setImageResource(R.drawable.grocery);
        //icon.setColorFilter(tabUnseletedColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        RelativeLayout tabTwo = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.item_tabs, null);
        ImageView icon2 = (ImageView) tabTwo.findViewById(R.id.item_tab_title);
        icon2.setImageResource(R.drawable.search);
        TextView item_text2 = (TextView)tabOne.findViewById(R.id.item_text);
        item_text2.setText("Search");
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        RelativeLayout tabThree = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.item_tabs, null);
        ImageView icon3 = (ImageView) tabThree.findViewById(R.id.item_tab_title);
        icon3.setImageResource(R.drawable.home);
        TextView item_text3 = (TextView)tabOne.findViewById(R.id.item_text);
        item_text3.setText("Home");
        tabLayout.getTabAt(2).setCustomView(tabThree);

        RelativeLayout tabFour = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.item_tabs, null);
        ImageView icon4 = (ImageView) tabFour.findViewById(R.id.item_tab_title);
        icon4.setImageResource(R.drawable.myorder);
        TextView item_text5 = (TextView)tabOne.findViewById(R.id.item_text);
        item_text5.setText("Oders");
        tabLayout.getTabAt(3).setCustomView(tabFour);

        RelativeLayout tabFive = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.item_tabs, null);
        ImageView icon5 = (ImageView) tabFive.findViewById(R.id.item_tab_title);
        icon5.setImageResource(R.drawable.menu);
        TextView item_text4 = (TextView)tabOne.findViewById(R.id.item_text);
        item_text4.setText("Menu");
        tabLayout.getTabAt(4).setCustomView(tabFive);*/

        for (int i = 0; i < tabsTitle.length; i++) {

            RelativeLayout tabTwo = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.item_tabs, null);
            ImageView icon2 = (ImageView) tabTwo.findViewById(R.id.item_tab_title);


            if (i == 2) {

                if (currentTab == 2) {
                    icon2.setImageResource(iconsSelected[2]);
                    TextView item_text6 = (TextView) tabTwo.findViewById(R.id.item_text);
                    item_text6.setText(tabsTitle[2]);
                    item_text6.setTextColor(TextTabColor[2]);
                } else {
                    icon2.setImageResource(iconsUnSelected[2]);
                    TextView item_text6 = (TextView) tabTwo.findViewById(R.id.item_text);
                    item_text6.setText(tabsTitle[2]);
                    item_text6.setTextColor(TextTabDefualt[2]);
                }

            } else {
                icon2.setImageResource(iconsUnSelected[i]);
                TextView item_text6 = (TextView) tabTwo.findViewById(R.id.item_text);
                item_text6.setText(tabsTitle[i]);
                item_text6.setTextColor(TextTabDefualt[i]);
            }


            tabLayout.getTabAt(i).setCustomView(tabTwo);

        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                RelativeLayout tabView = (RelativeLayout) tab.getCustomView();
                ImageView icon5 = (ImageView) tabView.findViewById(R.id.item_tab_title);
                //int tabIconColor = ContextCompat.getColor(HomeActivity.this, android.R.color.black);
                //icon5.setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

                if (tab.getPosition() == 1) {
                    startActivity(new Intent(HomeActivity.this, SearchActivity.class));

                }

                TextView item_text4 = (TextView) tabView.findViewById(R.id.item_text);
                item_text4.setText(tabsTitle[tab.getPosition()]);
                item_text4.setTextColor(TextTabColor[tab.getPosition()]);
                icon5.setImageResource(iconsSelected[tab.getPosition()]);

                if (UserPreferenceUtil.getInstance(HomeActivity.this).UserId().equalsIgnoreCase("0")) {
                    if (tab.getPosition() == 3 || tab.getPosition() == 4) {
                        startActivity(new Intent(HomeActivity.this, SignInActivityNew.class));
                        finish();
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                RelativeLayout tabView = (RelativeLayout) tab.getCustomView();
                ImageView icon5 = (ImageView) tabView.findViewById(R.id.item_tab_title);
                TextView item_text4 = (TextView) tabView.findViewById(R.id.item_text);

                item_text4.setText(tabsTitle[tab.getPosition()]);
                item_text4.setTextColor(TextTabDefualt[tab.getPosition()]);
                icon5.setImageResource(iconsUnSelected[tab.getPosition()]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                RelativeLayout tabView = (RelativeLayout) tab.getCustomView();
                ImageView icon5 = (ImageView) tabView.findViewById(R.id.item_tab_title);
                TextView item_text4 = (TextView) tabView.findViewById(R.id.item_text);
                item_text4.setText(tabsTitle[tab.getPosition()]);
                item_text4.setTextColor(TextTabColor[tab.getPosition()]);
                icon5.setImageResource(iconsSelected[tab.getPosition()]);
            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFrag(GroceryFragment.newInstance(), "Grocery");
        adapter.addFrag(SearchFragment.newInstance(), "Search");
        // adapter.addFrag(GroceryFragment.newInstance(), "Supplier");
        adapter.addFrag(HomeFragment.newInstance(), "Home");
        adapter.addFrag(OrderFragment.newInstance(), "Orders");
        adapter.addFrag(MenuFragment.newInstance(), "Menu");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(0);
    }

    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        Log.v(ConstantUtil.TAG, "OnLocationChanged Home Activity ");
        updateLocation();
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            String address = addresses.get(0).getAddressLine(0);
            UserPreferenceUtil.getInstance(this).setCurrentLocation(address);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // dialogLocation();
    }


    /**
     * Update decent fragments location messages
     */
    public void updateLocation() {
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        if (fragmentList.size() == 0)
            return;

        for (Fragment fragment : fragmentList) {
            if (fragment instanceof HomeFragment) {
                ((HomeFragment) fragment).updateLocation();
            } else if (fragment instanceof ResturantFragment) {
                ((ResturantFragment) fragment).updateLocation();
            } else if (fragment instanceof GroceryFragment) {
                ((GroceryFragment) fragment).updateLocation();
            }
        }
    }

    @Override
    public void onReOrder() {
        setCartFab();
    }

    @Override
    public void updateOrderList() {

        getOrderList();
    }

    public void getOrderList() {
        @SuppressLint("RestrictedApi") List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        if (fragmentList.size() == 0)
            return;

        for (Fragment fragment : fragmentList) {
            if (fragment instanceof OrderFragment) {
                ((OrderFragment) fragment).getOrderList();
            }
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            //    String s= tabsTitle[position];
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);

        }
    }

}