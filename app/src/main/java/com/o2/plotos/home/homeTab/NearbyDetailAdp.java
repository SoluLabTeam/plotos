package com.o2.plotos.home.homeTab;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.home.resturants.GroceryDishDetailAdp;
import com.o2.plotos.restapi.newApis.models.grocery.DishResponse;
import com.o2.plotos.restapi.newApis.models.home.NearbyResult;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NearbyDetailAdp extends RecyclerView.Adapter<NearbyDetailAdp.SupplierHolder> implements NearbyDishDetailAdp.ImageClickListner {


    private ArrayList<NearbyResult> list;
    private Context context;
    private ClickListnerOfNearby listner;


    @Override
    public void onClickOfImage(int pos, int parentPosition) {
        Utils.navigateToDishOrGroceryDetailsAct(list.get(parentPosition).getDishes().get(pos).getDish_id(), context, Constant.CAT_DISH);
    }


    public interface ClickListnerOfNearby {
        void onClickNearBy(int pos);
    }

    public NearbyDetailAdp(ArrayList<NearbyResult> list, Context context, ClickListnerOfNearby listner) {
        this.list = list;
        this.context = context;
        this.listner = listner;

    }

    @Override
    public SupplierHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grocery_detail, parent, false);
        return new SupplierHolder(view);
    }

    @Override
    public void onBindViewHolder(SupplierHolder holder, int position) {
        holder.tvTitle.setText(list.get(position).getRestaurant_name());
        holder.ivNext.setVisibility(View.VISIBLE);
        holder.iv_rest_image.setVisibility(View.VISIBLE);
        if (list.get(position).getImage() != null && !list.get(position).getImage().equals("")) {
            Picasso.with(context).load(list.get(position).getImage())
                    .error(R.drawable.placeholder)
                    .into(holder.iv_rest_image);
        }
        holder.rv_grocery_cat.setHasFixedSize(true);
        holder.rv_grocery_cat.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        NearbyDishDetailAdp mAdapter = new NearbyDishDetailAdp(list.get(position).getDishes(), context, this, position);
        holder.rv_grocery_cat.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SupplierHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_grocery_cat)
        RecyclerView rv_grocery_cat;
        @BindView(R.id.iv_next)
        ImageView ivNext;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.iv_rest_image)
        ImageView iv_rest_image;


        public SupplierHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick(R.id.iv_next)
        public void onViewClicked() {
            listner.onClickNearBy(getAdapterPosition());
        }
    }
}
