package com.o2.plotos.home.menu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 3/1/2018.
 */

public class NutritionExpertsActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.watchVideo)
    LinearLayout watchVideo;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.designation)
    TextView designation;
    @BindView(R.id.description)
    TextView description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutrition_experts);
        ButterKnife.bind(this);
        if (getIntent().getExtras().get("expert").equals("1")){
            image.setImageResource(R.drawable.expert11);
            name.setText("Nadine Tayara");
            designation.setText("Co-Founder, Licensed Dietitian");
            description.setText("Born and raised in the UAE, Nadine graduated from McGill University in Canada, with a Bachelor of Science in Nutritional Science.She moved back home to the UAE to build on her career in nutritional services, offering individual counseling and bespoke dietary plans. In her commitment to helping nourish her clients’ mind, body and soul, Nadine is also a certified mBIT and NLP Master Coach as well as an NLP, Time Line Therapy™ and Hypnotherapist Master Practitioner.");
        }else{
            image.setImageResource(R.drawable.expert22);
            name.setText("Maria Abi Hanna");
            designation.setText("Co-Founder, Licensed Dietitian");
            description.setText("After graduating from the American University of Beirut, with a Bachelor of Science in Nutrition and Dietetics, Maria moved to the UAE to pursue her career in nutrition. She has since worked in different areas from individual counselling to community work, as well as a full-time role in the foodservice industry. Maria is a certified Eating Disorder specialist, where she counsels and helps many individuals suffering from anorexia, bulimia and other related eating disorders.");
        }

    }

    @OnClick(R.id.img_back)
    public void onViewClickedBack() {
        finish();
    }

    @OnClick(R.id.watchVideo)
    public void onViewClicked() {
        Intent viewIntent1 =
                new Intent("android.intent.action.VIEW",
                        Uri.parse("https://youtu.be/wgkCRwd-WWk"));
        startActivity(viewIntent1);
    }
}
