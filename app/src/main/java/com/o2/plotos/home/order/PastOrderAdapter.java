package com.o2.plotos.home.order;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.home.homeTab.BakeryItemsActivity;
import com.o2.plotos.models.Order;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.orders.OrderModel;
import com.o2.plotos.restapi.newApis.models.orders.PastResult;
import com.o2.plotos.restapi.newApis.models.orders.RatingResponse;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hassan on 2/02/2017.
 */

public class PastOrderAdapter extends RecyclerView.Adapter<PastOrderAdapter.OrderViewHolder> {
    private List<PastResult> mOrderlist;
    private LayoutInflater mInflater;
    private reOrderLister lister;
    Context mContext;

    public interface reOrderLister {
        void onClickOfReOder(int pos);
    }

    public PastOrderAdapter(Context context, List<PastResult> orderList, reOrderLister lister) {
        mInflater = LayoutInflater.from(context);
        mOrderlist = orderList;
        mContext = context;
        this.lister = lister;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_past_order, parent, false);
        OrderViewHolder orderViewHolder = new OrderViewHolder(v);
        return orderViewHolder;
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, final int position) {

        holder.restName.setText(mOrderlist.get(position).getRestaurant_name());
        if (mOrderlist.get(position).getScheduled_date() != null && !mOrderlist.get(position).getScheduled_date().equals("")) {
            getDate(mOrderlist.get(position).getScheduled_date(), holder.tvDate, holder.tvMonth);
        }
        holder.rv_dishes.setHasFixedSize(true);
        holder.rv_dishes.setLayoutManager(new LinearLayoutManager(mContext));
        PastOrderDetailAdapter pastOrderDetailAdapter = new PastOrderDetailAdapter(mContext, mOrderlist.get(position).getChild());
        holder.rv_dishes.setAdapter(pastOrderDetailAdapter);
        holder.btnReorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lister.onClickOfReOder(position);
            }
        });
//
        holder.txt_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(mContext);
                dialog.setContentView(R.layout.dialog_rate_meal);
                final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
                TextView txt_rate = (TextView) dialog.findViewById(R.id.txt_rate);
                TextView txt_close = (TextView) dialog.findViewById(R.id.txt_close);
                txt_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                txt_rate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (mOrderlist.get(position).getRestaurant_id().equals("0")) {
                            rateRest(mOrderlist.get(position).getSupplier_id(), String.valueOf(ratingBar.getRating()));
                        } else {
                            rateRest(mOrderlist.get(position).getRestaurant_id(), String.valueOf(ratingBar.getRating()));
                        }

                    }
                });
                dialog.show();
            }
        });
//
        holder.btnFullMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOrderlist.get(position).getRestaurant_id().equals("0")) {
                    Utils.navigateToSuppAct(mOrderlist.get(position).getSupplier_id(), mContext);
                } else {
                    Utils.navigateToResturantDetailsAct(mOrderlist.get(position).getRestaurant_id(), mContext);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mOrderlist.size();
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_rate)
        TextView txt_rate;
        @BindView(R.id.restName)
        TextView restName;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvMonth)
        TextView tvMonth;
        @BindView(R.id.btn_reorder)
        Button btnReorder;
        @BindView(R.id.btn_full_menu)
        Button btnFullMenu;
        @BindView(R.id.rv_dishes)
        RecyclerView rv_dishes;


        public OrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void rateRest(String restid, String rating) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<RatingResponse> call = apiInterface.callRatingApi(UserPreferenceUtil.getInstance(mContext).UserId(), restid, rating);
        call.enqueue(new Callback<RatingResponse>() {
            @Override
            public void onResponse(Call<RatingResponse> call, Response<RatingResponse> response) {
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    Toast.makeText(mContext, response.body().getResult(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RatingResponse> call, Throwable t) {
            }
        });
    }

    private void getDate(String date, TextView tvdate, TextView tvmonth) {
        String oldFormat = "yyyy-MM-dd";
        String newFormat = "dd-MMM";
        String formatedDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat, Locale.US);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);
            SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat, Locale.US);
            formatedDate = timeFormat.format(myDate);
            tvdate.setText(formatedDate.substring(0, 2));
            tvmonth.setText(formatedDate.substring(formatedDate.length() - 3));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
    }
}
