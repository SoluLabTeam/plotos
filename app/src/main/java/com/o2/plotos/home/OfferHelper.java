package com.o2.plotos.home;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.OfferChildCategoryRequest;
import com.o2.plotos.restapi.endpoints.OfferDataRequest;
import com.o2.plotos.restapi.responses.DishesRequestResponse;
import com.o2.plotos.restapi.responses.GroceryResponse;
import com.o2.plotos.restapi.responses.OfferChildCategoryResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rania on 5/20/2017.
 */

public class OfferHelper {

    private OfferChildCategoriesListener mListener;

    public OfferHelper(OfferChildCategoriesListener offerChildCategoriesListener){
        mListener = offerChildCategoriesListener;
    }

    public void getOfferChildCategories(String offerID){
        OfferChildCategoryRequest request = ServiceGenrator.createService(OfferChildCategoryRequest.class);
        Call<OfferChildCategoryResponse> bannerCall = request.offerChildCategoryRequest(offerID);
        bannerCall.enqueue(new Callback<OfferChildCategoryResponse>() {
            @Override
            public void onResponse(Call<OfferChildCategoryResponse> call, Response<OfferChildCategoryResponse> response) {
                if(response.code()== 200) {
                    OfferChildCategoryResponse apiResponse = response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        mListener.onGetOfferChildCategoriesSuccessfully(apiResponse.offerChildCategories);
                    }else{
                        LogCat.LogDebug(ConstantUtil.TAG, response.message());
                        mListener.onGetOfferChildCategoriesFailure(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<OfferChildCategoryResponse> call, Throwable t) {
                LogCat.LogDebug(ConstantUtil.TAG,"Offer Child Category ap onFailure "+t.getMessage());
                mListener.onGetOfferChildCategoriesFailure("unexpected error");
            }
        });
    }

    public void getOfferDishes(final String catID, String latitude, String longitude){
        OfferDataRequest request = ServiceGenrator.createService(OfferDataRequest.class);
        Call<DishesRequestResponse> offerCall = request.offerDishesRequest(catID, latitude, longitude);
        offerCall.enqueue(new Callback<DishesRequestResponse>() {
            @Override
            public void onResponse(Call<DishesRequestResponse> call, Response<DishesRequestResponse> response) {
                if(response.code()== 200) {
                    DishesRequestResponse apiResponse = response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        mListener.onGetOfferDishesSuccess(apiResponse.dish, catID);
                    }else{
                        LogCat.LogDebug(ConstantUtil.TAG, response.message());
                        mListener.onGetOfferDishesFailure(response.message(), catID);
                    }
                }
            }

            @Override
            public void onFailure(Call<DishesRequestResponse> call, Throwable t) {
                LogCat.LogDebug(ConstantUtil.TAG,"Offer dish ap onFailure "+t.getMessage());
                mListener.onGetOfferDishesFailure("unexpected error", catID);
            }
        });
    }

    public void getOfferGrocery(final String catID, String latitude, String longitude){
        OfferDataRequest request = ServiceGenrator.createService(OfferDataRequest.class);
        Call<GroceryResponse> offerCall = request.offerGroceryRequest(catID, latitude, longitude);
        offerCall.enqueue(new Callback<GroceryResponse>() {
            @Override
            public void onResponse(Call<GroceryResponse> call, Response<GroceryResponse> response) {
                if(response.code()== 200) {
                    GroceryResponse apiResponse = response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        mListener.onGetOfferGrocerySuccess(apiResponse.groceryList, catID);
                    }else{
                        LogCat.LogDebug(ConstantUtil.TAG, response.message());
                        mListener.onGetOfferGroceryFailure(response.message(), catID);
                    }
                }
            }

            @Override
            public void onFailure(Call<GroceryResponse> call, Throwable t) {
                LogCat.LogDebug(ConstantUtil.TAG,"Offer Grocery ap onFailure "+t.getMessage());
                mListener.onGetOfferGroceryFailure("unexpected error", catID);
            }
        });
    }
}
