package com.o2.plotos.home.menu;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.home.order.OrderActionsListener;
import com.o2.plotos.home.order.OrderAdapter;
import com.o2.plotos.models.Option;
import com.o2.plotos.models.Order;
import com.o2.plotos.restapi.newApis.models.orders.PastResult;
import com.o2.plotos.utils.CustomFonts;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dell on 2/6/2018.
 */

public class FollowOrderAdapter extends RecyclerView.Adapter<FollowOrderAdapter.OrderViewHolder> {

    private List<PastResult> mOrderlist;
    private LayoutInflater mInflater;
    private Order mOrder;
    CustomFonts customFonts;
    Context mContext;

    public FollowOrderAdapter(Context context, List<PastResult> orderList) {
        mInflater = LayoutInflater.from(context);
        mOrderlist = orderList;
        //  mOrderListener = orderListener;
        mContext = context;
        customFonts = new CustomFonts(context);

    }

    @Override
    public FollowOrderAdapter.OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.row_menu_follow_order, parent, false);
        FollowOrderAdapter.OrderViewHolder orderViewHolder = new FollowOrderAdapter.OrderViewHolder(v);
        return orderViewHolder;
    }

    @Override
    public void onBindViewHolder(FollowOrderAdapter.OrderViewHolder holder, int position) {

        holder.textName.setText(mOrderlist.get(position).getRestaurant_name());
        if (mOrderlist.get(position).getScheduled_date()!=null && !mOrderlist.get(position).getScheduled_date().equalsIgnoreCase("")) {
            getDate(mOrderlist.get(position).getScheduled_date(),holder.textdateOrder);
        }
//        holder.textRemainingTime.setText(mOrderlist.get(position).get()+" MIN");

        String imgUrl = mOrderlist.get(position).getRestaurant_image();
        if (imgUrl != null && !imgUrl.equalsIgnoreCase("")) {
            Picasso.with(mContext).load(imgUrl)
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.imagePhoto);
        } else {
            holder.imagePhoto.setImageResource(R.drawable.placeholder);
        }

        holder.item_diet_dish_linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, HomeActivity.class);
                intent.putExtra("CurrentTab", 3);
                intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mOrderlist.size();
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_order_dish_name)
        TextView textName;
        @BindView(R.id.item_order_dish_dateoforder)
        TextView textdateOrder;
        @BindView(R.id.item_order_dish_timeRemaining)
        TextView textRemainingTime;
        @BindView(R.id.item_order_dish_image)
        ImageView imagePhoto;
        @BindView(R.id.item_diet_dish_linearLayout)
        LinearLayout item_diet_dish_linearLayout;


        public OrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnOrderListener {
        void onOrderClick(Order order);
    }

    private void getDate(String date, TextView tvdate) {
        String oldFormat = "yyyy-MM-dd";
        String newFormat = "MMM dd,yyyy";
        String formatedDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat, Locale.US);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);
            SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat, Locale.US);
            formatedDate = timeFormat.format(myDate);
            tvdate.setText(formatedDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
    }

}