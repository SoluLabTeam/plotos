package com.o2.plotos.home.order;

/**
 * Created by Rania on 4/3/2017.
 */
public interface UpdateOrderListListener {

    void updateOrderList();
}
