package com.o2.plotos.home.homeTab;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.models.home.NearbyResult;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeNearByAdp extends RecyclerView.Adapter<HomeNearByAdp.CatHolder> {



    private ArrayList<NearbyResult> list;
    private Context context;
    private ClickListnerOfNearby listner;

    public HomeNearByAdp(ArrayList<NearbyResult> list, Context context, ClickListnerOfNearby listner) {
        this.list = list;
        this.context = context;
        this.listner = listner;
    }


//    @Override
//    public void onClickOfImage(int pos, int parentPosition) {
//        //        Intent intent = new Intent(context, GroceryDetailActivity.class);
////        intent.putExtra("from","category");
////        intent.putExtra("rest_name",list.get(parentPosition).getName());
////        intent.putExtra("mainid",list.get(parentPosition).getMain_id());
////        context.startActivity(intent);
//    }


    public interface ClickListnerOfNearby {
        void onClickOfNearBy(int pos);
    }

    @Override
    public CatHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_nearby, parent, false);
        return new CatHolder(view);
    }

    @Override
    public void onBindViewHolder(CatHolder holder, int position) {

        holder.tvPrice.setText(list.get(position).getStart_price() + "-" + list.get(position).getEnd_price());
//        if (list.get(position).getClosing_time() != null && list.get(position).getOpening_time() != null) {
//            holder.tvTime.setText(Utils.getFormattedTime(list.get(position).getOpening_time(), "HH:mm:ss", "hh a")
//                    + "-" + Utils.getFormattedTime(list.get(position).getClosing_time(), "HH:mm:ss", "hh a"));
//        }
        holder.tvTime.setText(list.get(position).getRestaurant_name());
        if (list.get(position).getImage() != null && !list.get(position).getImage().equals("")) {
            Picasso.with(context).load(list.get(position).getImage())
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class CatHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.circularimg)
        CardView circularimg;

        @BindView(R.id.tvTime)
        TextView tvTime;

        @BindView(R.id.tvPrice)
        TextView tvPrice;

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.ll_restaurant)
        LinearLayout llRestaurant;

        public CatHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.ll_restaurant)
        public void onViewClicked() {
            listner.onClickOfNearBy(getAdapterPosition());
        }
    }
}
