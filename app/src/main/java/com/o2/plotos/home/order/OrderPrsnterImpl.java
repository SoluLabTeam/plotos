package com.o2.plotos.home.order;

import com.o2.plotos.models.Order;

import java.util.List;

/**
 * Created by Hassan on 22/02/2017.
 */

public class OrderPrsnterImpl implements IOrderPresenter, IOrderDataModel.OnGetorderRqustFinishLisener {

    IOrderView iOrderView;
    IOrderDataModel mOrderDataModel;

    public OrderPrsnterImpl() {
        mOrderDataModel = new OrderDataModelImpl(this);
    }


    @Override
    public void OnGetOrderSuccess(List<Order> orderList) {
        if (iOrderView != null) {
            iOrderView.hideProgress();
            iOrderView.onGetOrderSuccess(orderList);
        }
    }

    @Override
    public void OnGetOrderFailed(String error) {
        if (iOrderView != null) {
            iOrderView.hideProgress();
            iOrderView.onGetOrderFailed(error);
        }
    }

    @Override
    public void sendGetOrderRqust(String userId) {
        if (iOrderView != null) {
            iOrderView.showProgress();
            mOrderDataModel.getOrderRqust(userId);
        }
    }

    @Override
    public void bindView(Object view) {
        iOrderView = (IOrderView) view;
    }

    @Override
    public void unBindView() {
        iOrderView = null;
    }
}
