package com.o2.plotos.home.order;

import com.o2.plotos.models.Order;

import java.util.List;

/**
 * Created by Hassan on 21/02/2017.
 */

public interface IOrderView {
    void showProgress();
    void hideProgress();
    void onGetOrderSuccess(List<Order> orderList);
    void onGetOrderFailed(String error);
}
