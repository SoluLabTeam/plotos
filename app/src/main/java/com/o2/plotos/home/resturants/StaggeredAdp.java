package com.o2.plotos.home.resturants;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.models.StaggeredView;
import com.o2.plotos.restapi.newApis.models.grocery.Category_list;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StaggeredAdp extends RecyclerView.Adapter<StaggeredAdp.CatHolder> {


    public interface StaggerdItemClickListner {
        void onStaggerdItemClick(int pos);
    }

    private StaggerdItemClickListner listner;
    private List<Category_list> list = new ArrayList();
    Context context;
    private int screenWidth;

    public StaggeredAdp(List<Category_list> list,Context context, StaggerdItemClickListner listner) {
        this.list = list;
        this.context=context;
        this.listner = listner;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
    }

    @Override
    public CatHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_staggered, parent, false);
        return new CatHolder(view);
    }

    @Override
    public void onBindViewHolder(CatHolder holder, int position) {
        holder.textName.setText(list.get(position).getName());

        holder.imagePhoto.setMaxHeight(400);

        if ((position % 2) == 0) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(screenWidth/2, 500);
            holder.imagePhoto.setLayoutParams(layoutParams);
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(screenWidth/2, 300);
            holder.imagePhoto.setLayoutParams(layoutParams);
        }
        if (list.get(position).getImage_url() != null && !list.get(position).getImage_url().equals("")) {
            Picasso.with(context).load(list.get(position).getImage_url())
                    .error(R.drawable.placeholder)
                    .into(holder.imagePhoto);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CatHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text)
        TextView textName;
        @BindView(R.id.image)
        ImageView imagePhoto;
        @BindView(R.id.rl_staggered)
        RelativeLayout rlStaggered;

        @OnClick(R.id.rl_staggered)
        public void onViewClicked() {
            listner.onStaggerdItemClick(getAdapterPosition());
        }

        public CatHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
