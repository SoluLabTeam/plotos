package com.o2.plotos.home.resturants;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.RestaurantRequest;
import com.o2.plotos.restapi.responses.RestaurantRquestResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hassan on 31/01/2017.
 */

public class RestaurantDataModelImpl implements IRestaurantDataModel {

    OnGetRestaurantsRequestFinishLisener mListerner;

    public RestaurantDataModelImpl(OnGetRestaurantsRequestFinishLisener lisener) {
        mListerner = lisener;
    }

    @Override
    public void getRestaurantsRequest(String resturant_type, String latitude, String longitude) {
        RestaurantRequest restaurantRequest = ServiceGenrator.createService(RestaurantRequest.class);
        Call<RestaurantRquestResponse> restaurantRquestResponseCall =
                restaurantRequest.restaurantResquest(resturant_type, latitude, longitude);
        restaurantRquestResponseCall.enqueue(new Callback<RestaurantRquestResponse>() {
            @Override
            public void onResponse(Call<RestaurantRquestResponse> call, Response<RestaurantRquestResponse> response) {
                LogCat.LogDebug(ConstantUtil.TAG," -> Restaurant api response "+response.raw());
                if(response.code()== 200){
                    RestaurantRquestResponse apiResponse =  response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        mListerner.onGetRestaurantSuccess(apiResponse.restaurant);

                    }else if(responseCode.equalsIgnoreCase("1")){
                        mListerner.onGetRestaurantFailed("Restaurant request Failed(1)");
                    }else if(responseCode.equalsIgnoreCase("2")){
                        mListerner.onGetRestaurantFailed("Restaurant request Failed(2)");
                    }
                }
            }
            @Override
            public void onFailure(Call<RestaurantRquestResponse> call, Throwable t) {
                mListerner.onGetRestaurantFailed("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"resturant ap onFailure "+t.getMessage());
            }
        });
    }
}
