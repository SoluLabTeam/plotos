package com.o2.plotos.home.resturants;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.home.BannerViewListener;
import com.o2.plotos.home.BannerViewModel;
import com.o2.plotos.home.HeaderPagerAdapter;
import com.o2.plotos.models.Banner;
import com.o2.plotos.models.Place;
import com.o2.plotos.models.Restaurant;
import com.o2.plotos.places.DeliveryLocationActivity;
import com.o2.plotos.places.DeliveryTimeActivity;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.LogCat;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.viewpagerindicator.LinePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.facebook.FacebookSdk.getApplicationContext;


public class ResturantFragment extends Fragment implements IRestaurantView,
        SwipeRefreshLayout.OnRefreshListener,
        BannerViewListener {

    @BindView(R.id.fragment_restaurant_recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.fragment_restaurant_swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.fragment_restaurant_resturantofday)
    TextView resturntOfDay;
    @BindView(R.id.fragment_restaurant_emptyView)
    TextView emptyView;
    @BindView(R.id.fragment_restaurant_txt_location)
    TextView txtCurrentLocation;
    @BindView(R.id.fragment_restaurant_viewPager)
    ViewPager mViewPager;

    @BindView(R.id.no_location_layout)
    RelativeLayout mNoLocationLayout;
    @BindView(R.id.no_data_title)
    TextView mNoDataTile;
    @BindView(R.id.no_data_message)
    TextView mNoDataMessage;

    @BindView(R.id.fragment_restaurant_card_layout)
    CardView mRestaurantCard;

    CustomFonts customFonts;
    public String TYPE_RESTURANT = "get_restaurants";
    @BindView(R.id.linlay_delivery)
    LinearLayout linlayDelivery;
    @BindView(R.id.fragment_restaurant_listLayout)
    FrameLayout fragmentRestaurantListLayout;
    @BindView(R.id.no_data_image)
    ImageView noDataImage;
    @BindView(R.id.fragment_restaurant_coordinateLayout)
    CoordinatorLayout fragmentRestaurantCoordinateLayout;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.linlay_delivery_time)
    LinearLayout linlayDeliveryTime;
    @BindView(R.id.txt_time)
    TextView txtTime;

    private HeaderPagerAdapter mHeaderPagerAdapter;
    private Timer timer;
    int page = 0;


    private RestaurantAdapter mAdapter;
    private List<Place> mRestaurantsList;
    private IRestaurantPresenter mIRestaurantPresenter;

    BannerViewModel mBannerViewModel;

    public ResturantFragment() {
        // Required empty public constructor
    }

    public static ResturantFragment newInstance() {
        return new ResturantFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_resturant, container, false);
        mIRestaurantPresenter = new RestaurantPresenterImpl();
        mIRestaurantPresenter.bindView(this);
        ButterKnife.bind(this, rootView);
        customFonts = new CustomFonts(getActivity());
        customFonts.setOswaldBold(resturntOfDay);
        customFonts.setOpenSansBold(mNoDataTile);
        customFonts.setOpenSansRegulr(mNoDataMessage);


        getRestaurantList();

        mRestaurantsList = new ArrayList<>();
        swipeRefreshLayout.setOnRefreshListener(this);

        mBannerViewModel = new BannerViewModel(this);
        mBannerViewModel.getBanners();
        mHeaderPagerAdapter = new HeaderPagerAdapter(getActivity(), null);
        mViewPager.setAdapter(mHeaderPagerAdapter);

        LinePageIndicator indicator = (LinePageIndicator) rootView.findViewById(R.id.tutorial_pager_indicator);
        //  indicator.setSnap(true);
        indicator.setViewPager(mViewPager);

        indicator.setSelectedColor(Color.parseColor("#ffffff"));
        indicator.setGapWidth(10f);
        indicator.setLineWidth(40f);
        // indicator.setRadius(Utility.getPxFromDp(context, 5.83f));
        // indicator.setFillColor(Color.parseColor("#ffffff"));
        // indicator.setPageColor(Color.parseColor("#80ffffff"));
        indicator.setStrokeWidth(5);


        pageSwitcher(10);

//        if(isAdded()) {
//            BalloonPopup bp = BalloonPopup.Builder(getApplicationContext(), mRestaurantsListTitle)
//                    .text("We only show meals from restaurants that delivering to \n your selected location." +
//                            " You might get more options by \n" +
//                            " changing your location")
//                    .shape(BalloonPopup.BalloonShape.rounded_square)
//                    .bgColor(Color.parseColor("#4AB173"))
//                    .fgColor(Color.WHITE)
//                    .timeToLive(60000)
//                    .textSize(12)
//                    .gravity(BalloonPopup.BalloonGravity.center)
//                    .offsetX(100)
//                    .offsetY(770)
//                    .show();
//        }

//        DisplayMetrics metrics = getResources().getDisplayMetrics();
//        Tooltip.ClosePolicy mClosePolicy = Tooltip.ClosePolicy.TOUCH_ANYWHERE_CONSUME;
//        Tooltip.make(getActivity(),
//                new Tooltip.Builder(105)
//                        .anchor(mRestaurantsListTitle, Tooltip.Gravity.BOTTOM)
//                        .closePolicy(mClosePolicy, 20000)
////                        .activateDelay(800)
////                        .showDelay(300)
//                        .text("Hey! checkout our new & \n" +
//                                "fresh grocery section")
//                        .maxWidth(metrics.widthPixels -100)
//                        .withArrow(true)
//                        .withOverlay(true)
////                        .typeface(mYourCustomFont)
//                        .floatingAnimation(Tooltip.AnimationBuilder.SLOW)
//                        .build()
//        ).show();

        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
//            BalloonPopup restaurantBp = BalloonPopup.Builder(getApplicationContext(), mRestaurantsListTitle)
//                    .text("We only show meals from restaurants that delivering to \n your selected location." +
//                            " You might get more options by \n" +
//                            " changing your location")
//                    .shape(BalloonPopup.BalloonShape.rounded_square)
//                    .bgColor(Color.parseColor("#4AB173"))
//                    .fgColor(Color.WHITE)
//                    .timeToLive(60000)
//                    .textSize(12)
//                    .gravity(BalloonPopup.BalloonGravity.center)
//                    .offsetX(100)
//                    .offsetY(770)
//                    .show();
//            Tooltip.ClosePolicy mClosePolicy = Tooltip.ClosePolicy.TOUCH_ANYWHERE_CONSUME;
//            Tooltip.make(getActivity(),
//                    new Tooltip.Builder(105)
//                            .anchor(mRestaurantsListTitle, Tooltip.Gravity.BOTTOM)
//                            .closePolicy(mClosePolicy, 20000)
//                            .activateDelay(800)
//                            .showDelay(300)
//                            .text("Hey! checkout our new & \n" +
//                                    "fresh grocery section")
//                            .maxWidth(500)
//                            .withArrow(true)
//                            .withOverlay(true)
////                        .typeface(mYourCustomFont)
//                            .floatingAnimation(Tooltip.AnimationBuilder.SLOW)
//                            .build()
//            ).show();
        }
    }

    private void getRestaurantList() {

        if (InternetUtil.getInstance(getContext()).isNetWorkAvailable()) {
            LogCat.LogDebug(ConstantUtil.TAG, " -> TYPE_RESTURANT: " + TYPE_RESTURANT);

            String userLat = UserPreferenceUtil.getInstance(getActivity()).getLocationLat();
            String userLong = UserPreferenceUtil.getInstance(getActivity()).getLocationLng();
            if (!userLat.equals("0.0") && !userLong.equals("0.0")) {
                mIRestaurantPresenter.sendRestaurantsRequest(TYPE_RESTURANT, userLat, userLong);
            } else {
                Toast.makeText(getActivity(), getString(R.string.error_no_location), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();



        setUserPreferenceDate();

        setLocationPreference();

        getRestaurantList();
    }


    /**
     * Set the user preference
     */
    private void setUserPreferenceDate() {

      String  user_id=   UserPreferenceUtil.getInstance(getApplicationContext()).UserId();


      {
          if (UserPreferenceUtil.getInstance(getActivity()).getAsSoonCheck()) {
              txtTime.setText("ASAP");
          } else {
              txtTime.setText(UserPreferenceUtil.getInstance(getActivity()).getDeliveryDate());

          }
      }


    }

    /**
     * Update location from shared preference
     */
    private void setLocationPreference() {
        if (txtCurrentLocation != null) {
            txtCurrentLocation.setText("☛ " + UserPreferenceUtil.getInstance(getActivity()).getLocation());

           /* Spannable word = new SpannableString("Delivering to");

            word.setSpan(new ForegroundColorSpan(Color.BLACK), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            txtAddress.setText(word);
            Spannable wordTwo = new SpannableString(UserPreferenceUtil.getInstance(getActivity()).getLocation());

            wordTwo.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtAddress.append(wordTwo);*/

            String text = "<font color=#00000>Delivering to</font> <font color=#4AB173>" + UserPreferenceUtil.getInstance(getActivity()).getLocation() + "</font>";
            txtAddress.setText(Html.fromHtml(text));

            // txtAddress.setText("Delivering to " +UserPreferenceUtil.getInstance(getActivity()).getLocation());

        }
        /*if(UserPreferenceUtil.getInstance(getActivity()).getAsSoonCheck()){
            if(UserPreferenceUtil.getInstance(getActivity()).getCurrentLocationCheck()){
                txtCurrentLocation.setText("ASAP ☛ "+UserPreferenceUtil.getInstance(getActivity()).getCurrentLocation());
            } else {
                txtCurrentLocation.setText("ASAP ☛ "+UserPreferenceUtil.getInstance(getActivity()).getLocation());
            }
        } else {
            if(UserPreferenceUtil.getInstance(getActivity()).getCurrentLocationCheck()){
                txtCurrentLocation.setText(
                        UserPreferenceUtil.getInstance(getActivity()).getDeliveryDate()
                                +"☛ "+UserPreferenceUtil.getInstance(getActivity()).getCurrentLocation());
            }else{
                txtCurrentLocation.setText(
                        UserPreferenceUtil.getInstance(getActivity()).getDeliveryDate()
                                +"☛ "+UserPreferenceUtil.getInstance(getActivity()).getLocation());
            }
        }*/
    }

    @OnClick(R.id.fragment_restaurant_txt_location)
    public void onLocationClick() {
        startActivity(new Intent(getActivity(), DeliveryLocationActivity.class));
    }


    @Override
    public void onDetach() {
        super.onDetach();
        if (mIRestaurantPresenter != null) {
            mIRestaurantPresenter.unBindView();
        }
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onGetRestaurantSuccess(List<Restaurant> restaurantsList) {
        mRecyclerView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
        if (mRestaurantsList.size() > 0) {
            mRestaurantsList.clear();
        }
        if (restaurantsList.size() > 0) {
            mNoLocationLayout.setVisibility(View.GONE);
        } else {
            mNoLocationLayout.setVisibility(View.VISIBLE);
        }
        mRestaurantsList.addAll(restaurantsList);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new RestaurantAdapter(mRestaurantsList, getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onGetRestaurantFailed(String error) {
        mRecyclerView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        mNoLocationLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        getRestaurantList();
    }

    /**
     * Update Location
     */
    public void updateLocation() {
        setLocationPreference();
    }

    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay
        // in
        // milliseconds
    }

    @Override
    public void onGetBannerSuccessfully(List<Banner> bannerList) {
        if (mHeaderPagerAdapter != null) {
//            mHeaderPagerAdapter.setBanners(bannerList);
            mHeaderPagerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();



    }


    @OnClick({R.id.linlay_delivery, R.id.linlay_delivery_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linlay_delivery:
                Intent intent = new Intent(getActivity(), DeliveryLocationActivity.class);

                startActivity(intent);

                break;
            case R.id.linlay_delivery_time:

                Intent intent2 = new Intent(getActivity(), DeliveryTimeActivity.class);

                startActivity(intent2);

                //dialogForgot();
                break;
        }
    }


   /* public void dialogLocation()

    {
        final Dialog dialog2 = new Dialog(getContext());
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setCancelable(true);
        dialog2.setContentView(R.layout.dialog_changelocation);
        dialog2.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_top_bootom;
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView txt_changelocation = (TextView) dialog2.findViewById(R.id.txt_changelocation);
        TextView txt_cancel = (TextView) dialog2.findViewById(R.id.txt_cancel);
        EditText edit_location = (EditText)dialog2.findViewById(R.id.edit_location);

        edit_location.setText(txtAddress.getText().toString());


        txt_changelocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), DeliveryLocationActivity.class);
                startActivity(i);

                dialog2.dismiss();


            }
        });


        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog2.dismiss();
            }
        });

        dialog2.show();

    }*/

    // this is an inner class...
    class RemindTask extends TimerTask {

        @Override
        public void run() {

            try {
                // As the TimerTask run on a seprate thread from UI thread we have
                // to call runOnUiThread to do work on UI thread.
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {

                        if (page > 3) { // In my case the number of pages are 5
                            timer = new Timer();
                            page = 0;
                            mViewPager.setCurrentItem(0);
                            // Showing a toast for just testing purpose
                        } else {
                            mViewPager.setCurrentItem(page++);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


}