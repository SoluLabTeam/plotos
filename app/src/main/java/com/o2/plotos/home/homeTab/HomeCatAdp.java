package com.o2.plotos.home.homeTab;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.o2.plotos.R;
import com.o2.plotos.home.resturants.AddToCartActivity;
import com.o2.plotos.restapi.newApis.models.home.CategoryResult;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.OpensanBoldTextview;
import com.o2.plotos.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeCatAdp extends RecyclerView.Adapter implements CatDishDetailAdp.ImageClickListner,HomeNewPlotosAdp.ClickListnerOfNewOnPlotos,HomeNearByAdp.ClickListnerOfNearby {

    private ArrayList<CategoryResult> list;
    private Context context;
    private ClickListnerOfCat listner;

    public HomeCatAdp(ArrayList<CategoryResult> list, Context context, ClickListnerOfCat listner) {
        this.list = list;
        this.context = context;
        this.listner = listner;
    }

//

    @Override
    public int getItemViewType(int position) {

        switch (list.get(position).getTypeView()) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;

        }
        return 0;
    }

    @Override
    public void onClickOfDishOrGrocery(int pos, int parentPosition) {
        Utils.navigateToDishOrGroceryDetailsAct(list.get(parentPosition).getDishes().get(pos).getItem_id(), context, Constant.CAT_DISH);
    }

    @Override
    public void onClickOfNearBy(int pos) {
        Utils.navigateToResturantDetailsAct(list.get(1).getNearByResults().get(pos).getRestaurant_id(), context);
    }

    @Override
    public void onClickOfNewOnPlotos(int pos) {
        Utils.navigateToResturantDetailsAct(list.get(2).getNewRestResults().get(pos).getRestaurant_id(), context);
    }


    public interface ClickListnerOfCat {
        void onClickOfDishName(int pos);

        void onClickNearBy(int pos);

        void onClickNewPlotos(int pos);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cat_home, parent, false);
                return new CatHolder(view);
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nearby, parent, false);
                return new NearByViewHolder(view);
            case 2:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new_plotos, parent, false);
                return new NewPlotosViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (list.get(position).getTypeView()) {
            case 0:
                ((CatHolder) holder).tvTitle.setText(list.get(position).getName());
                ((CatHolder) holder).rvImages.setHasFixedSize(true);
                ((CatHolder) holder).rvImages.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                CatDishDetailAdp mAdapter = new CatDishDetailAdp(list.get(position).getDishes(), context, this, position);
                ((CatHolder) holder).rvImages.setAdapter(mAdapter);
                break;
            case 1:
                ((NearByViewHolder) holder).rv_nearby.setHasFixedSize(true);
                ((NearByViewHolder) holder).rv_nearby.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                HomeNearByAdp homeNearByAdp = new HomeNearByAdp(list.get(position).getNearByResults(), context, this);
                ((NearByViewHolder) holder).rv_nearby.setAdapter(homeNearByAdp);
                break;
            case 2:
                ((NewPlotosViewHolder) holder).rv_new_plotos.setHasFixedSize(true);
                ((NewPlotosViewHolder) holder).rv_new_plotos.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                HomeNewPlotosAdp homeNewPlotosAdp = new HomeNewPlotosAdp(list.get(position).getNewRestResults(), context, this);
                ((NewPlotosViewHolder) holder).rv_new_plotos.setAdapter(homeNewPlotosAdp);
                break;
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class CatHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_images)
        RecyclerView rvImages;
        @BindView(R.id.tvTitle)
        OpensanBoldTextview tvTitle;
        @BindView(R.id.iv_next)
        ImageView ivNext;
        @BindView(R.id.ttl_view)
        LinearLayout ttl_view;

        public CatHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.ttl_view)
        public void onViewClicked() {
            listner.onClickOfDishName(getAdapterPosition());
        }
    }

    public class NearByViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_nearby)
        RecyclerView rv_nearby;
        @BindView(R.id.llnearby)
        LinearLayout llnearby;


        public NearByViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.llnearby)
        public void onViewClicked() {
            listner.onClickNearBy(getAdapterPosition());
        }
    }

    public class NewPlotosViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_new_plotos)
        RecyclerView rv_new_plotos;
        @BindView(R.id.llnewplotos)
        LinearLayout llnewplotos;


        public NewPlotosViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.llnewplotos)
        public void onViewClicked() {
            listner.onClickNewPlotos(getAdapterPosition());
        }
    }
}
