package com.o2.plotos.home.order;

/**
 * Created by Hassan on 22/02/2017.
 */

public interface IOrderPresenter {
    void sendGetOrderRqust(String userId);
    void bindView(Object view);
    void unBindView();
}
