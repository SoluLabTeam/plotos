package com.o2.plotos.home.menu;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2.plotos.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 3/1/2018.
 */

public class Disclaimer2Activity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.tvDesc)
    TextView tvDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disclaimer2);
        ButterKnife.bind(this);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        tvDesc.setTypeface(tf);
    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        finish();
    }
}
