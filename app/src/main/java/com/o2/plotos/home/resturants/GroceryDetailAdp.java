package com.o2.plotos.home.resturants;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.models.grocery.DishResponse;
import com.o2.plotos.restapi.newApis.models.grocery.SupplierResponse;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.OpensanTextview;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GroceryDetailAdp extends RecyclerView.Adapter<GroceryDetailAdp.SupplierHolder> implements GroceryDishDetailAdp.ImageClickListner {


    private ArrayList<DishResponse> list;
    private Context context;
    private ClickListnerOfGrocerySupplier listner;


    @Override
    public void onClickOfImage(int pos, int parentPosition) {
        Utils.navigateToDishOrGroceryDetailsAct(list.get(parentPosition).getItem().get(pos).getItem_id(), context, Constant.CAT_GROCERY);
    }


    public interface ClickListnerOfGrocerySupplier {
        void onClickOfArror(int pos);
    }

    public GroceryDetailAdp(ArrayList<DishResponse> list, Context context, ClickListnerOfGrocerySupplier listner) {
        this.list = list;
        this.context = context;
        this.listner = listner;

    }

    @Override
    public SupplierHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grocery_detail, parent, false);
        return new SupplierHolder(view);
    }

    @Override
    public void onBindViewHolder(SupplierHolder holder, int position) {
        holder.tvTitle.setText(list.get(position).getCategoryName());
        holder.rv_grocery_cat.setHasFixedSize(true);
        holder.rv_grocery_cat.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        GroceryDishDetailAdp mAdapter = new GroceryDishDetailAdp(list.get(position).getItem(), context, this, position);
        holder.rv_grocery_cat.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SupplierHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_grocery_cat)
        RecyclerView rv_grocery_cat;
        @BindView(R.id.iv_next)
        ImageView ivNext;
        @BindView(R.id.tvTitle)
        TextView tvTitle;


        public SupplierHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick(R.id.iv_next)
        public void onViewClicked() {
            listner.onClickOfArror(getAdapterPosition());
        }
    }
}
