package com.o2.plotos.home;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.cart.CartViewModel;
import com.o2.plotos.cart.OnGetGroceryByIDListener;
import com.o2.plotos.dietdetail.GroceryCategoryActivity;
import com.o2.plotos.dietdetail.GroceryDetailsActivity;
import com.o2.plotos.dietdetail.LowCrabActivity;
import com.o2.plotos.dietdetail.WithoutSpinnerActivity;
import com.o2.plotos.dishdetail.DishDetailActivity;
import com.o2.plotos.home.homeTab.NearByActivity;
import com.o2.plotos.home.resturants.IRestaurantDataModel;
import com.o2.plotos.home.resturants.RestaurantDataModelImpl;
import com.o2.plotos.models.Banner;
import com.o2.plotos.models.Dish;
import com.o2.plotos.models.Grocery;
import com.o2.plotos.models.Restaurant;
import com.o2.plotos.restapi.newApis.models.bannerImages.BannerImageResponse;
import com.o2.plotos.restapi.newApis.models.bannerImages.BannerImageResult;
import com.o2.plotos.restuarantdetail.IResturntDetalDataModl;
import com.o2.plotos.restuarantdetail.RestuarantDetailActivity;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Hassan on 3/02/2017.
 */

public class HeaderPagerAdapterNew extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    List<BannerImageResult> mBannerList;


    public HeaderPagerAdapterNew(Context context, List<BannerImageResult> bannerList) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mBannerList = bannerList;
    }


    @Override
    public int getCount() {
        return mBannerList != null ? mBannerList.size() : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_header_pager, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.item_header_pager_imageview);
        TextView message = (TextView) itemView.findViewById(R.id.item_header_pager_message);
        CustomFonts customFonts = new CustomFonts(mContext);
        customFonts.setOswaldBold(message);
            message.setText(mBannerList.get(position).getMessages());
            if (mBannerList != null && mBannerList.get(position).getImage() != null &&
                    !mBannerList.get(position).getImage().equalsIgnoreCase("")) {
                Picasso.with(mContext).load(mBannerList.get(position).getImage())
                        .resize(400, 200)
                        .centerCrop()
                        .error(R.drawable.placeholder)
                        .into(imageView);
            } else {
                imageView.setImageResource(R.drawable.placeholder);
            }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String typeValue = mBannerList.get(position).getType_value();
                String cat = mBannerList.get(position).getType();
                String name=mBannerList.get(position).getMessages();
                if (mBannerList != null && !mBannerList.get(position).getType().equals("0")
                        && !mBannerList.get(position).getType().equals("")) {

                    if (cat.equalsIgnoreCase("groceryitem")) {
                        Utils.navigateToDishOrGroceryDetailsAct(typeValue, mContext, cat);
                    }  else if (cat.equalsIgnoreCase("Supplier")) {
                        Utils.navigateToSuppAct(typeValue, mContext);
                    } else if (cat.equalsIgnoreCase("Program")) {
                        Intent intent = new Intent(mContext, NearByActivity.class);
                        intent.putExtra("title", name);
                        intent.putExtra("from", "plotosprogram");
                        intent.putExtra("id", typeValue);
                        mContext.startActivity(intent);
                    } else if (cat.equalsIgnoreCase("Signature")) {
                        Intent intent = new Intent(mContext, NearByActivity.class);
                        intent.putExtra("title", "Plotos Signature");
                        intent.putExtra("from", "plotosprogram");
                        mContext.startActivity(intent);
                    }
                }
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }

}
