package com.o2.plotos.home.order;

import com.o2.plotos.models.Order;

/**
 * Created by Rania on 4/3/2017.
 */
public interface OrderActionsListener {

    void onEditOrder(Order order);
    void onCancelOrder(String orderID);
}
