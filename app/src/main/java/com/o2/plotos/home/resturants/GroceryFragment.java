package com.o2.plotos.home.resturants;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.BaseFragment;
import com.o2.plotos.R;
import com.o2.plotos.adapters.ViewPagerAdapter;
import com.o2.plotos.disclaimer.DisclaimerActivity;
import com.o2.plotos.home.BannerViewListener;
import com.o2.plotos.home.BannerViewModel;
import com.o2.plotos.home.HeaderPagerAdapter;
import com.o2.plotos.home.HeaderPagerAdapterNew;
import com.o2.plotos.models.Banner;
import com.o2.plotos.models.Place;
import com.o2.plotos.models.Supplier;
import com.o2.plotos.places.DeliveryLocationActivity;
import com.o2.plotos.places.DeliveryTimeActivity;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.bannerImages.BannerImageResponse;
import com.o2.plotos.restapi.newApis.models.bannerImages.BannerImageResult;
import com.o2.plotos.restapi.newApis.models.submitQuery.SubmitQueryResponse;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;
import com.o2.plotos.widgets.DisableScrollViewPager;
import com.viewpagerindicator.LinePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;


public class GroceryFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        SupplierListener, BannerViewListener {

    @BindView(R.id.fragment_restaurant_recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.fragment_restaurant_swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.fragment_restaurant_resturantofday)
    TextView resturntOfDay;
    @BindView(R.id.fragment_restaurant_emptyView)
    TextView emptyView;
    @BindView(R.id.fragment_restaurant_txt_location)
    TextView txtCurrentLocation;
    @BindView(R.id.fragment_restaurant_viewPager)
    ViewPager mViewPager;

    @BindView(R.id.no_location_layout)
    RelativeLayout mNoLocationLayout;
    @BindView(R.id.no_data_title)
    TextView mNoDataTile;
    @BindView(R.id.no_data_message)
    TextView mNoDataMessage;


    CustomFonts customFonts;
    public String TYPE_RESTURANT = "get_restaurants";
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.linlay_delivery)
    LinearLayout linlayDelivery;
    @BindView(R.id.fragment_restaurant_listLayout)
    FrameLayout fragmentRestaurantListLayout;
    @BindView(R.id.fragment_restaurant_card_layout)
    CardView fragmentRestaurantCardLayout;
    @BindView(R.id.no_data_image)
    ImageView noDataImage;
    @BindView(R.id.fragment_restaurant_coordinateLayout)
    CoordinatorLayout fragmentRestaurantCoordinateLayout;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.linlay_delivery_time)
    LinearLayout linlayDeliveryTime;
    @BindView(R.id.grocery_tabs)
    TabLayout groceryTabs;
    @BindView(R.id.grocery_pager)
    DisableScrollViewPager groceryPager;

    private HeaderPagerAdapter mHeaderPagerAdapter;
    private HeaderPagerAdapterNew mHeaderPagerAdapterNew;

    private ViewPagerAdapter viewPagerAdapter;

    private Timer timer;
    int page = 0;


    private RestaurantAdapter mAdapter;
    private List<Place> mSuppliersList;

    private SupplierViewModel mSupplierViewModel;

    BannerViewModel mBannerViewModel;
    ArrayList<BannerImageResult> bannerImageResults = new ArrayList<>();
    LinePageIndicator indicator;

    public GroceryFragment() {
        // Required empty public constructor
    }

    public static GroceryFragment newInstance() {
        return new GroceryFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.frag_grocery, container, false);
        ButterKnife.bind(this, rootView);


        customFonts = new CustomFonts(getActivity());
        customFonts.setOswaldBold(resturntOfDay);
        customFonts.setOpenSansBold(mNoDataTile);
        customFonts.setOpenSansRegulr(mNoDataMessage);


        mSuppliersList = new ArrayList<>();
        swipeRefreshLayout.setOnRefreshListener(this);

        mBannerViewModel = new BannerViewModel(this);
        mBannerViewModel.getBanners();
        if (Utils.isNetworkAvailable(getContext())) {
            callBannerApi();
        }
//        mHeaderPagerAdapter = new HeaderPagerAdapter(getActivity(), null);
//        mViewPager.setAdapter(mHeaderPagerAdapter);

        indicator = (LinePageIndicator) rootView.findViewById(R.id.tutorial_pager_indicator);
        //  indicator.setSnap(true);


        mSupplierViewModel = new SupplierViewModel(this);
//        getSupplierList();

        setupViewPager(groceryPager);
        groceryTabs.setupWithViewPager(groceryPager);

        //Added by parth
        groceryTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        if(isAdded()) {
//            BalloonPopup bp = BalloonPopup.Builder(getApplicationContext(), mRestaurantsListTitle)
//                    .text("We only show grocery from groceries that delivering to \n your selected location." +
//                            " You might get more options by \n" +
//                            " changing your location")
//                    .shape(BalloonPopup.BalloonShape.rounded_square)
//                    .bgColor(Color.parseColor("#4AB173"))
//                    .fgColor(Color.WHITE)
//                    .timeToLive(60000)
//                    .textSize(12)
//                    .gravity(BalloonPopup.BalloonGravity.center)
//                    .offsetX(100)
//                    .offsetY(770)
//                    .show();
//        }

//        SimpleHintContentHolder hintBlock = new SimpleHintContentHolder.Builder(context)
////                .setContentTitle(R.string.title)
//                .setContentText(R.string.description)
//                .setTitleStyle(R.style.title)
//                .setContentStyle(R.style.content)
//                .setImageDrawableId(R.drawable.happy_welcome)
//                .build();


        return rootView;
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFrag(new GrocerySuppliersFragments(), "Suppliers");
        viewPagerAdapter.addFrag(new GroceryCategoryFragments(), "Categories");
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(2);
        viewPagerAdapter.notifyDataSetChanged();
        viewPager.invalidate();
        viewPager.destroyDrawingCache();
    }


    private void callBannerApi() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BannerImageResponse> call = apiInterface.callGetBanner(UserPreferenceUtil.getInstance(getContext()).getLocationLat(), UserPreferenceUtil.getInstance(getContext()).getLocationLng());
        call.enqueue(new Callback<BannerImageResponse>() {
            @Override
            public void onResponse(Call<BannerImageResponse> call, Response<BannerImageResponse> response) {
                if (response.body().getResponse().equalsIgnoreCase("0")) {

                    bannerImageResults.clear();
                    bannerImageResults.addAll(response.body().getBannerImageResults());
                    for(int x = bannerImageResults.size() - 1; x >= 0; x--) {
                        if (bannerImageResults.get(x).getType().equals("restaurant") || bannerImageResults.get(x).getType().equals("dish")) {
                            bannerImageResults.remove(x);
                        }
                    }
                    mHeaderPagerAdapterNew = new HeaderPagerAdapterNew(getContext(), bannerImageResults);
                    mViewPager.setAdapter(mHeaderPagerAdapterNew);
                    indicator.setViewPager(mViewPager);
                    indicator.setSelectedColor(Color.parseColor("#ffffff"));
                    indicator.setGapWidth(10f);
                    indicator.setLineWidth(80f);
                    indicator.setStrokeWidth(16f);

                    pageSwitcher(10);
                }
            }

            @Override
            public void onFailure(Call<BannerImageResponse> call, Throwable t) {
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
//            BalloonPopup supplierBp = BalloonPopup.Builder(getApplicationContext(), mRestaurantsListTitle)
//                    .text("We only show grocery from groceries that delivering to \n your selected location." +
//                            " You might get more options by \n" +
//                            " changing your location")
//                    .shape(BalloonPopup.BalloonShape.rounded_square)
//                    .bgColor(Color.parseColor("#4AB173"))
//                    .fgColor(Color.WHITE)
//                    .timeToLive(60000)
//                    .textSize(12)
//                    .gravity(BalloonPopup.BalloonGravity.center)
//                    .offsetX(100)
//                    .offsetY(770)
//                    .show();
        }
    }

    private void getSupplierList() {

        //Request suppliers
        if (InternetUtil.getInstance(getContext()).isNetWorkAvailable()) {
            String userLat = UserPreferenceUtil.getInstance(getActivity()).getLocationLat();
            String userLong = UserPreferenceUtil.getInstance(getActivity()).getLocationLng();
            if (!userLat.equals("0.0") && !userLong.equals("0.0")) {
                mSupplierViewModel.getSupplierRequest(userLat, userLong);
            } else {
                Toast.makeText(getActivity(), getString(R.string.error_no_location), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        setLocationPreference();

        setUserPreferenceDate();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mSupplierViewModel != null) {
            mSupplierViewModel.onDestroy();
        }
    }

    /**
     * Update location from shared preference
     */
    private void setLocationPreference() {
        if (txtCurrentLocation != null) {
            txtCurrentLocation.setText("☛ " + UserPreferenceUtil.getInstance(getActivity()).getLocation());
            String text = "<font color=#00000>Delivering to</font> <font color=#4AB173>" + UserPreferenceUtil.getInstance(getActivity()).getLocation() + "</font>";
            txtAddress.setText(Html.fromHtml(text));

        }
    }

    /**
     * Set the user preference
     */
    private void setUserPreferenceDate() {

        String user_id = UserPreferenceUtil.getInstance(getApplicationContext()).UserId();


        {
            if (UserPreferenceUtil.getInstance(getActivity()).getAsSoonCheck()) {
                txtTime.setText("ASAP");
            } else {
                txtTime.setText(UserPreferenceUtil.getInstance(getActivity()).getDeliveryDate());

            }
        }


    }

    @OnClick(R.id.fragment_restaurant_txt_location)
    public void onLocationClick() {
        startActivity(new Intent(getActivity(), DeliveryLocationActivity.class));
    }

    @Override
    public void onGetSupplierSuccess(List<Supplier> supplierList) {
        mSuppliersList.addAll(supplierList);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new RestaurantAdapter(mSuppliersList, getActivity());
        mRecyclerView.setAdapter(mAdapter);
        if (mAdapter.getItemCount() == 0) {
            mNoLocationLayout.setVisibility(View.VISIBLE);
        } else {
            mNoLocationLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onGetSupplierFailed(String error) {

        mNoLocationLayout.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {
//        getSupplierList();
    }

    /**
     * Update Location
     */
    public void updateLocation() {
        setLocationPreference();
    }

    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay
        // in
        // milliseconds
    }

  /*  public void dialogLocation()

    {
        final Dialog dialog2 = new Dialog(getContext());
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setCancelable(true);
        dialog2.setContentView(R.layout.dialog_changelocation);
        dialog2.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_top_bootom;
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView txt_changelocation = (TextView) dialog2.findViewById(R.id.txt_changelocation);
        TextView txt_cancel = (TextView) dialog2.findViewById(R.id.txt_cancel);
        EditText edit_location = (EditText) dialog2.findViewById(R.id.edit_location);

        edit_location.setText(txtAddress.getText().toString());


        txt_changelocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), DeliveryLocationActivity.class);
                startActivity(i);

                dialog2.dismiss();


            }
        });


        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog2.dismiss();
            }
        });

        dialog2.show();

    }*/

    @Override
    public void onGetBannerSuccessfully(List<Banner> bannerList) {
//        if (mHeaderPagerAdapter != null) {
//            mHeaderPagerAdapter.setBanners(bannerList);
//            mHeaderPagerAdapter.notifyDataSetChanged();
//        }
    }


    @OnClick({R.id.linlay_delivery, R.id.linlay_delivery_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linlay_delivery:


                Intent intent = new Intent(getActivity(), DeliveryLocationActivity.class);

                startActivity(intent);

                break;
            case R.id.linlay_delivery_time:

                Intent intent2 = new Intent(getActivity(), DeliveryTimeActivity.class);

                startActivity(intent2);

                break;
        }
    }


    // this is an inner class...
    class RemindTask extends TimerTask {

        @Override
        public void run() {

            try {
                // As the TimerTask run on a seprate thread from UI thread we have
                // to call runOnUiThread to do work on UI thread.
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {

                        if (page > 3) { // In my case the number of pages are 5
                            timer = new Timer();
                            page = 0;
                            mViewPager.setCurrentItem(0);
                            // Showing a toast for just testing purpose
                        } else {
                            mViewPager.setCurrentItem(page++);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}