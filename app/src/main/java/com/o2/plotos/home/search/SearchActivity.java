package com.o2.plotos.home.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.home.homeTab.NearByActivity;
import com.o2.plotos.home.resturants.AddToCartActivity;
import com.o2.plotos.home.resturants.GroceryCategoryDetailActivity;
import com.o2.plotos.home.resturants.MainSerachResultAdp;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.search.SearchData;
import com.o2.plotos.restapi.newApis.models.search.SearchResponse;
import com.o2.plotos.restuarantdetail.RestuarantDetailActivity;
import com.o2.plotos.restuarantdetail.ResturantDetailsActivity;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.OpensanTextview;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 3/1/2018.
 */

public class SearchActivity extends BaseActivity implements MainSerachResultAdp.clickListner{


    @BindView(R.id.img_close)
    ImageView imgClose;

    Context mContext;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.rv_search_main_items)
    RecyclerView rvSearchMainItems;
    @BindView(R.id.tv_count_result)
    OpensanTextview tvCountResult;
    @BindView(R.id.edt_search)
    EditText edtSearch;

    @BindView(R.id.catSpinner)
    Spinner catSpinner;
    private List<String> listOfTitle = new ArrayList<>();
    private List<List<SearchData>> listSearch = new ArrayList<>();
    private MainSerachResultAdp serachResultAdp;

    String[] searchFilter = {"All", "Restaurants", "Dishes", "Suppliers", "Program", "Grocery", "Signature"};

    String strFilter = "all";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_search);
        ButterKnife.bind(this);
        mContext = this;
        ArrayAdapter aa = new ArrayAdapter(this, R.layout.spinner_item, searchFilter);
        aa.setDropDownViewResource(R.layout.spinner_item);
        catSpinner.setAdapter(aa);
        editTextListner();
        bindAdapter();
    }

    private void bindAdapter() {
        serachResultAdp = new MainSerachResultAdp(listOfTitle, listSearch, this, this);
        rvSearchMainItems.setLayoutManager(new LinearLayoutManager(this));
        rvSearchMainItems.setAdapter(serachResultAdp);
    }

    private void editTextListner() {
        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    callSearchApi();
                    return true;
                }

                return false;
            }
        });
    }

    private void callSearchApi() {
        Utils.hideSoftKeyboard(this);
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SearchResponse> call = apiInterface.getSearchResults(getDataFromEditText(edtSearch), UserPreferenceUtil.getInstance(this).getLocationLat(), UserPreferenceUtil.getInstance(this).getLocationLng());
        call.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                hideProgress();
                strFilter=catSpinner.getSelectedItem().toString().trim();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    listOfTitle.clear();
                    listSearch.clear();
                    serachResultAdp.notifyDataSetChanged();
                    if (strFilter.equalsIgnoreCase("all") || strFilter.equalsIgnoreCase("Restaurants")) {
                        if (response.body().getSearchListing().getList_restaurant() != null && response.body().getSearchListing().getList_restaurant().size() > 0) {
                            listOfTitle.add("Restaurants");
                            listSearch.add(response.body().getSearchListing().getList_restaurant());
                        }
                    }
                    if (strFilter.equalsIgnoreCase("all") || strFilter.equalsIgnoreCase("Dishes")) {
                        if (response.body().getSearchListing().getList_dishes() != null && response.body().getSearchListing().getList_dishes().size() > 0) {
                            listOfTitle.add("Dishes");
                            listSearch.add(response.body().getSearchListing().getList_dishes());
                        }
                    }
                    if (strFilter.equalsIgnoreCase("all") || strFilter.equalsIgnoreCase("Suppliers")) {
                        if (response.body().getSearchListing().getList_suppliers() != null && response.body().getSearchListing().getList_suppliers().size() > 0) {
                            listOfTitle.add("Suppliers");
                            listSearch.add(response.body().getSearchListing().getList_suppliers());
                        }
                    }
                    if (strFilter.equalsIgnoreCase("all") || strFilter.equalsIgnoreCase("Program")) {
                        if (response.body().getSearchListing().getList_program() != null && response.body().getSearchListing().getList_program().size() > 0) {
                            listOfTitle.add("Program");
                            listSearch.add(response.body().getSearchListing().getList_program());
                        }
                    }
                    if (strFilter.equalsIgnoreCase("all") || strFilter.equalsIgnoreCase("Grocery")) {
                        if (response.body().getSearchListing().getList_grocery() != null && response.body().getSearchListing().getList_grocery().size() > 0) {
                            listOfTitle.add("Grocery");
                            listSearch.add(response.body().getSearchListing().getList_grocery());
                        }
                    }
                    if (strFilter.equalsIgnoreCase("all") || strFilter.equalsIgnoreCase("Signature")) {
                        if (response.body().getSearchListing().getList_signature() != null && response.body().getSearchListing().getList_signature().size() > 0) {
                            listOfTitle.add("Signature");
                            listSearch.add(response.body().getSearchListing().getList_signature());
                        }
                    }
                    if (listOfTitle.size() > 0) {
                        serachResultAdp.notifyDataSetChanged();
                    } else {
                        showToast("No result found");
                    }
                }/* else {
                    showToast(response.body().getMsg());
                }*/
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }

    @OnClick({R.id.img_close})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.img_close:
                Intent intent = new Intent(mContext, HomeActivity.class);
                intent.putExtra("CurrentTab", 2);
                intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                finish();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(mContext, HomeActivity.class);
        intent.putExtra("CurrentTab", 2);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
        finish();
    }


    @Override
    public void onClickItem(String id, String name, String cat) {
        if (cat.equalsIgnoreCase(Constant.CAT_DISH) || cat.equalsIgnoreCase(Constant.CAT_GROCERY)) {
            Utils.navigateToDishOrGroceryDetailsAct(id, this, cat);
        } else if (cat.equalsIgnoreCase("Restaurants")) {
            Utils.navigateToResturantDetailsAct(id, this);
        } else if (cat.equalsIgnoreCase("Suppliers")) {
            Utils.navigateToSuppAct(id, SearchActivity.this);
        } else if (cat.equalsIgnoreCase("Program")) {
            Intent intent = new Intent(this, NearByActivity.class);
            intent.putExtra("title", name);
            intent.putExtra("from", "plotosprogram");
            intent.putExtra("id", id);
            startActivity(intent);
        } else if (cat.equalsIgnoreCase("Signature")) {
            Intent intent = new Intent(this, NearByActivity.class);
            intent.putExtra("title", "Plotos Signature");
            intent.putExtra("from", "plotosprogram");
            startActivity(intent);
        }
    }

}
