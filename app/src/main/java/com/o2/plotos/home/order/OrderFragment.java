package com.o2.plotos.home.order;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.o2.plotos.BaseFragment;
import com.o2.plotos.R;
import com.o2.plotos.adapters.ViewPagerAdapter;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.models.Order;
import com.o2.plotos.models.User;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.orders.OrderModel;
import com.o2.plotos.restapi.newApis.models.orders.OrdersResponse;
import com.o2.plotos.restapi.newApis.models.orders.PastResult;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderFragment extends BaseFragment implements IOrderView {


    @BindView(R.id.fragment_orders_tablayout)
    TabLayout tabLayout;
    @BindView(R.id.fragment_orders_viewpager)
    ViewPager viewPager;

    @BindView(R.id.txt_clear_orders)
    TextView txt_clear_orders;
    ViewPagerAdapter viewPagerAdapter;

    private IOrderPresenter mOrderPresenter;

    private ArrayList<Order> mOrderList = new ArrayList<>();

    private ArrayList<Order> upcommingList;
    private ArrayList<Order> pastList;

    private List<PastResult> pastResults = new ArrayList<>();
    private List<PastResult> upcomingResults = new ArrayList<>();


    public static final String EXTRA_USER = "user_id";
    private String userId;


    public OrderFragment() {
        // Required empty public constructor
    }


    public static OrderFragment newInstance() {
        return new OrderFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_order, container, false);
        mOrderPresenter = new OrderPrsnterImpl();
        mOrderPresenter.bindView(this);

//        getUserDetail();
        ButterKnife.bind(this, rootView);
        getOrderList();
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 1) {
//                    gone for now make it visible after approval
                    txt_clear_orders.setVisibility(View.GONE);
                } else {
                    txt_clear_orders.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setupViewPager(viewPager);
        callOrdersApi();
        return rootView;
    }

    public void getOrderList() {
        if (InternetUtil.getInstance(getContext()).isNetWorkAvailable()) {
            mOrderPresenter.sendGetOrderRqust(userId);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    //    private void getUserDetail() {
//        DataBaseHelper dataBaseHelper = new DataBaseHelper(getActivity());
//        User user;
//        try {
//            user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();
//            userId = user.getId();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void setupViewPager(ViewPager viewPager) {

        upcommingList = new ArrayList<>();
        pastList = new ArrayList<>();

        for (int i = 0; i < mOrderList.size(); i++) {
            if (!mOrderList.get(i).getOrderStatus().equalsIgnoreCase("4")) {
                upcommingList.add(mOrderList.get(i));
            }
            if (mOrderList.get(i).getOrderStatus().equalsIgnoreCase("4")) {
                pastList.add(mOrderList.get(i));
            }
        }
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFrag(Orderdetail.newInstance(upcomingResults), "Upcoming");
        viewPagerAdapter.addFrag(PastOrderdetail.newInstance(pastResults), "Past");
        viewPager.setAdapter(viewPagerAdapter);
//        viewPager.setOffscreenPageLimit(2);
        viewPager.invalidate();
        viewPager.destroyDrawingCache();
        tabLayout.setupWithViewPager(viewPager);
    }

//    @Override
//    public void showProgress() {
//        if (isDetached() && mProgressDialog != null) {
//            mProgressDialog.show();
//        }
//    }
//
//    @Override
//    public void hideProgress() {
//        mProgressDialog.hide();
//    }

//    @Override
//    public void onGetOrderSuccess(List<Order> orderList) {
//        mOrderList = (ArrayList<Order>) orderList;
//        setupViewPager(viewPager);
//        tabLayout.setupWithViewPager(viewPager);
//    }
//
//    @Override
//    public void onGetOrderFailed(String error) {
//        Log.v(ConstantUtil.TAG,"onGetOrderFailed "+error);
//        setupViewPager(viewPager);
//        tabLayout.setupWithViewPager(viewPager);
//    }

    private void callOrdersApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderModel> call = apiInterface.getOrders(UserPreferenceUtil.getInstance(getContext()).UserId(), "0");
        call.enqueue(new Callback<OrderModel>() {
            @Override
            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                hideProgress();
                pastResults.clear();
                upcomingResults.clear();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    if (response.body().getResult().getPast() != null && response.body().getResult().getPast().size() > 0) {
                        pastResults.addAll(response.body().getResult().getPast());
                        viewPagerAdapter.notifyDataSetChanged();
                    }
                    if (response.body().getResult().getUpcoming() != null && response.body().getResult().getUpcoming().size() > 0) {
                        upcomingResults.addAll(response.body().getResult().getUpcoming());
                        viewPagerAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderModel> call, Throwable t) {
                hideProgress();
            }
        });
    }

    @Override
    public void onGetOrderSuccess(List<Order> orderList) {

    }

    @Override
    public void onGetOrderFailed(String error) {

    }
}
