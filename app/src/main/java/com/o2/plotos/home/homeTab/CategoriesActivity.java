package com.o2.plotos.home.homeTab;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.plotosProgram.PlotosResponse;
import com.o2.plotos.restapi.newApis.models.plotosProgram.PlotosResult;
import com.o2.plotos.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 2/14/2018.
 */

public class CategoriesActivity extends BaseActivity implements CategoriesAdapter.ClickListnerOfGrocerySupplier {

    @BindView(R.id.img_close)
    ImageView imgClose;
    @BindView(R.id.delivery_location_listView)
    RecyclerView deliveryLocationListView;
    @BindView(R.id.activity_delivery_location)
    LinearLayout activityDeliveryLocation;

    ArrayList<PlotosResult> arrayList = new ArrayList<>();
    CategoriesAdapter categoriesAdapter;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);
        context = this;
        if (Utils.isNetworkAvailable(this)) {
            callApi();
        } else {
            showToast(getString(R.string.error_internet_connection));
        }

        deliveryLocationListView.setHasFixedSize(true);
        deliveryLocationListView.setLayoutManager(new LinearLayoutManager(this));
        categoriesAdapter = new CategoriesAdapter(arrayList, this, this);
        deliveryLocationListView.setAdapter(categoriesAdapter);
    }


    private void callApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PlotosResponse> call = apiInterface.getPlotosProgram();
        call.enqueue(new Callback<PlotosResponse>() {
            @Override
            public void onResponse(Call<PlotosResponse> call, Response<PlotosResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    if (response.body().getResponse().equalsIgnoreCase("0")) {
                        arrayList.add(new PlotosResult("0", "Near by Restaurants", "0"));
                        arrayList.add(new PlotosResult("0", "Plotos Picks", "0"));
                        arrayList.add(new PlotosResult("0", "Plotos Signature", "0"));
                        arrayList.addAll(response.body().getResult());
                        categoriesAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<PlotosResponse> call, Throwable t) {
                hideProgress();
            }
        });
    }

    @OnClick(R.id.img_close)
    public void onViewClicked() {
        finish();
    }

    @Override
    public void onClickOfArror(int pos) {
        if (pos == 0) {
            Intent intent = new Intent(this, NearByActivity.class);
            intent.putExtra("title", "Near by Restaurants");
            intent.putExtra("from", "plotosprogram");
            startActivity(intent);
        } else if (pos == 1) {
            finish();
        } else if (pos == 2) {
            Intent intent = new Intent(this, NearByActivity.class);
            intent.putExtra("title", "Plotos Signature");
            intent.putExtra("from", "plotosprogram");
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, NearByActivity.class);
            intent.putExtra("title",arrayList.get(pos).getProgram_name());
            intent.putExtra("from", "plotosprogram");
            intent.putExtra("id", arrayList.get(pos).getProgram_id());
            startActivity(intent);
        }

    }

}
