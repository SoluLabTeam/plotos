package com.o2.plotos.home.order;

import android.util.Log;

import com.o2.plotos.cart.OnGetGroceryByIDListener;
import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.DishRequest;
import com.o2.plotos.restapi.responses.DishesRequestResponse;
import com.o2.plotos.restapi.responses.GroceryResponse;
import com.o2.plotos.restuarantdetail.IResturntDetalDataModl;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rania on 4/3/2017.
 */
public class OrderDetailsViewModel implements DeleteOrderRequestListener{

    private DeleteOrderHelper mDeleteOrderHelper;
    private UpdateOrderListener mUpdateOrderListener;
    private IResturntDetalDataModl.OnGetResturntDishRqustFinishLisener mDishRqustFinishLisener;
    OnGetGroceryByIDListener mOnGetGroceryByIDListener;

    public OrderDetailsViewModel(UpdateOrderListener updateOrderListener,
                                 IResturntDetalDataModl.OnGetResturntDishRqustFinishLisener dishRqustFinishLisener,
                                 OnGetGroceryByIDListener onGetGroceryByIDListener){
        mDeleteOrderHelper = new DeleteOrderHelper(this);
        mUpdateOrderListener = updateOrderListener;
        mDishRqustFinishLisener = dishRqustFinishLisener;
        mOnGetGroceryByIDListener = onGetGroceryByIDListener;
    }

    public void deleteOrder(String orderID){
        mDeleteOrderHelper.deleteOrderRequest(orderID);
    }

    @Override
    public void OnResponseSuccess() {
        Log.d("RANIA", "Success delete");
        mUpdateOrderListener.onUpdateSuccess();
    }

    @Override
    public void onResponseFailure(String message) {
        Log.d("RANIA", "Failure delete");
        mUpdateOrderListener.onUpdateFailure(message);
    }

    public void onDestroy(){
        if(mUpdateOrderListener != null){
            mUpdateOrderListener = null;
        }
    }

    public void getDishByID(String dishId){
        DishRequest resturntDishRqust = ServiceGenrator.createService(DishRequest.class);
        Call<DishesRequestResponse> resturantDishRequestCall =
                resturntDishRqust.dishRequest(dishId);
        resturantDishRequestCall.enqueue(new Callback<DishesRequestResponse>() {
            @Override
            public void onResponse(Call<DishesRequestResponse> call, Response<DishesRequestResponse> response) {
                LogCat.LogDebug(ConstantUtil.TAG," -> ResturandResult Request api response "+response.raw());
                if(response.code()== 200){
                    DishesRequestResponse apiResponse =  response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        mDishRqustFinishLisener.OnGetResturntDishSuccess(apiResponse.dish);

                    }else if(responseCode.equalsIgnoreCase("1")){
                        mDishRqustFinishLisener.OnGetResturntDishFailed("Resturant ResturandResult request Failed(1)");
                    }else if(responseCode.equalsIgnoreCase("2")){
                        mDishRqustFinishLisener.OnGetResturntDishFailed("Resturant ResturandResult request Failed(2)");
                    }
                }
            }
            @Override
            public void onFailure(Call<DishesRequestResponse> call, Throwable t) {
                mDishRqustFinishLisener.OnGetResturntDishFailed("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"Resturant dish ap onFailure "+t.getMessage());
            }
        });
    }

    public void getGroceryByID(String dishId){
        DishRequest groceryRequest = ServiceGenrator.createService(DishRequest.class);
        Call<GroceryResponse> groceryRequestCall =
                groceryRequest.groceryRequest(dishId);
        groceryRequestCall.enqueue(new Callback<GroceryResponse>() {
            @Override
            public void onResponse(Call<GroceryResponse> call, Response<GroceryResponse> response) {
                LogCat.LogDebug(ConstantUtil.TAG," -> Grocery Request api response "+response.raw());
                if(response.code()== 200){
                    GroceryResponse apiResponse =  response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        mOnGetGroceryByIDListener.onGetGrocerySuccess(apiResponse.groceryList.get(0));

                    }else if(responseCode.equalsIgnoreCase("1")){
                        mOnGetGroceryByIDListener.onGetGeoceryFailed("Grocery request Failed(1)");
                    }else if(responseCode.equalsIgnoreCase("2")){
                        mOnGetGroceryByIDListener.onGetGeoceryFailed("Grocery request Failed(2)");
                    }
                }
            }
            @Override
            public void onFailure(Call<GroceryResponse> call, Throwable t) {
                mOnGetGroceryByIDListener.onGetGeoceryFailed("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"Grocery dish ap onFailure "+t.getMessage());
            }
        });
    }
}
