package com.o2.plotos.home.resturants;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.o2.plotos.BaseFragment;
import com.o2.plotos.R;
import com.o2.plotos.home.HeaderPagerAdapterNew;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.bannerImages.BannerImageResponse;
import com.o2.plotos.restapi.newApis.models.grocery.SupplierModel;
import com.o2.plotos.restapi.newApis.models.grocery.SupplierResponse;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;


import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class GrocerySuppliersFragments extends BaseFragment implements GrocerySupplierAdp.ClickListnerOfGrocerySupplier {
    @BindView(R.id.rv_grocery_suppliers)
    RecyclerView rvGrocerySuppliers;
    ArrayList<SupplierResponse>  supplierResponseArrayList=new ArrayList<>();
    GrocerySupplierAdp mAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_grocery_suppliers, container, false);
        ButterKnife.bind(this, rootView);

        mAdapter = new GrocerySupplierAdp(supplierResponseArrayList, getActivity(), this);
        rvGrocerySuppliers.setAdapter(mAdapter);

        if (Utils.isNetworkAvailable(getContext())){
            callApi();
        }
        return rootView;
    }

    private void callApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        // id is 0 to get all list
        Call<SupplierModel> call = apiInterface.callSupplierList("0",UserPreferenceUtil.getInstance(getContext()).getLocationLat(), UserPreferenceUtil.getInstance(getContext()).getLocationLng());
        call.enqueue(new Callback<SupplierModel>() {
            @Override
            public void onResponse(Call<SupplierModel> call, Response<SupplierModel> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    supplierResponseArrayList.addAll(response.body().getResult());
                    rvGrocerySuppliers.setHasFixedSize(true);
                    rvGrocerySuppliers.setLayoutManager(new LinearLayoutManager(getActivity()));
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<SupplierModel> call, Throwable t) {
                hideProgress();
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClickOfArror(int pos) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.GROCERYSUPPERLIERDETAILS, Parcels.wrap(supplierResponseArrayList.get(pos)));
        Intent intent = new Intent(getActivity(), GroceryCategoryDetailActivity.class);
        intent.putExtras(bundle);
        getActivity().startActivity(intent);
    }
}
