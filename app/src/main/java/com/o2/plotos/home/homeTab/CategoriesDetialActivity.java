package com.o2.plotos.home.homeTab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.adapters.SignatureDetailAdp;
import com.o2.plotos.home.order.DeliverToActivity;
import com.o2.plotos.home.search.SearchDetailActivity;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.models.home.CategoryResult;
import com.o2.plotos.restapi.newApis.models.home.DishDetail;
import com.o2.plotos.restapi.newApis.models.resturantDetails.DishData;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 2/15/2018.
 */

public class CategoriesDetialActivity extends AppCompatActivity implements CatDetailAdp.ClickListnerOfNearby, SignatureDetailAdp.ClickListnerOfSignature {

    @BindView(R.id.rv_cat_details)
    RecyclerView rvCatDetails;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_back)
    ImageView imgBack;
    CatDetailAdp catDetailAdp;
    SignatureDetailAdp signatureDetailAdp;
    CategoryResult categoryResult;
    List<ItemDetais> dishDataList;
//    =new ArrayList<>();

    @BindView(R.id.cartLayout)
    RelativeLayout cartLayout;
    @BindView(R.id.tvItemCount)
    TextView tvItemCount;
    @BindView(R.id.tvTotal)
    TextView tvTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_detail);
        ButterKnife.bind(this);
        rvCatDetails.setHasFixedSize(true);
        rvCatDetails.setLayoutManager(new LinearLayoutManager(this));
        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("from") && getIntent().getStringExtra("from").equals("nearby")) {
                txtTitle.setText(getIntent().getStringExtra("title"));
                dishDataList = Parcels.unwrap(getIntent().getParcelableExtra(Constant.SIGNATUREDETAILS));
                signatureDetailAdp = new SignatureDetailAdp(dishDataList, this, this);
                rvCatDetails.setAdapter(signatureDetailAdp);
                signatureDetailAdp.notifyDataSetChanged();
            } else {
                categoryResult = Parcels.unwrap(getIntent().getParcelableExtra(Constant.CATEGORYDETAILS));
                txtTitle.setText(categoryResult.getName());
                catDetailAdp = new CatDetailAdp(categoryResult.getDishes(), this, this);
                rvCatDetails.setAdapter(catDetailAdp);
                catDetailAdp.notifyDataSetChanged();
            }
        }
        setCartFab();
    }

    private void setCartFab() {
        int cartItemSize = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST).size();
        if (cartItemSize > 0) {
            cartLayout.setVisibility(View.VISIBLE);
            tvItemCount.setText("" + cartItemSize);
            makeList();
        } else {
            cartLayout.setVisibility(View.GONE);
        }
    }

    private void makeList() {
        Double tootal = 0.00;
        List<ItemDetais> listOfCartItems = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST);
        for (int i = 0; i < listOfCartItems.size(); i++) {
            ItemDetais itemDetais = listOfCartItems.get(i);
            tootal = tootal + (itemDetais.getQuantity() * itemDetais.getBasicPrice());
        }
        tvTotal.setText(tootal + " AED");
    }

    @OnClick({R.id.img_back, R.id.cartLayout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.cartLayout:
                startActivity(new Intent(this, DeliverToActivity.class));
                break;
        }
    }

    @Override
    public void onClickCatSignature(int pos) {
        Utils.navigateToDishOrGroceryDetailsAct(dishDataList.get(pos).getItem_id(), this, Constant.CAT_DISH);
    }

    @Override
    public void onClickOfPlusinCartSign(int pos) {
        ItemDetais itemDetais = dishDataList.get(pos);
        itemDetais.setQuantity(Utils.updateQuantity(itemDetais.getQuantity(), true));
        dishDataList.set(pos, itemDetais);
        signatureDetailAdp.notifyItemChanged(pos);
        Utils.storeDatainToCart(itemDetais, this, itemDetais.getQuantity(), Double.valueOf(itemDetais.getPrice()));
        setCartFab();
    }

    @Override
    public void onClickOfMinusinCartSign(int pos) {
        ItemDetais itemDetais = dishDataList.get(pos);
        itemDetais.setQuantity(Utils.updateQuantity(itemDetais.getQuantity(), false));
        dishDataList.set(pos, itemDetais);
        signatureDetailAdp.notifyItemChanged(pos);
        Utils.storeDatainToCart(itemDetais, this, itemDetais.getQuantity(), Double.valueOf(itemDetais.getPrice()));
        setCartFab();
    }

    @Override
    public void onClickofWholeView(int pos) {

    }


    @Override
    public void onClickOfPlusinCart(int pos) {
        ItemDetais itemDetais = categoryResult.getDishes().get(pos);
        itemDetais.setQuantity(Utils.updateQuantity(itemDetais.getQuantity(), true));
        categoryResult.getDishes().set(pos, itemDetais);
        catDetailAdp.notifyItemChanged(pos);
        Utils.storeDatainToCart(itemDetais, this, itemDetais.getQuantity(), Double.valueOf(itemDetais.getPrice()));
        setCartFab();
    }

    @Override
    public void onClickOfMinusinCart(int pos) {
        ItemDetais itemDetais = categoryResult.getDishes().get(pos);
        itemDetais.setQuantity(Utils.updateQuantity(itemDetais.getQuantity(), false));
        categoryResult.getDishes().set(pos, itemDetais);
        catDetailAdp.notifyItemChanged(pos);
        Utils.storeDatainToCart(itemDetais, this, itemDetais.getQuantity(), Double.valueOf(itemDetais.getPrice()));
        setCartFab();
    }
}
