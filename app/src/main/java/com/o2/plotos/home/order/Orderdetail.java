package com.o2.plotos.home.order;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.R;
import com.o2.plotos.cart.OnGetGroceryByIDListener;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.dishdetail.DishDetailActivity;
import com.o2.plotos.models.Cart;
import com.o2.plotos.models.Dish;
import com.o2.plotos.models.Grocery;
import com.o2.plotos.models.Option;
import com.o2.plotos.models.Order;
import com.o2.plotos.models.User;
import com.o2.plotos.restapi.newApis.models.orders.PastResult;
import com.o2.plotos.restuarantdetail.IResturntDetalDataModl;
import com.o2.plotos.utils.UserPreferenceUtil;

import org.parceler.Parcels;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Orderdetail extends Fragment implements OrderAdapter.OnOrderListener,
        OrderActionsListener, UpdateOrderListener, IResturntDetalDataModl.OnGetResturntDishRqustFinishLisener,
        OnGetGroceryByIDListener {
    @BindView(R.id.fragment_order_recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.fragment_order_emptyView)
    TextView emptyView;

    private OnReOrderListener listener;
    private ArrayList<Order> mOrderList;
    private ArrayList<PastResult> mUpcomingList;
    private ProgressDialog mProgressDialog;
    private OrderDetailsViewModel mViewModel;
    private UpdateOrderListListener mUpdateOrderListListener;
    private Order mEditOrder;
    private String mDeletedOrder;
    private OrderAdapter mOrderAdapter;

    public Orderdetail() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnReOrderListener) context;
            mUpdateOrderListListener = (UpdateOrderListListener) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    public static Orderdetail newInstance(List<PastResult> orderList) {
        Orderdetail orderdetail = new Orderdetail();
        Bundle bundle = new Bundle();
        bundle.putParcelable("orderList", Parcels.wrap(orderList));
        orderdetail.setArguments(bundle);
        return orderdetail;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUpcomingList = Parcels.unwrap(getArguments().getParcelable("orderList"));
//        mViewModel = new OrderDetailsViewModel(this, this, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcomming_order, container, false);
        ButterKnife.bind(this, view);
//        mProgressDialog = new ProgressDialog(getActivity());
//        mProgressDialog.setMessage("Please wait...");
//        mProgressDialog.setCancelable(false);
        setOrderAdapter();
        return view;
    }

    private void setOrderAdapter() {
        if (mUpcomingList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(linearLayoutManager);
            mOrderAdapter = new OrderAdapter(getContext(), mUpcomingList, this, this);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setAdapter(mOrderAdapter);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onOrderClick(Order order) {
//        if (order.getOrderStatus().equalsIgnoreCase("4")) {
//            sendOrderRequest(order);
//        } else {
//            Intent intent = new Intent(getActivity(), FollowOrderActivity.class);
//            intent.putExtra(FollowOrderActivity.EXTRA_ORDER, order);
//            startActivity(intent);
//
//            //getActivity().overridePendingTransition(R.anim.slide_up, android.R.anim.fade_out);
//        }
    }

    private void sendOrderRequest(Order order) {

        String userLat;
        String userLong;
        String userAddress;

        /*if (UserPreferenceUtil.getInstance(getContext()).getAsSoonCheck()) {
            if (UserPreferenceUtil.getInstance(getContext()).getCurrentLocationCheck()) {
                userLat = UserPreferenceUtil.getInstance(getContext()).getCurrentLat();
                userLong = UserPreferenceUtil.getInstance(getContext()).getCurrentLng();
                userAddress = UserPreferenceUtil.getInstance(getContext()).getCurrentLocation();
            } else {
                userLat = UserPreferenceUtil.getInstance(getContext()).getLocationLat();
                userLong = UserPreferenceUtil.getInstance(getContext()).getLocationLng();
                userAddress = UserPreferenceUtil.getInstance(getContext()).getLocation();

            }
        } else {
            if (UserPreferenceUtil.getInstance(getContext()).getCurrentLocationCheck()) {
                userLat = UserPreferenceUtil.getInstance(getContext()).getCurrentLat();
                userLong = UserPreferenceUtil.getInstance(getContext()).getCurrentLng();
                userAddress = UserPreferenceUtil.getInstance(getContext()).getCurrentLocation();
            } else {
                userLat = UserPreferenceUtil.getInstance(getContext()).getLocationLat();
                userLong = UserPreferenceUtil.getInstance(getContext()).getLocationLng();
                userAddress = UserPreferenceUtil.getInstance(getContext()).getLocation();
            }
        }*/

        userLat = UserPreferenceUtil.getInstance(getContext()).getLocationLat();
        userLong = UserPreferenceUtil.getInstance(getContext()).getLocationLng();
        userAddress = UserPreferenceUtil.getInstance(getContext()).getLocation();

        DataBaseHelper dataBaseHelper = new DataBaseHelper(getContext());
        User user;
        try {
            user = dataBaseHelper.getUserIntegerDao().queryBuilder().queryForFirst();

            Cart cart = new Cart();
            cart.setDishName(order.getDishName());
            cart.setDishPrice(order.getPrice());
            cart.setDishId(order.getDishId());
            cart.setReceiver_name(user.getFirstName() + " " + user.getLastName());
            cart.setTime(order.getDeliveryTime());
            cart.setStatusCode("1");
            cart.setUserNumber(user.getNumber());
            cart.setUserAddress(userAddress);
            cart.setUserId(user.getId());
            cart.setUserLat(userLat);
            cart.setUserLng(userLong);
            cart.setNote("");
            cart.setPromoCode("0");
            cart.setCardNumber("4283432098");
            cart.setResturantId(order.getResturantId());
            cart.setItem_count("1");
            cart.setCurrenyCode(order.getCurrency());
            String addOnIds = "";

            for (int i = 0; i < order.getOptionList().size(); i++) {
                Option op = order.getOptionList().get(i);
                addOnIds += op.getAddOnId() + ",";
            }
            cart.setAddOnId(addOnIds);

            dataBaseHelper.saveCartItem(cart, false);
            if (listener != null) {
                listener.onReOrder();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEditOrder(Order order) {
//        mEditOrder = order;
//        if (mEditOrder.getDishId() != null && !mEditOrder.getDishId().equals("0")) {
//            mViewModel.getDishByID(order.getDishId());
//        } else {
//            mViewModel.getGroceryByID(order.getItemGroceryID());
//        }
//        if (mProgressDialog != null) {
//            mProgressDialog.show();
//        }
    }

    @Override
    public void onCancelOrder(final String orderID) {
//        mDeletedOrder = orderID;
//        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
//        alert.setMessage("Are you sure to cancel this order?");
//        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                dialog.dismiss();
//                //Call cancel api
//                if (mProgressDialog != null) {
//                    mProgressDialog.show();
//                }
//                mViewModel.deleteOrder(orderID);
//
//            }
//        });
//        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                dialog.dismiss();
//            }
//        });
//
//        alert.show();
    }

    @Override
    public void onUpdateSuccess() {
//        if (mProgressDialog != null) {
//            mProgressDialog.dismiss();
//        }
//        if (mUpdateOrderListListener != null) {
//            mUpdateOrderListListener.updateOrderList();
//        }
//        if (mDeletedOrder != null) {
//            List<Order> orders = mOrderAdapter.getmOrderlist();
//            for (Order order : orders) {
//                if (order.getOrderId().contentEquals(mDeletedOrder)) {
//                    orders.remove(order);
//                    break;
//                }
//            }
//            mOrderAdapter.setmOrderlist(orders);
//            mOrderAdapter.notifyDataSetChanged();
//            mDeletedOrder = null;
//        }
    }

    @Override
    public void onUpdateFailure(String message) {
//        if (mProgressDialog != null) {
//            mProgressDialog.dismiss();
//        }
//        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void OnGetResturntDishSuccess(List<Dish> dishList) {
//        if (mProgressDialog != null) {
//            mProgressDialog.dismiss();
//        }
//        getActivity().finish();
//        Intent intent = new Intent(getActivity(), DishDetailActivity.class);
//        intent.putExtra(DishDetailActivity.EXTRA_IS_EDIT_ORDER, true);
//        intent.putExtra(DishDetailActivity.EXTRA_DISH, dishList.get(0));
//        intent.putExtra(DishDetailActivity.EXTRA_EDIT_ORDER, mEditOrder);
//        startActivity(intent);
    }

    @Override
    public void OnGetResturntDishFailed(String error) {
//        if (mProgressDialog != null) {
//            mProgressDialog.dismiss();
//        }
//        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetGrocerySuccess(Grocery grocery) {
//        Intent intent = new Intent(getActivity(), DishDetailActivity.class);
//        intent.putExtra(DishDetailActivity.EXTRA_IS_EDIT_ORDER, true);
//        intent.putExtra(DishDetailActivity.EXTRA_DISH, grocery);
//        intent.putExtra(DishDetailActivity.EXTRA_EDIT_ORDER, mEditOrder);
//        startActivity(intent);
    }

    @Override
    public void onGetGeoceryFailed(String message) {
//        Toast.makeText(getActivity(), "Can not get grocery details", Toast.LENGTH_SHORT).show();
    }


    public interface OnReOrderListener {
        void onReOrder();
    }
}
