package com.o2.plotos.home.resturants;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.dietdetail.GroceryCategoryActivity;
import com.o2.plotos.models.Place;
import com.o2.plotos.models.Restaurant;
import com.o2.plotos.models.Supplier;
import com.o2.plotos.restuarantdetail.RestuarantDetailActivity;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.TimeUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import me.grantland.widget.AutofitTextView;

/**
 * Created by Hassan on 31/01/2017.
 */

public class RestaurantAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//    private OnRestaurantListener mListener;
    private List<Place> mRestaurantList;
    private Context mContext;
    private LayoutInflater mInflater;
    private int mColorRed;
    private int mColorGray;
    CustomFonts customFonts;
    TimeUtil timeUtil;
    public static final int RESTAURANT_TYPE = 0;
    public static final int SUPPLIER_TYPE = 1;

    public RestaurantAdapter(List<Place> restaurant, Context context) {
        mRestaurantList = restaurant;
        mInflater = LayoutInflater.from(context);
        mContext = context;
//        mListener = listener;
        customFonts = new CustomFonts(context);
        timeUtil = new TimeUtil(context);
        mColorRed = ContextCompat.getColor(mContext, R.color.textRed);
        mColorGray = ContextCompat.getColor(mContext, R.color.textMidGray);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_resturant, parent, false);

        switch (viewType) {
            case RESTAURANT_TYPE:
                return new RestaurantViewHolder(v);
            case SUPPLIER_TYPE:
                return new SupplierViewHolder(v);
        }
//        RestaurantViewHolder viewHolder = new RestaurantViewHolder(v);
            return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final Place place = mRestaurantList.get(position);
        if(place != null) {
            if(place instanceof Restaurant){
                ((RestaurantViewHolder)holder).restaurantName.setText(((Restaurant) place).getName());
                ((RestaurantViewHolder)holder).restaurantDescrptn.setText(((Restaurant) place).getResturntDescrptn());
                ((RestaurantViewHolder)holder).restaurantPrice.setText(((Restaurant) place).getStartPrice() + " - " + ((Restaurant) place).getEndPrice());

                if (!((Restaurant) place).getPreBooking().equalsIgnoreCase("0")) {
                    ((RestaurantViewHolder)holder).timerIcon.setImageResource(R.drawable.timer);
                }
                else{
                    ((RestaurantViewHolder)holder).timerIcon.setImageResource(0);
                }

                //if(timeUtil.isPlaceOpened(place.getOpeningTime(), place.getClosingTime())){
                ((RestaurantViewHolder)holder).hideView.setVisibility(View.GONE);
                if (!((Restaurant) place).getPreBooking().equalsIgnoreCase("0")) {
                    ((RestaurantViewHolder)holder).hoursNotice.setText(((Restaurant) place).getPreBooking() +" Hours notice");
                    ((RestaurantViewHolder)holder).hoursNotice.setTextColor(Color.rgb(246,122,64));
                } else {
                    ((RestaurantViewHolder)holder).hoursNotice.setVisibility(View.GONE);
                }
        /*}
        else {
            holder.hideView.setVisibility(View.VISIBLE);
            holder.openCloseTime.setText("Closed");
        }*/

                ((RestaurantViewHolder)holder).openCloseTime.setText(((Restaurant) place).getResturantOpenTime() + " - " + ((Restaurant) place).getResturantCloseTime());

                // set opened/closed color
                if(timeUtil.isPlaceOpened(((Restaurant) place).getOpeningTime(), ((Restaurant) place).getClosingTime())) {
                    ((RestaurantViewHolder)holder).openCloseTime.setTextColor(mColorGray);
                    ((RestaurantViewHolder)holder).closedFrameLayout.setVisibility(View.GONE);
                }
                else {
                    ((RestaurantViewHolder)holder).openCloseTime.setTextColor(mColorRed);
                    ((RestaurantViewHolder)holder).closedFrameLayout.setVisibility(View.VISIBLE);
                }

                customFonts.setOpenSansBold(((RestaurantViewHolder)holder).restaurantName);
                customFonts.setOpenSansBold(((RestaurantViewHolder)holder).closedTextView);
                customFonts.setOpenSansSemiBold(((RestaurantViewHolder)holder).restaurantPrice);
                customFonts.setOpenSansSemiBold(((RestaurantViewHolder)holder).restaurantDescrptn);
                customFonts.setOpenSansSemiBold(((RestaurantViewHolder)holder).openCloseTime);
                customFonts.setOpenSansSemiBold(((RestaurantViewHolder)holder).hoursNotice);


                if (!((Restaurant) place).getImage_url().equalsIgnoreCase("")) {
                    Picasso.with(mContext).load(((Restaurant) place).getImage_url())
                            .resize(400,400)
                            .centerCrop()
                            .error(R.drawable.placeholder)
                            .into(((RestaurantViewHolder)holder).imageView);
                } else {
                    ((RestaurantViewHolder)holder).imageView.setImageResource(R.drawable.placeholder);
                }

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) ((RestaurantViewHolder)holder).cardView.getLayoutParams();
                if( position != getItemCount()-1 ) {
                    //holder.cardView.setPadding(0, 0, 0, 0);
                    params.bottomMargin = 0;
                }
                else {
                    //holder.cardView.setPadding(0, 0, 0, 100);
                    params.bottomMargin = 100;
                }
               // ((RestaurantViewHolder)holder).cardView.setLayoutParams(params);

                ((RestaurantViewHolder)holder).cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        mListener.onRestaurantClick(((Restaurant) place));
                        Intent intent = new Intent(mContext, RestuarantDetailActivity.class);
                        intent.putExtra(RestuarantDetailActivity.EXTRA_RESTURANT, place);
                        mContext.startActivity(intent);
                    }
                });

                ((RestaurantViewHolder)holder).linlay_test.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        mListener.onRestaurantClick(((Restaurant) place));
                        Intent intent = new Intent(mContext, RestuarantDetailActivity.class);
                        intent.putExtra(RestuarantDetailActivity.EXTRA_RESTURANT, place);
                        mContext.startActivity(intent);
                    }
                });

            }else if(place instanceof Supplier){
                ((SupplierViewHolder)holder).restaurantName.setText(((Supplier) place).getSupplier_name());
                ((SupplierViewHolder)holder).restaurantDescrptn.setText(((Supplier) place).getSupplier_description());
                ((SupplierViewHolder)holder).restaurantPrice.setText(((Supplier) place).getStart_price() + " - " + ((Supplier) place).getEnd_price());
                ((SupplierViewHolder)holder).openCloseTime.setText(((Supplier) place).getSupplier_opening_time() + " - " + ((Supplier) place).getSupplier_closing_time());

                // set opened/closed color
                if(timeUtil.isPlaceOpened(((Supplier) place).getOpening_time(), ((Supplier) place).getClosing_time())) {
                    ((SupplierViewHolder)holder).openCloseTime.setTextColor(mColorGray);
                    ((SupplierViewHolder)holder).closedFrameLayout.setVisibility(View.GONE);
                }
                else {
                    ((SupplierViewHolder)holder).openCloseTime.setTextColor(mColorRed);
                    ((SupplierViewHolder)holder).closedFrameLayout.setVisibility(View.VISIBLE);
                }

                if (!((Supplier) place).getPre_booking().equalsIgnoreCase("0")) {
                    ((SupplierViewHolder)holder).timerIcon.setImageResource(R.drawable.timer);
                }
                else{
                    ((SupplierViewHolder)holder).timerIcon.setImageResource(0);
                }

                //if(timeUtil.isPlaceOpened(place.getOpeningTime(), place.getClosingTime())){
                ((SupplierViewHolder)holder).hideView.setVisibility(View.GONE);
                if (!((Supplier) place).getPre_booking().equalsIgnoreCase("0")) {
                    ((SupplierViewHolder)holder).hoursNotice.setText(((Supplier) place).getPre_booking() +" Hours notice");
                    ((SupplierViewHolder)holder).hoursNotice.setTextColor(Color.rgb(246,122,64));
                } else {
                    ((SupplierViewHolder)holder).hoursNotice.setVisibility(View.GONE);
                }

                customFonts.setOpenSansBold(((SupplierViewHolder)holder).restaurantName);
                customFonts.setOpenSansBold(((SupplierViewHolder)holder).closedTextView);
                customFonts.setOpenSansSemiBold(((SupplierViewHolder)holder).restaurantPrice);
                customFonts.setOpenSansSemiBold(((SupplierViewHolder)holder).restaurantDescrptn);
                customFonts.setOpenSansSemiBold(((SupplierViewHolder)holder).openCloseTime);

                if (!((Supplier) place).getImage_url().equalsIgnoreCase("")) {
                    Picasso.with(mContext).load(((Supplier) place).getImage_url())
                            .resize(400,400)
                            .centerCrop()
                            .error(R.drawable.placeholder)
                            .into(((SupplierViewHolder)holder).imageView);
                } else {
                    ((SupplierViewHolder)holder).imageView.setImageResource(R.drawable.placeholder);
                }

                ((SupplierViewHolder)holder).cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, GroceryCategoryActivity.class);
                        intent.putExtra(GroceryCategoryActivity.EXTRA_SUPPLIER, ((Supplier) place).getSupplier_id());
                        mContext.startActivity(intent);
                    }
                });

                ((SupplierViewHolder)holder).linlay_test.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, GroceryCategoryActivity.class);
                        intent.putExtra(GroceryCategoryActivity.EXTRA_SUPPLIER, ((Supplier) place).getSupplier_id());
                        mContext.startActivity(intent);
                    }
                });

            }
        }

    }

    @Override
    public int getItemCount() {
        return mRestaurantList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mRestaurantList != null) {
            Place object = mRestaurantList.get(position);
            if (object instanceof Supplier) {
                return SUPPLIER_TYPE;
            }
        }
        return 0;
    }

    public static class RestaurantViewHolder extends RecyclerView.ViewHolder {
        AutofitTextView restaurantName;
        TextView restaurantDescrptn, restaurantPrice, openCloseTime, hoursNotice, closedTextView;
        ImageView imageView, timerIcon;
        RelativeLayout cardView;
        FrameLayout hideView, closedFrameLayout;
        LinearLayout linlay_test;

        public RestaurantViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.item_restaurant_image);
            timerIcon = (ImageView) v.findViewById(R.id.item_resturant_prebooking);
            restaurantName = (AutofitTextView) v.findViewById(R.id.item_restaurant_textView_name);
            restaurantDescrptn = (TextView) v.findViewById(R.id.item_restaurant_textView_descritption);
            restaurantPrice = (TextView) v.findViewById(R.id.item_restaurant_textView_price);
            cardView = (RelativeLayout) v.findViewById(R.id.item_restaurant_cardView);
            hideView = (FrameLayout) v.findViewById(R.id.resturant_closed);
            openCloseTime = (TextView) v.findViewById(R.id.item_resturant_opening_closing_time);
            hoursNotice = (TextView) v.findViewById(R.id.item_resturant_hours_notice);
            closedFrameLayout = (FrameLayout) v.findViewById(R.id.closed_frameLayout);
            closedTextView = (TextView) v.findViewById(R.id.closed_textView);
            linlay_test = (LinearLayout)v.findViewById(R.id.linlay_test);
        }
    }

    public static class SupplierViewHolder extends RecyclerView.ViewHolder {
        AutofitTextView restaurantName;
        TextView restaurantDescrptn, restaurantPrice, openCloseTime, hoursNotice, closedTextView;
        ImageView imageView, timerIcon;
        CardView cardView;
        FrameLayout hideView, closedFrameLayout;
        LinearLayout linlay_test;
        public SupplierViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.item_restaurant_image);
            timerIcon = (ImageView) v.findViewById(R.id.item_resturant_prebooking);
            restaurantName = (AutofitTextView) v.findViewById(R.id.item_restaurant_textView_name);
            restaurantDescrptn = (TextView) v.findViewById(R.id.item_restaurant_textView_descritption);
            restaurantPrice = (TextView) v.findViewById(R.id.item_restaurant_textView_price);
            cardView = (CardView) v.findViewById(R.id.item_restaurant_cardView);
            hideView = (FrameLayout) v.findViewById(R.id.resturant_closed);
            openCloseTime = (TextView) v.findViewById(R.id.item_resturant_opening_closing_time);
            hoursNotice = (TextView) v.findViewById(R.id.item_resturant_hours_notice);
            closedFrameLayout = (FrameLayout) v.findViewById(R.id.closed_frameLayout);
            closedTextView = (TextView) v.findViewById(R.id.closed_textView);
            linlay_test = (LinearLayout)v.findViewById(R.id.linlay_test);
        }
    }

//    public interface OnRestaurantListener {
//        void onRestaurantClick(Restaurant restaurant);
//    }
}
