package com.o2.plotos.home.profile;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.o2.plotos.R;
import com.o2.plotos.authentication.signin.SignInActivity;
import com.o2.plotos.authentication.signup.SignUpActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.disclaimer.DisclaimerActivity;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.information.InformationActivity;
import com.o2.plotos.models.User;
import com.o2.plotos.payment.PaymentActivity;
import com.o2.plotos.places.DeliveryLocationActivity;
import com.o2.plotos.promo.PromoActivity;
import com.o2.plotos.recentimages.ImageAdapter;
import com.o2.plotos.recentimages.MenuAdapter;
import com.o2.plotos.recentimages.MenuItem;
import com.o2.plotos.recentimages.RecentImages;
import com.o2.plotos.recentimages.RecyclerItemClickListener;
import com.o2.plotos.recentimages.RuntimePermission;
import com.o2.plotos.recentimages.TwoWayAdapterView;
import com.o2.plotos.recentimages.TwoWayGridView;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

public class ProfileFragment extends Fragment {

    @BindView(R.id.fragment_profile_payment_textview)
    TextView payment_textview;
    @BindView(R.id.fragment_profile_name_textview)
    TextView name;
    @BindView(R.id.fragment_profile_image)
    ImageView profilePic;
    @BindView(R.id.fragment_profile_informattion_textview)
    TextView information;
    @BindView(R.id.fragment_profile_promo_textview)
    TextView promo;
    @BindView(R.id.fragment_profile_places_textview)
    TextView places;
    @BindView(R.id.fragment_profile_disclaimer_textview)
    TextView disclaimer;
    @BindView(R.id.fragment_profile_logout_textview)
    TextView logout;

    CustomFonts customFonts;
    @BindView(R.id.fragment_profile_login_textview)
    TextView fragmentProfileLoginTextview;
    @BindView(R.id.img_profile)
    ImageView imgProfile;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    private static Bitmap bitmap1,bitmap2,bitmap3,bitmap4,bitmap5;
    private ArrayList<MenuItem> menuItems = new ArrayList<>();
    private TwoWayGridView gridview;
    private ContentResolver cr;
    private Uri imageUri1,imageUri2,imageUri3,imageUri4,imageUri5;
    private static final int TAKE_PICTURE1 = 1,TAKE_PICTURE2=2,TAKE_PICTURE3=3,TAKE_PICTURE4=4,TAKE_PICTURE5=5;
    private static final int SELECT_PHOTO1 = 6,SELECT_PHOTO2=7,SELECT_PHOTO3=8,SELECT_PHOTO4=9,SELECT_PHOTO5=10;
    private Dialog mBottomSheetDialog;

    public ProfileFragment() {
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);

        customFonts = new CustomFonts(getActivity());
        customFonts.setOswaldBold(name);
        customFonts.setOpenSansRegulr(payment_textview);
        customFonts.setOpenSansRegulr(information);
        customFonts.setOpenSansRegulr(promo);
        customFonts.setOpenSansRegulr(places);
        customFonts.setOpenSansRegulr(disclaimer);
        customFonts.setOpenSansRegulr(logout);

        //   sharedPreferences = getContext().getSharedPreferences(getContext().getPackageName(),REqa)
      String profile=  UserPreferenceUtil.getInstance(getActivity()).getProfile();

        if(!profile.equals(""))
        {
            byte[] b= Base64.decode(profile,Base64.DEFAULT);
            profilePic.setImageBitmap(BitmapFactory.decodeByteArray(b,0,b.length));
        }

        if(menuItems.size()!=0)
        {
            menuItems.clear();
        }

        createBottomsheetDialog();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUserInfo();
    }

    private void setUserInfo() {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(getActivity());
        try {
            List<User> users = dataBaseHelper.getUserIntegerDao().queryForAll();
            if (users.size() > 0) {
                String userName = users.get(0).getFirstName() + " " + users.get(0).getLastName();
                name.setText(userName);
                /*String userImageUrl = users.get(0).getImageUrl();
                if (!userImageUrl.equalsIgnoreCase("")) {
                    Picasso.with(getActivity()).load(userImageUrl).into(profilePic);
                } else {
                    Picasso.with(getActivity()).load(R.drawable.image_placeholder).into(profilePic);
                }*/
                imgProfile.setVisibility(View.VISIBLE);
                logout.setVisibility(View.VISIBLE);
            } else {
                imgProfile.setVisibility(View.GONE);
                fragmentProfileLoginTextview.setVisibility(View.VISIBLE);
                profilePic.setImageResource(R.drawable.image_placeholder);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.fragment_profile_payment_textview)
    public void onClickPaymentTextview() {
        startActivity(new Intent(getActivity(), PaymentActivity.class));
    }

    @OnClick(R.id.fragment_profile_promo_textview)
    public void onClickPromoTextview() {
        startActivity(new Intent(getActivity(), PromoActivity.class));
    }

    @OnClick(R.id.fragment_profile_informattion_textview)
    public void onClickInformationTextview() {
        startActivity(new Intent(getActivity(), InformationActivity.class));
    }

    @OnClick(R.id.fragment_profile_places_textview)
    public void onClickPlacesTextview() {
        startActivity(new Intent(getActivity(), DeliveryLocationActivity.class));
    }

    @OnClick(R.id.fragment_profile_disclaimer_textview)
    public void onClickDisclaimerTextview() {
        startActivity(new Intent(getActivity(), DisclaimerActivity.class));
    }

    @OnClick(R.id.fragment_profile_logout_textview)
    public void onClickLogout() {
//        LoginManager.getInstance().logOut();
        DataBaseHelper dataBaseHelper = new DataBaseHelper(getActivity());
        try {
            if (dataBaseHelper.getUserIntegerDao().deleteBuilder().delete() == 1) {
                dataBaseHelper.deleteAllCartItems();
               // List<User> users = dataBaseHelper.getUserIntegerDao().queryForAll();
                //dataBaseHelper.deleteUser(users.get(0));
                getActivity().startActivity(new Intent(getActivity(), HomeActivity.class));
                getActivity().finish();
                UserPreferenceUtil.getInstance(getActivity()).setUserId("0");
                UserPreferenceUtil.getInstance(getActivity()).setNumber("");

                //UserPreferenceUtil.getInstance(getActivity()).
                //Intercom logout
                Intercom.client().reset();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }


    @OnClick({R.id.img_profile,R.id.fragment_profile_login_textview})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_profile:

                if (Build.VERSION.SDK_INT >= 21) {
                    if (RuntimePermission.checkPermission(getActivity())) {
                        showBottomSheetDialog(imgProfile);

                    }
                    else
                    {
                        RuntimePermission.requestPermission(getActivity());
                    }
                }


                break;
           /* case R.id.fragment_profile_logout_textview:
            SharedPreferences    sharedPref = getContext().getSharedPreferences(UserPreferenceUtil.prefName, Context.MODE_PRIVATE);
                SharedPreferences.Editor  editor = sharedPref.edit();
                editor.clear();

                getActivity().startActivity(new Intent(getActivity(), SignInActivity.class));
                getActivity().finish();
                break;*/
            case R.id.fragment_profile_login_textview:
                getActivity().startActivity(new Intent(getActivity(), SignInActivity.class));
                getActivity().finish();
                break;
        }
    }

    private void createBottomsheetDialog( ) {
        final View bottomSheet = getActivity().getLayoutInflater().inflate(R.layout.bottom_sheet, null);
        //image = (ImageView) findViewById(R.id.imageView);
        gridview = (TwoWayGridView) bottomSheet.findViewById(R.id.gridview);


        mBottomSheetDialog = new Dialog(getContext(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(bottomSheet);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);

        menuItems.add(new MenuItem("Camera", R.drawable.ic_local_see_black_48dp));
        menuItems.add(new MenuItem("Gallery", R.drawable.ic_action_image));
       // menuItems.add(new MenuItem("Remove", R.drawable.ic_delete));

        cr = getContext().getContentResolver();

        final RecentImages ri = new RecentImages();
        final ImageAdapter adapter = ri.getAdapter(getContext());

        RecyclerView menu = (RecyclerView) bottomSheet.findViewById(R.id.menu);
        MenuAdapter menuAdapter = new MenuAdapter(menuItems);
        menu.setLayoutManager(new LinearLayoutManager(getContext()));
        menu.setAdapter(menuAdapter);

        menu.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), menu, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int i) {
                if (i == 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        if (RuntimePermission.checkPermissionForCamera(getActivity())) {

                            takePhoto(TAKE_PICTURE1);
                            mBottomSheetDialog.dismiss();
                        } else {
                            RuntimePermission.requestPermissionForCamera(getActivity());

                        }

                    } else {
                        takePhoto(TAKE_PICTURE1);
                        mBottomSheetDialog.dismiss();
                    }


                } else if (i == 1) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, SELECT_PHOTO1);
                    mBottomSheetDialog.dismiss();

                } /*else if (i == 2) {
                    //imgProfile.setImageResource(R.drawable.test);
                  //  bitmap = ((BitmapDrawable) imgProfile.getDrawable()).getBitmap();
                  //  mBottomSheetDialog.dismiss();
                }*/
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));



    }

    private void showBottomSheetDialog(final ImageView imgProfile ) {
        mBottomSheetDialog.show();

        final RecentImages ri2 = new RecentImages();
        ImageAdapter adapter2 = ri2.getAdapter(getContext());

        gridview.setAdapter(adapter2);
        gridview.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            public void onItemClick(TwoWayAdapterView parent, View v, int position, long id) {


                Uri imageUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);

                try {
                    Bitmap  bitmap = decodeUri(getContext(), imageUri);
                    profilePic.setImageBitmap(bitmap);
                    bitmapToString(bitmap);
                    //imgRemove.setImageResource(R.drawable.ic_remove_photo);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                mBottomSheetDialog.dismiss();
            }
        });
    }

    public void takePhoto(int TAKE_PICTURE) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        String name = String.valueOf(System.currentTimeMillis() + ".jpg");
        File photo = new File(String.valueOf(Environment.getExternalStorageDirectory()), name);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        if(TAKE_PICTURE==1)
        {
            imageUri1 = Uri.fromFile(photo);
        }

        startActivityForResult(intent, TAKE_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == TAKE_PICTURE1) {


                if (imageUri1 != null) {

                    try {
                        Log.d("ImageURI", String.valueOf(imageUri1));
                        bitmap1 = decodeUri(getContext(), imageUri1);

                        profilePic.setImageBitmap(bitmap1);

                        bitmapToString(bitmap1);

                        //imgRemove1.setImageResource(R.drawable.ic_remove_photo);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            // Utility.redirectToActivity(AddGroupActivity.this, MainActivity.class, context, true, null);


        }


        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PHOTO1) {

                imageUri1 = data.getData();
                if (imageUri1 != null) {

                    try {
                        //Log.d("ImageURI", String.valueOf(imageUri));
                        bitmap1 = decodeUri(getContext(), imageUri1);
                        profilePic.setImageBitmap(bitmap1);
                        bitmapToString(bitmap1);
                       // imgRemove1.setImageResource(R.drawable.ic_remove_photo);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            // Utility.redirectToActivity(AddGroupActivity.this, MainActivity.class, context, true, null);


        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getContext(), "Thanks!",
                            Toast.LENGTH_SHORT).show();
                } else {
                    //If user denies Storage Permission, explain why permission is needed and prompt again.
                    Toast.makeText(getContext(), "Storage access is needed to display images.",
                            Toast.LENGTH_SHORT).show();
                    RuntimePermission.checkPermissions(getActivity());
                }
                break;


            case 123:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //takePhoto();
                }
                break;

            default:
                break;
        }
    }


    public static Bitmap decodeUri(Context context, Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o);
        final int REQUIRED_SIZE = 200;
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o2);


    }

    public  String bitmapToString(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        String encode = Base64.encodeToString(b,Base64.DEFAULT);
        UserPreferenceUtil.getInstance(getActivity()).setprofilepic(encode);

        return encode;



    }



}
