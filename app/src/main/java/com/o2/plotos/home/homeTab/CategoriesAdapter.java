package com.o2.plotos.home.homeTab;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.home.resturants.GroceryDetailAdp;
import com.o2.plotos.home.resturants.GroceryDishDetailAdp;
import com.o2.plotos.restapi.newApis.models.grocery.DishResponse;
import com.o2.plotos.restapi.newApis.models.plotosProgram.PlotosResult;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 2/14/2018.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.SupplierHolder> {

    private ArrayList<PlotosResult> list;
    private Context context;
    private ClickListnerOfGrocerySupplier listner;



    public interface ClickListnerOfGrocerySupplier {
        void onClickOfArror(int pos);
    }

    public CategoriesAdapter(ArrayList<PlotosResult> list, Context context, ClickListnerOfGrocerySupplier listner) {
        this.list = list;
        this.context = context;
        this.listner = listner;

    }

    @Override
    public SupplierHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_categories_list, parent, false);
        return new SupplierHolder(view);
    }

    @Override
    public void onBindViewHolder(SupplierHolder holder, int position) {
        holder.txt_category.setText(list.get(position).getProgram_name().toString().trim());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SupplierHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_category)
        TextView txt_category;


        public SupplierHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.txt_category)
        public void onViewClicked() {
            listner.onClickOfArror(getAdapterPosition());
        }
    }

}