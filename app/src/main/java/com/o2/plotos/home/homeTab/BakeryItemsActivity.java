package com.o2.plotos.home.homeTab;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.o2.plotos.R;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;

public class BakeryItemsActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bakery_items);
        ButterKnife.bind(this);
    }
    @OnClick({R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }

}
