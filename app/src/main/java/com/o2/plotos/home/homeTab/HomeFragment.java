package com.o2.plotos.home.homeTab;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.BaseFragment;
import com.o2.plotos.R;
import com.o2.plotos.dietdetail.GroceryCategoryActivity;
import com.o2.plotos.dietdetail.LowCrabActivity;
import com.o2.plotos.dietdetail.WithoutSpinnerActivity;
import com.o2.plotos.dietdetail.detox.DetoxResturntActivity;
import com.o2.plotos.home.BannerViewModel;
import com.o2.plotos.home.HeaderPagerAdapter;
import com.o2.plotos.models.Diet;
import com.o2.plotos.places.DeliveryLocationActivity;
import com.o2.plotos.places.DeliveryTimeActivity;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.bannerImages.BannerImageResponse;
import com.o2.plotos.restapi.newApis.models.bannerImages.BannerImageResult;
import com.o2.plotos.restapi.newApis.models.home.CategoryResult;
import com.o2.plotos.restapi.newApis.models.home.HomeModel;
import com.o2.plotos.restapi.newApis.models.home.NearbyResult;
import com.o2.plotos.restapi.newApis.models.home.NewRestResult;
import com.o2.plotos.restuarantdetail.ResturantDetailsActivity;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;
import com.viewpagerindicator.LinePageIndicator;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends BaseFragment implements HomeAdapter.OnDietClickListener, HomeCatAdp.ClickListnerOfCat, HomeNearByAdp.ClickListnerOfNearby, HomeNewPlotosAdp.ClickListnerOfNewOnPlotos {
    @BindView(R.id.fragment_diet_recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.fragment_diet_resturntofday)
    TextView resturntOfDay;
    @BindView(R.id.fragment_diet_viewpager)
    ViewPager mViewPager;
    CustomFonts customFonts;
    HeaderPagerAdapter mHeaderPagerAdapter;
    Timer timer;
    int page = 0;
    @BindView(R.id.rv_nearby)
    RecyclerView rvNearby;
    @BindView(R.id.rv_new_plotos)
    RecyclerView rvNewPlotos;
    @BindView(R.id.nearby_card)
    CardView nearbyCard;
    @BindView(R.id.newon_card)
    CardView newonCard;
    private long lastClickTime = 0;
    BannerViewModel mBannerViewModel;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.linlay_delivery)
    LinearLayout linlayDelivery;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.linlay_delivery_time)
    LinearLayout linlayDeliveryTime;
    @BindView(R.id.llnearby)
    LinearLayout llnearby;
    @BindView(R.id.llnewplotos)
    LinearLayout llnewplotos;
    @BindView(R.id.img_filter)
    ImageView imgFilter;
    LinePageIndicator indicator;

    ArrayList<BannerImageResult> bannerImageResults = new ArrayList<>();
    ArrayList<CategoryResult> categoryResults = new ArrayList<>();
    ArrayList<NearbyResult> nearByResults = new ArrayList<>();
    ArrayList<NewRestResult> newRestResults = new ArrayList<>();

    HomeCatAdp homeCatAdp;
    HomeNearByAdp homeNearByAdp;
    HomeNewPlotosAdp homeNewPlotosAdp;

    static boolean showDialog = false;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        setLocationPreference();
        setUserPreferenceDate();
        makeDietList();
        if (Utils.isNetworkAvailable(getContext())) {
            callBannerApi();
            callHomeListingApi();
        }
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        makeDietList();
        customFonts = new CustomFonts(getActivity());
        customFonts.setOswaldBold(resturntOfDay);
        indicator = (LinePageIndicator) view.findViewById(R.id.tutorial_pager_indicator);
//        mBannerViewModel = new BannerViewModel(this);
//        mBannerViewModel.getBanners();

        homeCatAdp = new HomeCatAdp(categoryResults, getContext(), this);
        recyclerView.setAdapter(homeCatAdp);

        homeNearByAdp = new HomeNearByAdp(nearByResults, getContext(), this);
        rvNearby.setAdapter(homeNearByAdp);

        homeNewPlotosAdp = new HomeNewPlotosAdp(newRestResults, getContext(), this);
        rvNewPlotos.setAdapter(homeNewPlotosAdp);

        if (!showDialog) {
            showDialog();
            showDialog = true;
        }
        return view;
    }

    private void callHomeListingApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<HomeModel> call = apiInterface.callHomeList("0", "1", UserPreferenceUtil.getInstance(getContext()).getLocationLat(), UserPreferenceUtil.getInstance(getContext()).getLocationLng());
        call.enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    categoryResults.clear();
                    nearByResults.clear();
                    newRestResults.clear();
                    if (response.body().getResult() != null) {
                        if (response.body().getResult().getCategory() != null && response.body().getResult().getCategory().size() > 0) {
                            categoryResults.addAll(response.body().getResult().getCategory());
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            homeCatAdp.notifyDataSetChanged();
                        }

                        if (response.body().getResult().getNear_restaurent() != null && response.body().getResult().getNear_restaurent().size() > 0) {
//                            nearbyCard.setVisibility(View.VISIBLE);
                            nearByResults.addAll(response.body().getResult().getNear_restaurent());
//                            pushHorizontalList();
                            CategoryResult categoryResult=new CategoryResult();
                            categoryResult.setTypeView(1);
                            categoryResult.getNearByResults().addAll(nearByResults);
                            if (categoryResults.size()>0) {
                                categoryResults.add(1, categoryResult);
                            }else{
                                categoryResults.add(0, categoryResult);
                            }
                            homeCatAdp.notifyItemChanged(1);

//                            rvNearby.setHasFixedSize(true);
//                            rvNearby.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
//                            homeNearByAdp.notifyDataSetChanged();
                        }

                        if (response.body().getResult().getNew_restaurent() != null && response.body().getResult().getNew_restaurent().size() > 0) {
//                            newonCard.setVisibility(View.VISIBLE);
                            newRestResults.addAll(response.body().getResult().getNew_restaurent());
                            CategoryResult categoryResult=new CategoryResult();
                            categoryResult.setTypeView(2);
                            categoryResult.getNewRestResults().addAll(newRestResults);
                            if (categoryResults.size()>1) {
                                categoryResults.add(2, categoryResult);
                            }else{
                                categoryResults.add(0, categoryResult);
                            }
                            homeCatAdp.notifyItemChanged(2);
//                            rvNewPlotos.setHasFixedSize(true);
//                            rvNewPlotos.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
//                            homeNearByAdp.notifyDataSetChanged();
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                hideProgress();
            }
        });
    }

    private void pushHorizontalList() {
        int catSize=categoryResults.size();
        CategoryResult categoryResult=new CategoryResult();
        categoryResult.setTypeView(1);
        categoryResults.add(2,categoryResult);

        CategoryResult categoryResult1=new CategoryResult();
        categoryResult1.setTypeView(2);
        categoryResults.add(3,categoryResult1);

    }

    private void callBannerApi() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BannerImageResponse> call = apiInterface.callGetBanner(UserPreferenceUtil.getInstance(getContext()).getLocationLat(), UserPreferenceUtil.getInstance(getContext()).getLocationLng());
        call.enqueue(new Callback<BannerImageResponse>() {
            @Override
            public void onResponse(Call<BannerImageResponse> call, Response<BannerImageResponse> response) {
                if (response.code() == 200) {
                    if (response.body().getResponse().equalsIgnoreCase("0")) {
                        bannerImageResults.clear();
                        bannerImageResults.addAll(response.body().getBannerImageResults());
                        for (int x = bannerImageResults.size() - 1; x >= 0; x--) {
                            if (bannerImageResults.get(x).getType().equals("groceryitem") || bannerImageResults.get(x).getType().equals("supplier")) {
                                bannerImageResults.remove(x);
                            }
                        }

                        mHeaderPagerAdapter = new HeaderPagerAdapter(getActivity(), bannerImageResults);
                        mViewPager.setAdapter(mHeaderPagerAdapter);
                        //  indicator.setSnap(true);
                        indicator.setViewPager(mViewPager);
                        indicator.setSelectedColor(Color.parseColor("#ffffff"));
                        indicator.setGapWidth(10f);
                        indicator.setLineWidth(80f);
                        // indicator.setRadius(Utility.getPxFromDp(context, 5.83f));
                        // indicator.setFillColor(Color.parseColor("#ffffff"));
                        // indicator.setPageColor(Color.parseColor("#80ffffff"));
                        indicator.setStrokeWidth(16f);

                        pageSwitcher(10);
                    }
                }
            }

            @Override
            public void onFailure(Call<BannerImageResponse> call, Throwable t) {
            }
        });
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_location);
        dialog.getWindow().setGravity(Gravity.TOP | Gravity.CENTER);
        WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
//        layoutParams.x = 100; // left margin
        layoutParams.y = 200; // bottom margin
        dialog.getWindow().setAttributes(layoutParams);
        TextView txt_address = (TextView) dialog.findViewById(R.id.txt_address);
        if (UserPreferenceUtil.getInstance(getContext()).getCurrentLocation() != null) {
            txt_address.setText(UserPreferenceUtil.getInstance(getContext()).getCurrentLocation());
        }
        TextView txt_changelocation = (TextView) dialog.findViewById(R.id.txt_changelocation);
        txt_changelocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(getContext(), DeliveryLocationActivity.class);
                startActivity(intent);
            }
        });
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }


    public void updateLocation() {
        setLocationPreference();
    }

    /**
     * Make Default Diet list
     */
    private void makeDietList() {
        setLocationPreference();
        List<Diet> dietList = new ArrayList<>();

//        Diet diet1 = new Diet();
//        diet1.setName("Calorie Controlled");
//        diet1.setImageId(R.drawable.calorie_controlled);
        //dietList.add(diet1);

        Diet diet2 = new Diet();
        diet2.setName("Healthy & Balanced");
        diet2.setImageId(R.drawable.healthy_balance);
        dietList.add(diet2);

        Diet diet9 = new Diet();
        diet9.setName("Boutique Grocery");
        diet9.setImageId(R.drawable.healthy_grocery);
        dietList.add(diet9);

        Diet diet4 = new Diet();
        diet4.setName("Paleo");
        diet4.setImageId(R.drawable.paleo);
        dietList.add(diet4);

        Diet diet5 = new Diet();
        diet5.setName("Vegan");
        diet5.setImageId(R.drawable.nutri_vegan);
        dietList.add(diet5);

        Diet diet6 = new Diet();
        diet6.setName("Vegetarian");
        diet6.setImageId(R.drawable.nutri_vegetarian);
        dietList.add(diet6);

        Diet diet7 = new Diet();
        diet7.setName("Wheat Free");
        diet7.setImageId(R.drawable.wheat_free);
        dietList.add(diet7);

        Diet diet8 = new Diet();
        diet8.setName("Low Carb");
        diet8.setImageId(R.drawable.nutri_low_carb);
        dietList.add(diet8);

        Diet diet3 = new Diet();
        diet3.setImageId(R.drawable.detox);
        diet3.setName("Detox");
        dietList.add(diet3);

        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(gridLayoutManager);
//        HomeAdapter homeAdapter = new HomeAdapter(getContext(), dietList, this);
//        recyclerView.setAdapter(homeAdapter);
    }


    /**
     * Update location from shared preference
     */
    private void setLocationPreference() {
        String text = "<font color=#00000>Delivering to</font> <font color=#4AB173>" + UserPreferenceUtil.getInstance(getActivity()).getLocation() + "</font>";
        txtAddress.setText(Html.fromHtml(text));
    }

    public void pageSwitcher(int seconds) {
        timer = new Timer();
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000);
    }

    @Override
    public void onDietClick(Diet diet, int postn, String name) {
        if (postn == 1) { // Grocery
            Intent intent = new Intent(getActivity(), GroceryCategoryActivity.class);
            startActivity(intent);
        }
//        else if (postn == 8) {
//            Intent intent = new Intent(getActivity(), CalorieControlledActivity.class);
//            intent.putExtra("catName", name);
//            intent.putExtra("cat_Id", "" + (postn + 2));
//            startActivity(intent);
//            //getActivity().overridePendingTransition(R.anim.slide_up, android.R.anim.fade_out);
//        }
        else if (postn == 7) {
            Intent intent = new Intent(getActivity(), DetoxResturntActivity.class);
            intent.putExtra("catName", name);
            intent.putExtra("cat_Id", "" + (postn + 2));
            startActivity(intent);
            //getActivity().overridePendingTransition(R.anim.slide_up, android.R.anim.fade_out);

        } else if (postn == 6) {
            Intent intent = new Intent(getActivity(), LowCrabActivity.class);
            intent.putExtra("catName", name);
            intent.putExtra("cat_Id", "8");
            startActivity(intent);
            //getActivity().overridePendingTransition(R.anim.slide_up, android.R.anim.fade_out);

        } else {

            Intent intent = new Intent(getActivity(), WithoutSpinnerActivity.class);
            intent.putExtra("catName", name);
            intent.putExtra("cat_Id", "" + (postn + 2));
            startActivity(intent);
            //getActivity().overridePendingTransition(R.anim.slide_up, android.R.anim.fade_out);

        }
    }

//    @Override
//    public void onGetBannerSuccessfully(List<Banner> bannerList) {
//        if (mHeaderPagerAdapter != null) {
////            mHeaderPagerAdapter.setBanners(bannerList);
//            mHeaderPagerAdapter.notifyDataSetChanged();
//        }
//    }

    @OnClick({R.id.linlay_delivery, R.id.linlay_delivery_time, R.id.llnearby, R.id.llnewplotos})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linlay_delivery:
                startActivity(new Intent(getActivity(), DeliveryLocationActivity.class));
                break;
            case R.id.linlay_delivery_time:
                Intent intent2 = new Intent(getActivity(), DeliveryTimeActivity.class);
                startActivity(intent2);
                break;
            case R.id.llnearby:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constant.NEARBYDETAILS, Parcels.wrap(nearByResults));
                bundle.putString("title", "Near by Restaurants");
                bundle.putString("from", "home");
                Intent intent = new Intent(getContext(), NearByActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.llnewplotos:
                Bundle bundles = new Bundle();
                bundles.putParcelable(Constant.NEWPLOTOSDETAILS, Parcels.wrap(newRestResults));
                bundles.putString("title", "New on Plotos");
                bundles.putString("from", "home");
                Intent inten = new Intent(getContext(), NearByActivity.class);
                inten.putExtras(bundles);
                startActivity(inten);
                break;
        }
    }

    /**
     * Set the user preference
     */
    private void setUserPreferenceDate() {

        String user_id = UserPreferenceUtil.getInstance(getApplicationContext()).UserId();


        {
            if (UserPreferenceUtil.getInstance(getActivity()).getAsSoonCheck()) {
                txtTime.setText("ASAP");
            } else {
                txtTime.setText(UserPreferenceUtil.getInstance(getActivity()).getDeliveryDate());

            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @OnClick(R.id.img_filter)
    public void onViewClicked() {
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();

        Intent i = new Intent(getActivity(), CategoriesActivity.class);
        startActivity(i);


       /* imgFilter.setEnabled(false);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(getActivity(), CategoriesActivity.class);
                startActivity(i);
                // This method will be executed once the timer is over
                imgFilter.setEnabled(true);
                Log.d("tete", "resend1");

            }
        }, 600);// set time as per your requirement
*/


    }

    @Override
    public void onClickOfDishName(int pos) {
        Bundle bundles = new Bundle();
        bundles.putParcelable(Constant.CATEGORYDETAILS, Parcels.wrap(categoryResults.get(pos)));
        Intent intent = new Intent(getContext(), CategoriesDetialActivity.class);
        intent.putExtras(bundles);
        startActivity(intent);
    }

    @Override
    public void onClickNearBy(int pos) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.NEARBYDETAILS, Parcels.wrap(nearByResults));
        bundle.putString("title", "Near by Restaurants");
        bundle.putString("from", "home");
        Intent intent = new Intent(getContext(), NearByActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onClickNewPlotos(int pos) {
        Bundle bundles = new Bundle();
        bundles.putParcelable(Constant.NEWPLOTOSDETAILS, Parcels.wrap(newRestResults));
        bundles.putString("title", "New on Plotos");
        bundles.putString("from", "home");
        Intent inten = new Intent(getContext(), NearByActivity.class);
        inten.putExtras(bundles);
        startActivity(inten);
    }

    @Override
    public void onClickOfNearBy(int pos) {

    }

    @Override
    public void onClickOfNewOnPlotos(int pos) {

    }


    class RemindTask extends TimerTask {

        @Override
        public void run() {

            try {

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {

                        if (page > 3) {
                            timer = new Timer();
                            page = 0;
                            mViewPager.setCurrentItem(0);
                        } else {
                            mViewPager.setCurrentItem(page++);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
