package com.o2.plotos.home.resturants;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.home.order.DeliverToActivity;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.grocery.Category_list;
import com.o2.plotos.restapi.newApis.models.grocery.SupplierResponse;
import com.o2.plotos.restapi.newApis.models.supplierDetails.SuppliersDetailsResponse;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroceryCategoryDetailActivity extends BaseActivity implements StaggeredAdp.StaggerdItemClickListner {

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.rv_grocery_cat)
    RecyclerView rv_grocery_cat;


    StaggeredGridLayoutManager gaggeredGridLayoutManager;

    SupplierResponse supplierResponse;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.iv_rest_image)
    ImageView ivRestImage;
    @BindView(R.id.tv_rest_name)
    TextView tvRestName;
    @BindView(R.id.tvPriceTime)
    TextView tvPriceTime;
    @BindView(R.id.tv_rest_detail)
    TextView tvRestDetail;
    @BindView(R.id.img_banner)
    ImageView imgBanner;
    private String supplier_id;
    private StaggeredAdp staggeredAdp;
    private List<Category_list> category_lists;

    @BindView(R.id.cartLayout)
    RelativeLayout cartLayout;
    @BindView(R.id.tvItemCount)
    TextView tvItemCount;
    @BindView(R.id.tvTotal)
    TextView tvTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_category_detail);
        ButterKnife.bind(this);
        bindAdp();
        if (getIntent().getExtras() != null) {
            if (!TextUtils.isEmpty(getIntent().getStringExtra(Constant.SUPPLIER_ID))) {
                supplier_id = getIntent().getStringExtra(Constant.SUPPLIER_ID);
                getSupplierDetailsApi(supplier_id);

            } else if (getIntent().getParcelableExtra(Constant.GROCERYSUPPERLIERDETAILS) != null) {
                supplierResponse = Parcels.unwrap(getIntent().getParcelableExtra(Constant.GROCERYSUPPERLIERDETAILS));
                setData();

            }
        }
        setCartFab();
    }

    private void bindAdp() {
        category_lists = new ArrayList<>();
        staggeredAdp = new StaggeredAdp(category_lists, this, this);
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        rv_grocery_cat.setLayoutManager(gaggeredGridLayoutManager);
        rv_grocery_cat.setAdapter(staggeredAdp);
    }

    private void getSupplierDetailsApi(String id) {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SuppliersDetailsResponse> call = apiInterface.getSupplierDetails(id);
        call.enqueue(new Callback<SuppliersDetailsResponse>() {
            @Override
            public void onResponse(Call<SuppliersDetailsResponse> call, Response<SuppliersDetailsResponse> response) {
                hideProgress();
                supplierResponse = response.body().getSupplierData();
                setData();
            }

            @Override
            public void onFailure(Call<SuppliersDetailsResponse> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }

    private void setData() {
        tvRestName.setText(supplierResponse.getSupplier_name());
        tvTitle.setText(supplierResponse.getSupplier_name());
        if (supplierResponse.getImage() != null && !supplierResponse.getImage().equals("")) {
            Picasso.with(this).load(supplierResponse.getImage())
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(ivRestImage);
        } else {
            ivRestImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.placeholder));
        }
        if (supplierResponse.getBanner_image() != null && !supplierResponse.getBanner_image().equals("")) {
            Picasso.with(this).load(supplierResponse.getBanner_image())
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(imgBanner);
        } else {
            imgBanner.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.placeholder));
        }
        tvRestDetail.setText(supplierResponse.getSupplier_description());
        tvPriceTime.setText(supplierResponse.getPre_booking() + " hours "
                + "| Min Order " + supplierResponse.getMin_order_amount() + " | Open at " +
                supplierResponse.getOpening_time() + " - " +
                supplierResponse.getClosing_time()
        );
        category_lists.clear();
        category_lists.addAll(supplierResponse.getCategory_list());
        staggeredAdp.notifyDataSetChanged();
    }

    private void setCartFab() {
        int cartItemSize = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST).size();
        if (cartItemSize > 0) {
            cartLayout.setVisibility(View.VISIBLE);
            tvItemCount.setText("" + cartItemSize);
            makeList();
        } else {
            cartLayout.setVisibility(View.GONE);
        }
    }

    private void makeList() {
        Double tootal = 0.00;
        List<ItemDetais> listOfCartItems = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST);
        for (int i = 0; i < listOfCartItems.size(); i++) {
            ItemDetais itemDetais = listOfCartItems.get(i);
            tootal = tootal + (itemDetais.getQuantity() * itemDetais.getBasicPrice());
        }
        tvTotal.setText(tootal + " AED");
    }

    @OnClick({R.id.img_back, R.id.cartLayout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.cartLayout:
                startActivity(new Intent(this, DeliverToActivity.class));
                break;
        }
    }

    @Override
    public void onStaggerdItemClick(int pos) {
        Intent intent = new Intent(this, GroceryDetailActivity.class);
        intent.putExtra("from", "category");
        intent.putExtra("rest_name", supplierResponse.getCategory_list().get(pos).getName());
        intent.putExtra("mainid", supplierResponse.getCategory_list().get(pos).getMain_id());
        startActivity(intent);
    }
}
