package com.o2.plotos.home.resturants;

import com.o2.plotos.models.Supplier;

import java.util.List;

/**
 * Created by Rania on 5/9/2017.
 */

public interface SupplierListener {
    void onGetSupplierSuccess(List<Supplier> supplierList);
    void onGetSupplierFailed(String error);
}
