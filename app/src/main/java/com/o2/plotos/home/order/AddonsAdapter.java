package com.o2.plotos.home.order;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.models.dishdetails.AddOns;
import com.o2.plotos.restapi.newApis.models.orders.AddonsResult;
import com.o2.plotos.restapi.newApis.models.orders.ItemOrders;
import com.o2.plotos.utils.OpensanBoldTextview;
import com.o2.plotos.utils.OpensanTextview;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddonsAdapter extends RecyclerView.Adapter<AddonsAdapter.OrderViewHolder> {


    private List<AddOns> mOrderlist;
    private LayoutInflater mInflater;
    Context mContext;

    public AddonsAdapter(Context context, List<AddOns> orderList) {
        mInflater = LayoutInflater.from(context);
        mOrderlist = orderList;
        mContext = context;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_addons_new, parent, false);
        OrderViewHolder orderViewHolder = new OrderViewHolder(v);

        return orderViewHolder;
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, int position) {


        /*if (mOrderlist.get(position).getAddon() != null
                && mOrderlist.get(position).getAddon().size() > 0) {
            for (int i = 0; i < mOrderlist.get(position).getAddon().size(); i++) {
                holder.itemName.setText(mOrderlist.get(position).getAddon().get(i).getName());
                holder.price.setText(mOrderlist.get(position).getAddon().get(i).getPrice());
            }
        }*/

        holder.itemName.setText(mOrderlist.get(position).getName());
        holder.price.setText(mOrderlist.get(position).getPrice() + " AED");


    }

    @Override
    public int getItemCount() {
        return mOrderlist.size();
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemName)
        OpensanTextview itemName;
        @BindView(R.id.price)
        OpensanBoldTextview price;

        public OrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
