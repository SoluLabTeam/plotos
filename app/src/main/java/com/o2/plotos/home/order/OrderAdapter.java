package com.o2.plotos.home.order;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.models.Order;
import com.o2.plotos.restapi.newApis.models.dishdetails.AddOns;
import com.o2.plotos.restapi.newApis.models.orders.AddonsResult;
import com.o2.plotos.restapi.newApis.models.orders.PastResult;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.OpensanTextview;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hassan on 2/02/2017.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {


    private OnOrderListener mOrderListener;
    private List<PastResult> mOrderlist;
    private LayoutInflater mInflater;
    private Order mOrder;
    CustomFonts customFonts;
    Context mContext;
    private OrderActionsListener mOrderActionsListener;

    public OrderAdapter(Context context, List<PastResult> orderList, OnOrderListener orderListener, OrderActionsListener orderActionsListener) {
        mInflater = LayoutInflater.from(context);
        mOrderlist = orderList;
        mOrderListener = orderListener;
        mContext = context;
        customFonts = new CustomFonts(context);
        mOrderActionsListener = orderActionsListener;
    }

//    public List<Order> getmOrderlist() {
//        return mOrderlist;
//    }
//
//    public void setmOrderlist(List<Order> mOrderlist) {
//        this.mOrderlist = mOrderlist;
//    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_order, parent, false);
        OrderViewHolder orderViewHolder = new OrderViewHolder(v);
        return orderViewHolder;
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, int position) {
        holder.restName.setText(mOrderlist.get(position).getRestaurant_name());
        if (mOrderlist.get(position).getStatus() != null) {
            String status = mOrderlist.get(position).getStatus();
            if (!status.equals("")) {
                if (status.equals("1")) {
                    holder.ivRecRest.setImageResource(R.drawable.check);
                } else if (status.equals("2")) {
                    holder.ivPrep.setImageResource(R.drawable.check);
                } else if (status.equals("3")) {
                    holder.ivOnWay.setImageResource(R.drawable.check);
                } else if (status.equals("4")) {
                    holder.ivDelivered.setImageResource(R.drawable.check);
                }
            }
        }
        double price = 0;
        for (int i = 0; i < mOrderlist.get(position).getChild().size(); i++) {
            price = price + Double.parseDouble(mOrderlist.get(position).getChild().get(i).getPrice());
        }
        holder.tvTotal.setText(price + " AED");
        StringBuilder instructions = new StringBuilder();
        for (int i = 0; i < mOrderlist.get(position).getChild().size(); i++) {
            if (!mOrderlist.get(position).getChild().get(i).getSpecial_note().equals("")) {
                instructions.append(mOrderlist.get(position).getChild().get(i).getSpecial_note()).append(",");
            }
        }
        holder.tvInstructions.setText(instructions.toString());
        if (mOrderlist.get(position).getScheduled_date() != null && !mOrderlist.get(position).getScheduled_date().equals("")) {
            getDate(mOrderlist.get(position).getScheduled_date(), holder.tvDate, holder.tvMonth);
        }
        holder.rvDishes.setHasFixedSize(true);
        holder.rvDishes.setLayoutManager(new LinearLayoutManager(mContext));
        PastOrderDetailAdapter pastOrderDetailAdapter = new PastOrderDetailAdapter(mContext, mOrderlist.get(position).getChild());
        holder.rvDishes.setAdapter(pastOrderDetailAdapter);
        List<AddOns> addonsResults = new ArrayList<>();
        for (int i = 0; i < mOrderlist.get(position).getChild().size(); i++) {
            if (mOrderlist.get(position).getChild().get(i).getList_addons() != null) {
                for (int j = 0; j < mOrderlist.get(position).getChild().get(i).getList_addons().size(); j++) {
                    addonsResults.addAll(mOrderlist.get(position).getChild().get(i).getList_addons());
                }
            }
        }
        holder.rv_addons.setHasFixedSize(true);
        holder.rv_addons.setLayoutManager(new LinearLayoutManager(mContext));
        AddonsAdapter addonsAdapter = new AddonsAdapter(mContext,addonsResults);
        holder.rv_addons.setAdapter(addonsAdapter);

    }

    @Override
    public int getItemCount() {
        return mOrderlist.size();
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.restName)
        OpensanTextview restName;
        @BindView(R.id.tvPrepTime)
        TextView tvPrepTime;
        @BindView(R.id.tvTotal)
        TextView tvTotal;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvMonth)
        TextView tvMonth;
        @BindView(R.id.tvInstructions)
        TextView tvInstructions;
        @BindView(R.id.rv_dishes)
        RecyclerView rvDishes;
        @BindView(R.id.rv_addons)
        RecyclerView rv_addons;
        @BindView(R.id.ivRecRest)
        ImageView ivRecRest;
        @BindView(R.id.ivPrep)
        ImageView ivPrep;
        @BindView(R.id.ivOnWay)
        ImageView ivOnWay;
        @BindView(R.id.ivDelivered)
        ImageView ivDelivered;

        public OrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void getDate(String date, TextView tvdate, TextView tvmonth) {
        String oldFormat = "yyyy-MM-dd";
        String newFormat = "dd-MMM";
        String formatedDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat, Locale.US);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);
            SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat, Locale.US);
            formatedDate = timeFormat.format(myDate);
            tvdate.setText(formatedDate.substring(0, 2));
            tvmonth.setText(formatedDate.substring(formatedDate.length() - 3));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
    }

    public interface OnOrderListener {
        void onOrderClick(Order order);
    }

}
