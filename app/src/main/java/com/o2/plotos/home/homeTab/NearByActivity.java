package com.o2.plotos.home.homeTab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.adapters.PlotosSignatureAdp;
import com.o2.plotos.home.order.DeliverToActivity;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.NearByRestaurant.NearByResponse;
import com.o2.plotos.restapi.newApis.models.home.DishDetail;
import com.o2.plotos.restapi.newApis.models.home.NearbyResult;
import com.o2.plotos.restapi.newApis.models.home.NewRestResult;
import com.o2.plotos.restapi.newApis.models.plotosProgram.PlotosSpecialDetoxResponse;
import com.o2.plotos.restapi.newApis.models.plotosProgram.PlotosSpecialResponse;
import com.o2.plotos.restapi.newApis.models.resturantDetails.DishData;
import com.o2.plotos.restapi.newApis.models.search.SearchData;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;


import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NearByActivity extends BaseActivity implements NearbyDetailAdp.ClickListnerOfNearby, NewPlotosDetailAdp.ClickListnerOfGrocerySupplier, PlotosSignatureAdp.ClickListnerOfPlotosSignature {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.rv_cat_details)
    RecyclerView rv_cat_details;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.cartLayout)
    RelativeLayout cartLayout;
    @BindView(R.id.tvItemCount)
    TextView tvItemCount;
    @BindView(R.id.tvTotal)
    TextView tvTotal;

    ArrayList<NearbyResult> nearbyResults = new ArrayList<>();
    ArrayList<NewRestResult> newPlotosResults = new ArrayList<>();
    private List<String> listOfTitleOfSignature = new ArrayList<>();
    private List<List<ItemDetais>> listSignature = new ArrayList<>();

    NearbyDetailAdp nearbyDetailAdp;
    NewPlotosDetailAdp newPlotosDetailAdp;
    PlotosSignatureAdp plotosSignatureAdp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_by);
        ButterKnife.bind(this);
        rv_cat_details.setHasFixedSize(true);
        rv_cat_details.setLayoutManager(new LinearLayoutManager(this));

        if (getIntent() != null && getIntent().getExtras() != null) {
            txt_title.setText(getIntent().getStringExtra("title"));
            if (getIntent().getStringExtra("from").equals("plotosprogram")) {
                if (txt_title.getText().equals("Near by Restaurants")) {
                    callNearbyApi();
                } else if (txt_title.getText().equals("Plotos Signature")) {
                    callPlotosSignature("", true);
                } else if (txt_title.getText().toString().equalsIgnoreCase("detox")) {
                    callPlotosSignatureDetox(getIntent().getStringExtra("id"));
                } else {
                    callPlotosSignature(getIntent().getStringExtra("id"), false);
                }
            } else {
                if (txt_title.getText().equals("New on Plotos")) {
                    newPlotosResults = Parcels.unwrap(getIntent().getParcelableExtra(Constant.NEWPLOTOSDETAILS));
                    newPlotosDetailAdp = new NewPlotosDetailAdp(newPlotosResults, this, this);
                    rv_cat_details.setAdapter(newPlotosDetailAdp);
                    newPlotosDetailAdp.notifyDataSetChanged();
                } else {
                    nearbyResults = Parcels.unwrap(getIntent().getParcelableExtra(Constant.NEARBYDETAILS));
                    nearbyDetailAdp = new NearbyDetailAdp(nearbyResults, this, this);
                    rv_cat_details.setAdapter(nearbyDetailAdp);
                    nearbyDetailAdp.notifyDataSetChanged();
                }
            }
        }
        setCartFab();
    }

    private void callPlotosSignatureDetox(String id) {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PlotosSpecialDetoxResponse> call;
        call = apiInterface.callPlotosSpecialDetox(id, UserPreferenceUtil.getInstance(this).getLocationLat(), UserPreferenceUtil.getInstance(this).getLocationLng());
        call.enqueue(new Callback<PlotosSpecialDetoxResponse>() {
            @Override
            public void onResponse(Call<PlotosSpecialDetoxResponse> call, Response<PlotosSpecialDetoxResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    if (response.body().getResponse().equalsIgnoreCase("0") && response.body() != null) {
                        listOfTitleOfSignature.clear();
                        listSignature.clear();
                        if (response.body().getAllDishDetails() != null) {
                            if (response.body().getAllDishDetails().size() > 0) {
                                Map<String, List<ItemDetais>> map = response.body().getAllDishDetails();
                                for (Map.Entry<String, List<ItemDetais>> entry : map.entrySet()) {
                                    listOfTitleOfSignature.add(entry.getKey());
                                    listSignature.add(entry.getValue());
                                }
                                if (listOfTitleOfSignature.size() > 0) {
                                    plotosSignatureAdp = new PlotosSignatureAdp(listSignature, listOfTitleOfSignature, NearByActivity.this, NearByActivity.this);
                                    rv_cat_details.setAdapter(plotosSignatureAdp);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<PlotosSpecialDetoxResponse> call, Throwable t) {
                hideProgress();
            }
        });
    }

    private void callNearbyApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NearByResponse> call = apiInterface.callNearbyRestaurantList("0", UserPreferenceUtil.getInstance(this).getLocationLat(), UserPreferenceUtil.getInstance(this).getLocationLng());
        call.enqueue(new Callback<NearByResponse>() {
            @Override
            public void onResponse(Call<NearByResponse> call, Response<NearByResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    if (response.body().getResponse().equalsIgnoreCase("0")) {
                        nearbyResults.addAll(response.body().getResult());
                        nearbyDetailAdp = new NearbyDetailAdp(nearbyResults, NearByActivity.this, NearByActivity.this);
                        rv_cat_details.setAdapter(nearbyDetailAdp);
                        nearbyDetailAdp.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<NearByResponse> call, Throwable t) {
                hideProgress();
            }
        });
    }

    private void callPlotosSignature(String id, boolean isSignature) {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PlotosSpecialResponse> call;
        if (isSignature) {
//            call = apiInterface.callPlotosSignature(UserPreferenceUtil.getInstance(this).getCurrentLat(), UserPreferenceUtil.getInstance(this).getCurrentLng());
            call = apiInterface.callPlotosSignature(UserPreferenceUtil.getInstance(this).getLocationLat(), UserPreferenceUtil.getInstance(this).getLocationLng());
        } else {
//            call = apiInterface.callPlotosSpecial(id,UserPreferenceUtil.getInstance(this).getCurrentLat(), UserPreferenceUtil.getInstance(this).getCurrentLng());
            call = apiInterface.callPlotosSpecial(id, UserPreferenceUtil.getInstance(this).getLocationLat(), UserPreferenceUtil.getInstance(this).getLocationLng());
        }

        call.enqueue(new Callback<PlotosSpecialResponse>() {
            @Override
            public void onResponse(Call<PlotosSpecialResponse> call, Response<PlotosSpecialResponse> response) {
                hideProgress();
                if (response.code() == 200) {
                    if (response.body().getResponse().equalsIgnoreCase("0")
                            && response.body() != null) {
                        listOfTitleOfSignature.clear();
                        listSignature.clear();
                        if (response.body().getAllDishDetails() != null) {
                            if (response.body().getAllDishDetails().getList_breakfast() != null && response.body().getAllDishDetails().getList_breakfast().size() > 0) {
                                listOfTitleOfSignature.add("Breakfast");
                                listSignature.add(response.body().getAllDishDetails().getList_breakfast());
                            }
                            if (response.body().getAllDishDetails().getList_appetizers() != null && response.body().getAllDishDetails().getList_appetizers().size() > 0) {
                                listOfTitleOfSignature.add("Appetizers");
                                listSignature.add(response.body().getAllDishDetails().getList_appetizers());
                            }
                            if (response.body().getAllDishDetails().getListMains() != null && response.body().getAllDishDetails().getListMains().size() > 0) {
                                listOfTitleOfSignature.add("Mains");
                                listSignature.add(response.body().getAllDishDetails().getListMains());
                            }
                            if (response.body().getAllDishDetails().getList_snack() != null && response.body().getAllDishDetails().getList_snack().size() > 0) {
                                listOfTitleOfSignature.add("Snacks");
                                listSignature.add(response.body().getAllDishDetails().getList_snack());
                            }
                            if (response.body().getAllDishDetails().getList_beverages() != null && response.body().getAllDishDetails().getList_beverages().size() > 0) {
                                listOfTitleOfSignature.add("Beverages");
                                listSignature.add(response.body().getAllDishDetails().getList_beverages());
                            }

                            if (listOfTitleOfSignature.size() > 0) {
                                plotosSignatureAdp = new PlotosSignatureAdp(listSignature, listOfTitleOfSignature, NearByActivity.this, NearByActivity.this);
                                rv_cat_details.setAdapter(plotosSignatureAdp);
                                plotosSignatureAdp.notifyDataSetChanged();
                            } else {
                                showToast("No result found");
                            }
                        } else {
                            showToast("No result found");
                            finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<PlotosSpecialResponse> call, Throwable t) {
                hideProgress();
            }
        });
    }

    private void setCartFab() {
        int cartItemSize = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST).size();
        if (cartItemSize > 0) {
            cartLayout.setVisibility(View.VISIBLE);
            tvItemCount.setText("" + cartItemSize);
            makeList();
        } else {
            cartLayout.setVisibility(View.GONE);
        }
    }

    private void makeList() {
        Double tootal = 0.00;
        List<ItemDetais> listOfCartItems = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST);
        for (int i = 0; i < listOfCartItems.size(); i++) {
            ItemDetais itemDetais = listOfCartItems.get(i);
            tootal = tootal + (itemDetais.getQuantity() * itemDetais.getBasicPrice());
        }
        tvTotal.setText(tootal + " AED");
    }

    @OnClick({R.id.img_back, R.id.cartLayout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.cartLayout:
                startActivity(new Intent(this, DeliverToActivity.class));
                break;
//            case R.id.linlay_test:
//                Intent i2 = new Intent(this, BakeryItemsActivity.class);
//                startActivity(i2);
//                break;
        }
    }

    @Override
    public void onClickNearBy(int pos) {
        Utils.navigateToResturantDetailsAct(nearbyResults.get(pos).getRestaurant_id(), this);

    }

    @Override
    public void onClickNewPlotos(int pos) {
        Utils.navigateToResturantDetailsAct(newPlotosResults.get(pos).getRestaurant_id(), this);
    }

    @Override
    public void onClickPlotosSignature(int pos) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.SIGNATUREDETAILS, Parcels.wrap(listSignature.get(pos)));
        bundle.putString("title", listOfTitleOfSignature.get(pos));
        bundle.putString("from", "nearby");
        Intent intent = new Intent(this, CategoriesDetialActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
