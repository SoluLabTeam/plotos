package com.o2.plotos.home;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.BannerRequest;
import com.o2.plotos.restapi.responses.BannerRequestResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rania on 5/20/2017.
 */

public class BannerHelper {

    private BannerListener mListener;

    public BannerHelper(BannerListener bannerListener){
        mListener = bannerListener;
    }

    public void getBanners(){
        BannerRequest bannerRequest = ServiceGenrator.createService(BannerRequest.class);
        Call<BannerRequestResponse> bannerCall = bannerRequest.bannerRequest();
        bannerCall.enqueue(new Callback<BannerRequestResponse>() {
            @Override
            public void onResponse(Call<BannerRequestResponse> call, Response<BannerRequestResponse> response) {
                if(response.code()== 200) {
                    BannerRequestResponse apiResponse = response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        mListener.onGetBannerSuccessfully(apiResponse.bannerList);
                    }else{
                        LogCat.LogDebug(ConstantUtil.TAG, response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<BannerRequestResponse> call, Throwable t) {
                LogCat.LogDebug(ConstantUtil.TAG,"Banner ap onFailure "+t.getMessage());
            }
        });
    }
}
