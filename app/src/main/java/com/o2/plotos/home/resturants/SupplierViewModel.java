package com.o2.plotos.home.resturants;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.SupplierRequest;
import com.o2.plotos.restapi.responses.SupplierRequestResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rania on 5/9/2017.
 */

public class SupplierViewModel {

    private SupplierListener mListener;

    public SupplierViewModel(SupplierListener supplierListener){
        mListener = supplierListener;
    }

    public void getSupplierRequest(String latitude, String longitude) {
        SupplierRequest supplierRequest = ServiceGenrator.createService(SupplierRequest.class);
        Call<SupplierRequestResponse> supplierRequestResponseCall = supplierRequest.supplierRequest(latitude, longitude);
        supplierRequestResponseCall.enqueue(new Callback<SupplierRequestResponse>() {
            @Override
            public void onResponse(Call<SupplierRequestResponse> call, Response<SupplierRequestResponse> response) {
                LogCat.LogDebug(ConstantUtil.TAG," -> Supplier api response "+response.raw());
                if(response.code()== 200){
                    SupplierRequestResponse apiResponse =  response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        if(mListener != null) {
                            mListener.onGetSupplierSuccess(apiResponse.supplierList);
                        }

                    }else if(responseCode.equalsIgnoreCase("1")){
                        mListener.onGetSupplierFailed("Restaurant request Failed(1)");
                    }else if(responseCode.equalsIgnoreCase("2")){
                        mListener.onGetSupplierFailed("Restaurant request Failed(2)");
                    }
                }
            }

            @Override
            public void onFailure(Call<SupplierRequestResponse> call, Throwable t) {

                if(mListener!=null)
                {
                    mListener.onGetSupplierFailed("unexpected error");
                    LogCat.LogDebug(ConstantUtil.TAG,"supplier ap onFailure "+t.getMessage());
                }

            }
        });
    }

    void onDestroy(){
        if(mListener != null){
            mListener = null;
        }
    }
}
