package com.o2.plotos.home.resturants;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.adapters.AddonsAdp;
import com.o2.plotos.adapters.NutrisionAdp;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.home.order.DeliverToActivity;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.dishdetails.AddOns;
import com.o2.plotos.restapi.newApis.models.dishdetails.DishDetails;
import com.o2.plotos.restapi.newApis.models.dishdetails.DishDetailsResponse;
import com.o2.plotos.restapi.newApis.models.groceryDetails.GroceryDetails;
import com.o2.plotos.restapi.newApis.models.groceryDetails.GroceryDetailsResponse;
import com.o2.plotos.restapi.newApis.models.home.DishDetail;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.OpensanBoldTextview;
import com.o2.plotos.utils.OpensanTextview;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddToCartActivity extends BaseActivity implements AddonsAdp.addOnClickListner {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tool)
    RelativeLayout tool;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.ingredients)
    TextView ingredients;
    @BindView(R.id.rv_nutrision)
    RecyclerView rvNutrision;
    @BindView(R.id.minus)
    TextView minus;
    @BindView(R.id.count)
    TextView count;
    @BindView(R.id.plus)
    TextView plus;
    @BindView(R.id.ll_addons)
    LinearLayout llAddons;
    @BindView(R.id.edt_note)
    EditText edtNote;
    @BindView(R.id.llBottom)
    LinearLayout llBottom;
    @BindView(R.id.rv_addons)
    RecyclerView rvAddons;
    @BindView(R.id.tv_add_to_cart)
    TextView tvAddToCart;
    @BindView(R.id.ttl_price)
    TextView tv_ttlPrice;
    @BindView(R.id.ivShare)
    ImageView ivShare;
    @BindView(R.id.cartLayout)
    RelativeLayout cartLayout;
    @BindView(R.id.tvItemCount)
    TextView tvItemCount;
    @BindView(R.id.tvTotal)
    TextView tvTotal;
    private String cat, id;
    private List<Drawable> list_nutrition;
    private List<AddOns> list_addone;
    private NutrisionAdp nutrisionAdp;
    private AddonsAdp addonsAdp;
    private int quantity = 0;
    private Double ttl_price = 0.0;
    private Double item_price = 0.0;
    private Double ttl_addons_amount = 0.0;
    private ItemDetais itemData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_cart);
        ButterKnife.bind(this);
        cat = getIntent().getStringExtra(Constant.CAT);
        id = getIntent().getStringExtra(Constant.ID);
        quantity=getIntent().getIntExtra(Constant.QUANTITY,0);
        bindAdapter();

        if (cat.equalsIgnoreCase(Constant.CAT_GROCERY)) {
            getItemDetails(id);
        }
        if (cat.equalsIgnoreCase(Constant.CAT_DISH)) {
            getDishDetails(id);
        }
    }

    private void setQuantity() {
        ttl_price = quantity * item_price;
        ttl_price = ttl_price + (quantity * ttl_addons_amount);
        count.setText(String.valueOf(quantity));
        tvAddToCart.setText(String.format(getString(R.string.add_to_cart), quantity));
        tv_ttlPrice.setText(String.format(getString(R.string.ttl_price), ttl_price));
    }

    private void bindAdapter() {
        list_nutrition = new ArrayList<>();
        nutrisionAdp = new NutrisionAdp(list_nutrition, this);
        rvNutrision.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvNutrision.setAdapter(nutrisionAdp);

        list_addone = new ArrayList<>();
        addonsAdp = new AddonsAdp(list_addone, this, this);
        rvAddons.setLayoutManager(new LinearLayoutManager(this));
        rvAddons.setAdapter(addonsAdp);

    }

    private void makeListOfNutrition(String nutrition) {
        list_nutrition.clear();
        String[] strings = nutrition.split(",");
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].equalsIgnoreCase("1")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.nutri_vegan));
            }
            if (strings[i].equalsIgnoreCase("2")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.nutri_vegetarian));

            }
            if (strings[i].equalsIgnoreCase("3")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.gluten_free));

            }
            if (strings[i].equalsIgnoreCase("4")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.dairy_free));

            }
            if (strings[i].equalsIgnoreCase("5")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.contains_nuts));

            }
            if (strings[i].equalsIgnoreCase("6")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.contains_peanuts));

            }
            if (strings[i].equalsIgnoreCase("7")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.nutri_paleo));

            }
            if (strings[i].equalsIgnoreCase("8")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.contains_eggs));

            }
            if (strings[i].equalsIgnoreCase("9")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.egg_free));

            }
            if (strings[i].equalsIgnoreCase("10")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.nutri_low_carb));

            }
            if (strings[i].equalsIgnoreCase("11")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.no_added_sugar));

            }
            if (strings[i].equalsIgnoreCase("12")) {
                list_nutrition.add(ContextCompat.getDrawable(this, R.drawable.nutri_raw));

            }
        }
        nutrisionAdp.notifyDataSetChanged();
    }

    @OnClick({R.id.img_back, R.id.llBottom, R.id.minus, R.id.plus,R.id.ivShare,R.id.cartLayout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.llBottom:
                storeDatainToCart();

//                startActivity(new Intent(this, DeliverToActivity.class));
                break;
            case R.id.minus:
                if (quantity > 0) {
                    quantity--;
                    setQuantity();
                }
                break;
            case R.id.plus:
                quantity++;
                setQuantity();
                break;
            case R.id.ivShare:
                String link="https://play.google.com/store/apps/details?id=\"" + getPackageName();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Check out the app :"+link);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;

            case R.id.cartLayout:
                startActivity(new Intent(this, DeliverToActivity.class));
                break;

        }
    }

    private void storeDatainToCart() {
        itemData.setItem_note(getDataFromEditText(edtNote));
        Utils.storeDatainToCart(itemData, this, quantity, ttl_price / quantity);
        setCartFab();
    }

    private void getDishDetails(String id) {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<DishDetailsResponse> call = apiInterface.getDishDetails(id);
        call.enqueue(new Callback<DishDetailsResponse>() {
            @Override
            public void onResponse(Call<DishDetailsResponse> call, Response<DishDetailsResponse> response) {
                hideProgress();
                setDishDetails(response.body().getDishDetailsResult().getListDishDetails().get(0), response.body().getDishDetailsResult().getListAddons());

            }

            @Override
            public void onFailure(Call<DishDetailsResponse> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }

    private void getItemDetails(String id) {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<GroceryDetailsResponse> call = apiInterface.getGroceryDetails(id);
        call.enqueue(new Callback<GroceryDetailsResponse>() {
            @Override
            public void onResponse(Call<GroceryDetailsResponse> call, Response<GroceryDetailsResponse> response) {
                hideProgress();
                setGroceryData(response.body().getListGroceryDetails().get(0));

            }

            @Override
            public void onFailure(Call<GroceryDetailsResponse> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }

    private void setGroceryData(ItemDetais data) {
        itemData = data;
        itemData.setGrocery(true);

        quantity++;
        item_price = Double.parseDouble(data.getPrice());
        setQuantity();

        txtTitle.setText(data.getName());
        if (!data.getDelivery_time().trim().equals("0")) {
            time.setText(data.getDelivery_time() + " MINS");
        }
        Picasso.with(this)
                .load(data.getImage())
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(img);
        ingredients.setText(data.getIngredients());
        makeListOfNutrition(data.getNutritional_info());


    }

    private void setDishDetails(ItemDetais dishDetails, List<AddOns> list_addones) {
        itemData = dishDetails;
        itemData.setGrocery(false);

        quantity++;
        item_price = Double.parseDouble(dishDetails.getPrice());
        setQuantity();

        if (list_addones != null && list_addones.size() > 0) {
            itemData.setList_addons(list_addones);
            llAddons.setVisibility(View.VISIBLE);

            list_addone.clear();
            list_addone.addAll(list_addones);
            addonsAdp.notifyDataSetChanged();
        }
        txtTitle.setText(dishDetails.getName());
        if (!dishDetails.getDelivery_time().equals("0")) {
            time.setText(dishDetails.getDelivery_time() + " MINS");
        }
        Picasso.with(this)
                .load(dishDetails.getImage())
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(img);
        ingredients.setText(dishDetails.getIngredients());
        makeListOfNutrition(dishDetails.getNutritional_info());

    }

    @Override
    public void onClickOfAddons(boolean b, float price, int pos) {
        if (b) {
            ttl_addons_amount += price;
        } else {
            ttl_addons_amount -= price;
        }
        itemData.getList_addons().get(pos).setSelected(b);
        setQuantity();
    }

    private void setCartFab() {
        int cartItemSize = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST).size();
        if (cartItemSize > 0) {
            cartLayout.setVisibility(View.VISIBLE);
            tvItemCount.setText("" + cartItemSize);
            makeList();
        } else {
            cartLayout.setVisibility(View.GONE);
        }
    }

    private void makeList() {
        Double tootal = 0.00;
        List<ItemDetais> listOfCartItems = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST);
        for (int i = 0; i < listOfCartItems.size(); i++) {
            ItemDetais itemDetais = listOfCartItems.get(i);
            tootal = tootal + (itemDetais.getQuantity() * itemDetais.getBasicPrice());
        }
        tvTotal.setText(tootal + " AED");
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCartFab();
    }
}
