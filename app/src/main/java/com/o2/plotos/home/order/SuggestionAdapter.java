package com.o2.plotos.home.order;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.models.Order;
import com.o2.plotos.utils.CustomFonts;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 2/6/2018.
 */

public class SuggestionAdapter extends RecyclerView.Adapter<SuggestionAdapter.OrderViewHolder> {

    private List<ItemDetais> mOrderlist;
    Context mContext;
    private onClickOfProductListner onClickOfProductListner;

    public interface onClickOfProductListner {
        void onClickOfProduct(int pos);

        void onClickOfAddToCart(int pos);
    }

    public SuggestionAdapter(Context mContext, List<ItemDetais> mOrderlist, onClickOfProductListner onClickOfProductListner) {
        this.mOrderlist = mOrderlist;
        this.mContext = mContext;
        this.onClickOfProductListner = onClickOfProductListner;
    }


    @Override
    public SuggestionAdapter.OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_menu_suggest_order, parent, false);
        SuggestionAdapter.OrderViewHolder orderViewHolder = new SuggestionAdapter.OrderViewHolder(v);
        return orderViewHolder;
    }

    @Override
    public void onBindViewHolder(SuggestionAdapter.OrderViewHolder holder, int position) {
        Picasso.with(mContext)
                .load(mOrderlist.get(position).getImage())
                .placeholder(ContextCompat.getDrawable(mContext, R.drawable.placeholder))
                .error(ContextCompat.getDrawable(mContext, R.drawable.placeholder))
                .into(holder.imagePhoto);
        holder.textName.setText(mOrderlist.get(position).getName());
        holder.textdateOrder.setText(mOrderlist.get(position).getIngredients());
        holder.textRemainingTime.setText(mOrderlist.get(position).getPre_booking() + " Mins");

    }

    @Override
    public int getItemCount() {
        return mOrderlist.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_order_dish_name)
        TextView textName;
        @BindView(R.id.item_order_dish_dateoforder)
        TextView textdateOrder;
        @BindView(R.id.item_order_dish_timeRemaining)
        TextView textRemainingTime;
        @BindView(R.id.item_add_to_cart)
        TextView item_add_to_cart;
        @BindView(R.id.item_order_dish_image)
        ImageView imagePhoto;
        @BindView(R.id.item_diet_dish_linearLayout)
        LinearLayout item_diet_dish_linearLayout;


        public OrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.item_diet_dish_linearLayout,R.id.item_add_to_cart})
        public void onViewClicked(View view) {
            switch (view.getId()){
                case R.id.item_diet_dish_linearLayout:
                    onClickOfProductListner.onClickOfProduct(getAdapterPosition());
                    break;
                case R.id.item_add_to_cart:
                    onClickOfProductListner.onClickOfAddToCart(getAdapterPosition());
                    break;
            }

        }

    }
}