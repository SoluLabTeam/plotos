package com.o2.plotos.home.order;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.DeleteOrderRequest;
import com.o2.plotos.restapi.responses.DeleteOrderResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rania on 4/3/2017.
 */
public class DeleteOrderHelper {

    DeleteOrderRequestListener mListener;

    public DeleteOrderHelper(DeleteOrderRequestListener deleteOrderListener){
        mListener = deleteOrderListener;
    }

    public void deleteOrderRequest(String orderID){

        DeleteOrderRequest deleteOrderRequest = ServiceGenrator.createService(DeleteOrderRequest.class);
        Call<DeleteOrderResponse> deleteOrderResponseCall = deleteOrderRequest.DeleteRequest(orderID);
        deleteOrderResponseCall.enqueue(new Callback<DeleteOrderResponse>() {
            @Override
            public void onResponse(Call<DeleteOrderResponse> call, Response<DeleteOrderResponse> response) {
                if(response.code()== 200) {
                    DeleteOrderResponse apiResponse = response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        mListener.OnResponseSuccess();
//                    }else if(responseCode.equalsIgnoreCase("1")){
////                        mListener.onResponseFailure("Restaurant is not in your radius");
                    }else{
                        mListener.onResponseFailure(apiResponse.message);
                    }
                }
            }

            @Override
            public void onFailure(Call<DeleteOrderResponse> call, Throwable t) {
                mListener.onResponseFailure("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"Delete order onFailure "+t.getMessage());
            }
        });
    }
}
