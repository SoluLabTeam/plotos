package com.o2.plotos.home.order;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.models.Order;
import com.o2.plotos.utils.CustomFonts;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.graphics.Color.argb;

public class FollowOrderActivity extends AppCompatActivity {

    @BindView(R.id.follow_order_activity_time)
    TextView time;
    @BindView(R.id.follow_order_activity_estimate_text)
    TextView estimateText;
    @BindView(R.id.follow_order_activity_view2)
    View view2;
    @BindView(R.id.follow_order_activity_view3)
    View view3;
    @BindView(R.id.follow_order_activity_view4)
    View view4;
    @BindView(R.id.follow_order_activity_layout1_textview)
    TextView textView1;
    @BindView(R.id.follow_order_activity_layout2_textview)
    TextView textView2;
    @BindView(R.id.follow_order_activity_layout3_textview)
    TextView textView3;
    @BindView(R.id.follow_order_activity_layout4_textview)
    TextView textView4;
    @BindView(R.id.follow_order_activity_fish)
    ImageView fish;
    @BindView(R.id.follow_order_activity_car)
    ImageView car;
    @BindView(R.id.follow_order_activity_food)
    ImageView food;

    CustomFonts customFonts;


    public static final String EXTRA_ORDER = "order";
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.follow_order_activity_layout2)
    LinearLayout followOrderActivityLayout2;
    @BindView(R.id.follow_order_activity_layout3)
    LinearLayout followOrderActivityLayout3;
    @BindView(R.id.follow_order_activity_layout4)
    LinearLayout followOrderActivityLayout4;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    private Order mOrder;

    private ActionBar mActionbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_order);
        ButterKnife.bind(this);
        customFonts = new CustomFonts(this);
        setUI();

        if (getIntent().getExtras() != null) {
            mOrder = getIntent().getExtras().getParcelable(EXTRA_ORDER);
        }

        txtTime.setText(mOrder.getDeliveryTime() + " Min");

        txtAddress.setText(mOrder.getCardNumber());

        mActionbar = getSupportActionBar();
        mActionbar.setDisplayHomeAsUpEnabled(true);
        setSelected();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setUI() {
        customFonts.setOswaldBold(time);
        customFonts.setOpenSansSemiBold(estimateText);
        customFonts.setOpenSansBold(textView1);
        customFonts.setOpenSansBold(textView2);
        customFonts.setOpenSansBold(textView3);
        customFonts.setOpenSansBold(textView4);
    }

    public void setSelected() {
        if (mOrder.getOrderStatus().equalsIgnoreCase("2")) {
            view2.setBackgroundResource(R.drawable.selected_radiobtn);
            textView2.setTextColor(argb(255, 77, 177, 115));
            fish.setColorFilter(argb(255, 000, 000, 000));
        }
        if (mOrder.getOrderStatus().equalsIgnoreCase("3")) {
            textView3.setTextColor(argb(255, 77, 177, 115));
            view3.setBackgroundResource(R.drawable.selected_radiobtn);
            car.setColorFilter(android.R.color.black);

        }
        if (mOrder.getOrderStatus().equalsIgnoreCase("4")) {
            textView4.setTextColor(argb(255, 77, 177, 115));
            view4.setBackgroundResource(R.drawable.selected_radiobtn);
            food.setColorFilter(android.R.color.black);

        }
    }
}
