package com.o2.plotos.home.resturants;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.models.grocery.Category_list;
import com.o2.plotos.restapi.newApis.models.grocery.SubCategoryList;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImagesWithNameCatAdp extends RecyclerView.Adapter<ImagesWithNameCatAdp.ImgHolder> {

    private List<SubCategoryList> list;
    private ImageClickListner listner;
    Context context;
    private int parentPosition;

    public interface ImageClickListner {
        void onClickOfImage(int pos,int parentPosition);
    }

    public ImagesWithNameCatAdp(List<SubCategoryList> list, Context context, ImageClickListner listner,int parentPosition) {
        this.list = list;
        this.context=context;
        this.listner = listner;
        this.parentPosition=parentPosition;
    }

    @Override
    public ImgHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_img_with_name, parent, false);
        return new ImgHolder(view);
    }

    @Override
    public void onBindViewHolder(ImgHolder holder, int position) {

        holder.tvTitle.setText(list.get(position).getCategoryName());
        if (list.get(position).getImage_url() != null && !list.get(position).getImage_url().equals("")) {
            Picasso.with(context).load(list.get(position).getImage_url())
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.image);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ImgHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.circularimg)
        CardView circularimg;

        @BindView(R.id.tvTitle)
        TextView tvTitle;

        @BindView(R.id.image)
        ImageView image;

        public ImgHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.circularimg)
        public void onViewClicked() {
            listner.onClickOfImage(getAdapterPosition(),parentPosition);
        }
    }
}
