package com.o2.plotos.home.resturants;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.models.search.SearchData;
import com.o2.plotos.utils.OpensanTextview;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainSerachResultAdp extends RecyclerView.Adapter<MainSerachResultAdp.MainSearchHolder> implements SubSearchResultsAdp.SearchListner {

    private List<String> listOfTitle;
    private List<List<SearchData>> listSearch;
    private Context context;
    private clickListner clickListner;

    @Override
    public void onClickOfSearchItem(String id, String name, String cat) {
        clickListner.onClickItem(id, name, cat);
    }

    public interface clickListner {
        void onClickItem(String id, String name, String cat);
    }

    public MainSerachResultAdp(List<String> listOfTitle, List<List<SearchData>> listSearch, Context contex, clickListner clickListnert) {
        this.listOfTitle = listOfTitle;
        this.listSearch = listSearch;
        this.context = context;
        this.clickListner = clickListnert;
    }

    @Override
    public MainSearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_result, parent, false);
        return new MainSearchHolder(view);
    }

    @Override
    public void onBindViewHolder(MainSearchHolder holder, int position) {
        holder.tvTtl.setText(listOfTitle.get(position));
        holder.rvSearchSubItems.setLayoutManager(new LinearLayoutManager(context));
        holder.rvSearchSubItems.setAdapter(new SubSearchResultsAdp(listSearch.get(position), context, this, listOfTitle.get(position)));
    }

    @Override
    public int getItemCount() {
        return listSearch.size();
    }


    public class MainSearchHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_ttl)
        OpensanTextview tvTtl;
        @BindView(R.id.rv_search_sub_items)
        RecyclerView rvSearchSubItems;

        public MainSearchHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
