package com.o2.plotos.home.resturants;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.o2.plotos.BaseFragment;
import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.grocery.CategoryModel;
import com.o2.plotos.restapi.newApis.models.grocery.CategoryResponse;
import com.o2.plotos.restapi.newApis.models.grocery.SupplierModel;
import com.o2.plotos.restapi.newApis.models.grocery.SupplierResponse;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroceryCategoryFragments extends BaseFragment implements GroceryCatAdp.ClickListnerOfGroceryCatSupplier{
    @BindView(R.id.rv_grocery_cat)
    RecyclerView rvGroceryCat;

    ArrayList<CategoryResponse>  categoryResponseArrayList=new ArrayList<>();
    GroceryCatAdp mAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_grocery_cat, container, false);
        ButterKnife.bind(this, rootView);
        mAdapter = new GroceryCatAdp(categoryResponseArrayList, getActivity(),this);
        rvGroceryCat.setAdapter(mAdapter);

        if (Utils.isNetworkAvailable(getContext())){
            callApi();
        }
        return rootView;
    }

    private void callApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CategoryModel> call = apiInterface.callCatList(UserPreferenceUtil.getInstance(getContext()).getLocationLat(), UserPreferenceUtil.getInstance(getContext()).getLocationLng());
        call.enqueue(new Callback<CategoryModel>() {
            @Override
            public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    categoryResponseArrayList.addAll(response.body().getResult());
                    rvGroceryCat.setHasFixedSize(true);
                    rvGroceryCat.setLayoutManager(new LinearLayoutManager(getActivity()));
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<CategoryModel> call, Throwable t) {
                hideProgress();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClickOfArror(int pos) {
        Intent intent = new Intent(getContext(), GroceryDetailActivity.class);
        intent.putExtra("from","category");
        intent.putExtra("rest_name",categoryResponseArrayList.get(pos).getName());
        intent.putExtra("mainid",categoryResponseArrayList.get(pos).getMain_id());
        getContext().startActivity(intent);
    }
}
