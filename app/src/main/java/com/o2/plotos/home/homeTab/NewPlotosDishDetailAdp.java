package com.o2.plotos.home.homeTab;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.models.home.DishDetail;
import com.o2.plotos.utils.OpensanTextview;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewPlotosDishDetailAdp extends RecyclerView.Adapter<NewPlotosDishDetailAdp.ImgHolder> {


    private List<DishDetail> list;
    private ImageClickListner listner;
    Context context;
    int parentPosition;

    public interface ImageClickListner {
        void onClickOfImage(int pos,int parentPosition);
    }

    public NewPlotosDishDetailAdp(List<DishDetail> list, Context context, ImageClickListner listner, int parentPosition) {
        this.list = list;
        this.context = context;
        this.listner = listner;
        this.parentPosition = parentPosition;
    }

    @Override
    public ImgHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grocery_inner_detail, parent, false);
        return new ImgHolder(view);
    }

    @Override
    public void onBindViewHolder(ImgHolder holder, int position) {
        if (list.get(position).getImage() != null && !list.get(position).getImage().equals("")) {
            Picasso.with(context).load(list.get(position).getImage())
                    .error(R.drawable.placeholder)
                    .into(holder.ivDishImage);
        }
        holder.tvMealName.setText(list.get(position).getName());
//        holder.tvHours.setText(list.get(position).getPre_booking() + " hrs");
        holder.tvRestName.setText(list.get(position).getRestaurant_name());
        holder.tvRestDetail.setText(list.get(position).getIngredients());
        holder.tvPrice.setText(list.get(position).getPrice()+" AED");
        holder.tvPrice.setTextColor(context.getResources().getColor(R.color.colorAccent));
        if (list.get(position).getOther_details() != null && !list.get(position).getOther_details().equals("")) {
            String details = list.get(position).getOther_details();
            String[] dArray = details.split(",");
            if (dArray.length >= 1) {
                if (!dArray[0].trim().equals("NaN")&&!dArray[0].trim().equals("0")) {
                    holder.tvkcal1.setVisibility(View.VISIBLE);
                    holder.tvkcal1.setText(dArray[0] + " kcal");
                }
            }
            if (dArray.length >= 2) {
                if (!dArray[1].trim().equals("NaN")&&!dArray[1].trim().equals("0")) {
                    holder.tvkcal2.setVisibility(View.VISIBLE);
                    holder.tvkcal2.setText(dArray[1] + " Cho");
                }
            }
            if (dArray.length >= 3) {
                if (!dArray[2].trim().equals("NaN")&&!dArray[2].trim().equals("0")) {
                    holder.tvkcal3.setVisibility(View.VISIBLE);
                    holder.tvkcal3.setText(dArray[2] + " Pro");
                }
            }
            if (dArray.length >= 4) {
                if (!dArray[3].trim().equals("NaN")&&!dArray[3].trim().equals("0")) {
                    holder.tvkcal4.setVisibility(View.VISIBLE);
                    holder.tvkcal4.setText(dArray[3] + " Fat");
                }
            }
            if (dArray.length >= 5) {
                if (!dArray[4].trim().equals("NaN")&&!dArray[4].trim().equals("0")) {
                    holder.tvkcal5.setVisibility(View.VISIBLE);
                    holder.tvkcal5.setText(dArray[4] + " Ccal");
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ImgHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_dish_image)
        ImageView ivDishImage;
        @BindView(R.id.tv_meal_name)
        TextView tvMealName;
        @BindView(R.id.tv_hours)
        TextView tvHours;
        @BindView(R.id.tv_rest_name)
        TextView tvRestName;
        @BindView(R.id.tv_rest_detail)
        TextView tvRestDetail;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.linlay_test)
        LinearLayout linlayTest;
        @BindView(R.id.tvkcal1)
        TextView tvkcal1;
        @BindView(R.id.tvkcal2)
        TextView tvkcal2;
        @BindView(R.id.tvkcal3)
        TextView tvkcal3;
        @BindView(R.id.tvkcal4)
        TextView tvkcal4;
        @BindView(R.id.tvkcal5)
        TextView tvkcal5;


        public ImgHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.linlay_test)
        public void onViewClicked() {
            listner.onClickOfImage(getAdapterPosition(),parentPosition);
        }
    }
}
