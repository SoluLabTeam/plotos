package com.o2.plotos.home.resturants;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.models.search.SearchData;
import com.o2.plotos.utils.OpensanTextview;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubSearchResultsAdp extends RecyclerView.Adapter<SubSearchResultsAdp.SubSearchHolder> {

    private List<SearchData> listSearch;
    private Context context;
    private SearchListner listner;
    private String cat;
    private String image;
    public interface SearchListner {
        void onClickOfSearchItem(String id, String name, String cat);
    }

    public SubSearchResultsAdp(List<SearchData> listSearch, Context context, SearchListner listner, String cat) {
        this.listSearch = listSearch;
        this.context = context;
        this.listner = listner;
        this.cat = cat;
    }

    @Override
    public SubSearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_result_sub_item, parent, false);
        return new SubSearchHolder(view);
    }

    @Override
    public void onBindViewHolder(final SubSearchHolder holder, int position) {
        image=listSearch.get(position).getSearch_item_image();
        Picasso.with(context)
                .load(image)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.imgItem, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        Picasso.with(context)
                                .load(image)
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .into(holder.imgItem);

                    }
                });
        holder.itemName.setText(listSearch.get(position).getSearch_item_name());
    }

    @Override
    public int getItemCount() {
        return listSearch.size();
    }


    public class SubSearchHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_item)
        ImageView imgItem;
        @BindView(R.id.card_img)
        CardView cardImg;
        @BindView(R.id.item_name)
        OpensanTextview itemName;
        @BindView(R.id.ll_search_item)
        LinearLayout llSearchItem;

        public SubSearchHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.ll_search_item)
        public void onViewClicked() {
            listner.onClickOfSearchItem(listSearch.get(getAdapterPosition()).getSearch_item_id(), listSearch.get(getAdapterPosition()).getSearch_item_name(), cat);

        }
    }
}
