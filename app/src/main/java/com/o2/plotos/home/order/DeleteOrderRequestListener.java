package com.o2.plotos.home.order;

/**
 * Created by Rania on 4/3/2017.
 */
public interface DeleteOrderRequestListener {
    void OnResponseSuccess();
    void onResponseFailure(String message);
}
