package com.o2.plotos.home.order;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.checkout.CardValidator;
import com.checkout.CheckoutKit;
import com.checkout.exceptions.CardException;
import com.checkout.exceptions.CheckoutException;
import com.checkout.models.Card;
import com.checkout.models.CardToken;
import com.checkout.models.CardTokenResponse;
import com.google.gson.Gson;
import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.adapters.OrderListingAdp;
import com.o2.plotos.authentication.signin.SignInActivityNew;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.home.menu.SaveCardActivity;
import com.o2.plotos.models.ConfirmOrder;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.models.PlaceOrderModel;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;
import com.o2.plotos.restapi.newApis.models.dishdetails.AddOns;
import com.o2.plotos.restapi.newApis.models.login.LoginResponce;
import com.o2.plotos.restapi.newApis.models.login.UserData;
import com.o2.plotos.restapi.newApis.models.orders.PastResult;
import com.o2.plotos.restapi.newApis.models.promo.PromoResponse;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.InternetUtil;
import com.o2.plotos.utils.OpensanBtn;
import com.o2.plotos.utils.OpensanTextview;
import com.o2.plotos.utils.UserPreferenceUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmOrderActivity extends BaseActivity implements OrderListingAdp.ClickListnerOfDelete {
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.rv_order)
    RecyclerView rvOrder;
    @BindView(R.id.total)
    OpensanTextview total;
    @BindView(R.id.ed_promo)
    EditText edPromo;
    @BindView(R.id.btn_apply)
    OpensanBtn btnApply;
    @BindView(R.id.edt_fname)
    EditText edtFname;
    @BindView(R.id.edt_lname)
    EditText edtLname;
    @BindView(R.id.edt_mobile)
    EditText edtMobile;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.instrucrion)
    EditText instrucrion;
    @BindView(R.id.btn_placeOrder)
    OpensanBtn btnPlaceOrder;
    @BindView(R.id.ivCash)
    ImageView ivCash;
    @BindView(R.id.ivCreditCard)
    ImageView ivCreditCard;

    @BindView(R.id.tvAddCard)
    TextView tvAddCard;
    @BindView(R.id.llPayCash)
    LinearLayout llPayCash;
    @BindView(R.id.llPayCard)
    LinearLayout llPayCard;


//    @BindView(R.id.tvDf)
//    TextView tvDf;

    private Double tootal = 0.00;
    private List<ItemDetais> listOfCartItems;
    private List<ItemDetais> listOfpastOrder;
    private List<ConfirmOrder> listConfirmOrder;
    private OrderListingAdp orderListingAdp;
    private List<PlaceOrderModel> listPlaceOrder;

    private String publicKey = "pk_test_ac6b84e1-8e6f-48a5-a210-364420f249a9";

    private PastResult pastResult;

    boolean isCOD = true;
    private Double promoAmnt = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);
        ButterKnife.bind(this);
        UserPreferenceUtil.getInstance(this).setDeliveryTotal("");
        bindAdp();
        if (!UserPreferenceUtil.getInstance(this).UserId().equalsIgnoreCase("0")) {
            setPrefData();
        }
        if (getIntent().getParcelableExtra(Constant.OBJ_PAST_ORDER) != null) {
            doReorder();
        } else {
            makeList();
        }
    }

    private void applyPromoCode(String code, final String totalAmount) {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PromoResponse> call = apiInterface.applyPromo(code, UserPreferenceUtil.getInstance(this).UserId(), totalAmount);
        call.enqueue(new Callback<PromoResponse>() {
            @Override
            public void onResponse(Call<PromoResponse> call, Response<PromoResponse> response) {
                hideProgress();
                showToast(response.body().getMsg());
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    promoAmnt = response.body().getDiscount();
                    double tAmount = Double.parseDouble(totalAmount);
                    double s = tAmount - promoAmnt;
                    Spanned spanned = Html.fromHtml(s + "<sup><small> AED</small></sup>");
                    total.setText(spanned);
//                    total.setText(String.valueOf(tootal - promoAmnt));
                }
            }

            @Override
            public void onFailure(Call<PromoResponse> call, Throwable t) {
                hideProgress();
            }
        });
    }

    private void doReorder() {
        pastResult = Parcels.unwrap(getIntent().getParcelableExtra(Constant.OBJ_PAST_ORDER));

        List<ItemDetais> list = pastResult.getChild();
        for (int i = 0; i < list.size(); i++) {
            ItemDetais itemDetais = list.get(i);
            itemDetais.setQuantity(Integer.parseInt(itemDetais.getItem_count()));
            itemDetais.setBasicPrice(Double.valueOf(itemDetais.getPrice()) / itemDetais.getQuantity());
            tootal = tootal + (itemDetais.getQuantity() * itemDetais.getBasicPrice());
            list.set(i, itemDetais);
        }
        listOfpastOrder = list;
        /*total.setText(Utils.getAEDSpannable(tootal));*/


        ConfirmOrder confirmOrder = new ConfirmOrder();
        confirmOrder.setProvider_id(list.get(0).getProvider_id());
        confirmOrder.setProvider_name(list.get(0).getProvider_name());
        confirmOrder.setProvider_img(list.get(0).getProvider_img());
        confirmOrder.setPre_booking(list.get(0).getPre_booking());
        confirmOrder.setMin_order_amount(list.get(0).getMin_order_amount());
        confirmOrder.setOpening_time(list.get(0).getOpening_time());
        confirmOrder.setClosing_time(list.get(0).getClosing_time());
        confirmOrder.setItemDetais(list);

        listConfirmOrder.add(confirmOrder);
        orderListingAdp.notifyDataSetChanged();
//        double ltotal=0.0;
//        for (int i = 0; i <listConfirmOrder.size() ; i++) {
//            ltotal+= listConfirmOrder.get(i).getFinal_total();
//        }
//        total.setText(Utils.getAEDSpannable(ltotal));
    }

    private void bindAdp() {
        listConfirmOrder = new ArrayList<>();
        orderListingAdp = new OrderListingAdp(listConfirmOrder, this, this);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        rvOrder.setAdapter(orderListingAdp);
    }


    private void callifUserExist() {
        List<EditText> list = new ArrayList<>();
        list.add(edtEmail);
        list.add(edtFname);
        list.add(edtLname);
        list.add(edtMobile);
        if (isAllFieldFill(list)) {
            showProgress();
            Map<String, String> map = new HashMap<>();
            map.put("email", getDataFromEditText(edtEmail));
            map.put("first_name", getDataFromEditText(edtFname));
            map.put("last_name", getDataFromEditText(edtLname));
            map.put("number", getDataFromEditText(edtMobile));
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<LoginResponce> call = apiInterface.getUserIsExist(map);
            call.enqueue(new Callback<LoginResponce>() {
                @Override
                public void onResponse(Call<LoginResponce> call, Response<LoginResponce> response) {
                    hideProgress();
                    showToast(response.body().getMsg());
                    if (response.body().getResponse().equalsIgnoreCase("2")) {
//                        callPlaceOrderApi();

//                        if (UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).UserId().equalsIgnoreCase("0")) {
//
//                        }
//                        else {
                        Intent newIntent = new Intent(ConfirmOrderActivity.this, SignInActivityNew.class);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent);
//                        }

                    } else if (response.body().getResponse().equalsIgnoreCase("0")) {
//                        UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).setUserId(response.body().getUser().getId());
//                        UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).storeUserData(response.body().getUser());

                        Intent newIntent = new Intent(ConfirmOrderActivity.this, SignInActivityNew.class);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent);
//                        }

                    } else if (response.body().getResponse().equalsIgnoreCase("0")) {
                        UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).setUserId(response.body().getUserData().getUser_id());
                        UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).storeUserData(response.body().getUserData());
                        callPlaceOrderApi();
                    } else if (response.body().getResponse().equalsIgnoreCase("1")) {
                        showToast(response.body().getMsg());
                    }
                }

                @Override
                public void onFailure(Call<LoginResponce> call, Throwable t) {
                    hideProgress();
                    showTryAgainMsg();
                }
            });
        }

    }

    private void makeList() {
        listOfCartItems = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST);
        outerloop:
        for (int i = 0; i < listOfCartItems.size(); i++) {
            ItemDetais itemDetais = listOfCartItems.get(i);
            for (int j = 0; j < listConfirmOrder.size(); j++) {
                if (listConfirmOrder.get(j).getProvider_id().equalsIgnoreCase(itemDetais.getProvider_id())) {
                    listConfirmOrder.get(j).getItemDetais().add(itemDetais);
                    tootal = tootal + (itemDetais.getQuantity() * itemDetais.getBasicPrice());
                    continue outerloop;

                }
            }
            ConfirmOrder confirmOrder = new ConfirmOrder();
            confirmOrder.setProvider_id(itemDetais.getProvider_id());
            confirmOrder.setProvider_name(itemDetais.getProvider_name());
            confirmOrder.setProvider_img(itemDetais.getProvider_img());
            confirmOrder.setPre_booking(itemDetais.getPre_booking());
            confirmOrder.setMin_order_amount(itemDetais.getMin_order_amount());
            confirmOrder.setOpening_time(itemDetais.getOpening_time());
            confirmOrder.setClosing_time(itemDetais.getClosing_time());
            confirmOrder.getItemDetais().add(itemDetais);

            tootal = tootal + (itemDetais.getQuantity() * itemDetais.getBasicPrice());
            listConfirmOrder.add(confirmOrder);
        }
        orderListingAdp.notifyDataSetChanged();


    }

    private void setPrefData() {
        UserData user = UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).getUserData();
        edtEmail.setText(user.getEmail());
        edtFname.setText(user.getFirst_name());
        edtLname.setText(user.getLast_name());
        edtMobile.setText(user.getNumber());
        desebleEditing(edtFname);
        desebleEditing(edtLname);
        desebleEditing(edtMobile);
        desebleEditing(edtEmail);
    }

    private void callPlaceOrderApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BaseApiModel> call = apiInterface.doPlaceorder(getOrderString());
        call.enqueue(new Callback<BaseApiModel>() {
            @Override
            public void onResponse(Call<BaseApiModel> call, Response<BaseApiModel> response) {
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    hideProgress();
                    showToast("Your order placed successfully");
                    UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).setDeliveryTime("ASAP");
                    List<ItemDetais> list = new ArrayList<>();
                    UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).storeListing(list, Constant.PREFITEMLIST);
                    Intent newIntent = new Intent(ConfirmOrderActivity.this, HomeActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }
            }

            @Override
            public void onFailure(Call<BaseApiModel> call, Throwable t) {
                hideProgress();
                showTryAgainMsg();
            }
        });
    }

    private String getOrderString() {
        listPlaceOrder = new ArrayList<>();
        if (listOfCartItems != null) {
            for (int i = 0; i < listOfCartItems.size(); i++) {
                ItemDetais itemDetais = listOfCartItems.get(i);
                makePlaceOrderList(itemDetais);

            }
        } else {
            for (int i = 0; i < listOfpastOrder.size(); i++) {
                ItemDetais itemDetais = listOfpastOrder.get(i);
                makePlaceOrderList(itemDetais);
            }
        }
        Gson gson = new Gson();
        return gson.toJson(listPlaceOrder).toString();
    }

    private void makePlaceOrderList(ItemDetais itemDetais) {
        PlaceOrderModel placeOrderModel = new PlaceOrderModel();

        placeOrderModel.setUser_address(UserPreferenceUtil.getInstance(this).getLocation());
        if (!itemDetais.isGrocery()) {
            placeOrderModel.setDish_id(itemDetais.getItem_id());
            placeOrderModel.setRestaurant_id(itemDetais.getProvider_id());
            placeOrderModel.setDish_order_count(String.valueOf(itemDetais.getQuantity()));

        } else {
            placeOrderModel.setItem_id(itemDetais.getItem_id());
            placeOrderModel.setSupplier_id(itemDetais.getProvider_id());
            placeOrderModel.setItem_order_count(String.valueOf(itemDetais.getQuantity()));
        }
        placeOrderModel.setNote(itemDetais.getItem_note());
        placeOrderModel.setSpecial_note(getDataFromEditText(instrucrion));
        placeOrderModel.setDish_name(itemDetais.getName());
        placeOrderModel.setUser_id(UserPreferenceUtil.getInstance(this).UserId());
        placeOrderModel.setUser_latitude(UserPreferenceUtil.getInstance(this).getLocationLat());
        placeOrderModel.setUser_longitude(UserPreferenceUtil.getInstance(this).getLocationLng());
        placeOrderModel.setUser_number(UserPreferenceUtil.getInstance(this).getUserData().getNumber());
        placeOrderModel.setPrice(String.valueOf(itemDetais.getBasicPrice()));
        placeOrderModel.setDiscount(String.valueOf(promoAmnt));
        placeOrderModel.setOrder_delivery_time(UserPreferenceUtil.getInstance(this).getDeliveryTime());
        if (!isCOD) {
            placeOrderModel.setCc_token(UserPreferenceUtil.getInstance(this).PaymentToken());
        }
        placeOrderModel.setReceiver_name(UserPreferenceUtil.getInstance(this).getUserData().getFirst_name());
        if (itemDetais.getList_addons() != null && itemDetais.getList_addons().size() > 0) {
            List<AddOns> listAddons = itemDetais.getList_addons();
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < listAddons.size(); j++) {
                if (listAddons.get(j).isSelected()) {
                    builder.append(listAddons.get(j).getAdd_on_id() + ",");
                }
            }
            if (builder.length() > 0) {
                builder.setLength(builder.length() - 1);
            }
            placeOrderModel.setAdd_on_id(builder.toString());
        }
        listPlaceOrder.add(placeOrderModel);
    }


    @OnClick({R.id.ivCash, R.id.ivCreditCard, R.id.img_back, R.id.btn_placeOrder, R.id.tvAddCard, R.id.btn_apply, R.id.llPayCash, R.id.llPayCard})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivCash:
                isCOD = true;
                ivCash.setImageResource(R.drawable.check);
                ivCreditCard.setImageResource(R.drawable.uncheck);
                break;
            case R.id.btn_apply:
                if (isEdittextfilled(edPromo)) {
                    if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
                        applyPromoCode(edPromo.getText().toString().trim(), UserPreferenceUtil.getInstance(this).getDeliveryTotal());
                    } else {
                        Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showToast("Please enter promo code");
                }
                break;
            case R.id.ivCreditCard:
                if (UserPreferenceUtil.getInstance(this).getCreditCardData() != null) {
                    ivCash.setImageResource(R.drawable.uncheck);
                    ivCreditCard.setImageResource(R.drawable.check);
                    isCOD = false;
                    new ConnectionTask().execute();
                    showProgress();
                } else {
                    showToast("Please Add Card Details");
                }
                break;
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_placeOrder:
                if (UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).UserId().equalsIgnoreCase("0")) {

                    if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
                        callifUserExist();
                    } else {
                        Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                    }

                } else {

                    if (InternetUtil.getInstance(this).isNetWorkAvailable()) {
                        callPlaceOrderApi();
                    } else {
                        Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.tvAddCard:
                Intent intent = new Intent(this, SaveCardActivity.class);
                startActivity(intent);
                break;
            case R.id.llPayCash:
                isCOD = true;
                ivCash.setImageResource(R.drawable.check);
                ivCreditCard.setImageResource(R.drawable.uncheck);
                break;
            case R.id.llPayCard:
                if (UserPreferenceUtil.getInstance(this).getCreditCardData() != null) {
                    ivCash.setImageResource(R.drawable.uncheck);
                    ivCreditCard.setImageResource(R.drawable.check);
                    isCOD = false;
                    new ConnectionTask().execute();
                    showProgress();
                } else {
                    showToast("Please Add Card Details");
                }
                break;
        }
    }

    private class ConnectionTask extends AsyncTask<String, Void, String> {
        String name = UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).getCreditCardData().getName();
        String number = UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).getCreditCardData().getNumber();
        String cvv = UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).getCreditCardData().getCvv();
        String getMonth = UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).getCreditCardData().getMonth();
        String month = getMonth.substring(0, 2);
        String year = getMonth.substring(getMonth.length() - 2);

        @Override
        protected String doInBackground(String... urls) {
            try {
                Card card = new Card(number, name, month, year, cvv);
                CheckoutKit ck = CheckoutKit.getInstance(publicKey, CheckoutKit.Environment.SANDBOX);
                com.checkout.httpconnector.Response<CardTokenResponse> resp = ck.createCardToken(card);
                if (resp.hasError) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgress();
                        }
                    });
                } else {
                    hideProgress();
                    UserPreferenceUtil.getInstance(ConfirmOrderActivity.this).setPaymentToken(resp.model.getCardToken());
                    return resp.model.getCardToken();
                }
            } catch (final CardException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                    }
                });
            } catch (CheckoutException | IOException e2) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                    }
                });
            }
            return "";
        }

    }

    private void desebleEditing(EditText editText) {
        editText.setFocusable(false);
        editText.setFocusableInTouchMode(false);
        editText.setClickable(false);
    }

    @Override
    public void onClickDelete(int pos, int parentPosition) {
        showDeleteDialog("Are you sure you want to delete this item?", parentPosition, pos);
//        listConfirmOrder.get(parentPosition).getItemDetais().remove(pos);


    }

    private void removeItemAndStorePref(String id) {
        for (int i = 0; i < listOfCartItems.size(); i++) {
            ItemDetais itemDetais = listOfCartItems.get(i);
            if (itemDetais.getItem_id().equalsIgnoreCase(id)) {
                listOfCartItems.remove(i);
                break;
            }
        }
        UserPreferenceUtil.getInstance(this).storeListing(listOfCartItems, Constant.PREFITEMLIST);
        if (listOfCartItems.size() == 0) {
            Intent newIntent = new Intent(ConfirmOrderActivity.this, HomeActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(newIntent);
        } else {
            Intent intent = new Intent(this, ConfirmOrderActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void showDeleteDialog(String title, final int parentPosition, final int pos) {
        final Dialog dialog = new Dialog(ConfirmOrderActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        txt_title.setText(title);
        TextView txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                removeItemAndStorePref(listConfirmOrder.get(parentPosition).getItemDetais().get(pos).getItem_id());

            }
        });
        TextView txt_no = (TextView) dialog.findViewById(R.id.txt_no);
        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String s) {
        Spanned spanned = Html.fromHtml(s + "<sup><small> AED</small></sup>");
        total.setText(spanned);
    }
}
