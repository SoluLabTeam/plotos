package com.o2.plotos.home.homeTab;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.home.search.SearchDetailActivity;
import com.o2.plotos.models.Diet;
import com.o2.plotos.utils.CustomFonts;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hassan on 2/1/2017.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.DietViewHolder> {

    private List<Diet> mDietList;
    private LayoutInflater mInflater;
    private OnDietClickListener mListener;
    private CustomFonts customFonts;
    Context mcontext;

    public HomeAdapter(Context context , List<Diet> dietList, OnDietClickListener listener){
        mInflater = LayoutInflater.from(context);
        mDietList = dietList;
        customFonts = new CustomFonts(context);
        mListener = listener;
        mcontext = context;

    }

    @Override
    public DietViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v  = mInflater.inflate(R.layout.item_diet,parent,false);
        DietViewHolder dietViewHolder = new DietViewHolder(v);
        return dietViewHolder;
    }

    @Override
    public void onBindViewHolder(DietViewHolder holder, final int position) {
        holder.textName.setText(mDietList.get(position).getName());
        holder.imagePhoto.setImageResource(mDietList.get(position).getImageId());
        customFonts.setOpenSansBold(holder.textName);
        final String catName = mDietList.get(position).getName();

        if(position==1 || position ==5)
        {
            holder.cardView2.setVisibility(View.VISIBLE);
            holder.cardView.setVisibility(View.GONE);
        }
        else
        {
            holder.cardView2.setVisibility(View.GONE);
            holder.cardView.setVisibility(View.VISIBLE);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i2 = new Intent(mcontext, SearchDetailActivity.class);
                mcontext.startActivity(i2);
               // categoriesActivity.finish();
                /* if(mListener!=null){
                    mListener.onDietClick(mDietList.get(position),position,catName);
                }*/
            }
        });

        holder.cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if(mListener!=null){
                    mListener.onDietClick(mDietList.get(position),position,catName);
                }*/
                Intent i2 = new Intent(mcontext, NearByActivity.class);
                mcontext.startActivity(i2);
            }
        });

        holder.linlay_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if(mListener!=null){
                    mListener.onDietClick(mDietList.get(position),position,catName);
                }*/
//                Intent i2 = new Intent(mcontext, CategoryIteamListActivity.class);
//                mcontext.startActivity(i2);
            }
        });


        holder.linlay_test2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if(mListener!=null){
                    mListener.onDietClick(mDietList.get(position),position,catName);
                }*/
                Intent i2 = new Intent(mcontext, BakeryItemsActivity.class);

                mcontext.startActivity(i2);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDietList.size();
    }

    public static class DietViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_diet_text_name)
        TextView textName;
        @BindView(R.id.item_diet_image)
        ImageView imagePhoto;
        @BindView(R.id.item_restaurant_cardView)
        CardView cardView;
        @BindView(R.id.item_restaurant_cardView2)
        CardView cardView2;
        @BindView(R.id.linlay_test)
        LinearLayout linlay_test;
        @BindView(R.id.linlay_test2)
        LinearLayout linlay_test2;

        public DietViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public interface OnDietClickListener{
        void onDietClick(Diet diet , int postn, String name);
    }
}
