package com.o2.plotos.home.resturants;

import com.o2.plotos.models.Restaurant;

import java.util.List;

/**
 * Created by Hassan on 31/01/2017.
 */

public class RestaurantPresenterImpl implements IRestaurantPresenter,
        IRestaurantDataModel.OnGetRestaurantsRequestFinishLisener{

    IRestaurantView iRestaurantView;
    IRestaurantDataModel mRestaurantDataModel;

    public RestaurantPresenterImpl(){
        mRestaurantDataModel = new RestaurantDataModelImpl(this);
    }

    @Override
    public void sendRestaurantsRequest(String resturant_type, String latitude, String longitude) {
        if(iRestaurantView!=null){
            iRestaurantView.showProgress();
            mRestaurantDataModel.getRestaurantsRequest(resturant_type, latitude, longitude);
        }
    }

    @Override
    public void bindView(Object view) {
        iRestaurantView = (IRestaurantView) view;
    }

    @Override
    public void unBindView() {
        iRestaurantView = null;
    }
    @Override
    public void onGetRestaurantSuccess(List<Restaurant> restaurantList) {
        if(iRestaurantView!=null){
            iRestaurantView.hideProgress();
            iRestaurantView.onGetRestaurantSuccess(restaurantList);
        }
    }

    @Override
    public void onGetRestaurantFailed(String error) {
        if(iRestaurantView!=null){
            iRestaurantView.hideProgress();
            iRestaurantView.onGetRestaurantFailed(error);
        }
    }
}
