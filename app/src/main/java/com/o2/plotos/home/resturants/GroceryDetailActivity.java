package com.o2.plotos.home.resturants;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.home.order.DeliverToActivity;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.grocery.CategoryModel;
import com.o2.plotos.restapi.newApis.models.grocery.DishDetailModel;
import com.o2.plotos.restapi.newApis.models.grocery.DishResponse;
import com.o2.plotos.utils.Constant;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroceryDetailActivity extends BaseActivity implements GroceryDetailAdp.ClickListnerOfGrocerySupplier {


    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.rv_grocery_cat)
    RecyclerView rvGroceryCat;

    private String strSuppID;
    private String strMainID;
    private String strRestName;
    private String fromIntent;
    GroceryDetailAdp groceryDetailAdp;

    ArrayList<DishResponse> dishResponses = new ArrayList<>();

    @BindView(R.id.cartLayout)
    RelativeLayout cartLayout;
    @BindView(R.id.tvItemCount)
    TextView tvItemCount;
    @BindView(R.id.tvTotal)
    TextView tvTotal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_detail);
        ButterKnife.bind(this);
        if (getIntent() != null && getIntent().getExtras() != null) {
            groceryDetailAdp = new GroceryDetailAdp(dishResponses, this, this);
            rvGroceryCat.setAdapter(groceryDetailAdp);
            fromIntent = getIntent().getExtras().getString("from");
            if (fromIntent.equals("category")) {
                strMainID = getIntent().getExtras().getString("mainid");
                strRestName = getIntent().getExtras().getString("rest_name");
                callApi("0");
            } else {
                strSuppID = getIntent().getExtras().getString("suppid");
                strMainID = getIntent().getExtras().getString("mainid");
                strRestName = getIntent().getExtras().getString("rest_name");
                callApi(strSuppID);
            }
            tvTitle.setText(strRestName);
        }
        setCartFab();
    }

    private void callApi(String supplierID) {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<DishDetailModel> call = apiInterface.callSupplierDishList(supplierID, strMainID, UserPreferenceUtil.getInstance(this).getLocationLat(), UserPreferenceUtil.getInstance(this).getLocationLng());
        call.enqueue(new Callback<DishDetailModel>() {
            @Override
            public void onResponse(Call<DishDetailModel> call, Response<DishDetailModel> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    dishResponses.addAll(response.body().getResult());
                    rvGroceryCat.setHasFixedSize(true);
                    rvGroceryCat.setLayoutManager(new LinearLayoutManager(GroceryDetailActivity.this));
                    groceryDetailAdp.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<DishDetailModel> call, Throwable t) {
                hideProgress();
            }
        });
    }

    private void setCartFab() {
        int cartItemSize = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST).size();
        if (cartItemSize > 0) {
            cartLayout.setVisibility(View.VISIBLE);
            tvItemCount.setText("" + cartItemSize);
            makeList();
        } else {
            cartLayout.setVisibility(View.GONE);
        }
    }

    private void makeList() {
        Double tootal = 0.00;
        List<ItemDetais> listOfCartItems = UserPreferenceUtil.getInstance(this).getItemList(Constant.PREFITEMLIST);
        for (int i = 0; i < listOfCartItems.size(); i++) {
            ItemDetais itemDetais = listOfCartItems.get(i);
            tootal = tootal + (itemDetais.getQuantity() * itemDetais.getBasicPrice());
        }
        tvTotal.setText(tootal + " AED");
    }

    @OnClick({R.id.img_back, R.id.cartLayout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.cartLayout:
                startActivity(new Intent(this, DeliverToActivity.class));
                break;
        }
    }

    @Override
    public void onClickOfArror(int pos) {


    }
}
