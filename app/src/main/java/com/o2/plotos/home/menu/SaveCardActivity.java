package com.o2.plotos.home.menu;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.checkout.CardValidator;
import com.checkout.CheckoutKit;
import com.checkout.exceptions.CardException;
import com.checkout.exceptions.CheckoutException;
import com.checkout.models.Card;
import com.checkout.models.CardToken;
import com.checkout.models.CardTokenResponse;
import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.authentication.signup.DeleverdLocationActivity;
import com.o2.plotos.models.CreditCard;
import com.o2.plotos.utils.OpensanBtn;
import com.o2.plotos.utils.OpensanTextview;
import com.o2.plotos.utils.UserPreferenceUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 2/23/2018.
 */

public class SaveCardActivity extends BaseActivity {


    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_skip)
    OpensanTextview txtSkip;
    @BindView(R.id.content_cart_placeorder_button)
    OpensanBtn contentCartPlaceorderButton;

    @BindView(R.id.number)
    EditText etnumber;
    @BindView(R.id.name)
    EditText etname;
    @BindView(R.id.month)
    EditText etmonth;
    @BindView(R.id.cvv)
    EditText etcvv;
    String from;
//    Bundle bundle;

    String month;
    String year;
    final int errorColor = Color.rgb(255, 0, 0);
    private String publicKey = "pk_test_ac6b84e1-8e6f-48a5-a210-364420f249a9";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_savecard);
        ButterKnife.bind(this);
        if (getIntent() != null && getIntent().getExtras() != null) {
            from = "registration";
            txtSkip.setVisibility(View.VISIBLE);
//            bundle = getIntent().getExtras();
        }

        if (UserPreferenceUtil.getInstance(this).getCreditCardData() != null) {
            etcvv.setText(UserPreferenceUtil.getInstance(this).getCreditCardData().getCvv());
            etmonth.setText(UserPreferenceUtil.getInstance(this).getCreditCardData().getMonth());
            etnumber.setText(UserPreferenceUtil.getInstance(this).getCreditCardData().getNumber());
            etname.setText(UserPreferenceUtil.getInstance(this).getCreditCardData().getName());
        }

        etmonth.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() <= 0)
                    return;
                try {
                    char current = editable.charAt(editable.length() - 1);
                    if (current == '/') {
                        editable.delete(editable.length() - 1, editable.length());
                        return;
                    }

                    if (editable.length() == 2) {
                        if (Integer.parseInt(editable.toString()) > 12) {
                            editable.delete(editable.length() - 1, editable.length());
                            return;
                        } else if (Integer.parseInt(editable.toString()) <= 0) {
                            editable.delete(editable.length() - 1, editable.length());
                            return;
                        }
                    }
                    if (editable.length() == 1 && Integer.parseInt(editable.toString()) > 1) {
                        editable.insert(editable.length() - 1, String.valueOf("0"));
                        editable.insert(editable.length(), String.valueOf("/"));
                    }
                    if (editable.length() > 0 && (editable.length() % 3) == 0) {
                        final char c = editable.charAt(editable.length() - 1);
                        if ('/' == c) {
                            editable.delete(editable.length() - 1, editable.length());
                        }
                    }
                    if (editable.length() > 0 && (editable.length() % 3) == 0) {
                        char c = editable.charAt(editable.length() - 1);
                        if (Character.isDigit(c) && TextUtils.split(editable.toString(), String.valueOf("/")).length <= 2) {
                            editable.insert(editable.length() - 1, String.valueOf("/"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @OnClick({R.id.img_back, R.id.content_cart_placeorder_button, R.id.txt_skip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.content_cart_placeorder_button:
                String getMonth = etmonth.getText().toString();
                if (!etname.getText().toString().equals("") && !getMonth.equals("") && getMonth.length() > 3) {
                    month = getMonth.substring(0, 2);
                    year = getMonth.substring(getMonth.length() - 2);
                    new ConnectionTask().execute();
                    showProgress();
                } else {
                    Toast.makeText(this, "please enter valid card details", Toast.LENGTH_SHORT).show();
                    etname.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                    etmonth.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                }

                break;
            case R.id.txt_skip:
                Intent i1 = new Intent(this, DeleverdLocationActivity.class);
//                i1.putExtras(bundle);
                startActivity(i1);
                break;
        }
    }

    private void getValus() {
        UserPreferenceUtil.getInstance(this).storeCreditCardData(
                new CreditCard(
                        etcvv.getText().toString(),
                        etmonth.getText().toString(),
                        etnumber.getText().toString(),
                        etname.getText().toString()));
        if (from != null && from.equals("registration")) {
//            bundle.putString("number", etnumber.getText().toString());
//            bundle.putString("name", etname.getText().toString());
//            bundle.putString("month", etmonth.getText().toString());
//            bundle.putString("cvv", etcvv.getText().toString());
            Intent i1 = new Intent(this, DeleverdLocationActivity.class);
//            i1.putExtras(bundle);
            startActivity(i1);
        } else {
            finish();
        }
    }

    private class ConnectionTask extends AsyncTask<String, Void, String> {
        final String name = etname.getText().toString();
        final String number = etnumber.getText().toString();
        final String cvv = etcvv.getText().toString();

        private boolean validateCardFields(final String number, final String month, final String year, final String cvv) {
            boolean error = false;
            clearFieldsError();

            if (!CardValidator.validateCardNumber(number)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                        etnumber.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                    }
                });
                error = true;
            }
            if (!CardValidator.validateExpiryDate(month, year)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                        etmonth.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                    }
                });
                error = true;
            }
            if (cvv.equals("")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                        etcvv.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                    }
                });
                error = true;
            }
            return !error;
        }

        private void clearFieldsError() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    etcvv.getBackground().clearColorFilter();
                    etnumber.getBackground().clearColorFilter();
                    etmonth.getBackground().clearColorFilter();
                }
            });
        }

        @Override
        protected String doInBackground(String... urls) {
            if (validateCardFields(number, month, year, cvv)) {
                clearFieldsError();
                try {
                    Card card = new Card(number, name, month, year, cvv);
                    CheckoutKit ck = CheckoutKit.getInstance(publicKey, CheckoutKit.Environment.SANDBOX);
                    final com.checkout.httpconnector.Response<CardTokenResponse> resp = ck.createCardToken(card);
                    if (resp.hasError) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgress();
                                Toast.makeText(SaveCardActivity.this, "error", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        hideProgress();
                        CardToken ct = resp.model.getCard();
                        UserPreferenceUtil.getInstance(SaveCardActivity.this).setPaymentToken(resp.model.getCardToken());
                        getValus();
                        return resp.model.getCardToken();
                    }
                } catch (final CardException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgress();
                            if (e.getType().equals(CardException.CardExceptionType.INVALID_CVV)) {
                                etcvv.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            } else if (e.getType().equals(CardException.CardExceptionType.INVALID_EXPIRY_DATE)) {
                                etmonth.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            } else if (e.getType().equals(CardException.CardExceptionType.INVALID_NUMBER)) {
                                etnumber.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            }
                        }
                    });
                } catch (CheckoutException | IOException e2) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgress();
                            Log.d("IOException", e2.toString());
                        }
                    });
                }
            }
            return "";
        }

    }
}
