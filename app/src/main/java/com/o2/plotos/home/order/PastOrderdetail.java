package com.o2.plotos.home.order;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.models.Order;
import com.o2.plotos.restapi.newApis.models.orders.PastResult;
import com.o2.plotos.utils.Constant;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PastOrderdetail extends Fragment implements PastOrderAdapter.reOrderLister {
    @BindView(R.id.fragment_order_recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.fragment_order_emptyView)
    TextView emptyView;


    private ArrayList<Order> mOrderList;
    private ArrayList<PastResult> pastResultArrayList;
    private ProgressDialog mProgressDialog;
    private OrderDetailsViewModel mViewModel;
    private UpdateOrderListListener mUpdateOrderListListener;
    private Order mEditOrder;
    private String mDeletedOrder;
    private PastOrderAdapter mOrderAdapter;


    public PastOrderdetail() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        try {
//            listener = (OnReOrderListener) context;
//            mUpdateOrderListListener = (UpdateOrderListListener) context;
//        } catch (ClassCastException e) {
//            e.printStackTrace();
//        }
    }

    public static PastOrderdetail newInstance(List<PastResult> orderList) {
        PastOrderdetail orderdetail = new PastOrderdetail();
        Bundle bundle = new Bundle();
        bundle.putParcelable("orderList", Parcels.wrap(orderList));
        orderdetail.setArguments(bundle);
        return orderdetail;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pastResultArrayList = Parcels.unwrap(getArguments().getParcelable("orderList"));
//        mViewModel = new OrderDetailsViewModel(this, this, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_past_order, container, false);
        ButterKnife.bind(this, view);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        setOrderAdapter();
        return view;
    }

    private void setOrderAdapter() {
        if (pastResultArrayList != null && pastResultArrayList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(linearLayoutManager);
            mOrderAdapter = new PastOrderAdapter(getContext(), pastResultArrayList, this);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setAdapter(mOrderAdapter);
            mOrderAdapter.notifyDataSetChanged();
        } else {
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClickOfReOder(int pos) {
        PastResult pastResult = pastResultArrayList.get(pos);
        Intent intent = new Intent(getActivity(), ConfirmOrderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.OBJ_PAST_ORDER, Parcels.wrap(pastResult));
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
