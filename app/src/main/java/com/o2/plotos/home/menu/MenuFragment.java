package com.o2.plotos.home.menu;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o2.plotos.BaseFragment;
import com.o2.plotos.R;
import com.o2.plotos.StartActivity;
import com.o2.plotos.disclaimer.DisclaimerActivity;
import com.o2.plotos.information.AboutPlotosActivity;
import com.o2.plotos.models.Order;
import com.o2.plotos.places.DeliveryLocationActivity;
import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.GetOrderRequest;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.BaseApiModel;
import com.o2.plotos.restapi.newApis.models.orders.OrderModel;
import com.o2.plotos.restapi.newApis.models.orders.PastResult;
import com.o2.plotos.restapi.responses.GetOrderResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.CustomFonts;
import com.o2.plotos.utils.LogCat;
import com.o2.plotos.utils.OpensanBoldTextview;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by dell on 2/2/2018.
 */

public class MenuFragment extends BaseFragment {

    @BindView(R.id.img_profile)
    ImageView imgProfile;
    @BindView(R.id.linlay_img)
    LinearLayout linlayImg;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.linlay_editprofile)
    LinearLayout linlayEditprofile;
    @BindView(R.id.linlay_chat)
    LinearLayout linlayChat;
    @BindView(R.id.reclerview_myorder)
    RecyclerView reclerviewMyorder;
    @BindView(R.id.txt_location)
    TextView txtLocation;
    @BindView(R.id.txt_about)
    TextView txtAbout;
    @BindView(R.id.txt_rate)
    TextView txtRate;
    @BindView(R.id.txt_query)
    TextView txtQuery;
    @BindView(R.id.txt_logout)
    TextView txtLogout;
    @BindView(R.id.txt_delete)
    TextView txtDelete;
    //  @BindView(R.id.reclerview_experts)
    RecyclerView reclerviewExperts;
    @BindView(R.id.txt_editprofile)
    TextView txtEditprofile;
    @BindView(R.id.txt_chatus)
    TextView txtChatus;
    @BindView(R.id.txt_followorder)
    TextView txtFolloworder;
    @BindView(R.id.txt_experts)
    TextView txtExperts;
    @BindView(R.id.txt_card)
    TextView txtCard;
    @BindView(R.id.linlay_test)
    LinearLayout linlay_test;
    @BindView(R.id.txt_disclaimer)
    TextView txtDisclaimer;
    @BindView(R.id.txt_no_orders)
    TextView txt_no_orders;
    @BindView(R.id.tvWatchVideo1)
    OpensanBoldTextview tvWatchVideo1;
    @BindView(R.id.tvWatchVideo2)
    OpensanBoldTextview tvWatchVideo2;
    @BindView(R.id.llExpert1)
    LinearLayout llExpert1;
    @BindView(R.id.llExpert2)
    LinearLayout llExpert2;
    private ArrayList<Order> mOrderList = new ArrayList<>();
    ProgressDialog mProgressDialog;
    FollowOrderAdapter followOrderAdapter;
    CustomFonts customFonts;
    String user_id;
    private List<PastResult> upcomingResults = new ArrayList<>();

    public MenuFragment() {
        // Required empty public constructor
    }

    public static MenuFragment newInstance() {

        return new MenuFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile_new, container, false);
        ButterKnife.bind(this, rootView);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(true);
        user_id = UserPreferenceUtil.getInstance(getActivity()).UserId();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        ;
        reclerviewMyorder.setLayoutManager(linearLayoutManager);


        customFonts = new CustomFonts(getContext());
/*  remove comment after prototype approved
        if (!user_id.equals("0")) {
            getOrderRqust(user_id);
        }*/
        followOrderAdapter = new FollowOrderAdapter(getContext(), upcomingResults);
        reclerviewMyorder.setAdapter(followOrderAdapter);

        callOrdersApi();

//        customeFont();


        return rootView;
    }

    public void customeFont() {
        customFonts.setOpenSansBold(txtName);
        customFonts.setOpenSansRegulr(txtAddress);
        customFonts.setOpenSansSemiBold(txtEditprofile);
        customFonts.setOpenSansRegulr(txtChatus);
        customFonts.setOpenSansBold(txtFolloworder);
        customFonts.setOpenSansBold(txtExperts);
        customFonts.setOpenSansRegulr(txtLocation);
        customFonts.setOpenSansRegulr(txtAbout);
        customFonts.setOpenSansRegulr(txtRate);
        customFonts.setOpenSansRegulr(txtQuery);
        customFonts.setOpenSansRegulr(txtLogout);
        customFonts.setOpenSansRegulr(txtDelete);
        customFonts.setOpenSansRegulr(txtCard);


    }


    private void callOrdersApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderModel> call = apiInterface.getOrders(UserPreferenceUtil.getInstance(getContext()).UserId(), "0");
        call.enqueue(new Callback<OrderModel>() {
            @Override
            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                hideProgress();
                upcomingResults.clear();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    if (response.body().getResult().getUpcoming() != null && response.body().getResult().getUpcoming().size() > 0) {
                        upcomingResults.addAll(response.body().getResult().getUpcoming());
                        followOrderAdapter.notifyDataSetChanged();
                        txt_no_orders.setVisibility(View.GONE);
                    } else {
                        txt_no_orders.setVisibility(View.VISIBLE);
                    }
                } else {
                    txt_no_orders.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<OrderModel> call, Throwable t) {
                hideProgress();
                txt_no_orders.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }

    @OnClick({R.id.linlay_img, R.id.linlay_editprofile, R.id.linlay_chat, R.id.txt_location, R.id.txt_about, R.id.txt_rate, R.id.txt_query, R.id.txt_logout, R.id.txt_delete, R.id.txt_card, R.id.linlay_test, R.id.txt_disclaimer, R.id.tvWatchVideo1, R.id.tvWatchVideo2,R.id.llExpert1, R.id.llExpert2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linlay_img:

                break;
            case R.id.linlay_editprofile:

                Intent i5 = new Intent(getActivity(), EditProfileActivity.class);
                startActivity(i5);

                break;
            case R.id.linlay_chat:
                Registration registration = Registration.create().withUserId(user_id);
                Intercom.client().registerIdentifiedUser(registration);
                Intercom.client().displayConversationsList();
                break;
            case R.id.txt_location:
                Intent i = new Intent(getActivity(), DeliveryLocationActivity.class);
                startActivity(i);
                break;


            case R.id.txt_card:
                Intent i4 = new Intent(getActivity(), SaveCardActivity.class);
                startActivity(i4);
                break;
            case R.id.txt_about:

                Intent i2 = new Intent(getActivity(), AboutPlotosActivity.class);
                startActivity(i2);
                break;
            case R.id.txt_rate:

                final String appPackageName = getActivity().getPackageName(); // package name of the app
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

                break;
            case R.id.txt_query:
                startActivity(new Intent(getActivity(), DisclaimerActivity.class));
                break;
            case R.id.txt_logout:
                if (Utils.isNetworkAvailable(getContext())) {
                    showDialog("Are you sure you want to logout?", true);
                    ;
                } else {
                    Toast.makeText(getContext(), R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.txt_delete:
                if (Utils.isNetworkAvailable(getContext())) {
                    showDialog("Are you sure you want to delete your account?", false);
                } else {
                    Toast.makeText(getContext(), R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                }
                break;
//            case R.id.linlay_test:
//
//                break;

            case R.id.txt_disclaimer:

                startActivity(new Intent(getActivity(), Disclaimer2Activity.class));

                break;

            case R.id.tvWatchVideo1:
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://youtu.be/wgkCRwd-WWk"));
                startActivity(viewIntent);
                break;
            case R.id.tvWatchVideo2:
                Intent viewIntent1 =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://youtu.be/wgkCRwd-WWk"));
                startActivity(viewIntent1);
                break;

            case R.id.llExpert1:
                Intent intent=new Intent(getActivity(), NutritionExpertsActivity.class);
                intent.putExtra("expert","1");
                startActivity(intent);
                break;
            case R.id.llExpert2:
                Intent intent2=new Intent(getActivity(), NutritionExpertsActivity.class);
                intent2.putExtra("expert","2");
                startActivity(intent2);
                break;
        }
    }

    private void callLogoutApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BaseApiModel> call = apiInterface.callLogout(UserPreferenceUtil.getInstance(getApplicationContext()).UserId(), "0");
        call.enqueue(new Callback<BaseApiModel>() {
            @Override
            public void onResponse(Call<BaseApiModel> call, Response<BaseApiModel> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    UserPreferenceUtil.getInstance(getContext()).clearPref();
                    Intent i6 = new Intent(getActivity(), StartActivity.class);
                    startActivity(i6);
                    getActivity().finishAffinity();
                }
            }

            @Override
            public void onFailure(Call<BaseApiModel> call, Throwable t) {
                hideProgress();
            }
        });
    }

    private void callDeleteAccountApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BaseApiModel> call = apiInterface.deleteAccount(UserPreferenceUtil.getInstance(getApplicationContext()).UserId());
        call.enqueue(new Callback<BaseApiModel>() {
            @Override
            public void onResponse(Call<BaseApiModel> call, Response<BaseApiModel> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    UserPreferenceUtil.getInstance(getContext()).clearPref();
                    Intent i6 = new Intent(getActivity(), StartActivity.class);
                    startActivity(i6);
                    getActivity().finishAffinity();
                }
            }

            @Override
            public void onFailure(Call<BaseApiModel> call, Throwable t) {
                hideProgress();
            }
        });
    }

    public void getOrderRqust(String userId) {
        mProgressDialog.show();

        GetOrderRequest orderRequest = ServiceGenrator.createService(GetOrderRequest.class);
        Call<GetOrderResponse> orderRequestCall =
                orderRequest.orderRequest(userId);
        orderRequestCall.enqueue(new Callback<GetOrderResponse>() {
            @Override
            public void onResponse(Call<GetOrderResponse> call, Response<GetOrderResponse> response) {
                LogCat.LogDebug(ConstantUtil.TAG, " -> order api response " + response.raw());
                if (response.code() == 200) {
                    GetOrderResponse apiResponse = response.body();
                    String responseCode = apiResponse.response;
                    if (responseCode.equalsIgnoreCase("0")) {
                        //  Toast.makeText(getContext(), "done", Toast.LENGTH_SHORT).show();
                        // mLisener.OnGetOrderSuccess(apiResponse.order);
                        mOrderList = (ArrayList<Order>) apiResponse.order;
                        ArrayList<Order> mOrderList2 = new ArrayList<>();
                        for (int i = 0; i < mOrderList.size(); i++) {
                            if (!mOrderList.get(i).getOrderStatus().equalsIgnoreCase("4")) {
                                mOrderList2.add(mOrderList.get(i));
                            }

                        }

//                        followOrderAdapter = new FollowOrderAdapter(getContext(), mOrderList2);
//                        reclerviewMyorder.setAdapter(followOrderAdapter);

                        mProgressDialog.hide();

                    } else if (responseCode.equalsIgnoreCase("1")) {
                        mProgressDialog.hide();
                        // mLisener.OnGetOrderFailed("Order request Failed(1)");
                    } else if (responseCode.equalsIgnoreCase("2")) {
                        //  mLisener.OnGetOrderFailed("Order request Failed(2)");
                        mProgressDialog.hide();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetOrderResponse> call, Throwable t) {
                //  mLisener.OnGetOrderFailed("unexpected error");
                mProgressDialog.hide();
                LogCat.LogDebug(ConstantUtil.TAG, "Resturant dish ap onFailure " + t.getMessage());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!UserPreferenceUtil.getInstance(getContext()).UserId().equalsIgnoreCase("0")) {
            txtName.setText(UserPreferenceUtil.getInstance(getContext()).getUserData().getFirst_name()
                    + " " + UserPreferenceUtil.getInstance(getContext()).getUserData().getLast_name());
            txtAddress.setText(UserPreferenceUtil.getInstance(getContext()).getCurrentLocation());
            String image = UserPreferenceUtil.getInstance(getContext()).getUserData().getImage();
            if (image != null && !image.equals("")) {
                Picasso.with(getContext()).load(image).error(R.drawable.profile_gray).placeholder(R.drawable.profile_gray).into(imgProfile);
            }
        }
    }

    private void showDialog(String title, final boolean isLogout) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm);
        TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
        txt_title.setText(title);
        TextView txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (isLogout) {
                    callLogoutApi();
                } else {
                    callDeleteAccountApi();
                }
            }
        });
        TextView txt_no = (TextView) dialog.findViewById(R.id.txt_no);
        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }
}
