package com.o2.plotos.home.resturants;

import com.o2.plotos.models.Restaurant;

import java.util.List;

/**
 * Created by Hassan on 31/01/2017.
 */

public interface IRestaurantView {
    void showProgress();
    void hideProgress();
    void onGetRestaurantSuccess(List<Restaurant> restaurantList);
    void onGetRestaurantFailed(String error);
}
