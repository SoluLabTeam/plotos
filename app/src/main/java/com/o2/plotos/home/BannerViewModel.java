package com.o2.plotos.home;

import com.o2.plotos.models.Banner;

import java.util.List;

/**
 * Created by Rania on 5/20/2017.
 */

public class BannerViewModel implements BannerListener{

    BannerHelper mHelper;
    BannerViewListener mListener;

    public BannerViewModel(BannerViewListener bannerViewListener){
        mListener = bannerViewListener;
        mHelper = new BannerHelper(this);
    }

    public void getBanners(){
        mHelper.getBanners();
    }

    @Override
    public void onGetBannerSuccessfully(List<Banner> bannerList) {
        mListener.onGetBannerSuccessfully(bannerList);
    }

    public void onDestroy(){
        if(mListener != null){
            mListener = null;
        }
    }
}
