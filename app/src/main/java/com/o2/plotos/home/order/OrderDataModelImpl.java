package com.o2.plotos.home.order;

import com.o2.plotos.restapi.ServiceGenrator;
import com.o2.plotos.restapi.endpoints.GetOrderRequest;
import com.o2.plotos.restapi.responses.GetOrderResponse;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.LogCat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hassan on 22/02/2017.
 */

public class OrderDataModelImpl implements IOrderDataModel {

    OnGetorderRqustFinishLisener mLisener;

    public OrderDataModelImpl(OnGetorderRqustFinishLisener lisener){
        mLisener = lisener;
    }

    @Override
    public void getOrderRqust(String userId) {
        GetOrderRequest orderRequest = ServiceGenrator.createService(GetOrderRequest.class);
        Call<GetOrderResponse> orderRequestCall =
                orderRequest.orderRequest(userId);
        orderRequestCall.enqueue(new Callback<GetOrderResponse>() {
            @Override
            public void onResponse(Call<GetOrderResponse> call, Response<GetOrderResponse> response) {
                LogCat.LogDebug(ConstantUtil.TAG," -> order api response "+response.raw());
                if(response.code()== 200){
                    GetOrderResponse apiResponse =  response.body();
                    String responseCode = apiResponse.response;
                    if(responseCode.equalsIgnoreCase("0")){
                        mLisener.OnGetOrderSuccess(apiResponse.order);

                    }else if(responseCode.equalsIgnoreCase("1")){
                        mLisener.OnGetOrderFailed("Order request Failed(1)");
                    }else if(responseCode.equalsIgnoreCase("2")){
                        mLisener.OnGetOrderFailed("Order request Failed(2)");
                    }
                }
            }
            @Override
            public void onFailure(Call<GetOrderResponse> call, Throwable t) {
                mLisener.OnGetOrderFailed("unexpected error");
                LogCat.LogDebug(ConstantUtil.TAG,"Resturant dish ap onFailure "+t.getMessage());
            }
        });
    }
}
