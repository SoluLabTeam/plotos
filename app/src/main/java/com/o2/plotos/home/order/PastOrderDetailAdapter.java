package com.o2.plotos.home.order;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.o2.plotos.R;
import com.o2.plotos.models.ItemDetais;
import com.o2.plotos.restapi.newApis.models.orders.ItemOrders;
import com.o2.plotos.utils.OpensanTextview;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hassan on 2/02/2017.
 */

public class PastOrderDetailAdapter extends RecyclerView.Adapter<PastOrderDetailAdapter.OrderViewHolder> {


    private List<ItemDetais> mOrderlist;
    private LayoutInflater mInflater;
    Context mContext;

    public PastOrderDetailAdapter(Context context, List<ItemDetais> orderList) {
        mInflater = LayoutInflater.from(context);
        mOrderlist = orderList;
        mContext = context;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_past_order_detail, parent, false);
        OrderViewHolder orderViewHolder = new OrderViewHolder(v);
        return orderViewHolder;
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, int position) {

        holder.itemOrderDishPrice.setText(mOrderlist.get(position).getPrice()+" AED");
        holder.itemOrderDishName.setText(mOrderlist.get(position).getName());
        String imgUrl=mOrderlist.get(position).getImage();
        if(imgUrl!=null && !imgUrl.equalsIgnoreCase("")){
            Picasso.with(mContext).load(imgUrl)
                    .resize(400, 200)
                    .centerCrop()
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.itemOrderDishImage);
        }else {
            holder.itemOrderDishImage.setImageResource(R.drawable.placeholder);
        }

    }

    @Override
    public int getItemCount() {
        return mOrderlist.size();
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_order_dish_image)
        ImageView itemOrderDishImage;
        @BindView(R.id.item_order_dish_name)
        OpensanTextview itemOrderDishName;
        @BindView(R.id.item_order_dish_dateoforder)
        OpensanTextview itemOrderDishDateoforder;
        @BindView(R.id.item_order_dish_timeRemaining)
        OpensanTextview itemOrderDishPrice;
        public OrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
