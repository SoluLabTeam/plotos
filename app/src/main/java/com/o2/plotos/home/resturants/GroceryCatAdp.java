package com.o2.plotos.home.resturants;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.models.grocery.CategoryResponse;
import com.o2.plotos.utils.OpensanBoldTextview;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GroceryCatAdp extends RecyclerView.Adapter<GroceryCatAdp.CatHolder> implements ImagesWithNameCatAdp.ImageClickListner {

    private ArrayList<CategoryResponse> list;
    private Context context;
    private ClickListnerOfGroceryCatSupplier listner;

    public GroceryCatAdp(ArrayList<CategoryResponse> list, Context context,ClickListnerOfGroceryCatSupplier listner) {
        this.list = list;
        this.context = context;
        this.listner = listner;
    }

    @Override
    public void onClickOfImage(int pos, int parentPosition) {
        Intent intent = new Intent(context, GroceryDetailActivity.class);
        intent.putExtra("from","category");
        intent.putExtra("rest_name",list.get(parentPosition).getName());
        intent.putExtra("mainid",list.get(parentPosition).getMain_id());
        context.startActivity(intent);
    }


    public interface ClickListnerOfGroceryCatSupplier {
        void onClickOfArror(int pos);
    }

    @Override
    public CatHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grocery_cat, parent, false);
        return new CatHolder(view);
    }

    @Override
    public void onBindViewHolder(CatHolder holder, int position) {

        holder.tvTitle.setText(list.get(position).getName());
        holder.rvImages.setHasFixedSize(true);
        holder.rvImages.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        ImagesWithNameCatAdp mAdapter = new ImagesWithNameCatAdp(list.get(position).getSub_category(), context, this,position);
        holder.rvImages.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class CatHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_images)
        RecyclerView rvImages;
        @BindView(R.id.tvTitle)
        OpensanBoldTextview tvTitle;
        @BindView(R.id.iv_next)
        ImageView ivNext;

        public CatHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.iv_next)
        public void onViewClicked() {
            listner.onClickOfArror(getAdapterPosition());
        }
    }
}
