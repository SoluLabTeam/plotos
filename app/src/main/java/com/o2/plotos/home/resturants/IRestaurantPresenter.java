package com.o2.plotos.home.resturants;

/**
 * Created by Hassan on 31/01/2017.
 */

public interface IRestaurantPresenter {
    void sendRestaurantsRequest(String resturant_type, String latitude, String longitude);
    void bindView(Object view);
    void unBindView();
}
