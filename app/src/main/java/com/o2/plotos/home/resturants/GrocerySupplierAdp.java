package com.o2.plotos.home.resturants;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2.plotos.R;
import com.o2.plotos.restapi.newApis.models.grocery.SupplierResponse;
import com.o2.plotos.utils.OpensanBoldTextview;
import com.o2.plotos.utils.OpensanTextview;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GrocerySupplierAdp extends RecyclerView.Adapter<GrocerySupplierAdp.SupplierHolder> implements ImagesWithNameAdp.ImageClickListner {


    private ArrayList<SupplierResponse> list;
    private Context context;
    private ClickListnerOfGrocerySupplier listner;

    @Override
    public void onClickOfImage(int pos,int parentPostion) {
        Intent intent = new Intent(context, GroceryDetailActivity.class);
        intent.putExtra("from","supplier");
        intent.putExtra("rest_name",list.get(parentPostion).getCategory_list().get(pos).getName());
        intent.putExtra("suppid",list.get(parentPostion).getSupplier_id());
        intent.putExtra("mainid",list.get(parentPostion).getCategory_list().get(pos).getMain_id());
        context.startActivity(intent);
    }


    public interface ClickListnerOfGrocerySupplier {
        void onClickOfArror(int pos);
    }

    public GrocerySupplierAdp(ArrayList<SupplierResponse> list, Context context, ClickListnerOfGrocerySupplier listner) {
        this.list = list;
        this.context = context;
        this.listner = listner;

    }

    @Override
    public SupplierHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grocery_supplier, parent, false);
        return new SupplierHolder(view);
    }

    @Override
    public void onBindViewHolder(SupplierHolder holder, int position) {
        holder.tv_rest_name.setText(list.get(position).getSupplier_name());
        if (!list.get(position).getPre_booking().equals("0")) {
            holder.tv_hours.setText(list.get(position).getPre_booking() + " hours");
        }
        if (list.get(position).getImage() != null && !list.get(position).getImage().equals("")) {
            Picasso.with(context).load(list.get(position).getImage())
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.ivRestImage);
        }
        holder.tvRestDetail.setText(list.get(position).getSupplier_description());
        holder.tvMinMax.setText(list.get(position).getStart_price() + " - " + list.get(position).getEnd_price());
        holder.tvMinOrder.setText("Min Order AED "+list.get(position).getMin_order_amount());
        holder.rvImages.setHasFixedSize(true);
        holder.rvImages.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        ImagesWithNameAdp mAdapter = new ImagesWithNameAdp(list.get(position).getCategory_list(), context,this,position);
        holder.rvImages.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SupplierHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_images)
        RecyclerView rvImages;
        @BindView(R.id.iv_next)
        ImageView ivNext;

        @BindView(R.id.tv_rest_name)
        TextView tv_rest_name;

        @BindView(R.id.tv_hours)
        TextView tv_hours;

        @BindView(R.id.iv_rest_image)
        ImageView ivRestImage;
        @BindView(R.id.tv_rest_detail)
        OpensanTextview tvRestDetail;
        @BindView(R.id.tv_min_max)
        OpensanTextview tvMinMax;
        @BindView(R.id.tv_min_order)
        OpensanTextview tvMinOrder;

        public SupplierHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick(R.id.iv_next)
        public void onViewClicked() {
            listner.onClickOfArror(getAdapterPosition());
        }
    }
}
