package com.o2.plotos;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o2.plotos.adapters.ViewPagerAdapter;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.utils.UserPreferenceUtil;

import java.util.ArrayList;

public class IntroActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {

    private ViewPager intro_images;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private TextView tvNext;
    private ViewPagerAdapter mAdapter;
    int pagerPosition;
    private ArrayList<IntroModel> mImageResources = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        if (!UserPreferenceUtil.getInstance(this).UserId().equalsIgnoreCase("0")) {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
        setReference();
    }

    public void setReference() {
        intro_images = (ViewPager) findViewById(R.id.pager_introduction);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        tvNext = (TextView) findViewById(R.id.tvNext);
        mImageResources.add(new IntroModel(R.drawable.intro1, "Welcome to the new Plotos", "NEW EXPERIENCE", "Our name comes from the Greek word ‘Plutus’ which means wealth. And for us at Plotos, we believe that the greatest wealth is health!"));
        mImageResources.add(new IntroModel(R.drawable.intro2, "Our Boutique Grocery", "FILL UP YOUR BASKET", "Scroll to find the items you are looking for in the healthy ‘Boutique Grocery’ section of our programs, or simply look through the geo-targeted list of products found around you."));
        mImageResources.add(new IntroModel(R.drawable.intro3, "Our Plotos Programs", "LIVE A HEALTHY LIFE", "We want to help you live a healthy life by delivering trusted, trendy food to your doorstep. We have a high-quality and diverse selection of restaurants and products to choose from."));
        mImageResources.add(new IntroModel(R.drawable.intro4, "New Partners to Our Plotos Family", "UP CLOSE AND PERSONAL", "We get UP CLOSE AND PERSONAL with our partners by hand-picking and tailor-making their meals and products to fit right into our concept!"));
        mAdapter = new ViewPagerAdapter(IntroActivity.this, mImageResources);
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        intro_images.setOnPageChangeListener(this);
        tvNext.setOnClickListener(this);
        setUiPageViewController();
    }

    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(IntroActivity.this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(6, 0, 6, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvNext:
                if (pagerPosition + 1 == dotsCount) {
//                    if (!UserPreferenceUtil.getInstance(this).UserId().equalsIgnoreCase("0")) {
//                        Intent intent = new Intent(this, HomeActivity.class);
//                        startActivity(intent);
//                        finish();
//                    } else {
                    UserPreferenceUtil.getInstance(this).setIsIntroSeen(true);
                    startActivity(new Intent(IntroActivity.this, StartActivity.class));
                    finish();
//                    }
                } else {
                    intro_images.setCurrentItem(pagerPosition + 1);
                }
                break;
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));

        if (position + 1 == dotsCount) {
            tvNext.setText("Done");
        } else {
            tvNext.setText("Next");
        }
        pagerPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public class ViewPagerAdapter extends PagerAdapter {

        private Context mContext;
        private ArrayList<IntroModel> mResources;

        public ViewPagerAdapter(Context mContext, ArrayList<IntroModel> mResources) {
            this.mContext = mContext;
            this.mResources = mResources;
        }

        @Override
        public int getCount() {
            return mResources.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.img_pager_item);
            ImageView ivGrocery = (ImageView) itemView.findViewById(R.id.ivGrocery);

            if (position == 0) {
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
            if (position == 1) {
                ivGrocery.setVisibility(View.VISIBLE);
            } else {
                ivGrocery.setVisibility(View.INVISIBLE);
            }

            imageView.setImageResource(mResources.get(position).image);
            TextView tvHeader = (TextView) itemView.findViewById(R.id.tvHeader);
            TextView tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            TextView tvDetail = (TextView) itemView.findViewById(R.id.tvDetail);
            tvHeader.setText(mResources.get(position).getHeader());
            tvTitle.setText(mResources.get(position).getTitle());
            tvDetail.setText(mResources.get(position).getDetail());

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    private class IntroModel {

        int image;
        String title;
        String header;
        String detail;

        public IntroModel(int image, String title, String header, String detail) {
            this.image = image;
            this.title = title;
            this.header = header;
            this.detail = detail;
        }

        public int getImage() {
            return image;
        }

        public void setImage(int image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getHeader() {
            return header;
        }

        public void setHeader(String header) {
            this.header = header;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }
    }

}
