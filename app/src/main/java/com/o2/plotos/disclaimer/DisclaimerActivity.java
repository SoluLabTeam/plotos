package com.o2.plotos.disclaimer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.o2.plotos.BaseActivity;
import com.o2.plotos.R;
import com.o2.plotos.home.menu.EditProfileActivity;
import com.o2.plotos.restapi.newApis.ApiClient;
import com.o2.plotos.restapi.newApis.ApiInterface;
import com.o2.plotos.restapi.newApis.models.editProfile.EditProfileResponse;
import com.o2.plotos.restapi.newApis.models.submitQuery.SubmitQueryResponse;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;

import java.io.File;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisclaimerActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.etQuery)
    EditText etQuery;
    @BindView(R.id.content_cart_placeorder_button)
    Button contentCartPlaceorderButton;
    @BindView(R.id.activity_cart_frame)
    LinearLayout activityCartFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disclaimer);
        ButterKnife.bind(this);

    }

    private void callApi() {
        showProgress();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SubmitQueryResponse> call = apiInterface.callSubmitQueryResponse(UserPreferenceUtil.getInstance(getApplicationContext()).UserId(), etQuery.getText().toString());
        call.enqueue(new Callback<SubmitQueryResponse>() {
            @Override
            public void onResponse(Call<SubmitQueryResponse> call, Response<SubmitQueryResponse> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("0")) {
                    Toast.makeText(DisclaimerActivity.this, "Thanks for your query!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<SubmitQueryResponse> call, Throwable t) {
                hideProgress();
            }
        });
    }

    @OnClick({R.id.img_back, R.id.content_cart_placeorder_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.content_cart_placeorder_button:
                if (Utils.isNetworkAvailable(this)) {
                    if (etQuery.getText().toString().equals("")) {
                        etQuery.setError("Add query here");
                    } else {
                        callApi();
                    }
                } else {
                    Toast.makeText(this, R.string.error_internet_connection, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
