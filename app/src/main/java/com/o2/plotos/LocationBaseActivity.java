package com.o2.plotos;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.utils.ConstantUtil;
import com.o2.plotos.utils.UserPreferenceUtil;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Hassan on 2/28/2017.
 */

public class LocationBaseActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    /**
     * Request code for location permission
     */
    private static final int REQUEST_CHECK_SETTINGS = 123;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 12312;

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v(ConstantUtil.TAG, "onConnectionSuspended " + i);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.v(ConstantUtil.TAG, "onConnected");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LocationBaseActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
            return;
        }
        createLocationRequest();

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mLastLocation != null) {
            Log.v(ConstantUtil.TAG, " get Last location lat " + mLastLocation.getLatitude() + " lng " + mLastLocation.getLongitude());
            UserPreferenceUtil.getInstance(LocationBaseActivity.this).setCurrentLat(String.valueOf(mLastLocation.getLatitude()));
            UserPreferenceUtil.getInstance(LocationBaseActivity.this).setCurrentLng(String.valueOf(mLastLocation.getLongitude()));
            new LoadLocation().execute(String.valueOf(mLastLocation.getLatitude()), String.valueOf(mLastLocation.getLongitude()));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v(ConstantUtil.TAG, "onConnectionFailed");
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        if (mGoogleApiClient.isConnected()) {
                            startLocationUpdates();
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(
                                    LocationBaseActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;

                }
            }
        });
    }

    /**
     * start location updates from google fused location api
     */
    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        }
    }

    /**
     * Stop location updates form fused location api
     */
    protected void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }



    @Override
    public void onLocationChanged(Location location) {
        String mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        Log.v(ConstantUtil.TAG, "OnLocationChanged " + location.getLatitude() + " " + location.getLongitude() + " " + mLastUpdateTime);
        mLastLocation = location;
        if (mLastLocation != null) {
            UserPreferenceUtil.getInstance(LocationBaseActivity.this).setCurrentLat(String.valueOf(mLastLocation.getLatitude()));
            UserPreferenceUtil.getInstance(LocationBaseActivity.this).setCurrentLng(String.valueOf(mLastLocation.getLongitude()));
            new LoadLocation().execute(String.valueOf(mLastLocation.getLatitude()), String.valueOf(mLastLocation.getLongitude()));
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                if (mGoogleApiClient.isConnected()) {
                    startLocationUpdates();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createLocationRequest();
                } else {
                    Log.v(ConstantUtil.TAG, "Permission not granted!");
                    Toast.makeText(this, "We need your location access for better results!", Toast.LENGTH_LONG).show();
                }

            }
        }
    }

    /**
     * load location using Geocoder class
     */
    private class LoadLocation extends AsyncTask<String, Void, List<Address>> {

        @Override
        protected List<Address> doInBackground(String... strings) {
            Geocoder geocoder = new Geocoder(LocationBaseActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            try {

                addresses = geocoder.getFromLocation(
                        Double.parseDouble(strings[0]),
                        Double.parseDouble(strings[1]), 1);

            } catch (IOException e) {
                e.printStackTrace();
            }

            return addresses;
        }

        @Override
        protected void onPostExecute(List<Address> addresses) {
            super.onPostExecute(addresses);
            String countryName;
            if (addresses != null) {
                if( addresses.size() > 0 ) {
                    String cityName = addresses.get(0).getAddressLine(0);
                    String stateName = addresses.get(0).getAddressLine(1);
                    String countryCode = addresses.get(0).getCountryCode();
                    if (addresses.get(0).getAddressLine(2) != null) {
                        countryName = ", " + addresses.get(0).getAddressLine(2);
                    } else {
                        countryName = "";
                    }
                    String location = cityName + ", " + stateName + countryName;
                    UserPreferenceUtil.getInstance(LocationBaseActivity.this).setCountryCode(countryCode);
                    UserPreferenceUtil.getInstance(LocationBaseActivity.this).setCurrentLocation(location);
                }
            }
        }
    }
}
