package com.o2.plotos;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.o2.plotos.authentication.signin.SignInActivity;
import com.o2.plotos.authentication.signup.DeliveryDetailsActivity;
import com.o2.plotos.database.DataBaseHelper;
import com.o2.plotos.home.HomeActivity;
import com.o2.plotos.models.User;
import com.o2.plotos.utils.PlayGifView;
import com.o2.plotos.utils.UserPreferenceUtil;
import com.o2.plotos.utils.Utils;

import java.sql.SQLException;
import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3500;
    private PlayGifView playGifView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        playGifView = (PlayGifView) findViewById(R.id.splas_gif_icon);
        playGifView.setImageResource(R.drawable.splash_logo);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                try {
                    if (!UserPreferenceUtil.getInstance(SplashActivity.this).UserId().equalsIgnoreCase("0")) {
                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (!UserPreferenceUtil.getInstance(SplashActivity.this).getIsIntroSeen()) {
                        startActivity(new Intent(SplashActivity.this, IntroActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(SplashActivity.this, StartActivity.class));
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, SPLASH_TIME_OUT);
    }
}
